<?php
include "seguranca.php";
include 'scripts/limpa.php';

include 'scripts/auditoria.php';
include 'scripts/utils.php';

//if (!in_array($_SERVER['REMOTE_ADDR'], array('201.16.252.81', '0.0.0.0')) && $_POST['agencia'] != "4097") die("<center><span style='font-size: 24px; color: red'>O SisaCob esta offline para manutencao. Associados que forem efetuar descontos de titulos, favor encaminhar os borderos para Sicoob Credinova ate as 14:00h</span></center>");

$_POST = sanitize($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$agencia = (isset($_POST['agencia'])) ? $_POST['agencia'] : '';
	$cliente = (isset($_POST['cliente'])) ? $_POST['cliente'] : '';
	$conta = (isset($_POST['conta'])) ? removecaracterconta($_POST['conta']) : '';
	$senha = (isset($_POST['senha'])) ? $_POST['senha'] : '';
	if (valida($agencia, $cliente, $conta, $senha) == true) {
		auditoria($cliente,$agencia, $conta, $_SERVER['REMOTE_ADDR'], "logando", "NULL");
		header("Location: index.php");
	} else {
		session_start();
		$_SESSION['userAgencia'] = $agencia;
		$_SESSION['userCliente'] = $cliente;
		$_SESSION['userConta'] = $conta;


		// die(print_r($_SESSION));
		errolog();
	}
}
?>
