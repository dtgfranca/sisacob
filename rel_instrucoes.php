<?php
	include('config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
	//Limpando o erro de fontes do relatório
	if (file_exists('scripts/font/unifont/dejavusans-bold.mtx.php')) {
		unlink('scripts/font/unifont/dejavusans-bold.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusans-bold.cw.dat')) {
		unlink('scripts/font/unifont/dejavusans-bold.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat');	
	}
	$agencia = $_SESSION['userAgencia'];
	$conta = $_SESSION['userConta'];	
?>
<div id="section" class="duplo">
	<div class="titulo">
        <h2>RELATÓRIOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    	<form id="relatorio" name="relatorio" method="post" action="scripts/relatorios/r_instrucoes.php" target="_blank">
            <fieldset>
                <legend>INSTRUÇÕES</legend>
                <label for="cliente">Selecione um cliente ou gere o relatório geral</label>
                <br /><br />
                <select name="cliente" id="cliente" class="largo">
          	    	<option value="0">Selecione o cliente</option>
          	    	<?php
						$sql = "SELECT nome, sacado, cpf FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND (grupo <> '99999' OR grupo IS NULL) ORDER BY UPPER(nome)";
						$query = mysql_query($sql) or die(mysql_error());
						while($linha = mysql_fetch_array($query)){
							echo '
								<option value="'.$linha['sacado'].'">'.$linha['nome'].' - '.$linha['cpf'].'</option>
							';
						}
					?>
       	    	</select>           
            </fieldset>
            <input class="btn botao margins dir" type="submit" name="gerar" id="gerar" value="Gerar Relatório" />
        </form>
        <br class="clear" />
    </div>
</div>