<?php
	include('config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login.php?res=1');
	}
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
	$data_base = date("d/m/Y");
?>
<script type="text/javascript">
	$(document).ready(function() {
        $("#alt_senha").validate({
			rules:{
				atual:{
					required: true, maxlength: 10, minlength: 4
				},
				nova:{
					required: true, maxlength: 10, minlength: 4
				},
				confirma:{
					required: true, maxlength: 10, minlength: 4 , equalTo: "#nova"
				},
			},
			messages:{
				atual:{
					required: "<br>Digite a senha atual", 
					maxlength: "<br>Máximo 10 caracteres",
					minlength: "<br>Mínimo 4 caracteres",
				},
				nova:{
					required: "<br>Digite seu a nova senha", 
					maxlength: "<br>Máximo 10 caracteres",
					minlength: "<br>Mínimo 4 caracteres",
				},
				confirma:{
					required: "<br>Repita a nova senha", 
					maxlength: "<br>Máximo 10 caracteres",
					minlength: "<br>Mínimo 4 caracteres",
					equalTo: "<br>Confirmação inválida",
				}
			}
		});
    });
	function altera(){
		var atual = document.getElementById('atual').value;
		var nova = document.getElementById('nova').value;
		$.ajax({
			type: "GET",
			url: "scripts/utilit_funcoes.php",
			data: "funcao=alt_senha&at="+atual+"&n="+nova,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Alteração realizada com sucesso!");
					navega("principal.php");
				}
				else{
					alerta(retorno);
				}		
			}
		});
	}
</script>
<div id="section" class="quarto">
	<div class="titulo">
        <h2>ALTERAR SENHA</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  	<form id="alt_senha" name="alt_senha" method="post" action="">
      		<fieldset>
            	<table>
                	<tr>
                    	<td height="27"><label for="atual">Senha Atual:</label></td>
                        <td><input name="atual" type="password" id="atual" size="15" maxlength="10" /></td>
                    </tr>
                    <tr>
                    	<td><label for="nova">Nova Senha:</label></td>
                        <td><input name="nova" type="password" id="nova" size="15" maxlength="10" /></td>
                    </tr>
                    <tr>
                    	<td><label for="confirma">Confirma:</label></td>
                        <td><input name="confirma" type="password" id="confirma" size="15" maxlength="10" /></td>
                    </tr>
                </table>
			</fieldset>
  			<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="altera()" />
        <br class="clear" />
	  </form>
    </div>
</div>