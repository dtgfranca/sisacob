<?php
	include('config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
	$agencia = $_SESSION['userAgencia'];
	$conta = $_SESSION['userConta'];	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="dialog-confirm" title="Excluir sacador/avalista?" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Este sacador/avalista será excluído permanentemente. Deseja continuar?</p>
</div>
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready(function(){
    	$('#section').mede();
	});
	function busca(busca){
		$.ajax({
			type: 'GET',
			url: "scripts/sacador_funcoes.php",
			data: "funcao=busca&filtro="+busca,
			beforeSend: function() {
				$("#busca").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
			},
			success: function(retorno){
				$("#busca").html(retorno);
			}
		});	
	}
	function excluir(sacado){
		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Excluir": function() {
					$.ajax({
						type: 'GET',
						url: "scripts/sacador_funcoes.php",
						data: "funcao=excluir&sacado="+sacado,
						success: function(retorno){
							if(retorno == "ok"){
								alerta("Exclusão realizada com sucesso!");
								navega('sacadoravalista_buscar.php');
							}
							else {
								alerta ("Erro ao excluir.");
							}
						}					
					});
					$(this).dialog("close");
				},
				"Cancelar": function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
</script>
	<div class="titulo">
        <h2>Sacador/Avalista</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
   	  <fieldset class="busca">
   	    <legend>Buscar Sacador/Avalista
    		</legend><form id="busca_sacador" name="busca_sacador" method="post" action="" target="busca">
            	<table>
                	<tr>
                    	<td width="30%"><label for="buscar">Pesquisar <span style="font-size:10px;">(nome ou CPF/CNPJ)</span>:</label></td>
                        <td><input class="largo" name="buscar" type="text" id="buscar" size="50"  onKeyUp="busca(this.value)" /></td>
                	</tr>
              	</table>
   		  </form>
        <div id="busca">
        	<table>
            	<tr class="cinza">
                	<td class="destaque borda">NOME</td>
                    <td width="25%" class="destaque borda">CPF/CNPJ</td>
                    <td width="8%" class="destaque borda">	AÇÕES</td>
                </tr>
                <?php
					include('config.php');
					$agencia = $_SESSION['userAgencia'];
					$cliente = $_SESSION['userCliente'];
					$conta = $_SESSION['userConta'];
				 	mysql_query("SET NAMES UTF8") or die(mysql_error());
					$sql = "SELECT nome, cpf, sacado FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND grupo='99999' ORDER BY UPPER(nome)";
					$query = mysql_query ($sql) or die(mysql_error());
					while($linha = mysql_fetch_array($query)){
						if($_SESSION['userMaster'] == true){
							$acoes = '<a href="javascript:excluir('.$linha['sacado'].');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;&nbsp;
									<a href="javascript:navega(\'sacadoravalista_alterar.php?sacado='.$linha['sacado'].'\');" title="Alterar"><img src="img/editar.png" /></a>';
						}
						else {
							$acoes = '<a href="javascript:navega(\'sacadoravalista_alterar.php?sacado='.$linha['sacado'].'\');" title="Alterar"><img src="img/editar.png" /></a>';
						}
						echo '
							<tr>
								<td class="borda">'.$linha['nome'].'</td>
								<td class="borda">'.$linha['cpf'].'</td>
								<td class="borda">
									'.$acoes.'
								</td>
							</tr>
						';
					}				
				?>
            </table>
        </div>
   	  </fieldset>
    </div>
</div>