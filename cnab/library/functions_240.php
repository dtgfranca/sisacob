<?php
/************************************** FUNCÇÕES RELACIONADAS À válidaÇÃO ************************************************************/
// Funcão que válida a senha do banco com as senhas do teclado virtual
function confirma_senha($senha_banco, $senha_web1, $senha_web2){
  $length_banco = strlen($senha_banco);
  $length_web1  = strlen($senha_web1);
  $length_web2  = strlen($senha_web2);

  if($length_banco != $length_web1 || $length_web1 != $length_web2) return false; // retorna falso o tamanhos das senhas seja diferente

  for($i = 0; $i < $length_banco; $i++){
    if(substr($senha_banco, $i, 1) != substr($senha_web1, $i, 1) && substr($senha_banco, $i, 1) != substr($senha_web2, $i, 1)){
      return false; // Retorna falso se as senhas não são iguais
    }
  }

  return true;
}

// Função para validação de datas
function data_valida($data){
  $data_separada = preg_split("/\//", $data);
  return checkdate((int)$data_separada[1], (int)$data_separada[0], (int)$data_separada[2]);
}

// Função para validação de datas
function data_invalida($data){
  return data_valida($data) == false;
}

function compare_data ($data1, $data2){
    //formato de entrada: YYYY-MM-DD em formato STRING
    //Saida: 0 -> data1 == data2
    //      -1 -> data1 > data2 Ex.: 2011-05-05 > 2011-05-04
    //       1 -> data1 < data2 Ex.: 2011-05-04 < 2011-05-05
    $array = explode("-", $data1);

    $ano1 = $array[0];
    $mes1 = $array[1];
    $dia1 = $array[2];

    $array = explode("-", $data2);

    $ano2 = $array[0];
    $mes2 = $array[1];
    $dia2 = $array[2];

    if ($ano1 != $ano2 ){
        //testa anos diferentes
        if( $ano1 < $ano2 ) return -1;
        else return 1;
    }else{
        //testa meses diferentes
        if ($mes1 != $mes2){
            if( $mes1 < $mes2 ) return -1;
            else return 1;
        }else{
           //testa dias diferentes
           if( $dia1 != $dia2 ){
                if( $dia1 < $dia2 ) return -1;
                else return 1;
           }else{
               //datas iguais
               return 0;
           }
        }
      }
}

function validar_cabecalho_cnab_remesa( $linha ){
  $cabecalho = array( "erros" => array(), "quantidade_erros" => 0, "dados" => array() );
  if( strlen( $linha ) != 240 ){
    $cabecalho["erros"]["SISTEMA"] = "A linha deve conter 240 caracteres alfanuméricos!";
    $cabecalho["quantidade_erros"]++ ;
  }

  if( substr( $linha, 7, 1 ) != '0' ){
    $cabecalho["erros"]["1"] = "Cabeçalho não encontrado!"; // Layout de remessa
    $cabecalho["quantidade_erros"]++ ;
  }
  /*else {
    /*$cabecalho["dados"]["arquivo"]             = array( "tipo"    => substr( $linha, 1, 1  ), "nome"  => substr( $linha, 2, 7 ) );
    $cabecalho["dados"]["servico"]             = array( "tipo"    => substr( $linha, 9, 2  ), "nome"  => substr( $linha, 11, 8 ) );
    $cabecalho["dados"]["banco"]               = substr( $linha, 76, 18 );
    $cabecalho["dados"]["data_gravacao"]       = substr( $linha, 94, 6  );
    $cabecalho["dados"]["sequencial_remessa"]  = substr( $linha, 100, 7 );
    $cabecalho["dados"]["sequencial_registro"] = substr( $linha, 394, 6 );

    if( $cabecalho["dados"]["arquivo"]["tipo"] != '1' ){
      $cabecalho["erros"]["2"] = "O tipo do Arquivo não é válido!"; // Layout de remessa";
      $cabecalho["quantidade_erros"]++ ;
    }

    if( $cabecalho["dados"]["arquivo"]["nome"] != 'REMESSA' ){
      $cabecalho["erros"]["3"] = "O nome do arquivo não é válido!"; // Layout de remessa";
      $cabecalho["quantidade_erros"]++ ;
    }

    if( $cabecalho["dados"]["servico"]["tipo"] != '01' ){
      $cabecalho["erros"]["4"] = "O tipo do Serviço não é válido!"; // Layout de remessa
      $cabecalho["quantidade_erros"]++ ;
    }

    if( $cabecalho["dados"]["servico"]["nome"] != 'COBRANCA' ){
      $cabecalho["erros"]["5"] = "O nome do serviço não é válido"; // Layout de remessa
      $cabecalho["quantidade_erros"]++ ;
    }

    if( ( trim( $cabecalho["dados"]["banco"] ) != '001SISACOB' ) && ( trim( $cabecalho["dados"]["banco"] ) != '001DECLACOB' ) ){
      $cabecalho["erros"]["13"] = "O Código de Identificação do Banco não é válido!"; // Layout de remessa
      $cabecalho["quantidade_erros"]++ ;
    }
  }*/

  return $cabecalho;
}

function validar_registro_cnab_remessa( $linha, $db, $cliente ){
  $corpo = array( "erros" => array(), "quantidade_erros" => 0, "dados" => array() );

  if( strlen( $linha ) != 400 ){
    $corpo["erros"]["SISTEMA"] = "A linha devem conter 400 caracteres alfanuméricos!";
    $corpo["quantidade_erros"]++ ;
  }

  $corpo["dados"]["identificacao_registro"] = substr( $linha, 0, 1 );

  if( ( $corpo["dados"]["identificacao_registro"] != '9' ) && ( $corpo["dados"]["identificacao_registro"] != '1' ) ){
    $corpo["erros"]["1"] = "O Código Identificador do Registro não é válido!";
    $corpo["quantidade_erros"]++ ;
  } elseif( $corpo["dados"]["identificacao_registro"] == '1' ){
    $corpo["dados"]["identificacao_sequencia"] = substr( $linha, 1, 2 );
    $corpo["dados"]["cpf_cnpj"]                = substr( $linha, 3, 14 );
    $corpo["dados"]["cliente"]                 = array( "agencia" => substr( $linha, 17, 4 ), "conta" => substr( $linha, 22, 8 ),
                                                        "digito"  => substr( $linha, 30, 1 ) );
    $corpo["dados"]["controle_cliente"]        	= substr( $linha, 37, 25 );
    $corpo["dados"]["nosso_numero"]            	= substr( $linha, 62, 11 );
    $corpo["dados"]["opcao_protesto"]			= substr( $linha, 108, 2 );
	$corpo["dados"]["numero_documento"]        	= substr( $linha, 110, 10 );
    $corpo["dados"]["data_vencimento"]         	= substr( $linha, 120, 6 );
    $corpo["dados"]["valor_titulo"]            	= substr( $linha, 126, 13 );
    $corpo["dados"]["data_emissao"]            	= substr( $linha, 150, 6 );
    $corpo["dados"]["taxa_mora"]               	= substr( $linha, 160, 6 );
    $corpo["dados"]["taxa_multa"]              	= substr( $linha, 166, 6 );
    $corpo["dados"]["valor_desconto"]          	= substr( $linha, 205, 13 );
    $corpo["dados"]["sequencia_44"]            	= substr( $linha, 218, 2 );
    $corpo["dados"]["numero_inscricao"]        	= substr( $linha, 220, 14 );
    $corpo["dados"]["sacado"]                  	= array( "nome" => substr( $linha, 234, 40 ),   "endereco" => substr( $linha, 274, 37 ),
                                                        "bairro" => substr( $linha, 311, 15 ), "cep" => substr( $linha, 326, 8 ),
                                                        "cep" => substr( $linha, 326, 8 ),     "cidade" => substr( $linha, 334, 15 ),
                                                        "uf" => substr( $linha, 349, 2 ) );
    $corpo["dados"]["indicativo_sacador"]	   = substr( $linha, 81, 1);
	$corpo["dados"]["observacoes"]             = substr( $linha, 351, 40 );
    $corpo["dados"]["protesto"]                = substr( $linha, 391, 2 );

    if( preg_match( "/^0[12]\$/", $corpo["dados"]["identificacao_sequencia"] ) == false ){
      $corpo["erros"]["2"] = "O Código Identificador da Sequencia não é válido!";
      $corpo["quantidade_erros"]++ ;
    } else {
      if( $corpo["dados"]["identificacao_sequencia"] == '02' && preg_match( "/^\d{14}\$/", $corpo["dados"]["cpf_cnpj"] ) == false ){
        $corpo["erros"]["3"] = "O CNPJ não é válido!";
        $corpo["quantidade_erros"]++ ;
      }elseif( $corpo["dados"]["identificacao_sequencia"] == '01' && preg_match( "/^\d{11}(\d{3})?$/", trim($corpo["dados"]["cpf_cnpj"]) ) == false ){
        $corpo["erros"]["3"] = "O CPF não é válido! ";
        $corpo["quantidade_erros"]++ ;
      }
    }

    if( preg_match( "/^\d{4}\$/", $corpo["dados"]["cliente"]["agencia"] ) == false ){
      $corpo["erros"]["4"] = "A agência não é válida!";
      $corpo["quantidade_erros"]++ ;
    } elseif( (int)$corpo["dados"]["cliente"]["agencia"] != (int)$cliente["agencia"] ){
      $corpo["erros"]["4"] = "A Agência informada não condiz com a Agência do Usuário logado!";
      $corpo["quantidade_erros"]++ ;
    }

    $conta_valida = true;
    if( preg_match( "/^\d{8}\$/", $corpo["dados"]["cliente"]["conta"] ) == false ){
      $corpo["erros"]["6"] = "A Conta Corrente não é válida!";
      $corpo["quantidade_erros"]++ ;
      $conta_valida = false;
    }

    if( preg_match( "/^(\d|[^\d])\$/", $corpo["dados"]["cliente"]["digito"] ) == false ){
      $corpo["erros"]["7"] = "O Digito da Conta Corrente  não é válido!";
      $corpo["quantidade_erros"]++ ;
      $conta_valida = false;
    }

	$contaCliente = $cliente["conta"];
	while(strlen($contaCliente) < 10){
		$contaCliente = '0'.$contaCliente;
	}

	if( $conta_valida && $corpo["dados"]["cliente"]["conta"]."-".$corpo["dados"]["cliente"]["digito"] != $contaCliente ){
      $corpo["erros"]["6-7"] = "A Conta informada não condiz com a Conta do Usuário logado!";
      $corpo["quantidade_erros"]++ ;
    }

    // validação do nossonumero
    if( preg_match( "/^0{11}\$/", $corpo["dados"]["nosso_numero"] ) == false){
      if( preg_match( "/^[^0]\d{10}\$/", $corpo["dados"]["nosso_numero"] ) || // verifica se o primeiro digito é '0'
          preg_match( "/^0{5}\d{6}\$/",  $corpo["dados"]["nosso_numero"] ) || // verifica se o 4 proximos digitos são diferentes de '0000'
          preg_match( "/^0\d{4}0{6}\$/", $corpo["dados"]["nosso_numero"] ) ){ // verifica se o 6 próximos digitos são diferentes de '000000'
        $corpo["erros"]["10"] = "O Nosso Número não é válido!";
        $corpo["quantidade_erros"]++ ;
      }
    } elseif( preg_match( "/^(\d|[^\d]){25}\$/", $corpo["dados"]["controle_cliente"] ) == false ){
      $corpo["erros"]["9"] = "O Controle de Cliente não é válido!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^(\d|[^\d]|\s){10}\$/", $corpo["dados"]["numero_documento"] ) == false ){
      $corpo["erros"]["25"] = "O Número do Documento não é válido!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^\d{6}\$/", $corpo["dados"]["data_vencimento"] ) == false ||
      ( preg_match( "/^(8{6})|(9{6})\$/", $corpo["dados"]["data_vencimento"] ) == false &&
        data_valida( preg_replace( "/^(\d{2})(\d{2})(\d{2})\$/", '$1/$2/$3', $corpo["dados"]["data_vencimento"] ) ) == false ) ){
      $corpo["erros"]["26"] = "A Data de Vencimento não é válida!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^\d{13}\$/", $corpo["dados"]["valor_titulo"] ) == false ){
      $corpo["erros"]["27"] = "O Valor do Título não é valido!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^\d{6}\$/", $corpo["dados"]["data_emissao"] ) == false ||
        data_valida( preg_replace( "/^(\d{2})(\d{2})(\d{2})\$/", '$1/$2/$3', $corpo["dados"]["data_emissao"] ) ) == false ){
      $corpo["erros"]["33"] = "A Data de Emissão não é válida!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^\d{6}\$/", $corpo["dados"]["taxa_mora"] ) == false ){
      $corpo["erros"]["36"] = "A Taxa de Mora Mês não é válida!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^\d{6}\$/", $corpo["dados"]["taxa_multa"] ) == false ){
      $corpo["erros"]["37"] = "A Taxa de Multa não é válida!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^\d{13}\$/", $corpo["dados"]["valor_desconto"] ) == false ){
      $corpo["erros"]["42"] = "O Valor do Desconto não é válido!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^0[12]\$/", $corpo["dados"]["sequencia_44"] ) == false ){
      $corpo["erros"]["43"] = "O Código Identificador da Sequencia 44 não é válido!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^(\d|[^\d]){40}\$/", $corpo["dados"]["sacado"]["nome"] ) == false ){
      $corpo["erros"]["45"] = "O Nome do Sacado não é válido!";
      $corpo["quantidade_erros"]++ ;
    }

    if( preg_match( "/^(\d|[^\d]){37}\$/", $corpo["dados"]["sacado"]["endereco"] ) == false ){
      $corpo["erros"]["46"] = "O Endereço do Sacado não é válido!";
      $corpo["quantidade_erros"]++ ;
    }
	
    if( preg_match( "/^(\d|[^\d]){40}$/", $corpo["dados"]["observacoes"] ) == false ){
      $corpo["erros"]["51"] = "As Observações não são válidas!";
      $corpo["quantidade_erros"]++ ;
    }

	if($corpo["dados"]["indicativo_sacador"] == "A"){
	  		$corpo["dados"]["nome_sacador"] = substr($corpo["dados"]["observacoes"], 0, 25);
			$corpo["dados"]["cnpj_sacador"] = substr($corpo["dados"]["observacoes"], 26, 14);

			if($corpo["dados"]["nome_sacador"] == "                         "){
				$corpo["erros"]["51"] = "Nome do sacador avalista não pode estar vazio!";
			    $corpo["quantidade_erros"]++ ;
			}
			
			if(!valida_cpf_cnpj($corpo["dados"]["cnpj_sacador"])){
				$corpo["erros"]["53"] = "CNPJ do sacador avalista não é válido!";
			    $corpo["quantidade_erros"]++ ;
			}
			
			$corpo["dados"]["cnpj_sacador"] = formatarCPF_CNPJ($corpo["dados"]["cnpj_sacador"]);
	}

    if( (preg_match( "/^(\d{2})|(\s{2})$/", $corpo["dados"]["protesto"] ) == false ) || ($corpo["dados"]["opcao_protesto"] == '06' && $corpo["dados"]["protesto"]== "  ")){
	  $corpo["erros"]["52"] = "O Protesto não é válido!";
      $corpo["quantidade_erros"]++ ;
    }
  }

  return $corpo;
}
/********************************************** UTILITÁRIOS **************************************************************************/
/****************** manipulação do CNAB *******************/
function salvar_log_cnab( $db, $cliente, $linha_processada, $documento = "" ){
  $db->query( "INSERT INTO logs_cnab_remessa ( agencia, conta, login, linha_processada, documento ) VALUE ( '".$cliente["agencia"]."', '".$cliente["conta"]."', '".$cliente["login"]."', '".mysql_real_escape_string($linha_processada)."', ".(!empty($documento) ? "'".$documento."'" : "NULL")." ); " );
}

function recuperar_contador_retorno( $db, $cliente ){
  $query = $db->query( "SELECT clientes.contador_retorno + 1 AS contador_retorno FROM clientes where cliente = '".$cliente["cliente"]."'" );
  $db->query( "UPDATE clientes SET clientes.contador_retorno = '".$query->row["contador_retorno"]."' WHERE cliente = '".$cliente["cliente"]."'" );
  return $query->row["contador_retorno"]; // buscar do banco
}

function cabecalho( &$arquivo, $db, $cliente ){
  $contador_retorno = recuperar_contador_retorno( $db, $cliente );

  // cabeçalho
  $linha = "";
  $linha .= "0"; // Sequência 01
  $linha .= "2"; // Sequência 02
  $linha .= "RETORNO"; // Sequência 03
  $linha .= "01"; // Sequência 04
  $linha .= "COBRANCA"; // Sequência 05
  $linha .= add_espaco_direita( "", 7 ); // Sequência 06
  $linha .= add_zero_esquerda( $cliente["agencia"], 4 ); // Sequência 07
  $linha .= add_espaco_direita( "", 1 ); // Sequência 08
  $linha .= add_zero_esquerda( preg_replace( "/^(\d+)\-\d\$/", '$1', $cliente["conta"] ), 8 ); // Sequência 09
  $linha .= add_zero_esquerda( preg_replace( "/^\d+\-(\d)\$/", '$1', $cliente["conta"] ), 1 ); // Sequência 10
  $linha .= add_espaco_direita( "", 6 ); // Sequência 11
  $linha .= substr( add_espaco_direita( $cliente["razao_social"], 30 ), 0, 30 ); // Sequência 12
  $linha .= add_espaco_direita( "001SISACOB", 18 ); // Sequência 13
  $linha .= date( "dmy" ); // Sequência 14
  $linha .= add_zero_esquerda( $contador_retorno, 7 ); // Sequência 15
  $linha .= add_espaco_direita( "", 287 ); // Sequência 16
  $linha .= add_zero_esquerda("1", 6); // Sequência 17

  escrever_bloco( $arquivo, $linha );
}

function corpo(&$arquivo, $db, $num_registro, $cliente, $cooperativa, $dados){
  $num_registro_atual = 1+(int)$num_registro;

  // cabeçalho
  $linha = "";
  $linha .= "1"; // Sequência 01
  $linha .= add_espaco_direita( "", 2 ); // Sequência 02
  $linha .= add_zero_esquerda( preg_replace( "/\-|\.|\//", '', $cliente["cgc"] ), 14 ); // Sequência 03
  $linha .= add_zero_esquerda( $cliente["agencia"], 4 ); // Sequência 04
  $linha .= add_espaco_direita( "", 1 ); // Sequência 05
  $linha .= add_zero_esquerda( preg_replace( "/^(\d+)\-\d\$/", '$1', $cliente["conta"] ), 8 ); // Sequência 06
  $linha .= add_zero_esquerda( preg_replace( "/^\d+\-(\d)\$/", '$1', $cliente["conta"] ), 1 ); // Sequência 07
  $linha .= add_espaco_direita( "", 6 ); // Sequência 08
  $linha .= add_espaco_direita( $dados["seunumero"], 25 ); // Sequência 09
  $linha .= add_zero_esquerda( $dados["nossonumero"], 11 ); // Sequência 10
  $linha .= add_espaco_direita( "", 1 ); // Sequência 11
  $linha .= add_espaco_direita( "", 2 ); // Sequência 12
  $linha .= add_espaco_direita( "", 4 ); // Sequência 13
  $linha .= add_zero_esquerda( $dados["tipo_lancamento"], 2 ); // Sequência 14
  $linha .= add_espaco_direita( "", 3 ); // Sequência 15
  $linha .= add_espaco_direita( "", 3 ); // Sequência 16
  $linha .= add_espaco_direita( "", 1 ); // Sequência 17
  $linha .= add_espaco_direita( "", 5 ); // Sequência 18
  $linha .= add_espaco_direita( "", 1 ); // Sequência 19
  $linha .= add_espaco_direita( "", 5 ); // Sequência 20
  $linha .= add_espaco_direita( "", 5 ); // Sequência 21
  $linha .= add_espaco_direita( "", 1 ); // Sequência 22
  $linha .= add_espaco_direita( "", 2 ); // Sequência 23
  $linha .= add_espaco_direita( "", 2 ); // Sequência 24
  $linha .= add_zero_esquerda( $dados["data_movimentacao"], 6 ); // Sequência 25
  $linha .= add_espaco_direita( "", 10 ); // Sequência 26
  $linha .= add_espaco_direita( "", 20 ); // Sequência 27
  $linha .= add_zero_esquerda( $dados["data_vencimento"], 6 ); // Sequência 28
  $linha .= add_zero_esquerda( $dados["valor"], 13 ); // Sequência 29
  $linha .= add_espaco_direita( "", 3 ); // Sequência 30
  $linha .= add_espaco_direita( "", 4 ); // Sequência 31
  $linha .= add_espaco_direita( "", 1 ); // Sequência 32
  $linha .= add_espaco_direita( "", 2 ); // Sequência 33
  $linha .= add_zero_esquerda( $dados["data_credito"], 6 ); // Sequência 34
  $linha .= add_espaco_direita( "", 7 ); // Sequência 35
  $linha .= add_espaco_direita( "", 13 ); // Sequência 36
  $linha .= add_espaco_direita( "", 13 ); // Sequência 37
  $linha .= add_espaco_direita( "", 13 ); // Sequência 38
  $linha .= add_zero_esquerda( $dados["abatimento"], 13 ); // Sequência 39
  $linha .= add_zero_esquerda( $dados["desconto_concedido"], 13 ); // Sequência 40
  $linha .= add_zero_esquerda( $dados["valor_recebido"], 13 ); // Sequência 41
  $linha .= add_zero_esquerda( $dados["juros_mora"], 13 ); // Sequência 42
  $linha .= add_zero_esquerda( $dados["outros_recebimentos"], 13 ); // Sequência 43
  $linha .= add_zero_esquerda( $dados["abatimento_nao_aproveitado"], 13 ); // Sequência 44
  $linha .= add_zero_esquerda( $dados["valor_lancamento"], 13 ); // Sequência 45
  $linha .= add_zero_esquerda( $dados["indicativo_credito_debito"], 1 ); // Sequência 46
  $linha .= add_espaco_direita( "", 1 ); // Sequência 47
  $linha .= add_espaco_direita( "", 12 ); // Sequência 48
  $linha .= add_espaco_direita( "", 10 ); // Sequência 49
  $linha .= add_zero_esquerda( preg_replace( "/\-|\.|\//", '', $dados["cpf"] ), 14 ); // Sequência 50
  $linha .= add_zero_esquerda( "", 38 ); // Sequência 51
  $linha .= add_zero_esquerda( $num_registro_atual, 6 ); // Sequência 52

  escrever_bloco( $arquivo, $linha );
}

function rodape( &$arquivo, $db, $quantidade_registros, $cliente, $cooperativa ){

  // cabeçalho
  $linha = "";
  $linha .= "9"; // Sequência 01
  $linha .= "02"; // Sequência 02
  $linha .= add_zero_esquerda( $cooperativa["codigo_bancario"], 3); // Sequência 03
  $linha .= add_zero_esquerda( $cliente["agencia"], 4 ); // Sequência 04
  $linha .= preg_replace( "/^(\.{25})/", '$1', add_espaco_direita( $cooperativa["nome_fantasia"], 25 ) ); // Sequência 05
  $linha .= preg_replace( "/^(\.{50})/", '$1', add_espaco_direita( $cooperativa["endereco"], 50 ) ); // Sequência 06
  $linha .= preg_replace( "/^(\.{30})/", '$1', add_espaco_direita( $cooperativa["bairro"], 30 ) ); // Sequência 07
  $linha .= add_espaco_direita( preg_replace( "/^(\d{5})\-(\d{3})\$/", '$1$2', $cooperativa["cep"] ), 8 ); // Sequência 08
  $linha .= preg_replace( "/^(\.{30})/", '$1', add_espaco_direita( $cooperativa["cidade"], 30 ) ); // Sequência 09
  $linha .= add_espaco_direita( $cooperativa["estado"], 2 ); // Sequência 10
  $linha .= date( "Ymd" ); // Sequência 11
  $linha .= add_zero_esquerda( $quantidade_registros, 8 ); // Sequência 12
  $linha .= add_espaco_direita( "", 11 ); // Sequência 13
  $linha .= add_espaco_direita( "", 212 ); // Sequência 14
  $linha .= add_zero_esquerda( ( $quantidade_registros + 2 ), 6 ); // Sequência 15

  escrever_bloco( $arquivo, $linha );
}
/**********************************************************/

// Função para verificar se o valor da variavel está vazia, nula, ou se não existe
function esta_vazio(&$valor){
  return !isset($valor) || is_null($valor) || empty($valor);
}

// Função para verificar se o valor da variavel está preenchida
function esta_presente(&$valor){
  return !esta_vazio($valor);
}

// Função para verificar se o valor da variavel está preenchida
function reverter_array($array_string){

  return !esta_vazio($valor);
}

function num_to_dinheiro($numero, $prefixo = ""){
  return $prefixo . number_format( $numero, 2, ',', '.');
}

function texto_to_float($texto){
  return (float)preg_replace( "/^(\d+),(\d{2})/", "\$1.\$2", preg_replace( "/\./", "", $texto ) );
}

function add_zero_esquerda($texto, $num_casas){
  return str_pad($texto, (int)$num_casas, "0", STR_PAD_LEFT);
}

function add_espaco_direita($texto, $num_casas){
  return str_pad($texto, (int)$num_casas, " ", STR_PAD_RIGHT);
}

function data_br_to_data_banco($data){
  return preg_replace("/(\d{2})\/(\d{2})\/(\d{4})/", "\$3-\$2-\$1", $data );
}

function data_banco_to_data_br($data){
  return preg_replace("/(\d{4})\-(\d{2})\-(\d{2})/", "\$3/\$2/\$1", $data );
}

function raw($str){
  return strtr($this->get(), array('&amp;' => '&', '&gt;' => '>', '&lt;' => '<;', '&quot;' => '"'));
}

// Informações do cliente
function gerar_dados_cliente($db, $cliente, $id_conexao = null ){
    $dados = array();

$query_cliente = $db->query("
    SELECT
       clientes.cliente,
        clientes.agencia,
        clientes.conta,
        clientes.usuario,
        clientes.razao,
        clientes.nossonumero,
        clientes.nome_fantasia,
        clientes.cgc,
        clientes.endereco,
        clientes.cidade,
        clientes.estado,
        clientes.instrucao,
        clientes.instrucao_1via
    FROM
        clientes
    WHERE
        clientes.cliente = '".$cliente."'
    LIMIT
        1;");

    if( $query_cliente->executed && $query_cliente->num_rows == 1 ){
        $dados = array(
            "cliente"                => $query_cliente->row['cliente'],
            "agencia"                => $query_cliente->row['agencia'],
            "conta"                  => $query_cliente->row['conta'],
            "login"                  => $query_cliente->row['usuario'],
            "cliente_master"         => "true",
            "razao_social"           => $query_cliente->row['razao'],
            "nossonumero"            => $query_cliente->row['nossonumero'],
            "nome_fantasia"          => $query_cliente->row['nome_fantasia'],
            "nome_fantasia_ou_razao" => (esta_presente($query_cliente->row['nome_fantasia']) ? $query_cliente->row['nome_fantasia'] : $query_cliente->row['razao']),
            "cgc"                    => $query_cliente->row['cgc'],
            "endereco"               => $query_cliente->row['endereco'],
            "cidade"                 => $query_cliente->row['cidade'],
            "estado"                 => $query_cliente->row['estado'],
            "id_conexao"             => $id_conexao,
            "instrucao"              => $query_cliente->row['instrucao'],
            "instrucao_1via"         => (esta_presente($query_cliente->row['instrucao_1via']) ? $query_cliente->row['instrucao_1via'] : ''),
            "modelo_padrao"          => ( esta_presente( $query_cliente->row['boleto_padrao'] ) ? $query_cliente->row['boleto_padrao'] : 1 ),
            "acesso_administrador"   => esta_presente( $query_cliente->row['acesso_administrador'] ) && ( boolean )$query_cliente->row['acesso_administrador'],
            "cliente_master"         => esta_presente( $query_cliente->row['cliente_master'] ) && ($query_cliente->row['cliente_master'] == "true"),
        );
	}

  return $dados;
}

// Informações da cooperativa
function gerar_dados_cooperativa($db, $cliente){
  $dados = array();

  $query_cooperativa = $db->query("SELECT agencias.cod_banc AS codigo_bancario, agencias.variacao_carteira, agencias.contrato, agencias.carteira, agencias.convenio, agencias.razao AS fantasia, agencias.endereco, agencias.cep, agencias.bairro, agencias.cidade, agencias.estado, agencias.telefone, agencias.mensagem_local, agencias.cod_agen AS agencia_bancaria FROM agencias INNER JOIN clientes ON agencias.agencia = clientes.agencia WHERE clientes.cliente = '".$cliente."' LIMIT 1");
  if( $query_cooperativa->executed && $query_cooperativa->num_rows == 1 ){
    $dados = array(
      "agencia_bancaria"  => $query_cooperativa->row['agencia_bancaria'],
      "codigo_bancario"   => $query_cooperativa->row['codigo_bancario'],
      "convenio"          => $query_cooperativa->row['convenio'],
      "carteira"          => $query_cooperativa->row['carteira'],
      "contrato"          => $query_cooperativa->row['contrato'],
      "variacao_carteira" => $query_cooperativa->row['variacao_carteira'],
      "nome_fantasia"     => $query_cooperativa->row['fantasia'],
      "endereco"          => $query_cooperativa->row['endereco'],
      "cep"               => $query_cooperativa->row['cep'],
      "bairro"            => $query_cooperativa->row['bairro'],
      "cidade"            => $query_cooperativa->row['cidade'],
      "estado"            => $query_cooperativa->row['estado'],
      "telefone"          => $query_cooperativa->row['telefone'],
      "mensagem_local"    => $query_cooperativa->row['mensagem_local'],
    );
    $dados["endereco_completo"] = $dados["endereco"].", ".$dados["bairro"]." - ".$dados["cidade"]."/".$dados["estado"];
  }

  return $dados;
}

function gerar_filtros( $post_filtros = array("opca_maste" => "meus") ){
  $filtros = array();
//  var_dump($post_filtros);exit();
  $filtros["situacao"]       = ( esta_presente( $post_filtros["situacao"]->toDB() )       ? $post_filtros["situacao"]->toDB()       : "todos" );
  $filtros["status"]         = ( esta_presente( $post_filtros["status"]->toDB() )         ? $post_filtros["status"]->toDB()         : "ambos" );
  $filtros["tipo_data"]      = ( esta_presente( $post_filtros["data"]->toDB() )           ? $post_filtros["data"]->toDB()           : "emissao" );
  $filtros["data_inicio"]    = ( esta_presente( $post_filtros["data_inicio"]->toDB() )    ? $post_filtros["data_inicio"]->toDB()    : datetime_modificado( array( "formato" => "d/m/Y", "mes" => -1 ) ) );
  $filtros["data_fim"]       = ( esta_presente( $post_filtros["data_fim"]->toDB() )       ? $post_filtros["data_fim"]->toDB()       : datetime_modificado( array( "formato" => "d/m/Y" ) ) );
  $filtros["tipo_ordenacao"] = ( esta_presente( $post_filtros["tipo_ordenacao"]->toDB() ) ? $post_filtros["tipo_ordenacao"]->toDB() : "nome" );
  $filtros["opcao_master"]   = ( esta_presente( $post_filtros["opcao_master"])   ? $post_filtros["opcao_master"]   : "meus" );

  return $filtros;
}

// Gera o where de acordo com o filtro passado
function gerar_where_de_titulos( $filtros ){
  $where = "";

  switch ($filtros["tipo_data"]){
    case 'emissao':
      $where .= " AND '".data_br_to_data_banco( $filtros["data_inicio"] )." 00:00:00' <= titulos.data_emisao AND titulos.data_emisao <= '".data_br_to_data_banco( $filtros["data_fim"] )." 23:59:59'";
    break;
    case 'vencimento':
      $where .= " AND '".data_br_to_data_banco( $filtros["data_inicio"] )." 00:00:00' <= titulos.data_venc AND titulos.data_venc <= '".data_br_to_data_banco( $filtros["data_fim"] )." 23:59:59'";
    break;
    case 'liquidacao':
      $where .= " AND ( ( '".data_br_to_data_banco( $filtros["data_inicio"] )." 00:00:00' <= titulos.data_baixa AND titulos.data_baixa <= '".data_br_to_data_banco( $filtros["data_fim"] )." 23:59:59' ) OR ( '".data_br_to_data_banco( $filtros["data_inicio"] )." 00:00:00' <= titulos.data_baixa_manual AND titulos.data_baixa_manual <= '".data_br_to_data_banco( $filtros["data_fim"] )." 23:59:59' ) )";
    break;
    case 'credito':
      $where .= " AND ( '".data_br_to_data_banco( $filtros["data_inicio"] )." 00:00:00' <= titulos.data_credito AND titulos.data_credito <= '".data_br_to_data_banco( $filtros["data_fim"] )." 23:59:59' )";
    break;
    case 'cancelado':
      $where .= " AND ( '".data_br_to_data_banco( $filtros["data_inicio"] )." 00:00:00' <= titulos.cancelamento AND titulos.cancelamento <= '".data_br_to_data_banco( $filtros["data_fim"] )." 23:59:59' )";
    break;
    default :
      $where .= " AND FALSE"; // parametro passado errado, não deve exibir a listagem
    break;
  }

  switch ($filtros["status"]){
    case 'ambos':
      $where .= ""; // não acresenta nada, para que exiba tanto os descontados, quanto os não descontados
    break;
    case 'descontados':
      $where .= " AND titulos.desconto IS NOT NULL";
    break;
    case 'nao_descontados':
      $where .= " AND titulos.desconto IS NULL";
    break;
    default :
      $where .= " AND FALSE"; // parametro passado errado, não deve exibir a listagem
    break;
  }

  switch ($filtros["situacao"]){
    case 'todos':
      $where .= ""; // não acresenta nada, para exibir todos os titulos possíveis
    break;
    case 'recebidos':
      $where .= " AND ( titulos.data_baixa IS NOT NULL OR titulos.data_baixa_manual IS NOT NULL )";
    break;
    case 'pendentes':
      $where .= " AND titulos.cancelamento IS NULL AND titulos.data_baixa IS NULL AND titulos.data_baixa_manual IS NULL";
    break;
    case 'vencidos':
      $where .= " AND titulos.data_venc < NOW() AND titulos.cancelamento IS NULL AND titulos.data_baixa IS NULL AND titulos.data_baixa_manual IS NULL";
    break;
    case 'cancelados':
      $where .= " AND titulos.cancelamento IS NOT NULL";
    break;
    default :
      $where .= " AND FALSE"; // parametro passado errado, não deve exibir a listagem
    break;
  }
  return $where;
}

// Gera o ORDER BY de acordo com o filtro passado
function gerar_order_by_de_titulos( $filtros ){
  $order_by = "titulos.cliente";

  switch ($filtros["tipo_ordenacao"]){
    case 'nome':
      $order_by .= ", sacados.nome";
    break;
    case 'emissao':
      $order_by .= ", titulos.data_emisao, sacados.nome";
    break;
    case 'vencimento':
      $order_by .= ", titulos.data_venc, sacados.nome";
    break;
    case 'liquidacao':
      $order_by .= ", titulos.data_baixa, titulos.data_baixa_manual, sacados.nome";
    break;
    case 'credito':
      $order_by .= ", titulos.data_credito, sacados.nome";
    break;
  }

  return $order_by.", titulos.criacao";
}

function informacoes_associado($db, $conta){
  return "";
}

function datetime_modificado($opcoes = array() ){
  $params = array("formato" => "d/m/Y H:i:s",
                       "hora" => 0, "minuto" => 0, "segundo" => 0,
                       "mes" => 0, "dia" => 0,  "ano" => 0);
  foreach($opcoes as $chave => $valor){ $params[$chave] = $valor; } // atualiza o array com outro array
  $data = new DateTimeModify();

  $data->add_year((int)$params["ano"]);
  $data->add_month((int)$params["mes"]);
  $data->add_day((int)$params["dia"]);
  $data->add_hour((int)$params["hora"]);
  $data->add_minute((int)$params["minuto"]);
  $data->add_second((int)$params["segundo"]);

  return $data->format($params["formato"]);
}

function primeiro_dia($opcoes = array() ){
  $params = array("formato" => "Y-m-d", "mes" => idate('m'), "ano" => idate('Y'));

  foreach($opcoes as $chave => $valor){ $params[$chave] = $valor; } // atualiza o array com outro array

  $data = new DateTime();
  $data->setDate($params["ano"], $params["mes"], 1);
  return $data->format($params["formato"]);
}

function ultimo_dia($opcoes = array() ){
  $params = array("formato" => "Y-m-d", "mes" => idate('m'), "ano" => idate('Y'));

  foreach($opcoes as $chave => $valor){ $params[$chave] = $valor; } // atualiza o array com outro array

  $data = new DateTime();
  $data->setDate($params["ano"], $params["mes"], 1);
  $data->modify("+1 month");
  $data->modify("-1 day");
  return $data->format($params["formato"]);
}

function exibir_nome_mes( $mes ){
  $nome = "";
  switch ((int) $mes) {
    case  1: $nome = "Janeiro"; break;
    case  2: $nome = "Fevereiro"; break;
    case  3: $nome = "Março"; break;
    case  4: $nome = "Abril"; break;
    case  5: $nome = "Maio"; break;
    case  6: $nome = "Junho"; break;
    case  7: $nome = "Julho"; break;
    case  8: $nome = "Agosto"; break;
    case  9: $nome = "Setembro"; break;
    case 10: $nome = "Outubro"; break;
    case 11: $nome = "Novembro"; break;
    case 12: $nome = "Dezembro"; break;
  }
  return $nome;
}

// função que retorna as tentativas de acesso ao sistema; retorna se as três ultimas tentativas foram bem sucedidas
function tentativas_de_acesso($db, $agencia, $conta, $cliente){
  $tentativa = 0;
  $tentativas = array(false, true, true);

  $query_tentativas = $db->query("
  SELECT IF(  logs_acesso.acesso_concedido, \"true\", \"false\") as acesso_concedido
  FROM
    logs_acesso
   WHERE
    agencia = '$agencia'
  AND
    conta = '$conta'
  AND
    cliente = '$cliente'
  ORDER BY
   horario_acesso DESC LIMIT 3");
  foreach ($query_tentativas->rows as $key => $value) { $tentativas[$tentativa++] = $value["acesso_concedido"] == "true"; }

  return $tentativas;
}

// salva no log a tentativa de acesso ao sistema, se foi bem sucedida e o motivo
function salvar_tentativa_de_acesso($db, $agencia, $conta, $usuario, $cliente, $ip_de_acesso, $mensagem, $acesso_concedido = "false"){
  $db->query(
" INSERT INTO
    logs_acesso(
      agencia,
      conta,
      login,
      cliente,
      acesso_concedido,
      ip_acesso,
      motivo)
  VALUES(
      '$agencia',
      '$conta',
      '$usuario',

      '$cliente',
      $acesso_concedido,
      '$ip_de_acesso',
      '$mensagem')"
      );
}

// função que retorna o número de paginas que a listagem vai ter com a paginação
function num_paginas($db, $from, $where, $itens_por_pagina){
  $itens_por_pagina = (((int) $itens_por_pagina > 0) ? (int) $itens_por_pagina : PER_PAGE );
  $num_registros = (int) $db->query("SELECT COUNT(*) as num_registros FROM ".$from." WHERE ".$where)->row["num_registros"];

  return (int)( $num_registros / (int) $itens_por_pagina) + (( $num_registros % $itens_por_pagina) > 0 ? 1 : 0);
}

// Função para gerar os links da paginação
function paginacao($db, $from, $where, $itens_por_pagina, $pagina_corrente, $action, $option, $html_option = array(), $onClick = "" ){
  $pagina_corrente = (int) $pagina_corrente;
  $num_paginas = num_paginas($db, $from, $where, $itens_por_pagina);
  $num_paginas_ao_redor_corrente = 5;

  $html = array("url_complementar" => array(), "method" => "get");

  foreach($html_option as $chave => $valor){ $html[$chave] = $valor; } // atualiza o array com outro array

  $paginacao = "";
  if($num_paginas > 1){
    $paginacao .= "<div class=\"paginacao\">
    ";

    if($pagina_corrente > 0){
      $paginacao .= "<button data-href=\"".$_SERVER["URL"]."/?action=".$action."&option=".$option."&pagina=".($pagina_corrente - 1)."\" class=\"ativa botao pagina\" data-pagina=\"".($pagina_corrente - 1)."\" ".($onClick != "" ? "onClick = \"".$onClick."\"": "").">";
    } else {
      $paginacao .= "<button class=\"pagina_corrente botao_desabilitado\" disabled=\"disabled\">";
    }

    $paginacao .= " << Anterior </button> | ";

    for($pagina = 0; $pagina < $num_paginas; $pagina++){
      // Inserir a geração da paginação
      if( ( $pagina == 0 ) || ( $pagina == ( $num_paginas -1 ) ) || ( ( $pagina_corrente - $num_paginas_ao_redor_corrente ) <= $pagina && $pagina <= ( $pagina_corrente + $num_paginas_ao_redor_corrente ) ) ){
        if($pagina_corrente == $pagina){
          $paginacao .= "<button class=\"pagina_corrente botao_desabilitado\" disabled=\"disabled\" ".($onClick != "" ? "onClick = \"".$onClick."\"": "")."> ".($pagina + 1)." </button> | ";
        } else {
          $paginacao .= "<button data-href=\"".$_SERVER["URL"]."/?action=".$action."&option=".$option."&pagina=".$pagina."\" class=\"ativa pagina botao\" data-pagina=\"".$pagina."\" ".($onClick != "" ? "onClick = \"".$onClick."\"": "")."> ".($pagina + 1)." </button> | ";
        }
      }

      //
      if( ( $pagina == 1 && $pagina < ( $pagina_corrente - $num_paginas_ao_redor_corrente ) ) || ( $pagina == ($num_paginas - 2) && $pagina > ( $pagina_corrente + $num_paginas_ao_redor_corrente ) ) ){
        $paginacao .= "... | ";
      }

    }

    if($pagina_corrente < ($num_paginas - 1)){
      $paginacao .= "<button data-href=\"".$_SERVER["URL"]."/?action=".$action."&option=".$option."&pagina=".($pagina_corrente + 1)."\" class=\"ativa botao pagina\" data-pagina=\"".($pagina_corrente + 1)."\" ".($onClick != "" ? "onClick = \"".$onClick."\"": "").">";
    } else {
      $paginacao .= "<button class=\"pagina_corrente botao_desabilitado\" disabled=\"disabled\" >";
    }

    $paginacao .= " Próximo >> </button> ";
    $paginacao .= "</div>";
  } else {
    $paginacao .= "";
  }

  return $paginacao;
}
/****************************************** FUNÇÕES RELACIONADAS MANIPULAÇÃO DE ARQUIVOS *********************************************/
// lê o bloco do arquivo e retira os caracteres de fim de linha do arquivo quando existentes
function ler_bloco( &$arquivo, $quantidade ){
  $linha = preg_replace( "/\r|\n/", '', fgets( $arquivo, $quantidade ) );

  if(mb_detect_encoding($linha)=='UTF-8'){
    return utf8_decode($linha);
  }else{
    return $linha;
  }
}
// lê o bloco do arquivo e retira os caracteres de fim de linha do arquivo quando existentes
function escrever_bloco( &$arquivo, $texto ){
  fwrite( $arquivo, $texto."\r\n" );
}
/*************************************** FUNÇÕES RELACIONADAS À URLS *****************************************************************/
function link_to($action, $option, $text, $css = "botao" ){
  return "<a href=\"".$_SERVER["URL"]."/?action=".$action."&option=".$option."\" class=\"ativa ".$css."\" >".$text."</a>";
}

function link_novo($action, $text = "Cadastro", $css = "botao_novo" ){
  return link_to($action, "novo", $text, $css );
}

function link_listagem($action, $text = "Listagem", $css = "botao" ){
  return link_to($action, "listagem", $text, $css );
}

function link_editar($action, $id, $text = "Editar", $css = "botao_editar"){
  return "<a href=\"".$_SERVER["URL"]."/?action=".$action."&option=editar&id=".$id."\" class=\"ativa ".$css."\" >".$text."</a>";
}

function link_confirmar_titulo($text = "Confirmar", $css = "botao_confirmar", $onClick = ""){
  return "<a class=\"ativa ".$css."\" ".(esta_presente($onClick) ? "onClick=".$onClick : "").">".$text."</a>";
}

function link_apagar($action, $id, $text = "Apagar", $css = "botao_apagar"){
  return "<a href=\"".$_SERVER["URL"]."/?action=".$action."&option=apagar&id=".$id."\" class=\"ativa ".$css."\" onclick=\"if(!confirm('Deseja realmente apagar este registro?')){ return false; }\" >".$text."</a>";
}

function link_imprimir_boleto( $action, $id_web, $text = "Imprimir Boleto", $css = "botao_imprimir" ){
  return "<a href=\"".$_SERVER["URL"]."/boleto.php?action=".$action."&id=".$id_web."\" target=\"_blank\" alt=\"Imprimir Boleto\" class=\"ativa ".$css."\" >".$text."</a>";
}

function link_imprimir_boleto_pdf( $action, $id_web, $text = "Imprimir Boleto", $css = "botao_imprimir_pdf" ){
  return "<a href=\"".$_SERVER["URL"]."/boleto_pdf.php?action=".$action."&id=".$id_web."\" target=\"_blank\" alt=\"Imprimir Boleto PDF\" class=\"ativa ".$css."\" >".$text."</a>";
}

function link_enviar_email_boleto( $action, $id_web, $text = "Enviar por E-mail", $tipo = "boleto_unico", $css = "botao_enviar_email" ){
  return "<a href=\"".$_SERVER["URL"]."?action=".$action."&option=email&id=".$id_web."&tipo=".$tipo."\" target=\"_blank\" class=\"ativa ".$css."\" >".$text."</a>";
}

// corrigir erro de redirecionamento
// helper de link que permite redirecionar em get e post
function hiperlink( $url, $text, $options = array() ){

  $opcoes = array("params" => array(), "method" => "GET", "class" => "", "confirm" => "");

  foreach($options as $chave => $valor){ $opcoes[$chave] = $valor; } // atualiza o array com outro array

  $opcoes["method"] = strtoupper( strtoupper($opcoes["method"]) == "GET" || strtoupper($opcoes["method"]) == "POST" ? $opcoes["method"] : 'get' );

#  $submit  = "var f = document.createElement('form'); f.style.display = 'none'; this.parentNode.appendChild(f); f.method = '".$opcoes["method"]."'; f.action = this.href; ";
  $submit  = "var f = document.createElement('form'); f.style.display = 'none'; this.parentNode.appendChild(f); f.method = '".$opcoes["method"]."'; f.action = '".$url."'; ";
  if( is_array($opcoes["params"]) ){
    foreach(  $opcoes["params"] as $name => $value ){
      $submit .= "var ".$name." = document.createElement('input'); ".$name.".setAttribute('type', 'hidden'); ".$name.".setAttribute('name', ".$name."); s.setAttribute('value', '".$value."'); f.appendChild(".$name."); ";
    }
  }
  $submit .= "f.submit();";

  $onclick = ( esta_presente( $opcoes["confirm"] ) ? "if( confirm( '".$opcoes["confirm"]."' ) ){ ".$submit." }" : $submit )." return false;";

  return "<a href=\"".$url."\" class=\"".$opcoes["class"]."\" onclick=\"".$onclick."\" class=\"".$opcoes["class"]."\" >".$text."</a>";
}

function somente_numero($texto){
  return preg_replace( '/\D/', '', $texto );
}

function valida_cpf_cnpj($cCpfCNPJ){
    $cCpfCNPJ = somente_numero($cCpfCNPJ); // somente numero
    $cCNPJ    = "0".$cCpfCNPJ;

    if( !empty($cCpfCNPJ) && strlen($cCpfCNPJ) == 11 ){
        $d = array();
        for($nCont = 0; $nCont < 9; $nCont++){
            $d[$nCont+1] = $cCpfCNPJ[$nCont];
        }

        $df4 = 10*$d[1]+9*$d[2]+8*$d[3]+7*$d[4]+6*$d[5]+5*$d[6]+4*$d[7]+3*$d[8]+2*$d[9];
        $df5 = $df4 / 11;
        $df6 = (int)$df5 * 11;
        $resto1 = $df4 - $df6;

        if( $resto1 == 0 || $resto1 == 1){
            $pridig = 0;
        } else {
            $pridig = 11 - $resto1;
        }

        for($nCont = 0; $nCont < 9; $nCont++){
            $d[$nCont+1] = $cCpfCNPJ[$nCont];
        }
        $df4 = 11*$d[1]+10*$d[2]+9*$d[3]+8*$d[4]+7*$d[5]+6*$d[6]+5*$d[7]+4*$d[8]+3*$d[9]+2*$pridig;
        $df5 = $df4 / 11;
        $df6 = (int)$df5 * 11;
        $resto1 = $df4 - $df6;
        if( $resto1 == 0 || $resto1 == 1){
            $segdig = 0;
        } else {
            $segdig = 11 - $resto1;
        }
        $pridig = (string)$pridig;
        $segdig = (string)$segdig;

        if( $pridig == substr($cCpfCNPJ,9,1) && $segdig == substr($cCpfCNPJ,10,1)){
            return true;
        }
    } elseif(!empty($cCNPJ) && strlen($cCNPJ) == 15) {
        $d = array();

        for($nCont = 0; $nCont < 13; $nCont++){
            $d[$nCont+1] = $cCNPJ[$nCont];
        }

        $df1  = 6*$d[1]+5*$d[2]+4*$d[3]+3*$d[4]+2*$d[5]+9*$d[6]+8*$d[7]+7*$d[8];
        $df1 += 6*$d[9]+5*$d[10]+4*$d[11]+3*$d[12]+2*$d[13];
        $df2  = $df1/11;
        $df3  = (int)$df2*11;
        $resto1 = $df1 - $df3;

        if($resto1 == 0 || $resto1 == 1){
            $pridig = 0;
        } else {
            $pridig = 11 - $resto1;
        }

        //Verificacao do segundo digito
        $df4  = 7*$d[1]+6*$d[2]+5*$d[3]+4*$d[4]+3*$d[5]+2*$d[6]+9*$d[7]+8*$d[8];
        $df4 += 7*$d[9]+6*$d[10]+5*$d[11]+4*$d[12]+3*$d[13]+2*$pridig;
        $df5  = $df4/11;
        $df6  = (int)$df5*11;
        $resto2 = $df4 - $df6;

        if($resto2 == 0 || $resto2 == 1){
            $segdig = 0;
        } else {
            $segdig = 11 - $resto2;
        }

        if( $pridig == (int)substr($cCNPJ,13,1) && $segdig == (int)substr($cCNPJ,14,1) && strlen(trim($cCNPJ)) != 0){
            return true;
        }
    }

    return false;
}

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = str_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do código informado é válido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a máscara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se não quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno; 
}

function zero_lpad($string, $tamanho)
{
	return str_pad($string, $tamanho, "0", STR_PAD_LEFT);
}

function zero_rpad($string, $tamanho)
{
	return str_pad($string, $tamanho, "0", STR_PAD_RIGHT);
}

function space_lpad($string, $tamanho)
{
	return str_pad($string, $tamanho, " ", STR_PAD_LEFT);
}

function space_rpad($string, $tamanho)
{
	return str_pad($string, $tamanho, " ", STR_PAD_RIGHT);
}

function repeat($string, $vezes)
{
	return str_repeat($string, $vezes);
}

?>