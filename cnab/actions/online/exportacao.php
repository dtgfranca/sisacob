<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Títulos CNAB</title>
<link href="../style/app.css" rel="stylesheet" type="text/css" />
</head>
<body>
<img id="logo_sisacob" src="../imagens/logo_sisacob.png" alt="SISACOB"/><a href="../sair.php">
<img id="botao_sair" src="../imagens/close.png" alt="SAIR"/> </a>
<br /><div id="div_arquivo" style="padding: 20px">
<?php

session_start();
require_once('../library/config.php');
require_once('../library/functions.php');
require_once('../library/db.php');
require_once('../library/param.php');
require_once('../Log.php');

//echo "<center>EM MANUTENÇÃO!</center><br><br>";

Log::getInstance();
Log::setCaminho('../logs/exportacao.txt');

if (!$_SESSION['logado'] || !isset($_SESSION['logado']))
	header("Location: ../index.php");

$param = new Param();
if ($param->get('submit')->present())
{
	$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	$cliente = gerar_dados_cliente($db, $_SESSION['cliente']);
	
	//print_r($cliente);

	$inicio = $_POST['inicio'];
	$inicio = explode("/", $inicio);
	$inicio = $inicio[2]."-".$inicio[1]."-".$inicio[0];
	
	$fim = $_POST['fim'];
	$fim = explode("/", $fim);
	$fim = $fim[2]."-".$fim[1]."-".$fim[0];

	$query = mysql_query("SELECT rtb_data, clf_conta, rtb_linha, sql_rowid 
						FROM cb_retorno_bco 
						WHERE clf_conta = '".$cliente['conta']."' 
						AND rtb_data BETWEEN '".$inicio."' AND '".$fim."' 
						AND agencia = '".$cliente['agencia']."'");
						
	if (mysql_num_rows($query) == 0)
		die("<center>Nenhum resultado encontrado!<br>
			<input type='button' value='Voltar' onClick='window.location=\"importacao.php\"'>");
	
	else
	{
		ob_start();
		$counter = 1;
		
		$simples_qtde = 0;
		$simples_vlr = 0;
		$descontada_qtde = 0;
		$descontada_vlr = 0;

		$conta = $cliente['conta'];
		$split = explode("-", $conta);
		$conta = $split[0];
		$dv = $split[1];

		$adicionais = mysql_query("SELECT cont_exportacao, ate_codigo FROM clientes WHERE cliente = '".$_SESSION['cliente']."' AND conta = '".$cliente['conta']."'");
		$assoc = mysql_fetch_assoc($adicionais);
		$cont_exporta = ($assoc['cont_exportacao'] + 1);
		$ate_codigo = str_replace('-', '', $assoc['ate_codigo']);
		
		/////////////// Header //
		echo "0";													// Identificação do registro
		echo "2";													// Tipo de operação
		echo "RETORNO";												// Identificação do tipo de operação
		echo "01";													// Identificação do tipo de serviço
		echo "COBRANCA";											// Descrição do tipo de serviço
		echo repeat(" ", 7);										// Em branco
		echo zero_lpad($cliente['agencia'], 4);						// Código da agência
		echo "0";													// Dígito da agência
		echo zero_lpad($conta, 8);									// Número da conta
		echo $dv;													// Dígito da conta
		echo repeat("0", 6);										// Zeros
		echo str_pad(substr($cliente['nome_fantasia_ou_razao'], 30), 30);		// Nome do Cedente
		echo space_rpad("756CREDINOVA", 18);						// Banco
		echo date("dmy");											// Data da gravação
		echo zero_lpad($cont_exporta, 7);							// Sequencia de retorno
		echo repeat(" ", 42);										// Em branco
		echo zero_lpad($ate_codigo, 7);								// Número de convênio
		echo repeat(" ", 238);										// Em branco
		echo zero_lpad($counter, 6)."\r\n";							// Sequencial
		// Header ///////////////
		
		/////////////// Detalhe //
		while ($row = mysql_fetch_object($query))
		{	
			$counter++;
							
			$linha = $row->rtb_linha;
			$linha = substr_replace($linha, zero_lpad($cliente['agencia'], 4), 17, 4); 		// Código da agência
			$linha = substr_replace($linha, "0", 21, 1); 									// Dígito da agência
			$linha = substr_replace($linha, zero_lpad($conta, 8), 22, 8);			 		// Número da conta
			$linha = substr_replace($linha, $dv, 30, 1);			 						// Dígito da conta
			$linha = substr_replace($linha, zero_lpad($ate_codigo, 7), 31, 7);	 			// Código do convênio (substituído pelo código do associado)
			$linha = substr_replace($linha, "1677072", 63, 7);	 							// Código do convênio (substituído pelo início do nosso número)
			$linha = substr_replace($linha, zero_lpad($counter, 6), 394, 6);				// Sequencial
			
			// 1=> Simples, 4 => Descontada
			$tipo = substr($linha, 80, 1);			
			if ($tipo == 4)
			{
				$descontada_qtde++;
				$descontada_vlr += (substr($linha, 153, 11) / 100);							// Valor do título
			}
			else
			{
				$simples_qtde++;
				$simples_vlr += (substr($linha, 153, 11) / 100);							// Valor do título
			}
			
			echo $linha."\r\n";
		}
		// Detalhe ///////////////
		
		/////////////// Trailler //
		$counter++;
		echo "9";													// Identificação do registro
		echo "2";													// ---
		echo "01";													// ---
		echo "001";													// ---
		echo repeat(" ", 10);										// Em branco
		echo zero_lpad($simples_qtde, 8);							// Simples: Quantidade de títulos
		echo zero_lpad(($simples_vlr * 100), 14);					// Simples: Valor total
		echo zero_rpad(0, 8);										// Simples: Número do aviso
		echo repeat(" ", 10);										// Simples: Em branco
		echo zero_lpad(0, 8);										// Vinculada: Quantidade de títulos
		echo zero_lpad(0, 14);										// Vinculada: Valor total
		echo zero_rpad(0, 8);										// Vinculada: Número do aviso
		echo repeat(" ", 10);										// Vinculada: Em branco
		echo zero_lpad(0, 8);										// Caucionada: Quantidade de títulos
		echo zero_lpad(0, 14);										// Caucionada: Valor total
		echo zero_rpad(0, 8);										// Caucionada: Número do aviso
		echo repeat(" ", 10);										// Caucionada: Em branco
		echo zero_lpad($descontada_qtde, 8);						// Simples: Quantidade de títulos
		echo zero_lpad(($descontada_vlr * 100), 14);				// Simples: Valor total
		echo zero_rpad(0, 8);										// Descontada: Número do aviso
		echo repeat(" ", 50);										// Descontada: Em branco
		echo zero_lpad(0, 8);										// Vendor: Quantidade de títulos
		echo zero_lpad(0, 14);										// Vendor: Valor total
		echo zero_rpad(0, 8);										// Vendor: Número do aviso
		echo repeat(" ", 147);										// Vendor: Em branco
		echo zero_lpad($counter, 6);								// Sequencial
		// Trailler ///////////////
		
		/////////////// Geração do arquivo //
		$cnab = ob_get_contents();
		ob_end_clean();		
		$caminho = "../../arquivo_temp/";
		$arquivo = "CBR643".$conta.$dv.".ret";
		
		if (file_exists($caminho.$arquivo))
			unlink($caminho.$arquivo);
		
		$handle = fopen($caminho.$arquivo, 'w');
		fwrite($handle, $cnab);
		fclose($handle);
		mysql_query("UPDATE clientes SET cont_exportacao = (cont_exportacao + 1) WHERE cliente = '".$_SESSION['cliente']."'");
		header("Location: exporta.php?arquivo=".$arquivo);
		// Geração do arquivo ///////////////
	}
}

else
	header("Location: importacao.php");

?>
</center>
</body>
</html>