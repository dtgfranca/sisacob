<?php

	//if ($_COOKIE['auth'] != "true")
	//die("<br><br><br><center><h2><span style='color: red'>EM MANUTENCAO!</style></h2></center>");

	//Constantes de conexão
    define('DB_DRIVER', 'mysql');

    //Configuração local
    define('DB_HOSTNAME', '138.59.235.226');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', 'skill');
    define('DB_DATABASE', 'skill_sisacob');

    /*define('DB_DATABASE', 'cob');	
    define('DB_HOSTNAME', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '');*/
	
	
	
  // DIR
  define( 'DIR_ROOT'      , dirname(__FILE__).'/' );
  define( 'DIR_LIB'       , DIR_ROOT.'library/' );
  define( 'DIR_DATABASE'  , '../library/database/' );
  define( 'DIR_APP'       , DIR_ROOT.'application/' );
  define( 'DIR_IMAGE'     , DIR_ROOT.'images/' );
  define( 'DIR_CSS'       , DIR_ROOT.'stylesheets/' );
  define( 'DIR_JS'        , DIR_ROOT.'javascripts/' );
  define( 'DIR_TEMP'      , DIR_ROOT.'temp/' );
  define( 'DIR_INI'		  ,	DIR_ROOT.'ini/'  );
  
  // mensagens padrão
  define('ACCESS_DENIED', "A página solicitada não existe ou você não possui permissão para acessá-la.");
  define('TAKEN', "já cadastrado(a)");
  define('BLANK', "deve ser informado(a)");
  define('INVALID', "não é válido");
  define('SAVED', "Registro salvo com sucesso");
  define('FAIL', "Verifique as pendências");
  define('DESTROY_NOT_FOUND', "Este registro não pode ser apagado, pois não foi encontrado.");
  define('EDIT_NOT_FOUND', "Este registro não pode ser editado, pois não foi encontrado.");
  define('EDIT_NOT_FOUND_PENDING', "Este registro não pode ser editado, pois não foi encontrado ou possui algum bloqueio.");
  define('CANT_EDIT', "Este registro não pode ser editado, pois já se encontra confirmado.");
  define('CONFIRM', "O título foi confirmado com sucesso");
  define('CONFIRM_DENIED', "O título solicitado não pode ser confirmado, pois não foi encontrado");

  $trans = array('Ãš' => 'U', 'Ã‡' => "C", 'Ã‰' => 'E', 'Ã' => 'I', 'Ã' => 'A', 'Ãƒ' => 'A', 'Âª' => 'a', 'Âº' => 'o', 'º' => 'o', 'Ã"' => 'O', 'Ã¡' => 'a',
                 'Ã§' => 'c', 'Ã©' => 'e', 'Ã•' => 'O', 'Ã§' => 'c', 'Ã³' => 'o', 'Ã¢' => 'a', 'Ã£' => 'a', 'Ã¡' => 'a', 'lÃ­n' => 'li­n', 'NA°' => 'No', '°' => 'o',
                 'Á' => 'A', 'À' => 'A', 'Ã' => 'A', 'Â' => 'A', 'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a',
                 'É' => 'E', 'È' => 'E', 'Ẽ' => 'E', 'Ê' => 'E', 'é' => 'e', 'è' => 'e', 'ẽ' => 'e', 'ê' => 'e',
                 'Í' => 'I', 'Ì' => 'I', 'Ĩ' => 'I', 'Î' => 'I', 'í' => 'i', 'ì' => 'i', 'ĩ' => 'i', 'î' => 'i',
                 'Ó' => 'O', 'Ò' => 'O', 'Õ' => 'O', 'Ô' => 'O', 'ó' => 'o', 'ò' => 'o', 'õ' => 'o', 'ô' => 'o',
                 'Ú' => 'U', 'Ù' => 'U', 'Ũ' => 'U', 'Û' => 'U', 'ú' => 'u', 'ù' => 'u', 'ũ' => 'u', 'û' => 'u', 'ç' => 'c', 'Ç' => 'C' );
?>