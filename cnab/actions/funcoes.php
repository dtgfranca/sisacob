<?php
	include('../../../config.php');
	session_start();
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
	require_once('../library/config.php');
	require_once('../library/functions.php');
	require_once('../library/db.php');
	require_once('../library/param.php');
	require_once('../Log.php');
	
	Log::getInstance();
	Log::setCaminho('../logs/importacao.txt');
	
	$arquivo_valido = true;
	$param = new Param();
	
	if($param->get('submit')->present()){
		$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		$cliente = gerar_dados_cliente($db, $cliente);

		$temp_path      = $_FILES['remessa']['tmp_name'];
		$target_path    = DIR_TEMP."remessa_".$cliente["cliente"]."_".date( "YmdHis" ).".cnab";
	  	$cabecalho      = array("erros" => array(), "quantidade_erros" => 0, "dados" => array());
	  	$corpo          = array("quantidade_sacados_cadastros" => 0, "quantidade_titulos_cadastros" => 0);
	  	$arquivo_valido = false;
	  	$cabecalho["erros"]["SISTEMA"] = "";
	  	if($_FILES["remessa"]["error"] == UPLOAD_ERR_NO_FILE){
			$cabecalho["erros"]["SISTEMA"] .= "O Arquivo de Remessa deve ser informado";
			$cabecalho["quantidade_erros"]++ ;
			salvar_log_cnab($db, $cliente, $cabecalho["erros"]["SISTEMA"]);
	  	}
	  	else if($_FILES["remessa"]["error"] != UPLOAD_ERR_OK){
			$cabecalho["erros"]["SISTEMA"] .= "Não foi possivel ler o Arquivo de Remessa! O arquivo está corrompido ou não é válido";
			$cabecalho["quantidade_erros"]++ ;
			salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"]);
	  	}
	  	else {
			if(false && ( !$finfo || substr( finfo_file( $finfo, $temp_path ), 0, 10 ) != "ASCII text" )) {
		  		$cabecalho["erros"]["SISTEMA"] .= "Tipo de Arquivo não suportado! O Arquivo de Remessa deve conter somente texto";
		  		$cabecalho["quantidade_erros"]++ ;
		  		salvar_log_cnab($db, $cliente, $cabecalho["erros"]["SISTEMA"]);
			}
			else {
		  		if(file_exists($target_path)){
					unlink($target_path); // remove o arquivo caso ele já exista
		  		}
		  		if(move_uploaded_file( $temp_path, $target_path ) == false) {
					$cabecalho["erros"]["SISTEMA"] .= "Não foi possivel ler o Arquivo de Remessa! O Arquivo corrompeu-se ou não é válido";
					$cabecalho["quantidade_erros"]++ ;
					salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"]);
		  		}
				else{
	 				//INSPECT exit('arquivo upado com sucesso');
					$cnab_remessa = fopen($target_path, "r");
					if(feof($cnab_remessa)){
						$cabecalho["erros"]["SISTEMA"] .= "O Arquivo de Remessa está vázio";
						$cabecalho["quantidade_erros"]++ ;
						salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"]);
					}
					else {
						//INEPECT exit('tem algo escrito');
				  		unset($cabecalho["erros"]["SISTEMA"]);
				 		$linha = ler_bloco( $cnab_remessa, 410 ); // solicitado a recuperação  mais de 400 caracteres para validação do arquivo
						$cabecalho = validar_cabecalho_cnab_remesa( $linha ); // executa a validação do cabeçalho
						$continuar_processando = $cabecalho["quantidade_erros"] == 0;
						$arquivo_valido        = $cabecalho["quantidade_erros"] == 0;
						$contador_linha = 1;
						$mensagem["alert"] = "";
						if( $continuar_processando == false ){
							salvar_log_cnab( $db, $cliente, $linha );
						}
						// fica no laço enquanto não for o fim do arquivo ou não encontar a identificação do registro '9'
				  		while($continuar_processando && !feof($cnab_remessa)){
							$linha = ler_bloco( $cnab_remessa, 410 ); // solicitado a recuperação  mais de 400 caracteres para validação do arquivo
							//mysql_query("INSERT INTO sisa_debug (dados, responsavel) VALUES ('".$linha."', 'Sirot')");
							$contador_linha++;
							$corpo[$contador_linha] = validar_registro_cnab_remessa( $linha, $db, $cliente ); // executa a validação dos registros
							$continuar_processando = $corpo[$contador_linha]["dados"]["identificacao_registro"] != '9';
							$arquivo_valido        = $arquivo_valido && $corpo[$contador_linha]["quantidade_erros"] == 0;
							//INSPECT var_dump($corpo[$contador_linha]["quantidade_erros"]);exit();
							if($arquivo_valido == false){
								salvar_log_cnab($db, $cliente, $linha);
							}
							else if($continuar_processando == true) {
								// salvando o registro
								// Importação dos clientes
								$sacado_nome = trim($corpo[$contador_linha]["dados"]["sacado"]["nome"]);
								$sacado_cpf_cnpj = "";
								switch($corpo[$contador_linha]["dados"]["sequencia_44"]){
									case '01': // Colocando mascara de CPF
										$sacado_cpf_cnpj = preg_replace("/^(\d{3})(\d{3})(\d{3})(\d{2})/", '$1.$2.$3-$4', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
										$sacado_cpf_cnpj_sem_mascara = preg_replace("/^(\d{3})(\d{3})(\d{3})(\d{2})/", '$1$2$3$4', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
									break;
									case '02': // Colocando mascara de CNPJ
										$sacado_cpf_cnpj = preg_replace("/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})\$/", '$1.$2.$3/$4-$5', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
										$sacado_cpf_cnpj_sem_mascara = preg_replace("/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})\$/", '$1$2$3$4$5', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
									break;
								}
							  	//Verificar se sacado já existe no sistema
			  					$query_decla = $db->query("SELECT sac.sacado FROM sacados AS sac WHERE sac.agencia='".$cliente["agencia"]."' AND sac.conta='".$cliente["conta"]."' AND sac.nome='".mysql_real_escape_string($sacado_nome)."' AND (sac.cpf='".$sacado_cpf_cnpj."' OR sacados.cpf='".$sacado_cpf_cnpj_sem_mascara."') AND (sac.grupo IS NULL OR sac.grupo <> '99999') LIMIT 1");
								//Inserir sacado caso não exista
				  				if($query_decla->num_rows == 0){
									$sacado_endereco = trim($corpo[$contador_linha]["dados"]["sacado"]["endereco"]);
									$sacado_bairro   = trim($corpo[$contador_linha]["dados"]["sacado"]["bairro"]);
									$sacado_cep      = preg_replace("/^(\d{5})(\d{3})\$/", '$1-$2', $corpo[$contador_linha]["dados"]["sacado"]["cep"]);
									$sacado_cidade   = trim($corpo[$contador_linha]["dados"]["sacado"]["cidade"]);
									$sacado_uf       = strtoupper($corpo[$contador_linha]["dados"]["sacado"]["uf"]);
									$query_decla = $db->query("INSERT INTO sacados(nome,cpf,endereco,bairro,cep,cidade,uf,grupo,cliente,agencia,conta) 
									VALUE (\"".$sacado_nome."\",\"".$sacado_cpf_cnpj."\",\"".$sacado_endereco."\",\"".$sacado_bairro."\",\"".$sacado_cep."\",\"".$sacado_cidade."\",\"".$sacado_uf."\",NULL,\"".$cliente["cliente"]."\",\"".$cliente["agencia"]."\",\"".$cliente["conta"]."\");");
									$sacado_sacado = $db->getLastId();
									$corpo["quantidade_sacados_cadastros"]++;
				  				}
								else {
									$sacado_sacado = $query_decla->row["sacado"];
				  				}
						
						
						
					}
		
				
				
				}
				
				
		  }
		  
		  
		  
	  }	  
	  
	  
	}	
?>