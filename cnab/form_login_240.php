<?php session_start(); ?>
<script type="text/javascript">
	function loga(){
		var agencia = document.getElementById('agencia').value;
		var cliente = document.getElementById('login').value;
		var conta = document.getElementById('conta').value;
		var senha = document.getElementById('senha').value;
		$.ajax({
			type: "get",
			url: "cnab/actions/login.php",
			data: "ag="+agencia+"&cli="+cliente+"&con="+conta+"&se="+senha,
			success: function(retorno){
				if(retorno == 'ok'){
					navega('cnab/principal_240.php');
				}
				else {
					alerta(retorno);
				}
			}
		});
	}
</script>
    <div id="section" class="small">
        <div class="titulo">
            <h2>LOGIN CNAB 240</h2>
            <a href="javascript:navega('principal.php');" class="sair"></a>
        </div>
        <br class="clear" />
        <div class="corpo">
        	<form name="form_login" method="POST" action="">
            	<fieldset>
                    <table>
                    	<tr>
                        	<td>Agencia:</td>
                            <td><input type="text" name="agencia" id="agencia" value="<?=$_SESSION['userAgencia']; ?>" size="3" maxlength="4" /></td>
                       	</tr>
                        <tr>
                        	<td>Conta:</td>
                            <td><input type="text" name="conta" id="conta" value="<?=$_SESSION['userConta']; ?>" size="11" maxlength="11" onKeyDown="FormataDado(10,1,event)" /></td>
                       	</tr>
                        <tr>
                        	<td>Login:</td>
                            <td><input type="text" name="login" id="login" value="<?=$_SESSION['userLogin']; ?>" size="12" maxlength="20" /></td>
                       	</tr>
                        <tr>
                        	<td>Senha:</td>
                            <td><input type="password" name="senha" id="senha" size="10" maxlength="10"/></td>
                       	</tr>
                    </table>
             	</fieldset>
        	<input type="button" value="Logar" name="Button" class="botao margins dir" onClick="loga()"/>
            <br class="clear">
    		</form>
      	</div>
   	</div>