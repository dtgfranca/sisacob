<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Importação de Títulos CNAB</title>
<link href="../style/app.css" rel="stylesheet" type="text/css" />
</head>
<body>
<img id="logo_sisacob" src="../imagens/logo_sisacob.png" alt="SISACOB"/><a href="../sair.php">
<img id="botao_sair" src="../imagens/close.png" alt="SAIR"/> </a>
<?php
session_start();
require_once('../library/config.php');
require_once('../library/functions.php');
require_once('../library/db.php');
require_once('../library/param.php');
require_once('../Log.php');

//echo "<center>EM MANUTENÇÃO!</center><br><br>";

Log::getInstance();
Log::setCaminho('../logs/importacao.txt');
if(!$_SESSION['logado'] || !isset($_SESSION['logado']) ){
	header("Location: ../index.php");
}

	$arquivo_valido = true;
	$param = new Param();

	if( $param->get('submit')->present() ){
	  $db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	  $cliente = gerar_dados_cliente($db, $_SESSION['cliente']);

	  $temp_path      = $_FILES['remessa']['tmp_name'];
	  $target_path    = DIR_TEMP."remessa_".$cliente["cliente"]."_".date( "YmdHis" ).".cnab";
	  $cabecalho      = array( "erros" => array(), "quantidade_erros" => 0, "dados" => array() );
	  $corpo          = array( "quantidade_sacados_cadastros" => 0, "quantidade_titulos_cadastros" => 0 );
	  $arquivo_valido = false;
	  $cabecalho["erros"]["SISTEMA"] = "";

	  if( $_FILES["remessa"]["error"] == UPLOAD_ERR_NO_FILE ) {
		$cabecalho["erros"]["SISTEMA"] .= "O Arquivo de Remessa deve ser informado";
		$cabecalho["quantidade_erros"]++ ;
		salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"] );
	  } elseif( $_FILES["remessa"]["error"] != UPLOAD_ERR_OK ) {
		$cabecalho["erros"]["SISTEMA"] .= "Não foi possivel ler o Arquivo de Remessa! O arquivo está corrompido ou não é válido";
		$cabecalho["quantidade_erros"]++ ;
		salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"] );
	  } else {
	
		if( false && ( !$finfo || substr( finfo_file( $finfo, $temp_path ), 0, 10 ) != "ASCII text" ) ) {
		  $cabecalho["erros"]["SISTEMA"] .= "Tipo de Arquivo não suportado! O Arquivo de Remessa deve conter somente texto";
		  $cabecalho["quantidade_erros"]++ ;
		  salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"] );
		} else {
		  if( file_exists( $target_path ) ){
			unlink($target_path); // remove o arquivo caso ele já exista
		  }
		  if( move_uploaded_file( $temp_path, $target_path ) == false ) {
			$cabecalho["erros"]["SISTEMA"] .= "Não foi possivel ler o Arquivo de Remessa! O Arquivo corrompeu-se ou não é válido";
			$cabecalho["quantidade_erros"]++ ;
			salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"] );
		  } else{
	 		//INSPECT exit('arquivo upado com sucesso');
			$cnab_remessa = fopen($target_path, "r");
			if( feof( $cnab_remessa ) ){
			  	$cabecalho["erros"]["SISTEMA"] .= "O Arquivo de Remessa está vázio";
			  	$cabecalho["quantidade_erros"]++ ;
			  	salvar_log_cnab( $db, $cliente, $cabecalho["erros"]["SISTEMA"] );
			} else {
				
				  //INEPECT exit('tem algo escrito');
				  unset($cabecalho["erros"]["SISTEMA"]);
				  $linha = ler_bloco( $cnab_remessa, 410 ); // solicitado a recuperação  mais de 400 caracteres para validação do arquivo

				  $cabecalho = validar_cabecalho_cnab_remesa( $linha ); // executa a validação do cabeçalho
	
				  $continuar_processando = $cabecalho["quantidade_erros"] == 0;
				  $arquivo_valido        = $cabecalho["quantidade_erros"] == 0;
				  $contador_linha = 1;
				  $mensagem["alert"] = "";
		
				  if( $continuar_processando == false ){
					salvar_log_cnab( $db, $cliente, $linha );
				  }
		
				  // fica no laço enquanto não for o fim do arquivo ou não encontar a identificação do registro '9'
				  while( $continuar_processando && !feof( $cnab_remessa ) ){
					$linha = ler_bloco( $cnab_remessa, 410 ); // solicitado a recuperação  mais de 400 caracteres para validação do arquivo
					//mysql_query("INSERT INTO sisa_debug (dados, responsavel) VALUES ('".$linha."', 'Sirot')");
					$contador_linha++;
					$corpo[$contador_linha] = validar_registro_cnab_remessa( $linha, $db, $cliente ); // executa a validação dos registros
		
					$continuar_processando = $corpo[$contador_linha]["dados"]["identificacao_registro"] != '9';
					$arquivo_valido        = $arquivo_valido && $corpo[$contador_linha]["quantidade_erros"] == 0;
					//INSPECT var_dump($corpo[$contador_linha]["quantidade_erros"]);exit();
					if( $arquivo_valido == false ){
					  salvar_log_cnab( $db, $cliente, $linha );
					} elseif( $continuar_processando == true ) {
					  // salvando o registro
					  // Importação dos clientes
					  $sacado_nome = trim($corpo[$contador_linha]["dados"]["sacado"]["nome"]);
					  $sacado_cpf_cnpj = "";
		
					  switch($corpo[$contador_linha]["dados"]["sequencia_44"]){
						case '01': // Colocando mascara de CPF
						  $sacado_cpf_cnpj = preg_replace("/^(\d{3})(\d{3})(\d{3})(\d{2})/", '$1.$2.$3-$4', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
						  $sacado_cpf_cnpj_sem_mascara = preg_replace("/^(\d{3})(\d{3})(\d{3})(\d{2})/", '$1$2$3$4', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
						break;
						case '02': // Colocando mascara de CNPJ
						  $sacado_cpf_cnpj = preg_replace("/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})\$/", '$1.$2.$3/$4-$5', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
						  $sacado_cpf_cnpj_sem_mascara = preg_replace("/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})\$/", '$1$2$3$4$5', $corpo[$contador_linha]["dados"]["numero_inscricao"]);
						break;
			  }

			  //Verificar se sacado já existe no sistema
			  $query_decla = $db->query(
											" SELECT
												sacados.sacado
											  FROM
												sacados
											  WHERE
												sacados.agencia = '".$cliente["agencia"]."'
											  AND
												sacados.conta = '".$cliente["conta"]."'
											  AND
												sacados.nome = '".mysql_real_escape_string($sacado_nome)."'
											  AND
												( sacados.cpf = '".$sacado_cpf_cnpj."' OR sacados.cpf = '".$sacado_cpf_cnpj_sem_mascara."' )
											  AND
												( sacados.grupo IS NULL OR sacados.grupo <> '99999')
											  LIMIT
												1;
											" );
											
											
											
				 //Inserir sacado caso não exista
				  if( $query_decla->num_rows == 0 ){
					$sacado_endereco = trim($corpo[$contador_linha]["dados"]["sacado"]["endereco"]);
					$sacado_bairro   = trim($corpo[$contador_linha]["dados"]["sacado"]["bairro"]);
					$sacado_cep      = preg_replace("/^(\d{5})(\d{3})\$/", '$1-$2', $corpo[$contador_linha]["dados"]["sacado"]["cep"]);
					$sacado_cidade   = trim($corpo[$contador_linha]["dados"]["sacado"]["cidade"]);
					$sacado_uf       = strtoupper($corpo[$contador_linha]["dados"]["sacado"]["uf"]);
					$query_decla = $db->query(
						" INSERT INTO
							sacados (
									  nome,
									  cpf,
									  endereco,
									  bairro,
									  cep,
									  cidade,
									  uf,
									  grupo,
									  cliente,
									  agencia,
									  conta
									)
						  VALUE (
								   \"".$sacado_nome."\",
								   \"".$sacado_cpf_cnpj."\",
								   \"".$sacado_endereco."\",
								   \"".$sacado_bairro."\",
								   \"".$sacado_cep."\",
								   \"".$sacado_cidade."\",
								   \"".$sacado_uf."\",
								   NULL,
								   \"".$cliente["cliente"]."\",
								   \"".$cliente["agencia"]."\",
								   \"".$cliente["conta"]."\"
								   );
					  " );
					$sacado_sacado = $db->getLastId();
					$corpo["quantidade_sacados_cadastros"]++;
				  } else {
					$sacado_sacado = $query_decla->row["sacado"];
				  }
	
				  // Importação dos titulos
				  $titulo_nossonumero = preg_replace("/^0(\d{10})\$/", '$1', $corpo[$contador_linha]["dados"]["nosso_numero"]);
				  Log::escreve($titulo_nossonumero, false);
				  $titulo_modelo = $cliente["modelo_padrao"];
				  $query_decla = $db->query(
									" SELECT
										titulos.nossonumero
									  FROM
										titulos
									  WHERE
										titulos.nossonumero = '".$titulo_nossonumero."'
									  LIMIT
										1;
								" );
				  if( $query_decla->num_rows > 0 ){
					$corpo[$contador_linha]["erros"]["10"] = "O Nosso Número já cadastrado!";
					$corpo[$contador_linha]["quantidade_erros"]++ ;
					salvar_log_cnab( $db, $cliente, $linha );
				  } else{
					$nosso_numero = ( esta_presente( $query_decla->row["nossonumero"] ) ? $query_decla->row['nossonumero'] : '00000000000' );
					if( preg_match( "/^0{10}\$/", $titulo_nossonumero ) == true ){ // gerando um nossonumero novo
					  $query_decla = $db->query(
										  " SELECT
											  clientes.cliente,
											  clientes.nossonumero,
											  clientes.boleto_padrao AS modelo
											FROM
											  clientes
											WHERE
											  cliente='".$cliente["cliente"]."'
											LIMIT
											  1;
									  ");
					  $nosso_numero  = (int)$query_decla->row["nossonumero"];
					  $titulo_modelo = ( empty($query_decla->row["modelo"]) ? '1' : (int)$query_decla->row["modelo"] );
	
					  do{
						$nosso_numero++;
						$titulo_nossonumero = add_zero_esquerda( $cliente["cliente"], 4 ).add_zero_esquerda( $nosso_numero , 6 );
						$query_decla = $db->query(
									  " SELECT
										  titulos.nossonumero
										FROM
										  titulos
										WHERE
										  titulos.nossonumero = '".$titulo_nossonumero."'
										LIMIT
										  1;
									  " );
					  }while( $query_decla->num_rows > 0 );
					}
	
					$titulo_seunumero       = mb_strtoupper( trim( $corpo[$contador_linha]["dados"]["controle_cliente"] ) );
					$titulo_documento       = mb_strtoupper( add_zero_esquerda( trim( $corpo[$contador_linha]["dados"]["numero_documento"] ), 8 ) );
					$titulo_data_emissao    = preg_replace( "/^(\d{2})(\d{2})(\d{2})\$/", '20$3-$2-$1', $corpo[$contador_linha]["dados"]["data_emissao"] );
					$titulo_data_vencimento = $corpo[$contador_linha]["dados"]["data_vencimento"];
					$titulo_data_vencimento = ( preg_match( "/^8{6}\$/", $titulo_data_vencimento) ? "'".$titulo_data_emissao."'" : ( preg_match( "/^9{6}\$/", $titulo_data_vencimento) ? "NULL" : preg_replace( "/^(\d{2})(\d{2})(\d{2})\$/", '\'20$3-$2-$1\'', $titulo_data_vencimento ) ) );
					$titulo_valor           = preg_replace( "/^0*(0|[1-9]\d*)(\d{2})\$/", '$1.$2', $corpo[$contador_linha]["dados"]["valor_titulo"] );
					$titulo_valor_desconto  = preg_replace( "/^0*(0|[1-9]\d*)(\d{2})\$/", '$1.$2', $corpo[$contador_linha]["dados"]["valor_desconto"] );
					$titulo_multa           = preg_replace( "/^(\d{2})(\d{4})\$/", '$1.$2', $corpo[$contador_linha]["dados"]["taxa_multa"] );
					$titulo_juros           = preg_replace( "/^(\d{2})(\d{4})\$/", '$1.$2', $corpo[$contador_linha]["dados"]["taxa_mora"] );
					$titulo_instrucao       = trim( $corpo[$contador_linha]["dados"]["observacoes"] );
					$id_grupo_parcela       = $cliente["cliente"].date("YmdHis");
					
					// Tratamento do dias de protesto
					switch($corpo[$contador_linha]["dados"]["opcao_protesto"]){
						case '00': $titulo_dias_protesto = 0; $tipo_protesto = 'útil';
							break;

						case '01': $titulo_dias_protesto = 0; $tipo_protesto = 'útil';
							break;

						case '03': $titulo_dias_protesto = 3; $tipo_protesto = 'útil';
							break;

						case '04': $titulo_dias_protesto = 4; $tipo_protesto = 'útil';
							break;

						case '05': $titulo_dias_protesto = 5; $tipo_protesto = 'útil';
							break;

						case '10': $titulo_dias_protesto = 10; $tipo_protesto = 'corrido';
							break;

						case '15': $titulo_dias_protesto = 15; $tipo_protesto = 'corrido';
							break;

						case '20': $titulo_dias_protesto = 20; $tipo_protesto = 'corrido';
							break;

						case '25': $titulo_dias_protesto = 25; $tipo_protesto = 'corrido';
							break;

						case '30': $titulo_dias_protesto = 30; $tipo_protesto = 'corrido';
							break;

						case '45': $titulo_dias_protesto = 45; $tipo_protesto = 'corrido';
							break;

						case '06': $titulo_dias_protesto = $corpo[$contador_linha]["dados"]["protesto"]; $tipo_protesto = 'corrido';
							break;

						case '07': $titulo_dias_protesto = 0; $tipo_protesto = 'útil';
							break;

						case '22': $titulo_dias_protesto = 0; $tipo_protesto = 'útil';
							break;
					}
					
					$sacador = 'NULL';
					
					//Caso em que existre um sacador/avalista para cadastrar
					if($corpo[$contador_linha]["dados"]["indicativo_sacador"] == "A"){
						$titulo_instrucao = "";
						//Verificar se sacador/avalista  exista
						$select = $db->query("SELECT 
												*
											 FROM 
											 	sacados 
											WHERE 
												cpf = '".$corpo[$contador_linha]["dados"]["cnpj_sacador"]."' AND
												cliente = '".$cliente["cliente"]."'
												");
						//Inserir sacador/avalista caso nao exista
						if($select->num_rows == 0){
							$insert_sacado = "INSERT INTO 
													sacados (
														agencia, 
														conta, 
														cliente, 
														nome, 
														cpf, 
														grupo
													)
												VALUES(
													'".$cliente["agencia"]."', 
													'".$cliente["conta"]."', 
													'".$cliente["cliente"]."', 
													'".$corpo[$contador_linha]["dados"]["nome_sacador"]."', 
													'".$corpo[$contador_linha]["dados"]["cnpj_sacador"]."', 
													'99999'
												)";

							$query = $db->query($insert_sacado);
						}

						$select = $db->query("SELECT 
												sacado
											 FROM 
												sacados 
											WHERE 
												cpf = '".$corpo[$contador_linha]["dados"]["cnpj_sacador"]."' AND
												cliente = '".$cliente["cliente"]."'
												");
							
							$sacador = "'".$select->row['sacado']."'";
						}

					$select_confere = "SELECT * FROM logs_cnab_remessa WHERE agencia = '" . $cliente["agencia"] . "' AND login = '" . $cliente["login"] . "' AND documento = '" . $titulo_documento . "' AND linha_processada = '" . mysql_real_escape_string($linha) . "'";
					
					$query = $db->query($select_confere);
					if( $query->num_rows == 0 ){
						salvar_log_cnab($db, $cliente, $linha, $titulo_documento);
						$insert = " INSERT INTO
										  titulos (
													agencia,
													cliente,
													sacado,
													documento,
													sequencia,
													nossonumero,
													seunumero,
													data_emisao,
													data_venc,
													valor,
													descontos,
													multa,
													juros,
													protesto,
													dias_protesto,
													instrucao,
													modelo,
													sacador,
													so_desconto,
													criacao, 
													status,
													cad_completo
												   )
										VALUE (
													'".$cliente["agencia"]."',
													'".$cliente["cliente"]."',
													'".$sacado_sacado."',
													'".$titulo_documento."',
													'1',
													'".$titulo_nossonumero."',
													'".$titulo_seunumero."',
													'".date("Y-m-d")."',
													".$titulo_data_vencimento.",
													'".$titulo_valor."',
													'".$titulo_valor_desconto."',
													'".$titulo_multa."',
													'".$titulo_juros."',
													'".$titulo_dias_protesto."',
													'".$tipo_protesto."',
													'".$titulo_instrucao."',
													'".$titulo_modelo."',
													".$sacador.",
													'N',
													NOW(), 
													'01',
													'N'
													);
									  ";
					
					$query = $db->query($insert);
					
					//var_dump($query);
					$titulo_titulo = $db->getLastId();
					$corpo["quantidade_titulos_cadastros"]++;
					$query_decla = $db->query(
										" UPDATE
											clientes
										  SET
											nossonumero = '".$nosso_numero."'
										  WHERE
											cliente = '".$cliente["cliente"]."';
										");
					}else{
						$corpo["quantidade_titulos_nao_cadastros"]++;
						$corpo["documentos_nao_cadastrados"][] = $titulo_documento;
					}
				  }
				}
			  }
			}
	
			fclose( $cnab_remessa ); // fecha a leitura do arquivo
			unlink($target_path);    // remove o arquivo após sua utilização
	
			if( $arquivo_valido == false ){
			  $mensagem["error"] = "Verifique as pendências";
			}
		  }
		}
	  }
	}
	Log::geraArquivo();
?>
<br />
<div id="div_arquivo">
  <label style="text-align:center">
  <h3>Importação CNAB 400</h3>
  </label>
  <form action="importacao.php" method="POST" name="form_importacao" enctype="multipart/form-data" >
    <p>
      <label for="remessa">Selecione o Arquivo de Remessa</label>
      <br/>
      <input id="remessa" name="remessa" type="file" size="42"class="submit_botao" />
    </p>
    <p>
      <input type="submit" name="submit" value="Processar arquivo" class="input_botao" />
      <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    </p>
  </form>
</div>
<div id="div_arquivo"<?php /*if ($_SESSION['cliente'] != 2202) echo' style="display: none"';*/ ?>>
  <label style="text-align:center">
  <h3>Exportação CNAB 400</h3>
  </label>
  <form action="exportacao.php" method="POST" name="form_exportacao" enctype="multipart/form-data" >
    <p>
      De: <input type="text" name="inicio" value="<?php echo date("d/m/Y", strtotime("-5 days")); ?>" />&nbsp;&nbsp;&nbsp;
      Até: <input type="text" name="fim" value="<?php echo date("d/m/Y"); ?>" />&nbsp;&nbsp;&nbsp;
      <input type="submit" name="submit" value="Processar arquivo" class="input_botao" />
    </p>
  </form>
</div>
<?php
// PARTE RESPONSAVEL POR ESCRITA DE ERRO OU SUCESSO
	if( $param->get('submit')->present() ){
		$odd_even = "odd";
		$yield .= "
	<div id=\"div_tabela_informacoes\"><br>
	<table id=\"tabela\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" >
	  <thead class=\"cabecalho\">
		  <th class=\"texto_ao_centro\" >
			LEIAUTE - REGISTRO
		  </th>
		  <th width=\"40%\"class=\"texto_ao_centro\" >
			IDENTIFICADOR
		  </th>
		  <th width=\"40%\" class=\"texto_ao_centro\" >
			DESCRIÇÃO
		  </th>
		</tr>
	  </tread>
	  <tbody>
		<tr class=\"valid_".$odd_even."\">
		  <td class=\"esquerda texto_ao_centro esquerda_topo verde_tabela\" >
			DETALHE - Registro
		  </td>
		  <td class=\"texto_ao_centro\" >
		  </td>
		  <td class=\"direita texto_justificado direita_topo verde_tabela\" >
			".( $corpo["quantidade_sacados_cadastros"] <= 0 ? "- Não foi importado nenhum cliente" : ( $corpo["quantidade_sacados_cadastros"] == 1 ? "- Foi importado ".$corpo["quantidade_titulos_cadastros"]." Cliente" : "- Foram importados ".$corpo["quantidade_sacados_cadastros"]." Clientes" ) )."
		  </td>
		</tr>
		<tr class=\"valid_".($odd_even = ($odd_even == "odd" ? "even" : "odd"))."\">
		  <td class=\"esquerda texto_ao_centro\" >
		  </td>
		  <td class=\"texto_ao_centro\" >
		  </td>
		  <td class=\"direita texto_justificado verde_tabela\" >
			".( $corpo["quantidade_titulos_cadastros"] <= 0 ? "- Não foi importado nenhum título de cobrança" : ( $corpo["quantidade_titulos_cadastros"] == 1 ? "- Foi importado ".$corpo["quantidade_titulos_cadastros"]." Título de cobrança" : "- Foram importados ".$corpo["quantidade_titulos_cadastros"]." Títulos de cobrança" ) )."
		  </td>
		</tr>
	";
	
	if($corpo["quantidade_titulos_nao_cadastros"] > 0){
		$yield .= "<tr class=\"error_".($odd_even = ($odd_even == "odd" ? "even" : "odd"))."\">
		  <td class=\"esquerda texto_ao_centro\" >
		  </td>
		  <td class=\"texto_ao_centro\" >
		   - ".$corpo["quantidade_titulos_nao_cadastros"].( $corpo["quantidade_titulos_nao_cadastros"] > 1 ? " TÍTULOS NÃO FORAM IMPORTADOS" : "  TÍTULO NÃO FOI IMPORTADO " )." <br /> pois o número do documento já consta anteriormente na importação
		  </td>
		  <td class=\"direita texto_justificado verde_tabela\" >
			 Número dos documentos: <br />";
			 $cont = 1;
			 foreach($corpo["documentos_nao_cadastrados"] as $documento){
			 	$yield .= $documento."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".($cont++ % 4 == 0 ? "<br />": "");
			 }
		$yield .=" </td>
			</tr>";
	}
	
	  if( $cabecalho["quantidade_erros"] > 0 ){
		$exibir = true;
		$odd_even = ($odd_even == "odd" ? "even" : "odd");
		foreach(  $cabecalho["erros"] as $sequencia => $descricao ){
		  $yield .= "
		<tr class=\"error_".$odd_even."\">
		  <td class=\"esquerda texto_ao_centro esquerda_base vermelho_tabela\" >
			".( $exibir ? "HEADER" : "" )."
		  </td>
		  <td class=\"texto_ao_centro\" >
			".( $sequencia != "SISTEMA" ? $sequencia : "" )."
		  </td>
		  <td class=\"direita texto_justificado direita_base vermelho_tabela\" >
			- ".$descricao."
		  </td>
		</tr>
	";
		  $exibir = false;
		}
	  }
	
	  foreach( $corpo as $linha => $detalhes ){
		if( $detalhes["quantidade_erros"] > 0 ){
		  $odd_even = ($odd_even == "odd" ? "even" : "odd");
		  $exibir = true;
		  foreach( $detalhes["erros"] as $sequencia => $descricao ){
			$yield .= "
		<tr class=\"error_".$odd_even."\">
		  <td class=\"esquerda texto_ao_centro vermelho_tabela\" >
			".( $exibir ? "DETALHE - Registro ".$linha : "" )."
		  </td>
		  <td class=\"texto_ao_centro vermelho_tabela\" >
			".( $sequencia != "SISTEMA" ? "Sequência ".$sequencia : "" )."
		  </td>
		  <td class=\"direita texto_justificado vermelho_tabela\" >
			- ".$descricao."
		  </td>
		</tr>
	";
			$exibir = false;
		  }
		}
	  }
	  $yield .= "
	  </tbody>
	</table>
	<div>
	";
	echo $yield;
	}
?>
</body>
</html>