<?php
class Text {
    private $text;
    private $encode;

    function __construct($text = "", $encode = "UTF-8") {
        $this->encode($encode);
        $this->set($text);
    }

    function __destruct() {
        $this->set(null);
    }

    public function truncate($length = 80, $omission = '...') {
        $text = $this->substr(0, $length - strlen($omission));
        if($text->length() == ($length - strlen($omission))){
            $text->rappend($omission);
        }
        return $text;
    }

    public function lpad($length , $text_pad) {
        $text = str_pad($this->get(), $length, $text_pad, STR_PAD_LEFT);
        return $this->set($text);
    }

    public function rpad($length , $text_pad) {
        $text = str_pad($this->get(), $length, $text_pad, STR_PAD_RIGHT);
        return $this->set($text);
    }

    public function pad($length , $text_pad) {
        $text = str_pad($this->get(), $length, $text_pad, STR_PAD_BOTH);
        return $this->set($text);
    }

    public function ltrim() {
        return $this->set(ltrim($this->get()));
    }

    public function rtrim() {
        return $this->set(rtrim($this->get()));
    }

    public function trim() {
        return $this->set(trim($this->get()));
    }

    public function lappend($text) {
        return $this->set($text.$this->get());
    }

    public function rappend($text) {
        return $this->set($this->get().$text);
    }

    public function onlyNumber() {
        return new Text(preg_replace( "/\D/", '', $this->get()));
    }

    public function match($expression) {
        return preg_match( $expression, $this->get());
    }

    public function substr($start = 0, $length = 1) {
        return new Text(substr($this->get(), $start, $length));
    }

    public function length() {
        return strlen($this->get());
    }

    public function set($text) {
        if($this->encode == "UTF-8"){
            $this->text = ( mb_check_encoding($text, "UTF-8") ? $text : utf8_encode($text) );
        } else {
            $this->text = ( mb_check_encoding($text, "UTF-8") ? utf8_decode($text) : $text );
        }
        return $this;
    }

    public function encode($encode) {
        $this->encode = $encode;
        return $this;
    }

    private function get() {
        return $this->text;
    }

    public function toFile() {
        return $this->get();
    }

    public function toDB() {
        $str = addslashes($this->get());
        return $str;
    }

    public function toHtml() {
        $str = strtr($this->get(), array('&' => '&amp;', '>' => '&gt;', '<' => '&lt;', '"' => '&quot;'));
        return $str;
    }

    public function __toString(){
        return $this->get();
    }

    public function present() {
        return ($this->blank() == false);
    }

    public function null() {
        return is_null($this->get());
    }

    public function blank() {
        $val = $this->get();
        $val = $this->null() || empty($val);
        return $val;
    }
}
?>
