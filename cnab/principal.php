<div id="section" class="qua">
	<div class="titulo">
        <h2>INTEGRAÇÃO CNAB 400</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    <?php if($_SESSION['userAgencia'] != 5631):?>
		<form readonly='readonly' action="cnab/actions/importacao.php" target="_blank" method="POST" name="form_importacao" enctype="multipart/form-data" >
        	<fieldset >
                <legend>IMPORTAÇÃO CNAB 400</legend>
                <label for="remessa"><b>Selecione o arquivo de remessa:</b></label>
                <br />
                <br />
                <p>
                	<input id="remessa" name="remessa" type="file" size="42"class="submit_botao" />
                    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                </p>
            	<input type="submit" name="submit" value="Processar arquivo" class="botao margins dir" />
        	</fieldset>
       	</form>
       <?php endif; ?>
        <form action="cnab/actions/exportacao.php" target="_blank" method="POST" name="form_exportacao" enctype="multipart/form-data" >
        	<fieldset>
              <legend>EXPORTAÇÃO CNAB 400</legend>
                <table>
                	<tr>
                   	  	<td width="10%"><label for="inicio">De:</label></td>
                        <td width="40%"><input name="inicio" type="text" id="inicio" size="7" maxlength="10" value="<?php echo date("d/m/Y", strtotime("-5 days")); ?>" /></td>
                        <td width="10%"><label for="fim">Até:</label></td>
                        <td width="40%"><input name="fim" type="text" id="fim" size="7" maxlength="10" value="<?php echo date("d/m/Y"); ?>" /></td>
                    </tr>
                </table>
            	<input type="submit" name="submit" value="Processar arquivo" class="botao margins dir" />
        	</fieldset>   
       	</form>
	</div>
</div>