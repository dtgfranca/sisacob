<?php

function to_sql_date($date)
{
	$array = explode('/', $date);
	return $array[2].'-'.$array[1].'-'.$array[0];
}

function to_sql_decimal($number)
{
	if (strstr($number, '.'))
		$number = str_replace('.', '', $number);

	if (strstr($number, ','))
		$number = str_replace(',', '.', $number);

	return $number;
}

?>