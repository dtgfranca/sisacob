<?php
	include('config.php');

$_SG['conectaServidor'] = true;
$_SG['abreSessao'] = true;
$_SG['validaSempre'] = true;

if ($_SG['abreSessao'] == true) {
session_start();
}

/**
* Função que valida um usuário e senha
*
* @param string $usuario - O usuário a ser validado
* @param string $senha - A senha a ser validada
*
* @return bool - Se o usuário foi validado ou não (true/false)
*/
function valida($agencia, $cliente, $conta, $senha) {

	global $_SG;	
	$senha = strtolower($senha);
	
	mysql_query("SET NAMES UTF8") or die(mysql_error());
	
	// verifica se o usuário é master
	if(preg_match('~^[^\W_]+/[^\W_]+$~', $cliente)){
		$cliente = explode('/',$cliente);
		if($cliente[1] == "Master" || $cliente[1] == "master"){
			$usuario = $cliente[0];
			$sql = "SELECT cliente FROM clientes WHERE agencia='$agencia' AND usuario='Master' AND senha='$senha'";
			$query = mysql_query($sql) or die(mysql_error());
			$result = mysql_fetch_array($query);
			
			if ($result[0]<=0){
				return false;
			}
			else {
				$sql2 = "SELECT cliente, razao, ip, DATE_FORMAT(data_conexao,'%d/%m/%y às %l:%i %p') AS data_conexao FROM clientes WHERE agencia='$agencia' AND usuario='$usuario' AND conta LIKE '%$conta'";
				$query2 = mysql_query($sql2) or die(mysql_error());
				$result2 = mysql_fetch_array($query2);
				$codclien = $result2['cliente'];
				$novo_ip = $_SERVER['REMOTE_ADDR'];
				
				$at = "UPDATE clientes SET data_conexao=NOW(), ip='$novo_ip' WHERE cliente='$codclien'";
				mysql_query($at) or die(mysql_error());
				
				$_SESSION['userAgencia'] = $agencia;
				$_SESSION['userCliente'] = $result2['cliente'];			
				$_SESSION['userNome'] = $result2['razao'].'<b><font color="#f00">/MASTER</font></b>';			
				$_SESSION['userConec'] = $result2['data_conexao'];			
				$_SESSION['userIp'] = $result2['ip'];
				$_SESSION['userConta'] = $conta;
				$_SESSION['logado'] = true;
				$_SESSION['userMaster'] = true;
				if ($_SG['validaSempre'] == true) {
					$_SESSION['userLogin'] = $cliente;
					$_SESSION['userSenha'] = $senha;
				}	
				return true;
			}
		}
		else{
			$sql = "SELECT cliente, razao, ip, DATE_FORMAT(data_conexao,'%d/%m/%y às %l:%i %p') AS data_conexao FROM clientes WHERE agencia='$agencia' AND usuario='$cliente' AND conta LIKE '%$conta' AND senha='$senha'";
			$query = mysql_query($sql) or die(mysql_error());
			$result = mysql_fetch_array($query);
			
			if ($result[0]<=0){
				return false;
			}
			else {
				$codclien = $result['cliente'];
				$novo_ip = $_SERVER['REMOTE_ADDR'];
				$at = "UPDATE clientes SET data_conexao=NOW(), ip='$novo_ip' WHERE cliente='$codclien'";
				mysql_query($at) or die(mysql_error());
				
				$_SESSION['userAgencia'] = $agencia;
				$_SESSION['userCliente'] = $result['cliente'];			
				$_SESSION['userNome'] = $result['razao'];			
				$_SESSION['userConec'] = $result['data_conexao'];			
				$_SESSION['userIp'] = $result['ip'];
				$_SESSION['userConta'] = $conta;
				$_SESSION['logado'] = true;
				$_SESSION['userMaster'] = false;
				if ($_SG['validaSempre'] == true) {
					$_SESSION['userLogin'] = $cliente;
					$_SESSION['userSenha'] = $senha;
				}	
				return true;
			}
		}
	}
	else {
		// Monta uma consulta SQL (query) para procurar um usuário
		$sql = "SELECT cliente, razao, ip, DATE_FORMAT(data_conexao,'%d/%m/%y às %l:%i %p') AS data_conexao FROM clientes WHERE agencia='$agencia' AND usuario='$cliente' AND conta LIKE '%$conta' AND senha='$senha'";
		$query = mysql_query($sql) or die(mysql_error());
		$result = mysql_fetch_array($query);
		
		if ($result[0]<=0){
			return false;
		}
		else {
			$codclien = $result['cliente'];
			$novo_ip = $_SERVER['REMOTE_ADDR'];
			$at = "UPDATE clientes SET data_conexao=NOW(), ip='$novo_ip' WHERE cliente='$codclien'";
			mysql_query($at) or die(mysql_error());
			
			$_SESSION['userAgencia'] = $agencia;
			$_SESSION['userCliente'] = $result['cliente'];			
			$_SESSION['userNome'] = $result['razao'];			
			$_SESSION['userConec'] = $result['data_conexao'];			
			$_SESSION['userIp'] = $result['ip'];
			$_SESSION['userConta'] = $conta;
			$_SESSION['logado'] = true;
			$_SESSION['userMaster'] = false;
			if ($_SG['validaSempre'] == true) {
				$_SESSION['userLogin'] = $cliente;
				$_SESSION['userSenha'] = $senha;
			}	
			return true;
		}

	}

// Monta uma consulta SQL (query) para procurar um usuário
/*$sql = "SELECT snh_cod, snh_nome, COALESCE(snh.nvl_opcoes, per.nvl_opcoes) AS nvl_opcoes, snh_perfil, snh_setor FROM sisa_use AS snh LEFT JOIN sisa_per as per ON snh.prf_cod=per.prf_cod WHERE snh_cod='$usuario' OR snh_nome='$usuario' AND UCASE(DES_DECRYPT(snh_novasenha, 'SKILLNET'))='$senha'";
$query = mysql_query($sql);
$result = mysql_fetch_array($query);*/

// Monta uma consulta SQL (query) para verificar a permissão
/*$sql2 = "SELECT locate('01-','$result[nvl_opcoes]')";
$query2 = mysql_query($sql2) or die(mysql_error());
$resultado = mysql_fetch_array($query2);

if ($resultado[0]<=0) {
return false;
}
else {
	$_SESSION['userCliente'] = $result['snh_cod'];
	$_SESSION['userNome'] = $result['snh_nome'];

	if ($_SG['validaSempre'] == true) {
		$_SESSION['userLogin'] = $usuario;
		$_SESSION['userSenha'] = $senha;
	}

	return true;
}*/
}

/**
* Função que protege uma página
*/ 
function protege() {
	global $_SG;
	
	if (!isset($_SESSION['userCliente']) OR !isset($_SESSION['userNome'])) {
		expulsa();
	} 
	else if (!isset($_SESSION['userCliente']) OR !isset($_SESSION['userNome'])) {
		if ($_SG['validaSempre'] == true) {
			if (!valida($_SESSION['userLogin'], $_SESSION['userSenha'])) {
				expulsa();
			}
		}
	}
	/*if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
		session_unset();     // unset $_SESSION variable for the run-time 
		session_destroy();   // destroy session data in storage
		expira();
	}
	$_SESSION['LAST_ACTIVITY'] = time();*/
}

/**
* Função para expulsar um visitante
*/ 
function expulsa() {
global $_SG;
unset($_SESSION['userAgencia'], $_SESSION['userCliente'], $_SESSION['userNome'], $_SESSION['userConec'], $_SESSION['userIp'], $_SESSION['userConta'], $_SESSION['userLogin'], $_SESSION['userSenha']);
header('Location: login.php');
} 

/**
* Função para expulsar um visitante
*/ 
function errolog() {
global $_SG;
unset($_SESSION['userAgencia'], $_SESSION['userCliente'], $_SESSION['userNome'], $_SESSION['userConec'], $_SESSION['userIp'], $_SESSION['userConta'], $_SESSION['userLogin'], $_SESSION['userSenha']);
header('Location: login.php?res=0');
} 

/**
* Função para expulsar um visitante
*/ 
function expira() {
global $_SG;
unset($_SESSION['userAgencia'], $_SESSION['userCliente'], $_SESSION['userNome'], $_SESSION['userConec'], $_SESSION['userIp'], $_SESSION['userConta'], $_SESSION['userLogin'], $_SESSION['userSenha']);
header('Location: login.php?res=1');
} 

?>