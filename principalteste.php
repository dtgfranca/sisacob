<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

include 'config.php';
session_start();
$cooperativa = parse_ini_file("cooperativas.ini");

/*if (!in_array($_SERVER['REMOTE_ADDR'], array('X201.16.252.81', 'X179.192.148.5')) && $_SESSION['userAgencia'] != "4097")
die("<span style='font-size: 24px; color: red'>O SisaCob está offline para manutenção até 23:59 do dia 20/01/2014. Pedimos desculpas pelo inconveniente.</span>");*/

if (empty($_SESSION['userAgencia'])) {
	header('location: login_mini.php');
}
$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

$avencer = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 15, date("Y")));

mysql_query("SET NAMES UTF8") or die(mysql_error());
$sPrevA = "SELECT COUNT(data_venc) as prev
				FROM titulos AS t, sacados AS s
				WHERE t.cliente='$cliente'
				AND s.sacado=t.sacado
				AND data_venc>=CURDATE()
				AND data_venc<='$avencer'
				AND data_baixa_manual IS NULL
				AND data_baixa IS NULL
				AND cancelamento IS NULL
				AND t.cad_completo = 'S'";
$qPrevA = mysql_query($sPrevA) or die(mysql_error());
$aPrevA = mysql_fetch_array($qPrevA);
if ($aPrevA[0] == '0') {
	$numPrev = '0';
} else if ($aPrevA[0] == '1') {
	$numPrev = '1 título';
} else {
	$numPrev = $aPrevA[0] . ' títulos';
}

$sPrevB = "SELECT COUNT(data_venc) as atraso
				FROM titulos AS t, sacados AS s
				WHERE t.cliente='$cliente'
				AND s.sacado=t.sacado
				AND data_venc<CURDATE()
				AND data_baixa IS NULL
				AND cancelamento IS NULL
				AND status != '03'
				AND data_baixa_manual IS NULL
				AND t.cad_completo = 'S'";
$qPrevB = mysql_query($sPrevB) or die(mysql_error());
$aPrevB = mysql_fetch_array($qPrevB);
if ($aPrevB[0] == '0') {
	$numAtr = 'Nenhum título';
} else if ($aPrevB[0] == '1') {
	$numAtr = '1 título';
} else {
	$numAtr = $aPrevB[0] . ' títulos';
}

$qPrev = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t, sacados AS s
			WHERE t.cliente='$cliente'
			AND s.sacado=t.sacado
			AND data_venc>=CURDATE()
			AND data_venc<='$avencer'
			AND data_baixa_manual IS NULL
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND t.cad_completo = 'S'
			ORDER BY t.data_venc DESC LIMIT 50";
$sPrev = mysql_query($qPrev) or die(mysql_error());

$qAtr = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t, sacados AS s
			WHERE t.cliente='$cliente'
			AND s.sacado=t.sacado
			AND data_venc <  CURDATE()
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND status != '03'
			AND data_baixa_manual IS NULL
			AND t.cad_completo = 'S'
			 AND cancelamento IS NULL
			ORDER BY t.data_venc LIMIT 50";
$sAtr = mysql_query($qAtr) or die(mysql_error());

$qBaix =  "SELECT COALESCE(DATE_FORMAT(`data_baixa_manual`,'%d/%m/%Y'),'-')AS data_baixa_manual,COALESCE(DATE_FORMAT(data_baixa, '%d/%c/%Y'),'-') AS data_baixa, s.nome, t.valor
			FROM titulos AS t, sacados AS s
			WHERE t.cliente='$cliente'
			AND s.sacado=t.sacado
			AND (data_baixa  BETWEEN CURDATE() - INTERVAL 15 DAY  AND CURDATE() OR data_baixa_manual  BETWEEN CURDATE() - INTERVAL 15 DAY  AND CURDATE())
			AND (data_baixa IS NOT NULL OR data_baixa_manual IS NOT NULL)
			ORDER BY t.data_venc LIMIT 50";

$sBaix = mysql_query($qBaix) or die(mysql_error());

$qRej = "SELECT s.nome, t.nossonumero, t.valor, DATE_FORMAT(t.criacao, '%d/%c/%Y') as data
			FROM titulos t, sacados s
			WHERE t.sacado = s.sacado
      		AND t.cliente = '$cliente'
      		AND t.status = '03'
      		AND t.cancelamento is null
      		AND t.data_baixa is null
      		AND t.data_baixa_manual is null
      		AND t.criacao > DATE_SUB(CURDATE(), INTERVAL 15 DAY)
			AND t.cad_completo = 'N'
     		ORDER BY t.criacao DESC";
$sRej = mysql_query($qRej) or die(mysql_error());
$numRej = mysql_num_rows($sRej);
//AND t.desconto is null - tirada da função acima

$qIRej = "SELECT s.nome, t.nossonumero, t.valor, DATE_FORMAT(i.data_instr, '%d/%c/%Y') AS data
			FROM titulos t, sacados s, cad_instrucoes i
			WHERE i.sacado = s.sacado
     		AND i.boleto = t.titulo
     		AND i.cliente = '$cliente'
     		AND i.status = '03'
     		AND i.data_instr > DATE_SUB(CURDATE(), INTERVAL 15 DAY)
			AND t.cad_completo = 'S'
 			ORDER BY i.data_instr DESC";
$sIRej = mysql_query($qIRej) or die(mysql_error());
$numIRej = mysql_num_rows($sIRej);

?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#section').mede2();
		var TRej = "<?php echo $numRej;?>";
		var IRej = "<?php echo $numIRej;?>";
		if(TRej > 0 || IRej > 0){
			$("#section").removeAttr('class');
			$("#section").attr('class','largo');
			$("#previsao").removeAttr('class');
			$("#atraso").removeAttr('class');
			$("#previsao").attr('class','terca esq');
			$("#atraso").attr('class','terca esq');
		}
		if(TRej > 0){
			$("#tit_rej").removeAttr('style');
		}
		if(IRej > 0){
			$("#ins_rej").removeAttr('style');
		}
		$("#pes_prev").change(function(){
			var pesquisa = document.getElementById('pes_prev').value;
			if(pesquisa == 'periodo'){
				$("#busca_prev #per").removeAttr('style');
				$("#busca_prev #bnome").removeAttr('style');
				$("#busca_prev #pesq").removeAttr('style');
				$("#busca_prev #inicio").removeAttr('onkeypress');
				$("#busca_prev #fim").removeAttr('onkeypress');
				$("#busca_prev #inicio").val('');
				$("#busca_prev #fim").val('');
				$("#busca_prev #bnome").attr('style','display:none');
				$("#busca_prev #pesq").attr('class','botao top dir');
				$("#busca_prev #inicio").attr('onkeypress','formataCampo(this, "00/00/0000", event)');
				$("#busca_prev #fim").attr('onkeypress','formataCampo(this, "00/00/0000", event)');
				$("#busca_prev #pesq").attr('onclick','pesq_prev()');
				$("#busca_prev #inicio").focus();
			}
			else if(pesquisa == 'valor'){
				$("#busca_prev #per").removeAttr('style');
				$("#busca_prev #bnome").removeAttr('style');
				$("#busca_prev #pesq").removeAttr('style');
				$("#busca_prev #inicio").removeAttr('onkeypress');
				$("#busca_prev #fim").removeAttr('onkeypress');
				$("#busca_prev #inicio").val('');
				$("#busca_prev #fim").val('');
				$("#busca_prev #bnome").attr('style','display:none');
				$("#busca_prev #pesq").attr('class','botao top dir');
				$("#busca_prev #inicio").attr('onkeypress','FormataValor(this,event,17,2)');
				$("#busca_prev #fim").attr('onkeypress','FormataValor(this,event,17,2)');
				$("#busca_prev #pesq").attr('onclick','pesq_prev()');
				$("#busca_prev #inicio").focus();
			}
			else if(pesquisa == 'nome'){
				$("#busca_prev #bnome").removeAttr('style');
				$("#busca_prev #per").removeAttr('style');
				$("#busca_prev #pesq").removeAttr('style');
				$("#busca_prev #inicio").removeAttr('onkeypress');
				$("#busca_prev #fim").removeAttr('onkeypress');
				$("#busca_prev #per").attr('style','display:none');
				$("#busca_prev #pesq").attr('class','botao top dir');
				$("#busca_prev #pesq").attr('onclick','pesq_prev()');
				$("#busca_prev #nome").focus();
			}
			else {
				$("#busca_prev #per").removeAttr('style');
				$("#busca_prev #bnome").removeAttr('style');
				$("#busca_prev #pesq").removeAttr('style');
				$("#busca_prev #per").attr('style','display:none');
				$("#busca_prev #bnome").attr('style','display:none');
				$("#busca_prev #pesq").attr('class','bdesab top dir');
			}
		});
		$("#pes_atr").change(function(){
			var pesquisa = document.getElementById('pes_atr').value;

			if(pesquisa == 'periodo'){
				$("#busca_atr #per").removeAttr('style');
				$("#busca_atr #bnome").removeAttr('style');
				$("#busca_atr #pesq").removeAttr('style');
				$("#busca_atr #inicio1").removeAttr('onkeypress');
				$("#busca_atr #fim1").removeAttr('onkeypress');
				$("#busca_atr #inicio1").val('');
				$("#busca_atr #fim1").val('');
				$("#busca_atr #bnome").attr('style','display:none');
				$("#busca_atr #pesq").attr('class','botao top dir');
				$("#busca_atr #inicio1").attr('onkeypress','formataCampo(this, "00/00/0000", event)');
				$("#busca_atr #fim1").attr('onkeypress','formataCampo(this, "00/00/0000", event)');
				$("#busca_atr #pesq").attr('onclick','pesq_atr()');
				$("#busca_atr #inicio1").focus();
			}
			else if(pesquisa == 'valor'){
				$("#busca_atr #per").removeAttr('style');
				$("#busca_atr #bnome").removeAttr('style');
				$("#busca_atr #pesq").removeAttr('style');
				$("#busca_atr #inicio1").removeAttr('onkeypress');
				$("#busca_atr #fim1").removeAttr('onkeypress');
				$("#busca_atr #inicio1").val('');
				$("#busca_atr #fim1").val('');
				$("#busca_atr #bnome").attr('style','display:none');
				$("#busca_atr #pesq").attr('class','botao top dir');
				$("#busca_atr #inicio1").attr('onkeypress','FormataValor(this,event,17,2)');
				$("#busca_atr #fim1").attr('onkeypress','FormataValor(this,event,17,2)');
				$("#busca_atr #pesq").attr('onclick','pesq_atr()');
				$("#busca_atr #inicio1").focus();
			}
			else if(pesquisa == 'nome'){
				$("#busca_atr #bnome").removeAttr('style');
				$("#busca_atr #per").removeAttr('style');
				$("#busca_atr #pesq").removeAttr('style');
				$("#busca_atr #inicio1").removeAttr('onkeypress');
				$("#busca_atr #fim1").removeAttr('onkeypress');
				$("#busca_atr #per").attr('style','display:none');
				$("#busca_atr #pesq").attr('class','botao top dir');
				$("#busca_atr #pesq").attr('onclick','pesq_atr()');
				$("#busca_atr #nome1").focus();
			}
			else {
				$("#busca_atr #per").removeAttr('style');
				$("#busca_atr #bnome").removeAttr('style');
				$("#busca_atr #pesq").removeAttr('style');
				$("#busca_atr #per").attr('style','display:none');
				$("#busca_atr #bnome").attr('style','display:none');
				$("#busca_atr #pesq").attr('class','bdesab top dir');
			}
		});
		/*$("#busca_prev").hide();
		$('#mostra_prev').click(function() {
			$('#busca_prev').slideToggle('fast', function() {
		  	});
		});
		$("#busca_atr").hide();
		$('#mostra_atr').click(function() {
			$('#busca_atr').slideToggle('fast', function() {
		  	});
		});*/
		$("#busca_prev").hide();
		$('#mostra_prev').click(function() {
			$('#busca_prev').slideToggle('fast', function() {
				$('#previsao div form #busca2').css('min-height','80px');
		  	});
		});
		/*$('#mostra_prev').click(function() {
			$('#busca_prev').slideToggle('fast', function() {
				$('#previsao div form #busca2').css('min-height','200px');
				if($('#busca_prev').css('display') == 'none') {
					$("#previsao div form #busca2").css("max-height", $("#previsao div form #busca2").height() + $("#previsao div form #busca_prev").height() + 5);
				}
				else{
					$("#previsao div form #busca2").css("max-height", $("#previsao div form #busca2").height() - $("#previsao div form #busca_prev").height() - 5);
				}
		  	});
		});*/
		$("#busca_atr").hide();
		$('#mostra_atr').click(function() {
			$('#busca_atr').slideToggle('fast', function() {
				$('#previsao div form #busca2').css('min-height','200px');
		  	});
		});
		/*$('#mostra_atr').click(function() {
			$('#busca_atr').slideToggle('fast', function() {
				$('#previsao div form #busca2').css('min-height','200px');
				if($('#busca_atr').css('display') == 'none'){
					$("#atraso div form #busca2").css("max-height", $("#atraso div form #busca2").height() + $("#atraso div form #busca_atr").height() + 5);
				}
				else{
					$("#atraso div form #busca2").css("max-height", $("#atraso div form #busca2").height() - $("#atraso div form #busca_atr").height() - 5);
				}
		  	});
		});*/
	});
	function foc(campo){
		var len = campo.value.length;
		if(len == 10){
			$("#busca_prev #fim").focus();
		}
	}
	function foc1(campo){
		var len = campo.value.length;
		if(len == 10){
			$("#busca_atr #fim1").focus();
		}
	}
	function pesq_prev(){
		var pesquisa = document.getElementById('pes_prev').value;
		if(pesquisa == 'periodo'){
			var inicio = document.getElementById('inicio').value;
			var fim = document.getElementById('fim').value;
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_prev_per&inicio="+inicio+"&fim="+fim,
				beforeSend: function() {
					$("#previsao div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#previsao div form div#busca2").html(retorno);
				}
			});
		}
		else if(pesquisa == 'valor'){
			var inicio = document.getElementById('inicio').value;
			var fim = document.getElementById('fim').value;
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_prev_val&inicio="+inicio+"&fim="+fim,
				beforeSend: function() {
					$("#previsao div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#previsao div form div#busca2").html(retorno);
				}
			});
		}
		else if(pesquisa == 'nome'){
			var nome = document.getElementById('nome').value;
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_prev_nome&nome="+nome,
				beforeSend: function() {
					$("#previsao div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#previsao div form div#busca2").html(retorno);
				}
			});
		}
	}
	function pesq_atr(){
		var pesquisa = document.getElementById('pes_atr').value;
		if(pesquisa == 'periodo'){
			var inicio = document.getElementById('inicio1').value;
			var fim = document.getElementById('fim1').value;
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_atr_per&inicio="+inicio+"&fim="+fim,
				beforeSend: function() {
					$("#atraso div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#atraso div form div#busca2").html(retorno);
				}
			});
		}
		else if(pesquisa == 'valor'){
			var inicio = document.getElementById('inicio1').value;
			var fim = document.getElementById('fim1').value;
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_atr_val&inicio="+inicio+"&fim="+fim,
				beforeSend: function() {
					$("#atraso div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#atraso div form div#busca2").html(retorno);
				}
			});
		}
		else if(pesquisa == 'nome'){
			var nome = document.getElementById('nome1').value;
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_atr_nome&nome="+nome,
				beforeSend: function() {
					$("#atraso div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#atraso div form div#busca2").html(retorno);
				}
			});
		}
		else{
			$.ajax({
				type: 'GET',
				url: "scripts/funcoes.php",
				data: "funcao=pes_atr_nome&nome=",
				beforeSend: function() {
					$("#atraso div form div#busca2").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
				},
				success: function(retorno){
					$("#atraso div form div#busca2").html(retorno);
				}
			});
		}
	}
</script>
<?php if(!in_array($_SESSION['userAgencia'],$cooperativa)) {?>
<div id="section" class="max">
	<div class="titulo">
        <h2>VISÃO GERAL</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    	<fieldset>
        	<div id="tit_rej" class="terca esq" style="display:none">
            	<div id="trej" class="minmeio">
                	<div class="divtitulo" style="background: #ff9999; border-bottom: solid 1px #ff3333">
                    	Títulos Rejeitados nos Últimos 15 Dias
                    </div>
                    <table>
                    	<tr class="cinza">
                        	<td class="destaque borda centro">Sacado</td>
                            <td class="destaque borda centro" width="20%">Emissão</td>
                            <td class="destaque borda centro" width="25%">Nossonúmero</td>
                            <td class="destaque borda centro" width="15%">Valor</td>
                      	</tr>
                  	<?php while ($aRej = mysql_fetch_array($sRej)) {
	echo '
							<tr>
								<td class="borda centro">' . $aRej['nome'] . '</td>
								<td class="borda centro">' . $aRej['data'] . '</td>
								<td class="borda centro">' . $aRej['nossonumero'] . '</td>
								<td class="borda centro">R$ ' . number_format($aRej['valor'], 2, ',', '.') . '</td>
							</tr>
							';
}
?>
                  	</table>
                </div>
            </div>
            <div id="ins_rej" class="terca esq" style="display:none">
            	<div id="irej" class="minmeio">
                	<div class="divtitulo" style="background: #ff9999; border-bottom: solid 1px #ff3333">
                    	Instruções Rejeitadas nos Últimos 15 Dias
                    </div>
                    <table>
                    	<tr class="cinza">
                        	<td class="destaque borda centro">Sacado</td>
                            <td class="destaque borda centro" width="20%">Emissão</td>
                            <td class="destaque borda centro" width="25%">Nossonúmero</td>
                            <td class="destaque borda centro" width="15%">Valor</td>
                      	</tr>
                  	<?php while ($aIRej = mysql_fetch_array($sIRej)) {
	echo '
							<tr>
								<td class="borda centro">' . $aIRej['nome'] . '</td>
								<td class="borda centro">' . $aIRej['data'] . '</td>
								<td class="borda centro">' . $aIRej['nossonumero'] . '</td>
								<td class="borda centro">R$ ' . number_format($aIRej['valor'], 2, ',', '.') . '</td>
							</tr>
							';
}
?>
                  	</table>
                </div>
            </div>
            <div id="previsao" class="meio esq">
                <div id="prev" class="minmeio">
                    <form name="frmPrev" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                    	<div class="divtitulo">
                            Previsão em 15 dias: <?php echo $numPrev;?>
                        	<img src="img/busca.gif" width="16" height="14" class="dir" style="cursor: pointer;" id="mostra_prev" />
                       	</div>
                        <div id="busca_prev" class="divtitulo">
                        	<select name="pes_prev" id="pes_prev">
                            	<option value="0">- Pesquisa -</option>
                                <option value="periodo">Período</option>
                                <option value="valor">Valor</option>
                                <option value="nome">Nome</option>
                            </select>
                            <span id="per" style="display:none">
                                <input name="inicio" type="text" id="inicio" size="8" maxlength="10" onkeyup="foc(this)" /> à
                                <input name="fim" type="text" id="fim" size="8" maxlength="10" />
                            </span>
                            <span id="bnome" style="display:none">
                                <input name="nome" type="text" id="nome" size="20" maxlength="30" />
                            </span>
                          <input class="bdesab top dir" type="button" name="pesq" id="pesq" value="Pesquisar" />
                        </div>
                        <div class="btitulos" id="busca2">
                            <table>
                                <tr class="cinza">
                                    <td class="destaque borda centro" width="25%">Vencimento</td>
                                    <td class="destaque borda centro">Sacado</td>
                                    <td class="destaque borda centro" width="25%">Valor</td>
                                </tr>
                            <?php while ($aPrev = mysql_fetch_array($sPrev)) {
	echo '
                                <tr>
                                    <td class="borda centro">' . $aPrev['data_vencimento'] . '</td>
                                    <td class="borda centro">' . $aPrev['nome'] . '</td>
                                    <td class="borda centro">R$ ' . number_format($aPrev['valor'], 2, ',', '.') . '</td>
                                </tr>
                                ';
}
?>
                            </table>
                       	</div>
                    </form>
                </div>
        	</div>

            <div id="atraso"  style="float:right;">
                <div class="minmeio">
                    <form name="frmAtraso" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="divtitulo">
                            <?php echo $numAtr;?> em atraso
                        	<img src="img/busca.gif" width="16" height="14" class="dir" style="cursor: pointer;" id="mostra_atr" />
                        </div>
                        <div id="busca_atr" class="divtitulo2">
                        	<select name="pes_atr" id="pes_atr">
                            	<option value="0">- Pesquisa -</option>
                                <option value="periodo">Período</option>
                                <option value="valor">Valor</option>
                                <option value="nome">Nome</option>
                            </select>
                            <span id="per" style="display:none">
                                <input name="inicio1" type="text" id="inicio1" size="8" maxlength="10" onkeyup="foc1(this)" /> à
                                <input name="fim1" type="text" id="fim1" size="8" maxlength="10" />
                            </span>
                            <span id="bnome" style="display:none">
                                <input name="nome1" type="text" id="nome1" size="20" maxlength="30" />
                            </span>
                            <input class="bdesab top dir" type="button" name="pesq" id="pesq" value="Pesquisar" />
                        </div>
                        <div class="btitulos" id="busca2">
                            <table>
                                <tr class="cinza">
                                    <td class="destaque borda centro" width="25%">Vencimento</td>
                                    <td class="destaque borda centro">Sacado</td>
                                    <td class="destaque borda centro" width="25%">Valor</td>
                                </tr>
                            <?php while ($aAtr = mysql_fetch_array($sAtr)) {
	echo '
                                <tr>
                                    <td class="borda centro">' . $aAtr['data_vencimento'] . '</td>
                                    <td class="borda centro">' . $aAtr['nome'] . '</td>
                                    <td class="borda centro">R$ ' . number_format($aAtr['valor'], 2, ',', '.') . '</td>
                                </tr>
                                ';
}
?>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
       		<br class="clear" />
        </fieldset>
       	<br class="clear" />
  </div>
</div>
<?php } else { ?>
<div id="section" class="max" width="200">
	<div class="titulo">
        <h2>VISÃO GERAL</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    	<fieldset>


            <div id="baixados"  style="float:right;" width="1000000">
                <div class="minmeio">
                    <form name="frmBaixados" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                        <div class="divtitulo">
                           Titulos baixados nos últimos 15 dias
                        	<img src="img/busca.gif" width="16" height="14" class="dir" style="cursor: pointer;" id="mostra_atr" />
                        </div>
                        <div id="busca_baixados" class="divtitulo2">
                        	<select name="pes_baixados" id="pes_baixados">
                            	<option value="0">- Pesquisa -</option>
                                <option value="periodo">Período</option>
                                <option value="valor">Valor</option>
                                <option value="nome">Nome</option>
                            </select>
                            <span id="per" style="display:none">
                                <input name="inicio1" type="text" id="inicio1" size="8" maxlength="10" onkeyup="foc1(this)" /> à
                                <input name="fim1" type="text" id="fim1" size="8" maxlength="10" />
                            </span>
                            <span id="bnome" style="display:none">
                                <input name="nome1" type="text" id="nome1" size="20" maxlength="30" />
                            </span>
                            <input class="bdesab top dir" type="button" name="pesq" id="pesq" value="Pesquisar" />
                        </div>
                        <div class="btitulos" id="busca2">
                            <table>
                                <tr class="cinza">
                                    <td class="destaque borda centro" width="25%">Data da Baixa</td>
                                    <td class="destaque borda centro" width="25%">Data da Baixa Manual</td>
                                    <td class="destaque borda centro">Sacado</td>
                                    <td class="destaque borda centro" width="25%">Valor</td>
                                </tr>
                            <?php while ($aBaix = mysql_fetch_array($sBaix)) {
	echo '
                                <tr>
                                    <td class="borda centro">' . $aBaix['data_baixa'] . '</td>
                                    <td class="borda centro">' . $aBaix['data_baixa_manual'] . '</td>
                                    <td class="borda centro">' . $aBaix['nome'] . '</td>
                                    <td class="borda centro">R$ ' . number_format($sBaix['valor'], 2, ',', '.') . '</td>
                                </tr>
                                ';
}
?>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
       		<br class="clear" />
        </fieldset>
       	<br class="clear" />
  </div>
</div>

<?php }?>

