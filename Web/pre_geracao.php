<?php

function clearSpecialChars($str) {

	$clear_array = array( "á" => "a" , "é" => "e" , "í" => "i" , "ó" => "o" , "ú" => "u" ,
	"à" => "a" , "è" => "e" , "ì" => "i" , "ò" => "o" , "ù" => "ù" ,
	"ã" => "a" , "õ" => "o" , "â" => "a" , "ê" => "e" , "î" => "i" , "ô" => "o" , "û" => "u" ,
	"," => ""  , "!" => "" , "#" => "" , "%" => "", "¬" => "" , "{" => "" , "}" => "" ,
	"^" => ""  , "´" => "" , "`" => "" , "" => "" , "/" => "" , ";" => "" , ":" => "" , "?" => "" ,
	"¹" => "1" , "²" => "2" , "³" => "3" , "ª" => "a" , "º" => "o" , "ç" => "c" , "ü" => "u" ,
	"ä" => "a" ,"ï" => "i" , "ö" => "o" , "ë" => "e" , "$" => "s" , "ÿ" => "y" , "w" => "w" , "<" => "" ,
	">" => "" ,"[" => "" , "]" => "" , "&" => "e" , "'" => "" , '"' => ""  , '1' => '1' ,
	'2' => '2' , '3' => '3' , '4' => '4' , '5' => '5' , '6' => '6' , '7' => '7' , '8' => '8' , '9' => '9' ,
	'0' => '0' ,'á' => 'a' , 'Á' => 'A' ,  'é' => 'e' , 'É' => 'E' ,  'í' => 'i' , 'Í' => 'i' ,  'ó' => 'o' ,
	'Ó' => 'O' ,  'ú' => 'u' , 'Ú' => 'U' ,  'â' => 'â' , 'â' => 'â' ,  'ê' => 'ê' , 'Ê' => 'â' ,
	'ô' => 'ô' , 'Ô' => 'â' ,  'à' => 'a' , 'À' => 'â' ,  'ç' => 'c' , 'Ç' => 'C' ,  'ã' => 'a' ,
	'Ã' => 'ã' ,  'õ' => 'o' , 'Õ' => 'o' );
	
	foreach($clear_array as $key=>$val){
		$str = str_replace($key, $val, $str);
	}
	
	return $str;
}

if(isset($_POST['quant'])){
	$quant_post = $_POST['quant'];
	$sacado = $_POST['sacado'];
	$avalista = $_POST['avalista'];
	$modelo = $_POST['modelo'];
	$numero = $_POST['numero'];
	$quant = $_POST['quant'];
	$intervalo = $_POST['intervalo'];
	$emissao = $_POST['emissao'];
	$venc = $_POST['venc'];
	$protesto = $_POST['protesto'];
	$valor_total = $_POST['valor_total'];
	$valor = $_POST['valor'];
	$desconto = $_POST['desconto'];
	$multa = $_POST['multa'];
	$juros = $_POST['juros'];
	$instrucao = clearSpecialChars($_POST['instrucao']);
	$data_limite_desconto = $_POST['data_limite_desconto'];
}

if(isset($_GET['so_desconto_get'])){
	$so_desconto_get = $_GET['so_desconto_get'];
	$dias_protesto = $_GET['dias_protesto'];
	$sacado_get = $_GET['sacado_get'];
	$quant_get = $_GET['quant_get'];
	$numero = $_GET['numero'];
	$modelo = $_GET['modelo'];
	$avalista = $_GET['avalista'];
	$sacado = $_GET['sacado'];
	$emissao = $_GET['emissao'];
	$multa = $_GET['multa'];
	$juros = $_GET['juros'];
	$protesto = $_GET['protesto'];
	$intervalo = $_GET['intervalo'];
	$instrucao = clearSpecialChars($_GET['instrucao']);
	$valor_total_conf = $_GET['valor_total_conf'];
	$valor = $_GET['valor'];
	$vencimento= $_GET['vencimento'];
	$desconto = $_GET['desconto'];
	$data_limite_desconto = $_GET['data_limite_desconto'];
}

if(isset($_POST['sacado'])){
	$sacado_post = $_POST['sacado'];
}

$data_limite_desconto_post = (isset($_POST['data_limite_desconto']) ? $_POST['data_limite_desconto'] : "NULL");

if(isset($_POST['so_desconto']))
{
	$so_desconto_post = $_POST['so_desconto'];
}
else
{
	$so_desconto_post = "N";
}
if(isset($_POST['dias_protesto'])){
	if($_POST['protesto'] != ""){
		$dias_protesto = $_POST['dias_protesto'];
	}else{
		$dias_protesto="";
	}
}

if(!empty($valor_total)){
	$valor_total=str_replace(".","",$valor_total);
	$valor_total=str_replace(",",".",$valor_total);
	$valor=$valor_total/$quant;
	$valor2=number_format($valor, 2, '.', '');
	$valor=number_format($valor, 2, ',', '.');
}
function zero($casas,$zero){
   while (strlen($casas)<$zero) {
       $casas = "0".$casas;
   }
   return $casas;
}

if( $_GET['acao']=='Registrar' and !empty($sacado)){

	foreach($valor as $cod => $vl_fatura){
		$vl_fatura=str_replace(".","",$vl_fatura);
		$vl_fatura=str_replace(",",".",$vl_fatura);
		$conf_total=$conf_total+$vl_fatura;
	}
	
	if(!empty($valor_total_conf)){
	$valor_total_conf = number_format($valor_total_conf, 2, '.', '');
	$conf_total = number_format($conf_total, 2, '.', '');
		if($valor_total_conf!=$conf_total){
			?>
<SCRIPT>
					alert('A valor total da parcelas não conferem com o valor total informado.')
					history.back()
				</SCRIPT>
<?php
		}
	}
	include 'config.php';
	
	$cliente=$_COOKIE["cokcliente"];
	$agencia=$_COOKIE["cokagencia"];
	$conta=$_COOKIE["cokconta"];
	
	?>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="arquivos/MascaraValidacao.js" type="text/JavaScript"></script>
<script src="arquivos/filtro.js" type="text/JavaScript"></script>
</head>
<script language="JavaScript" type="text/JavaScript">
<!--
function boleto(cod, seq, quant) {
	var remote = null
	remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		<?php if ($agencia == 4097) echo 
			"remote.location.href = 'boletophp/boleto_bancoob.php?titulo='+cod+'&seq='+seq+'&quant='+quant";
		else echo "
			remote.location.href = 'boletophp/boleto_bb.php?titulo='+cod+'&seq='+seq+'&quant='+quant"; ?>
	}
}
// -->
<!--
function boleto3vias(cod, seq, quant, clien) {
	var remote = null
	remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		<?php if ($agencia == 4097) echo "
			remote.location.href = 'boletophp/boleto_3vias_bancoob.php?titulo='+cod+'&seq='+seq+'&quant='+quant+'&cliente='+clien";
		else echo "
			remote.location.href = 'boletophp/boleto_3vias.php?titulo='+cod+'&seq='+seq+'&quant='+quant+'&cliente='+clien"; ?>
	}
}
// -->
<!--
function boletocarne(cod, seq, quant, sac) {
	var remote = null
	remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'boletophp/carne.php?documento='+cod+'&seq='+seq+'&quant='+quant+'&sac='+sac
	}
}
// -->
<!--
function travaTela() {
	parent.parent.parent.document.getElementById('fundo').className = "fundo";
}
// -->
</script>

<body>
<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
    
    <?php
	
	$dados = mysql_query("select cliente, nossonumero from clientes where cliente='$cliente'")or die ("Não foi possível realizar a consulta ao banco de dados");
	if(mysql_num_rows($dados)>0){   
	$tupla = mysql_fetch_assoc($dados); 
	$nossonumero_base=$tupla["nossonumero"];
//	$cliente=$tupla["cliente"];
	}
	
	if(!empty($multa)):
			$multa=str_replace(".","",$multa);
			$multa=str_replace(",",".",$multa);
	else:
			$multa='0.00';
	endif;
	if(!empty($juros)):
			$juros=str_replace(".","",$juros);
			$juros=str_replace(",",".",$juros);
	else:
			$juros='0.00';
	endif;
	if(empty($emissao)):
			$emissao="null";
	else:
			list ($dia,$mes,$ano) = split ('/',$emissao);
			$emissao="'$ano-$mes-$dia'";
	endif;
	
	if(empty($data_limite_desconto)):
			$data_limite_desconto="null";
	else:
			list ($dia,$mes,$ano) = split ('/',$data_limite_desconto);
			$data_limite_desconto="'$ano-$mes-$dia'";
	endif;
	
	$numero=zero($numero,8);
	print "As faturas abaixo foram registradas<br>";
	
	foreach($valor as $cod => $vl_fatura){
		$reg_venc=$vencimento[$cod];
		$reg_descontos=$desconto[$cod];
		
		$nossonumero_base=$nossonumero_base+1;
		$nossonumero_base=zero($nossonumero_base,6);
		$cliente=zero($cliente,4);

		$nossonumero=$cliente."".$nossonumero_base;
		
		if(empty($reg_venc)):
				$reg_venc="null";
		else:
				list ($dia,$mes,$ano) = split ('/',$reg_venc);
				$reg_venc="'$ano-$mes-$dia'";
		endif;

		if(!empty($vl_fatura)):
				$vl_fatura=str_replace(".","",$vl_fatura);
				$vl_fatura=str_replace(",",".",$vl_fatura);
		else:
				$vl_fatura='0.00';
		endif;
		
		if(!empty($reg_descontos)):
				$reg_descontos=str_replace(".","",$reg_descontos);
				$reg_descontos=str_replace(",",".",$reg_descontos);
		else:
				$reg_descontos='0.00';
		endif;
		$cliente=$_COOKIE["cokcliente"];
		$dias_protesto = str_replace("ú","&uacute;",$dias_protesto);
		$cConsulta  = "insert into titulos (agencia,cliente,sacado,documento,sequencia,nossonumero,";
		$cConsulta .= "data_emisao,data_venc,valor,descontos,data_limite_desconto,multa,juros,protesto,dias_protesto,instrucao,";
		$cConsulta .= "modelo, sacador, so_desconto, status, criacao,cad_completo,intervalo) ";
		$cConsulta .= "values ('$agencia','$cliente','$sacado',UPPER('$numero'),'$cod','$nossonumero',";
		$cConsulta .= "$emissao,$reg_venc,$vl_fatura,$reg_descontos,$data_limite_desconto,$multa,$juros,".($protesto==""? "NULL" : "$protesto").",";
		$cConsulta .= ($dias_protesto==""? "NULL" : "'$dias_protesto'").",'$instrucao',".($modelo==""? "NULL" : "$modelo").", '$avalista', '$so_desconto_get', '01', NOW(),'N',$intervalo);";		

		$insere = mysql_query($cConsulta) or die ("Não foi possível registrar o boleto, verifique os dados informados.");
	}
	$atualiza = mysql_query("update clientes set nossonumero= '$nossonumero_base' where cliente='$cliente';")or die ("Não foi possível realizar a atualização do banco de dados");
	$sql = mysql_query("select titulo, documento, sequencia, nossonumero, DATE_FORMAT(data_emisao, '%d/%c/%Y') as data_emisao, DATE_FORMAT(data_venc, '%d/%c/%Y') as data_venc, valor from titulos where sacado='$sacado' and documento=UPPER('$numero') and cancelamento is null and data_emisao=$emissao;")or die ("Não foi possível realizar a consulta ao banco de dados");
    if(mysql_num_rows($sql)>0){
		print "<center><table border='0' width='50%'>";
		print "<tr  bgcolor='#999999'>";		
			print "<td  align='center'>Nº Documento</td>";
			print "<td  align='center'>Data emissão</td>";
			print "<td  align='center'>Data vencimento</td>";
			print "<td  align='center'>Valor da fatura</td>";
			print "<td  align='center'>Imprimir</td>";
		print "</tr>";
		while ($linha=mysql_fetch_array($sql)) {
			print "<tr>";		
				print "<td  align='right'>".$linha[documento]."/".$linha[sequencia]."</td>";
				print "<td  align='right'>".$linha[data_emisao]."</td>";
				print "<td  align='right'>".$linha[data_venc]."</td>";
				print "<td  align='right'>".$linha[valor]."</td>";
				print "<td  align='center'>";
				$quant_get 	= $_GET['quant_get'];
				$seq		= $linha['sequencia'];
				if($modelo==1){
					//print "<img src='images/print.jpg' onclick='boleto(".$linha[titulo].",".$linha[sequencia].",".$quant_get.");' height='20' width='24' /> 2 vias";
					print "<img src='images/print.jpg' height='20' width='24' /> 2 vias";
				}elseif($modelo==2){
					//print "<img src='images/print.jpg' onclick='boleto3vias(".$linha[titulo].",".$linha[sequencia].",".$quant_get.", ".$cliente.");' height='20' width='24' />3 vias";
					print "<img src='images/print.jpg' height='20' width='24' />3 vias";
				}elseif($modelo==3){
					?><!--
<img src='images/print.jpg' onClick="boletocarne('<?php print $linha[documento];?>', '<?php print $seq; ?>', '<?php print $quant_get;?>', '<?php print $sacado_get; ?>');" height='20' width='24' /> Carnê-->
<?php
				}
				print "</td>";
			print "</tr>";
		} 
	print "</table>";
	}
	print "
		  <script>
		  parent.parent.parent.document.getElementById('fundo').className = 'fimProcesso';
		  </script>	
	      ";
}

require('config.php');

list ($dia,$mes,$ano) = split ('/',$emissao);
$emissao2="$ano-$mes-$dia";
$numero2=zero($numero,8);
$verifica = mysql_query("select titulo from titulos where sacado='$sacado' and documento='$numero2' and data_emisao='$emissao2' and cancelamento is null;") or die ("");

if(mysql_num_rows($verifica)==0){
	
	if(!empty($sacado) and $quant>0 and !empty($venc) and ($valor_total>0 or $valor>0)){   
	
		?>
<form method="get" action="#">
  <input type="hidden" name="so_desconto_get" value="<?php echo $so_desconto_post; ?>">
  <input type="hidden" name="data_limite_desconto" value="<?php echo $data_limite_desconto_post; ?>">
  <input type="hidden" name="dias_protesto" value="<?php echo $dias_protesto; ?>">
  <input type="hidden" name="sacado_get" value="<?php echo $sacado_post; ?>">
  <input type="hidden" name="quant_get" value="<?php echo $quant_post; ?>">
  <input type="hidden" name="numero" value="<?php print $numero;?>">
  <input type="hidden" name="modelo" value="<?php print $modelo;?>">
  <input type="hidden" name="avalista" value="<?php print $avalista;?>">
  <input type="hidden" name="sacado" value="<?php print $sacado;?>">
  <input type="hidden" name="emissao" value="<?php print $emissao;?>">
  <input type="hidden" name="multa" value="<?php print $multa;?>">
  <input type="hidden" name="juros" value="<?php print $juros;?>">
  <input type="hidden" name="protesto" value="<?php print $protesto;?>">
  <input type="hidden" name="instrucao" value="<?php print $instrucao;?>">
  <input type="hidden" name="valor_total_conf" value="<?php print $valor_total;?>">
  <input type="hidden" name="valor" value="<?php print $valor;?>">
  <input type="hidden" name="intervalo" value="<?php print $intervalo;?>">
  <table border="0" width="80%">
    <?php
		
		$prox=0;
		if($quant<1){
			$quant=1;
		}
		for($x=1;$x<=$quant;$x++){
			if(!empty($valor_total) and $quant>1 and $x==$quant){
					$valor=$valor_total-($valor2*($quant-1));
					$valor=number_format($valor, 2, ',', '.');
			}
		
			print "<tr>";
				print "<td align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Fatura:</b></td>";
				print "<td align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'> ";
				print $numero;
				if($quant > 1)
					{
					print "/".$x;
					}
				print "  </td>";
				print "<td align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Valor:</b> </td>";
				print "<td><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><input type='text' value='$valor' name='valor[$x]' style='text-align:right; font:Verdana, Arial, Helvetica, sans-serif; font-size:10px; height:19; width:150' size='10'> </td>";
				print "<td align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Desconto:</b> </td>";
				print "<td><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><input type='text' value='$desconto' name='desconto[$x]' style='text-align:right; font:Verdana, Arial, Helvetica, sans-serif; font-size:10px; height:19; width:150' size='10'></td>";
				print "<td align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>Vencimento:</b> </td>";
				print "<td><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><input type='text' value='$venc' name='vencimento[$x]' size='10' maxlength='10' style='text-align:right; font:Verdana, Arial, Helvetica, sans-serif; font-size:10px; height:19; width:150' onkeypress=\"formataCampo(this,'00/00/0000',event)\" onkeyup=\"filtro(this,'data')\" /></td>";
			print "</tr>";
			
			$prox=$prox+1;
			if(empty($intervalo2)){	
				list ($dia,$mes,$ano) = split ('/',$venc);
				$intervalo2=$intervalo;
				$venc = date("d/m/Y",mktime(0,0,0,date($mes),date($dia)+$intervalo,date($ano)));
			}else{
				list ($dia,$mes,$ano) = split ('/',$venc);	
				$venc = date("d/m/Y",mktime(0,0,0,date($mes),date($dia)+$intervalo2,date($ano)));
			}
		}
		if(!empty($quant)){
			print "<tr><td align='center' colspan='8'><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b><input type='submit' name='acao' id='acao' value='Registrar' onclick='travaTela();'></td></tr>";
		}
	}
}else{
   ?>
    <script language="javascript">
		<!-- Esconder código!
		alert("Cliente já possui titulos com esta data de emissão e numero de documento!!!");
		// Exibir código -->
	</script>
    <?php
}
?>
  </table>
</form>
</body>
</html>
