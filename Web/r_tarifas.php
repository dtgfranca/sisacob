<?php
require('config.php');

$cliente = $_COOKIE['cokcliente'];
$conta 	 = $_COOKIE['cokconta'];
$agencia = $_COOKIE['cokagencia'];

$inicio  = substr($_GET['inicio'],6,4)."-".substr($_GET['inicio'],3,2)."-".substr($_GET['inicio'],0,2);
$fim     = substr($_GET['fim'],6,4)."-".substr($_GET['fim'],3,2)."-".substr($_GET['fim'],0,2);

$rs_cli  = mysql_query("SELECT razao FROM clientes WHERE cliente = $cliente");

$rs = mysql_query("
				  SELECT tit.nossonumero, s.nome, DATE_FORMAT(tit.data_venc, '%d/%m/%Y') AS data_venc, tit.valor, tar.valor_tarifa, tar.descricao_tarifa
				  FROM titulos tit, sacados s, inf_tarifa tar
				  WHERE tar.numero_titulo = tit.titulo
      					AND tit.sacado = s.sacado
      					AND tit.agencia = $agencia
      					AND tit.cliente = $cliente
      					AND (tar.datatarifa<='$fim' AND tar.datatarifa>='$inicio')
	              ");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Tarifas</title>
</head>
<body>

<table width="100%">
  <tr>
    <td align="right" width="60%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='3'><b>Relat&oacute;rio de Tarifas</b></font>
    </td>
    <td align="right" width="40%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
       <?php
       echo date("d/m/Y - H:i");
       ?>
       </font>
    </td>
  </tr>
</table>

<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center" style="font-size:14px;border:2px solid #9C9C9C">
			TARIFAS DE <b><?php echo $_GET['inicio']."</b> &Agrave; <b>".$_GET['fim']; ?></b>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="20%" style="font-size:12px;" >CLIENTE:</td>
					<td width="80%" style="font-size:12px;"><?php echo mysql_result($rs_cli,0); ?></td>
				</tr>
				<tr>
					<td style="font-size:12px;">AG&Ecirc;NCIA:</td>
					<td style="font-size:12px;"><?php echo $agencia; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTA:&nbsp;&nbsp;<?php echo $conta; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	    <td>
	        <table width="100%">
				<tr>
				   <td width="15%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Nosso Numero
				   </td>
				   <td width="20%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Sacado
				   </td>
				   <td width="15%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Vencimento
				   </td>
				   <td width="15%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Valor Titulo
				   </td>
				   <td width="20%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Descri&ccedil;&atilde;o
				   </td>
				   <td width="15%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Tarifa
				   </td>
				</tr>
				<?php
				$x=0;
				if (mysql_num_rows($rs) > 0){
				   while ($dados = mysql_fetch_array($rs)){								
						if ($x==0){
					       $cor = "#FFF";
					       $x=1;
					    }  
					    else{
					       $cor = "#FFF";
					  	   $x = 0;	
						}  
					
						echo "<tr style='background-color:$cor'>";				
						echo "  <td align='center'>".$dados['nossonumero']."</td>";
						echo "  <td align='left'>".$dados['nome']."</td>";
						echo "  <td align='center'>".$dados['data_venc']."</td>";
						echo "  <td align='right'>".$dados['valor']."</td>";
						echo "  <td align='left'>".$dados['descricao_tarifa']."</td>";
						echo "  <td align='right'>".$dados['valor_tarifa']."</td>";
						echo "</tr>";
				   }
				}else{
						echo "
						     <tr>
						        <td colspan='6'>&nbsp;</td>
						     </tr>
						     <tr>
						        <td colspan='6' align='center'>Cliente n&atilde;o possui tarifas</td>
						     </tr>
						     <tr>
						        <td colspan='6'>&nbsp;</td>
						     </tr>
						     ";
				} 
				?>
				<tr>
				   <td colspan="6" style="font-size:14px;border-top:2px solid #9C9C9C"></td>
				</tr>
	        </table>
	    </td>
	</tr>
</table>
</font>
</body>
</html>