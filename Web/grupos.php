<script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<div id="Accordion1" class="Accordion" tabindex="0">
  <div class="AccordionPanel">
    <div class="AccordionPanelTab"  style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit"><b>Clientes Agrupados</b></div>
    <div class="AccordionPanelContent">
    <?
    include("grupo_clientes.php");
	?>
    </div>
  </div>
  <div class="AccordionPanel">
    <div class="AccordionPanelTab" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit"><b>Cadastro de Grupos</b></div>
    <div class="AccordionPanelContent">
    <?
    include("grupo_cad.php");
	?>
    </div>
  </div>
</div>
<script type="text/javascript">
<!--
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
//-->
</script>
