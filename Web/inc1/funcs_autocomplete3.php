<?php

	// Substitui a fun��o "conecta"
	require('../config.php');	

	// conecta();		
	
	direciona();
	
	function direciona() {		
		if(isset($_GET['dadosSacado']) || isset($_GET['dadosAvalista'])) {
			if($_GET['dadosSacado']) {
				getSacado($_GET['dadosSacado']);
				unset($_GET['dadosSacado']);
				return;		
			} elseif($_GET['dadosAvalista']) {
				getSacadoAvalista($_GET['dadosAvalista']);
				unset($_GET['dadosAvalista']);
				return;
			}
		} else {											
			retornaAutoComplete($_GET['q'], $_GET['parametro']);				
		}
	}
	
	/**
	 * Fun��o utilizada para retornar o sacado, referente � op��o escolhida no autocomplete ("Cadastro de t�tulos" - "Cliente")
	 * @author Giorgio Lucca <giorgio.lucca@skillnet.com.br>
	 * @return bool
	 */
	function getSacado($dadosSacado = null) {
		$sacado = null;
		if(!empty($dadosSacado)) {
			$dadosSacado = explode('-', $_GET['dadosSacado']);
			$nome = trim($dadosSacado[0]);			
			$cpf = isset($dadosSacado[2]) ? trim($dadosSacado[1]) . '-' . trim($dadosSacado[2]) : trim($dadosSacado[1]);
			$cookies = getCookies();
			if(!empty($cpf)) {
				$sql = "SELECT sacado 
						FROM sacados
						WHERE
							agencia = '" . $cookies['agencia'] . "'
							AND cliente = '" . $cookies['cliente'] . "'
							AND nome = '{$nome}'
							AND cpf = '{$cpf}'";
			} else {
				$sql = "SELECT sacado 
						FROM sacados
						WHERE
							agencia = '" . $cookies['agencia'] . "'
							AND cliente = '" . $cookies['cliente'] . "'
							AND nome = '{$nome}'";
			}
			$query = mysql_query($sql);
			if($query) {
				if(mysql_num_rows($query) > 0) {
					$queryAssoc = mysql_fetch_assoc($query);
					$sacado = $queryAssoc['sacado'];
				}
			}
		}
		echo $sacado;
		return $sacado;
	}
	
	/**
	 * Fun��o utilizada para retornar o sacado do Avalista, referente � op��o escolhida no autocomplete ("Cadastro de t�tulos" - "Sacador/Avalista")
	 * @author Giorgio Lucca <giorgio.lucca@skillnet.com.br>
	 * @return bool
	 */
	function getSacadoAvalista($dadosAvalista = null) {
		$sacadoAvalista = null;
		if(!empty($dadosAvalista)) {						
			$cookies = getCookies();
			$sql = "SELECT sacado 
					FROM sacados
					WHERE
						agencia = '" . $cookies['agencia'] . "'
						AND cliente = '" . $cookies['cliente'] . "'
						AND nome = '" . trim($dadosAvalista) . "'";
			$query = mysql_query($sql);
			if($query) {
				if(mysql_num_rows($query) > 0) {
					$queryAssoc = mysql_fetch_assoc($query);
					$sacadoAvalista = $queryAssoc['sacado'];
				}
			}
		}
		echo $sacadoAvalista;
		return $sacadoAvalista;
	}
	
	function conecta() {
		$dbhost = 'localhost';
		// Usu�rio do MySQL
		$dbuser = 'sisacob';
		// Senha do MySQL
		$dbpasswd = 'skill69547';
		// Nome do Banco de dados
		$dbname = 'skill_sisacob';
		// Conex�o com o Banco de Dados
		$conexao = @mysql_connect($dbhost, $dbuser, $dbpasswd) or die ("N�o foi poss�vel conectar-se ao servidor MySQL");
		$db = @mysql_select_db($dbname) or die ("N�o foi poss�vel selecionar o banco de dados <b>$dbname</b>");
	}
	
	/**
	 * Fun��o utilizadas para realizar os testes com os "cookies" localmente
	 * @author Giorgio Lucca <giorgio.lucca@skillnet.com.br>
	 * @return bool
	 */
	function insertCookie() {
		$cookie = array(
		   "cokagencia" => "41",
		   "cokcliente" => "78",
		);		  
		$cookie = base64_encode(serialize($cookie));
		if(isset($cookie)) {			
			setcookie("SISACOB", $cookie);						
			return true;
		}
		return false;
	}
	
	/**
	  * Fun��o que recupera os cookies que ser�o utilizados como par�metros na query do autocomplete
	  * @author Giorgio Lucca <giorgio.lucca@skillnet.com.br>
	  * @return Array
	  */	  
	function getCookies() {
		$arrCookies = array();
		$cookie = unserialize(base64_decode($_COOKIE["SISACOB"]));							
		if($cookie) {
			if(isset($cookie['cokcliente']) && isset($cookie['cokagencia'])) {
				$arrCookies = array(
					'agencia' => $cookie['cokagencia'],
					'cliente' => $cookie['cokcliente'],
					'conta'   => $cookie['cokconta']
				);
			}
		}
		return $arrCookies;
	}

	/**
	  * Fun��o que preenche o autocomplete
	  * @author Giorgio Lucca <giorgio.lucca@skillnet.com.br>
	  * @param String $caracteres
	  * @param String $campo
	  * @return bool
	  */
	function retornaAutoComplete($caracteres = null, $campo = null) {			
		if(!empty($caracteres) && !empty($campo)) {	
			// Recupera os cookies que ser�o utilizados na query do autocomplete			
			$cookies = getCookies();						
			if(!empty($cookies)) {			
				switch($campo) {				
					case 'nome' :
						$sql = "SELECT nome FROM sacados
							WHERE 
								nome LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY nome ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['nome']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'identidade' : 
						$sql = "SELECT identidade FROM sacados
							WHERE 
								identidade LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY identidade ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['identidade']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'cpf' : 
						$sql = "SELECT cpf FROM sacados
							WHERE 
								cpf LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY cpf ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['cpf']) . "\n";					
							}
						} else {
							return false;
						}						
						break;					
					case 'endereco' : 
						$sql = "SELECT endereco FROM sacados
								WHERE 
									endereco LIKE '{$caracteres}%'
									AND agencia = '" . $cookies['agencia'] . "'
									AND cliente = '" . $cookies['cliente'] . "'
								GROUP BY endereco ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['endereco']) . "\n";					
							}
						} else {
							return false;
						}						
						break;										
					case 'cidade' :
						$sql = "SELECT cidade FROM sacados
								WHERE 
									cidade LIKE '{$caracteres}%'
									AND agencia = '" . $cookies['agencia'] . "'
									AND cliente = '" . $cookies['cliente'] . "'
								GROUP BY cidade ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['cidade']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'bairro' :
						$sql = "SELECT bairro FROM sacados
								WHERE 
									bairro LIKE '{$caracteres}%'
									AND agencia = '" . $cookies['agencia'] . "'
									AND cliente = '" . $cookies['cliente'] . "'
								GROUP BY bairro ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['bairro']) . "\n";					
							}
						} else {
							return false;
						}
						break;
					case 'uf' : 
						$sql = "SELECT uf FROM sacados
							WHERE 
								uf LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY uf ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['uf']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'cep' : 
						$sql = "SELECT cep FROM sacados
							WHERE 
								cep LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY cep ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['cep']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'telefonePessoal' : 
						$sql = "SELECT telef_pess FROM sacados
							WHERE 
								telef_pess LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY telef_pess ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['cidade']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'celular' : 
						$sql = "SELECT celular FROM sacados
							WHERE 
								celular LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY celular ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['celular']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'telefoneComercial' : 
						$sql = "SELECT telef_com FROM sacados
							WHERE 
								telef_com LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY telef_com ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['telef_com']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'fax' : 
						$sql = "SELECT fax FROM sacados
							WHERE 
								fax LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY fax ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['fax']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'email' : 
						$sql = "SELECT email FROM sacados
							WHERE 
								email LIKE '{$caracteres}%'
								AND agencia = '" . $cookies['agencia'] . "'
								AND cliente = '" . $cookies['cliente'] . "'
							GROUP BY email ASC";		
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['email']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					// Se o primeiro caracter for um n�mero, busca por cpf, sen�o, busca por nome
					case 'cliente' : 
						$numeros = array('0','1','2','3','4','5','6','7','8','9');						
						if(in_array(substr($caracteres, 0, 1), $numeros)) {
							$sql = "SELECT * FROM sacados 
								WHERE 
									(cpf LIKE '{$caracteres}%' OR nome LIKE '{$caracteres}%')
									AND agencia = '" . $cookies['agencia'] . "'
									AND conta = '" . $cookies['conta'] . "' 
									AND (grupo <> '99999' OR grupo IS NULL) 
								ORDER BY UPPER(nome)";														
						} else {
							$sql = "SELECT * FROM sacados 
								WHERE 
									(cpf LIKE '{$caracteres}%' OR nome LIKE '{$caracteres}%')
									AND agencia = '" . $cookies['agencia'] . "'
									AND conta = '" . $cookies['conta'] . "' 
									AND (grupo <> '99999' OR grupo IS NULL) 
								ORDER BY UPPER(nome)";	
						}			
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['nome']) . ' - ' . utf8_encode($resultado['cpf']) . "\n";					
							}
						} else {
							return false;
						}						
						break;
					case 'avalista' : 
						$sql = "SELECT * FROM sacados 
							    WHERE nome LIKE '{$caracteres}%'
									AND agencia = '" . $cookies['agencia'] . "'
									AND conta = '" . $cookies['conta'] . "'
									AND cliente = '" . $cookies['cliente'] . "'
									ORDER BY nome";
						$query = mysql_query($sql);
						if($query) {
							while($resultado = mysql_fetch_assoc($query)) {
								echo utf8_encode($resultado['nome']) . "\n";
							}
						} else {
							return false;
						}
						break;
				}
				return true;
			}
		}		
		return false;
	}

?>
