<?php
require('config.php');
//require('php/funcao_data.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Instruçoes do Título</title>
<style>
table, img{behavior: url(iepngfix.htc)}

* {font: normal 10px Arial, Helvetica, sans-serif; font-weight:bold}
</style>

<style type="text/css">
<!--
#bol{ 
width:100%; 
height:200px; 
overflow:auto; 
}
-->
</style>

</head>
<script language="javascript" src="arquivos/Mascaras.js"></script>
<script language="javascript" src="js/script.js"></script>
<script language="javascript" src="js/protot.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
/*
function soNumeros(campo, e){
	
	if (document.all){ 
		// Internet Explorer 
		var tecla = e.keyCode;
		//alert('IE >>' + tecla);
		if (tecla > 47 && tecla < 58){		
			// numeros de 0 a 9
			
			e.cancelBubble = false;
			e.returnValue = true;     
			return true;
		}else{
			e.cancelBubble = true;
          	e.returnValue = false;
		} 
	}else if(document.layers){ 
		// Nestcape 
		var tecla = e.which;
		//alert('netscape'); 
		if (tecla > 47 && tecla < 58){
			// numeros de 0 a 9
			e.returnValue = true; 
			campo.value = cpf_cnpj(campo);		
			     
			return true;
		} 
	}else{
		//Mozilla
		var tecla = e.charCode;
	    //alert('Mozilla >> ' + tecla);
	    alert(tecla);
		if ((tecla < 48 && tecla > 57) ){
			// numeros de 0 a 9
			e.preventDefault();    
			return false;
		}  
	}	 
}*/

///////////////////////////////////////////////////////////////
/*
 * FUNCAO INSTRUCOES
 * ACAO: MOSTRAR COMBO INSTRUCOES E GUARDAR SACADO EM UM HIDDEN
 */
function instrucoes(combo, id){
	var sacado = combo.options[combo.selectedIndex].value;
	document.getElementById('hid_sacado').value = sacado;
	limpar();
	document.getElementById(id).style.display = '';
	document.getElementById('instrucao').selectedIndex = 0;
}
///////////////////////////////////////////////////////////////
/*
 * FUNCAO  boletos
 * ACAO: MONTAR COMBO TUTULO E COM OS TITULOS DO SACADO
 */
function boletos(){
	var sacado = document.getElementById('hid_sacado').value;
	ajax('php/metodos.php?metodo=comboBoletos&sacado='+sacado, 'bol');
	limpar();
	document.getElementById('bol').style.display = '';
	//document.getElementById('bol1').style.display = '';
}
///////////////////////////////////////////////////////////////
/*
 * FUNCAO ALTERAR
 * ACAO: ALTERA NO BANCO DE ACORDO COM A ACAO DETERMINADA
 */
function alterar(combo, id){

    parent.parent.document.getElementById('fundo').className = "fundo";
		
	var boleto = document.getElementById('sel_boleto'+combo).value;
	var acao = document.getElementById('hid_acao').value;
	var titulo = document.getElementById('hid_titulo').value;
	
	document.getElementById('hid_boleto').value = boleto; 
	
	var sacado = jQuery("#sacado option:selected").val();

	if(sacado < 0){
		alert('O sacado precisa ser selecionado!');
		parent.parent.document.getElementById('fundo').className = "fimProcesso";
		return false;
	}
	
	if(acao == 2 && boleto != -1 ){
		if(confirm('CANCELAMENTO/SUSTAÇÃO DA INSTRUÇÃO DE PR\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaComando&boleto='+boleto+'&acao='+acao+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
						   remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE CANCELAMENTO/SUSTAÇÃO DA INSTRUÇÃO DE PR';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});	
		}
	}
	if(acao == 5 && boleto != -1 ){
		if(confirm('PROTESTAR\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaComando&boleto='+boleto+'&acao='+acao+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
						   remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE PROTESTO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
		}
	}
	
	if(acao == 6 && boleto != -1 ){
		if(confirm('SOLICITAÇÃO DE BAIXA\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaComando&boleto='+boleto+'&acao='+acao+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
						   remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE BAIXA';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
		}
	}
	
	if(acao == 7 && boleto != -1 ){
		if(confirm('CANCELAMENTO DE ABATIMENTO\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaComando&boleto='+boleto+'&acao='+acao+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
						   remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE CANCELAMENTO DE ABATIMENTO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
		}
	}
	
	if(acao == 8 && boleto != -1 ){
		if(confirm('CANCELAMENTO DE DESCONTO\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaComando&boleto='+boleto+'&acao='+acao+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
						   remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE CANCELAMENTO DE DESCONTO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
		}
	}
	
	for(var i = 1 ; i <= 13 ; i++){
		
		if(i != 2 && i != 5 && i != 6 && i != 7 && i != 8){
			if(acao == i){
				document.getElementById(id+i).style.display = '';
			}else{
				document.getElementById(id+i).style.display = 'none';
			}
		}
	}

	parent.parent.document.getElementById('fundo').className = "fimProcesso";
	
}
///////////////////////////////////////////////////////////////
/*
 * FUNCAO SALVAR
 * ACAO: SALVAR OS VALORES DOS CAMPOS NO BANCO
 */
function salvar(id,texto){

	parent.parent.document.getElementById('fundo').className = "fundo"; 
	
	var boleto = document.getElementById('hid_boleto').value;
	var acao = document.getElementById('hid_acao').value;
	
	var sacado = jQuery("#sacado option:selected").val();
	
	if(sacado < 0){
		alert('O sacado precisa ser selecionado!');
		parent.parent.document.getElementById('fundo').className = "fimProcesso";
		return false;
	}
	
	if(document.getElementById(texto).value == ''){
		alert('O campo precisa ser preenchido!');
		parent.parent.document.getElementById('fundo').className = "fimProcesso";
		return false;
	}
	
	if(id == 'sav1'){
		var data = document.getElementById('txt1').value;
		if(confirm('ALTERAÇÃO DE VENCIMENTO\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaData&boleto='+boleto+'&acao='+acao+'&data='+data+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE ALTERAÇÃO DE VENCIMENTO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}		
	}else if(id == 'sav3'){
		var valor = document.getElementById('txt3').value;
		if(confirm('CONCESSÃO DE ABATIMENTO\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaValor&boleto='+boleto+'&acao='+acao+'&valor='+valor+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE CONCESSÃO DE ABATIMENTO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}else if(id == 'sav4'){
		var valor = document.getElementById('txt4').value;
		if(confirm('MULTA\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaValor&boleto='+boleto+'&acao='+acao+'&valor='+valor+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE MULTA';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}else if(id == 'sav9'){
		var valor = document.getElementById('txt9').value;
		if(confirm('CONCESSÃO DE DESCONTO\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaValor&boleto='+boleto+'&acao='+acao+'&valor='+valor+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE CONCESSÃO DE DESCONTO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}else if(id == 'sav10'){
		var info = document.getElementById('txt10').value;
		if(confirm('CLIENTE-ALTERAÇÃO DE ENDEREÇO - CEP\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaInfo&boleto='+boleto+'&acao='+acao+'&info='+info+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE ALTERAÇÃO DE ENDEREÇO - CEP';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}else if(id == 'sav11'){
		var info = document.getElementById('txt11').value;
		if(confirm('CLIENTE-ALTERAÇÃO DE ENDEREÇO - CIDADE\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaInfo&boleto='+boleto+'&acao='+acao+'&info='+info+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE ALTERAÇÃO DE ENDEREÇO - CIDADE';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}else if(id == 'sav12'){
		var info = document.getElementById('txt12').value;
		if(confirm('CLIENTE-ALTERAÇÃO DE ENDEREÇO - UF\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaInfo&boleto='+boleto+'&acao='+acao+'&info='+info+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE ALTERAÇÃO DE ENDEREÇO - UF';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}else if(id == 'sav13'){
		var info = document.getElementById('txt13').value;
		if(confirm('CLIENTE-ALTERAÇÃO DE ENDEREÇO - RUA/BAIRRO\n\nDeseja continuar executando esta ação?')){
			jQuery.ajax({
				type: 'GET',
				url: 'php/metodos.php',
				data: 'metodo=salvaInfo&boleto='+boleto+'&acao='+acao+'&info='+info+'&sacado='+sacado,
				success: function(resposta){	
					if (resposta == "certo"){
						var remote = null;
						remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
						if(remote != null){
							remote.location.href = 'comprovante.php?titulo='+boleto+'&descricao=SOLICITAÇÃO DE ALTERAÇÃO DE ENDEREÇO - RUA/BAIRRO';
						}
						alert("Opera\u00E7\u00E3o realizada com sucesso!");
					}
					else if (resposta == "erro1"){
						alert("Campos vazios n\u00E3o \u00E9 permitido continuar.");
					}
					else if (resposta == "erro2"){
						alert("ERRO AO INSERIR OS DADOS 'salvaComando'");
					}			 	
					else if (resposta == "erro3"){
						alert("Instru\u00E7\u00E3o j\u00E1 comandada na data de hoje!");
					}
			    }
			});
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
		}else{
			parent.parent.document.getElementById('fundo').className = "fimProcesso";
			return false;
		}	
	}
	//limpar();
	document.getElementById('instrucao').selectedIndex = 0;
}
///////////////////////////////////////////////////////////////
/*
 * FUNCAO ACAO
 * ACAO: IDENTIFICAR INSTRUCAO A SER EXECUTADA PARA O SACADO ESCOLHIDO
 */
function acao(combo){
	var acao = combo.options[combo.selectedIndex].value;
	var sacado = document.getElementById('hid_sacado').value;
	
	document.getElementById('hid_acao').value = acao;
	
	limpar();
	
	if (acao > 0 && acao < 14){
		boletos();
	}else{
		document.getElementById('sav'+acao).style.display = '';
	}
	
}
/////////////////////////////////////////////////////////////////////////////
/*
 * FUNCAO LIMPAR
 * ACAO: SETA NONE NO DISPLAY DE TODOS AS DIV'S E LIMPA OS CAMPOS DE TEXTO
 */
function limpar(){
	
	for(var i = 1 ; i <= 13 ; i++){
		if(i != 2 && i != 5 && i != 6 && i != 7 && i != 8){
			document.getElementById('sav'+i).style.display = 'none';
			document.getElementById('txt'+i).value = '';
		}
	}
		
	document.getElementById('bol').style.display = 'none';
	//document.getElementById('bol1').style.display = 'none';
			
}
/////////////////////////////////////////////////////////////////////
function seleciona(cod){
	quant = document.getElementById('id_quant').value;
	document.getElementById('sel_boleto'+cod).checked = true;
	
	for(var i=1;i<=quant;i++){
		if(i==cod){
			document.getElementById('tr_sel_boleto'+i).style.backgroundColor	= "#CCCCCC";
		}else{
			document.getElementById('tr_sel_boleto'+i).style.backgroundColor	= "#FFFFFF";
		}
	}
}
/////////////////////////////////////////////////////////////////////
function verifica_data(campo){
	
    // A chamada AJAX propriamente dita
    new Ajax.Request("php/funcao_data.php?campo="+campo.value,
    {
        method:'get',
        onSuccess: function(transport) {
			var response = transport.responseText || "Nada Encontrado";
			
			var f = response;
			
			if(f == 'false')
			{
				alert('Data inválida!');
			}
			else if(f == 'true')
			{
				salvar('sav1', 'txt1');
			}
			else
			{
				alert('Inválido');
			}

        },
        onFailure: function(){ alert('Problemas...') }
    });
}
//////////////////////////////////////////////////////////////////////////////////
function formata_valor(campo,pos,teclapres){

	var tecla = teclapres.keyCode;
	//alert(tecla);
    var vr = campo.value;
    var tam = vr.length ;
    var tam2 = -1;
    var valor = "";
	var numero = "";
	var tam_num = 0;
	var montado = "";
    //alert(vr);
    while(tam != tam2){
    	tam2 = tam;
		vr = vr.replace( "-", "" );
		vr = vr.replace( ".", "" );
		vr = vr.replace( "/", "" );
		vr = vr.replace( ",", "" );
		vr = vr.replace( ";", "" );
		vr = vr.replace( "\\", "" );
		tam = vr.length;
    }
    
	if (tecla != 8 ){
		tam = vr.length + 1 ;
	}
	
	if (tecla == 8 || tecla == 32){
		tam = tam - 1 ;
	}
	
    if (tecla == 8 || (tecla >= 37 && tecla <= 57) || (tecla >= 96 && tecla <= 105) ){
		if (tam <= pos+1 ){
			campo.value = vr ;
		}

		if (tam > pos){
			campo.value = vr.substr( 0, tam - pos ) + ',' + vr.substr( tam - pos, tam );
			valor = campo.value;
		}
		
		numero = valor.split(',');
		tam_num = numero[0].length;
		
		if(tam_num > 3){
			campo.value = numero[0].substr(0,tam_num - 3)+'.'+numero[0].substr(tam_num - 3, tam_num)+','+numero[1];
			montado = campo.value;
			numero = montado.split('.');
			tam_num = numero[0].length;
			valor = montado;
			if(tam_num > 3){
				campo.value = numero[0].substr(0,tam_num - 3)+'.'+numero[0].substr(tam_num - 3, tam_num)+'.'+numero[1];
				montado = campo.value;
				numero = montado.split('.');
				tam_num = numero[0].length;
			}
		}
	}else{
		return false
	}
}

//-->

</script>
<body>
<div align="left" style="padding:10px; background-color:#F4F4F4"><img src="images/ppl18.gif" />&nbsp;<img src="images/titu23q.png" /></div>
<br>
<br>
<br>
<input type="hidden" name="hid_titulo" id="hid_titulo" value="" />
<input type="hidden" name="hid_boleto" id="hid_boleto" value="" />
<input type="hidden" name="hid_sacado" id="hid_sacado" value="" />
<input type="hidden" name="hid_acao" id="hid_acao" value="" />

<table align="center" cellpadding="0" cellspacing="0" border="0" width="700px" >
	<tr>
		<td width="90px">
			Cliente:
		</td>
		<td><div id="comboClientesInstru"></div></td>
	</tr>
	<tr>
		<td width="90px">
			
			Instru&ccedil;&atilde;o:
			
		</td>
		<td>
		<!-- Acao	Comando		Descri��o
			�1"		[06]   		Altera��o de Vencimento
			"2"		[10]  		Cancelamento/Susta��o da instru��es de Pr
			"3"		[04]  		Concess�o de abatimento
			"4"		[35]  		Multa
			"5"		[09]  		Protestar
			"6"		[02]  		Solicita��o de Baixa
			"7"		[05]  		Cancelamento de Abatimento
			"8"		[32]  		Cancelamento de Desconto
			"9"		[31]  		Concess�o de desconto
			"10"	[12]  		Cliente-Altera��o de endere�o � CEP
			"11"	[12]  		Cliente-Altera��o de endere�o - Cidade
			"12"	[12]  		Cliente-Altera��o de endere�o - UF
			"13"	[12]  		Cliente-Altera��o de endere�o - Rua/Bairro -->
		
			<div id="ins" style="display: none;" >
				<select name="instrucao" id="instrucao"  onchange="javascript: acao(this);" >
					<option value="-1" >-- Selecione --</option>
					<option value="1" >Altera&ccedil;&atilde;o de Vencimento</option>
					<option value="2" >Cancelamento/Susta&ccedil;&atilde;o da instruções de Pr</option>
					<option value="3" >Concess&atilde;o de abatimento</option>
					<option value="4" >Multa</option>
					<option value="5" >Protestar</option>
					<option value="6" >Solicita&ccedil;ao de Baixa</option>
					<option value="7" >Cancelamento de Abatimento</option>
					<option value="8" >Cancelamento de Desconto</option>
					<option value="9" >Concess&atilde;o de desconto</option>
					<option value="10" >Cliente-Altera&ccedil;&atilde;o de endere&ccedil;o - CEP </option>
					<option value="11" >Cliente-Altera&ccedil;&atilde;o de endere&ccedil;o - Cidade</option>
					<option value="12" >Cliente-Altera&ccedil;&atilde;o de endere&ccedil;o - UF</option>
					<option value="13" >Cliente-Altera&ccedil;&atilde;o de endere&ccedil;o - Rua/Bairro</option>		
				</select>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="90px" >
			&nbsp;
		</td>
		<td>
			<div id="bol" style="border: 1px solid #DADADA;display: none;">
				
			</div>
			<br>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;&nbsp;&nbsp;
		</td>
		<td colspan="2">	
			<div id="sav1" style="display: none;" >
				<fieldset>
					<input type="text" name="txt1" id="txt1" onkeyup="javascript: formatar(this, '##/##/####');" maxlength="10"  /> <font color="#696969">Ex: (dd/mm/aaaa)</font> &nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar1" value="Salvar" onclick="javascript: verifica_data(document.getElementById('txt1'));"/>
				</fieldset>
			</div>
		
			<div id="sav3" style="display: none;" >
				<fieldset>
					<input type="text" name="txt3" id="txt3" maxlength="14" onKeyDown="return formata_valor(this,2,event)" style="text-align: right;" />&nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar3" value="Salvar" onclick="salvar('sav3', 'txt3')"/>
				</fieldset>
			</div>
		
			<div id="sav4" style="display: none;" >
				<fieldset>
					<input type="text" name="txt4" id="txt4" maxlength="14" onKeyDown="return formata_valor(this,2,event)" style="text-align: right;" />&nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar4" value="Salvar" onclick="salvar('sav4', 'txt4')"/>
				</fieldset>
			</div>
					
			<div id="sav9" style="display: none;" >
				<fieldset>
					<input type="text" name="txt9" id="txt9" maxlength="14" onKeyDown="return formata_valor(this,2,event)" style="text-align: right;" />&nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar9" value="Salvar" onclick="salvar('sav9', 'txt9')"/>
				</fieldset>
			</div>
			
			<div id="sav10" style="display: none;" >
				<fieldset>
					<input type="text" name="txt10" id="txt10" style="text-transform: uppercase;"/> <font color="#696969">Ex: (00000-000)</font> &nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar10" value="Salvar" onclick="salvar('sav10', 'txt10')"/>
				</fieldset>
			</div>
		
			<div id="sav11" style="display: none;" >
				<fieldset>
					<input type="text" name="txt11" id="txt11" size="50" style="text-transform: uppercase;" />&nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar11" value="Salvar" onclick="salvar('sav11', 'txt11')"/>
				</fieldset>
			</div>
		
			<div id="sav12" style="display: none;" >
				<fieldset>
					<input type="text" name="txt12" id="txt12" style="text-transform: uppercase;" />&nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar12" value="Salvar" onclick="salvar('sav12', 'txt12')"/>
				</fieldset>
			</div>
		
			<div id="sav13" style="display: none;" >
				<fieldset>
					<input type="text" name="txt13" id="txt13" size="50" style="text-transform: uppercase;" />&nbsp;&nbsp;&nbsp;
					<input type="button" name="salvar13" value="Salvar" onclick="salvar('sav13', 'txt13')"/>
				</fieldset>
			</div>
		</td>
	</tr>
</table>
</body>
</html>