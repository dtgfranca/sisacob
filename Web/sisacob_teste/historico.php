<?php

function getStatus($status){
	
	if ($status == '02' || $status == '12' || $status == '13' || $status == '14' || $status == '16' || $status == '19' || $status == '21' || $status == '22' || $status == '25' || $status == '28' || $status == '31' || $status == '32' || $status == '33' || $status == '34' || $status == '35' || $status == '36' || $status == '37' || $status == '38' || $status == '39' || $status == '44' || $status == '46' || $status == '76' || $status == '96' ){
		$status = '11';
	} 
	
	if ($status == '20'){
		$status = '06';
	}
	
	if ($status == '98'){
		$status = '23';
	}
	
	$desc['01']='ANDAMENTO';
	$desc['1']='ANDAMENTO';
	$desc['02']='REGISTRO DE TITULO';                           
	$desc['03']='COMANDO RECUSADO';                             
	$desc['05']='LIQUIDADO SEM REGISTRO';                       
	$desc['06']='LIQUIDA&Ccedil;&Atilde;O NORMAL';                            
	$desc['07']='LIQUIDADO POR CONTA';                          
	$desc['08']='LIQUIDADO POR SALDO';                          
	$desc['09']='BAIXA DO TITULO';                              
	$desc['10']='BAIXA SOLICITADA';                             
	$desc['11']='TITULO EM SER';                                
	$desc['12']='ABATIMENTO CONCEDIDO';                         
	$desc['13']='ABATIMENTO CANCELADO';                         
	$desc['14']='ALTERA&Ccedil;&Atilde;O VENCIMENTO';                         
	$desc['15']='LIQUIDADO EM CARTORIO';                        
	$desc['16']='ALTERA&Ccedil;&Atilde;O JUROS DE MORA';                      
	$desc['19']='INSTRU&Ccedil;&Atilde;O PROTESTO';                          
	$desc['20']='DEBITO EM CONTA';                              
	$desc['21']='ALTERA&Ccedil;&Atilde;O DO NOME DO SACADO';                  
	$desc['22']='ALTERA&Ccedil;&Atilde;O ENDERE&Ccedil;O';                           
	$desc['23']='ENCAMINHAMENTO A CART&Oacute;RIO';                    
	$desc['24']='SUSTA&Ccedil;&Atilde;O PROTESTO';                           
	$desc['25']='DISPENSA JUROS DE MORA';                       
	$desc['28']='MANUTEN&Ccedil;&Atilde;O DE TITULO VENCIDO';                 
	$desc['31']='CONCESS&Atilde;O DESCONTO';                           
	$desc['32']='CANCELAMENTO CONCESS&Atilde;O DESCONTO';              
	$desc['33']='RETIFICAR DESCONTO';                           
	$desc['34']='ALTERAR DATA PARA DESCONTO';                   
	$desc['35']='COBRAR MULTA';                                 
	$desc['36']='DISPENSAR MULTA';                              
	$desc['37']='DISPENSAR INDEXADOR';                          
	$desc['38']='DISPENSAR PRAZO LIMITE PARA RECEBIMENTO';      
	$desc['39']='ALTERAR PRAZO LIMITE PARA RECEBIMENTO';        
	$desc['44']='TITULO PAGO COM CHEQUE DEVOLVIDO';             
	$desc['46']='TITULO PAGO COM CHEQUE AGUARDANDO COMPENSA&Ccedil;&Atilde;O';
	$desc['76']='ALTER&Ccedil;&Atilde;O DE TIPO DE COBRAN&Ccedil;A';                 
	$desc['96']='DEPESAS DE PROTESTO';                          
	$desc['97']='DESPESA SUSTA&Ccedil;&Atilde;O PROTESTO';                    
	$desc['98']='DEBITO DE CUSTAS ANTECIPADAS';     
	$desc['88']='RECUSADO P/ DESCONTO';     
	$desc['99']='CANCELADO';     
	
	if (array_key_exists($status, $desc)){
		return $desc[$status];
	}
	else{
		return "DESCONHECIDO";
	}
}

require('config.php');
$cont = 0;
$carne_cont = 0;
$carne = array();
$imprimir1="";
$imprimir2="";
$imprimir3="";

if(isset($_POST['sacado'])){
	
	$inicio=$_POST["inicio"];
	$fim=$_POST["fim"];
	$tipo=$_POST["tipo"];
	$tipo2=$_POST["tipo2"];
	$sacado=$_POST["sacado"];
	$ordem=$_POST["ordem"];
	$ordem2=$_POST["ordem2"];
	$filtro=$_POST["filtro"];
	
	$tipoB=$_POST["tipoB"];
	$n_titulo=$_POST["n_titulo"];
	$nosso_n=$_POST["nosso_n"];
	
	$flag=$_POST["flag"];
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<script language="javascript" src="arquivos/Mascaras.js"></script>

<script language="JavaScript" type="text/JavaScript">
<!--
function boleto(cod, seq, quant){
	var remote = null
	remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'boletophp/boleto_bb.php?titulo='+cod+'&seq='+seq+'&quant='+quant
	}
}
// -->
<!--
function boleto3vias(cod, seq, quant, clien){
	var remote = null
	remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'boletophp/boleto_3vias.php?titulo='+cod+'&seq='+seq+'&quant='+quant+'&cliente='+clien
	}
}
// -->
<!--
function boletocarne(doc, seq, quant, sacado){

	var remote = null
	remote = window.open('','fatura_'+doc,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'boletophp/carne.php?documento='+doc+'&seq='+seq+'&quant='+quant+'&sac='+sacado
	}
}
function geraBoletos(quant){
	var codigo = "";
	var nome ="";
	var tit = "";
	var teste ="";
	var cont=0;
	var remote = null;
	
	for(var i=0; i<quant;i++)
	{
		nome = 'imprimir_'+i;
		var teste = document.getElementById(nome).checked;
		var desab = document.getElementById(nome).disabled;
		
		if(teste && !desab){
			tit = document.getElementById(nome).value+"_";
			codigo = codigo+tit;
			cont++;
		}
	}
	
	if(cont==0){
		alert("Selecione os boletos que deseja imprimir!");
		return false;
	}
	
	remote = window.open('','fatura_'+codigo,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'boletophp/gera_boletos.php?cod='+codigo;
	}
}
// -->
</script>

<script language="JavaScript" type="text/JavaScript">
<!--
function relatorio() {
	var remote = null
	remote = window.open('','relatorio','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'relatorios.php?<?        print "inicio=$inicio&fim=$fim&tipo=$tipo&tipo2=$tipo2&sacado=$sacado&ordem=$ordem&ordem2=$ordem2&filtro=$filtro&tipoB=$tipoB&n_titulo=$n_titulo&nosso_n=$nosso_n";?>'
	}
}
// -->
<!--
function relatorio_simples() {
	var remote = null
	remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
	if (remote != null) {
		remote.location.href = 'relatorios_simples.php?<? print "inicio=$inicio&fim=$fim&tipo=$tipo&tipo2=$tipo2&sacado=$sacado&ordem=$ordem&ordem2=$ordem2&filtro=$filtro&tipoB=$tipoB&n_titulo=$n_titulo&nosso_n=$nosso_n";?>'
	}
}
// -->

<!--
function selecionar_tudo(){
	for (i=0;i<document.v_boletos.elements.length;i++){
      if(document.v_boletos.elements[i].type == "checkbox") 
         document.v_boletos.elements[i].checked=1
	}
} 
//-->

<!--
function deselecionar_tudo(){ 
	for (i=0;i<document.v_boletos.elements.length;i++){
      if(document.v_boletos.elements[i].type == "checkbox") 
         document.v_boletos.elements[i].checked=0
	}
}
function selecionar(campo, cont){
	if(campo.checked){
		for(var i=0;i<cont;i++){
			nome = 'imprimir_'+i;
			if(!document.getElementById(nome).disabled){
				document.getElementById(nome).checked=true;
			}
		}
	}else{
		for(var i=0;i<cont;i++){
			nome = 'imprimir_'+i;
			document.getElementById(nome).checked=false;
		}
	}
}

function desmarcar(campo){
	if(!campo.checked)
		document.getElementById("imprimir_todos").checked=false;
}
//-->

</script>
<body>
<center>
<form name="v_boletos" action="boleto/boleto.php" method="get" target="boleto">
<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
<table border="0" width="95%">
<?
if(!empty($flag)){
?>
<tr>
<?php if($agencia == '4117' || $agencia == '41')
		{
?>
	<td colspan="11">
	
<?php }else
	{
?>
	<td colspan="10">
<?php }?>
		<center>
		<input type="button" value="Relat&oacute;rio agrupado" onclick="relatorio();" style="font-family:Verdana, Arial, Helvetica, sans-serif; size:2" width="400">&nbsp;&nbsp;
		<input type="button" value="Relat&oacute;rio simples" onclick="relatorio_simples();" style="font-family:Verdana, Arial, Helvetica, sans-serif; size:2" width="400">
		</center>
	</td>
</tr>
<?
	if(!empty($inicio) and !empty($fim)){
		list ($dia,$mes,$ano) = split ('/',$inicio);
		$inicio="'$ano-$mes-$dia'";
		list ($dia,$mes,$ano) = split ('/',$fim);
		$fim="'$ano-$mes-$dia'";
		if($filtro=="data_baixa"){
			$b_data=" and ((titulos.data_baixa>=$inicio and titulos.data_baixa<=$fim) or (titulos.data_baixa_manual>=$inicio and titulos.data_baixa_manual<=$fim))";
		}else{
			$b_data=" and titulos.$filtro>=$inicio and titulos.$filtro<=$fim";
		}
	}
	if($tipo=="rec"){
		$b_tipo="and (titulos.data_baixa is not null or titulos.data_baixa_manual is not null) and (status = '05' or status = '06' or status = '07' or status = '08' or status = '15')";
	}
	if($tipo=="baixa"){
		$b_tipo="and (titulos.data_baixa is not null or titulos.data_baixa_manual is not null) and titulos.devolucao is not null and (titulos.status = '09'  or titulos.status = '10') ";
	}
	if($tipo=="pend"){
		$b_tipo="and (titulos.data_baixa is null and titulos.data_baixa_manual is null and titulos.cancelamento is null)";
	}
	if($tipo=="venc"){
		$b_tipo="and titulos.data_venc < CURDATE() and titulos.data_baixa is null and titulos.data_baixa_manual is null AND titulos.cancelamento IS NULL";
	}
	if($tipo=="rejeitados"){
		$b_tipo="and titulos.status='03' and titulos.cancelamento is null";
	}
	if($tipo=="nosso_n"){
		$b_tipo="and titulos.nossonumero like '%".$nosso_n."'";
	}else if($tipo=="cancel"){
		$cancel="and titulos.cancelamento is not null";
	}
	if($tipo=="lista_previa"){
	    $cad_completo = "and cad_completo = 'N' and status <> '99' ";
	}
	else if($tipo!="cancel"){
        $cad_completo = "and cad_completo = 'S' ";	
	}
	if(!empty($tipoB)){
		$b_tipo.=" and titulos.documento like '%".$n_titulo."'";
	}
	######## Rotina que filtra a pesquisa vinda do Situação2 ##########
	if(empty($_POST['tipo2']))
	{
		$b_tipo2 = "";
	}
	else
	{
		//valor vindo do filtro Situação 2 da página baixa.php
		$tipo2	= $_POST['tipo2'];
		
		switch($tipo2)
		{
			case "desc":
				if($tipo=="rec"){
					$b_tipo2 = "and titulos.desconto is not null and so_desconto='S'";
				}else if($tipo=="todos"){
					$b_tipo2 = "and so_desconto='S'";
				}else{
					$b_tipo2 = "and so_desconto='S'";
				}
			break;
			case "cobranca_simples":
				if($tipo=="rec"){
					$b_tipo2 = "and titulos.desconto is not null and so_desconto='N'";
				}else if($tipo=="todos"){
					$b_tipo2 = "and so_desconto='N'";
				}else{
					$b_tipo2 = "and so_desconto='N'";
				}
			break;
		}
	}
	######## Fim desta rotina ################
	
	if(!empty($sacado)){
		$b_sacado="and titulos.sacado='$sacado'";
	}

	$cliente	=$_COOKIE["cokcliente"];
	$agencia	=$_COOKIE["cokagencia"];
	$sql00 = "select titulos.titulo, titulos.documento, REPLACE(titulos.documento, '/', '') as doc, 
				titulos.nossonumero, DATE_FORMAT(titulos.data_emisao, '%d/%c/%Y') as data_emisao, 
				DATE_FORMAT(titulos.data_venc, '%d/%c/%Y') as data_venc, titulos.valor, titulos.modelo, titulos.devolucao, 
				titulos.sequencia,DATE_FORMAT(coalesce(titulos.data_baixa,titulos.data_baixa_manual), '%d/%c/%Y') as data_baixa, 
				DATE_FORMAT(titulos.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(titulos.data_credito, '%d/%c/%Y') as data_credito,  
				titulos.valor_baixa, titulos.sacado, titulos.status as st, sacados.nome
				from titulos left join sacados 
				on titulos.sacado=sacados.sacado where titulos.cliente='$cliente' 
				and data_emisao is not null $b_sacado $b_data $b_tipo $b_tipo2 $cancel $cad_completo 
				order by sacados.nome, $ordem2, titulos.nossonumero;";
				

	$sql = mysql_query($sql00)or die ("Consulta impossível de ser executada.");
	
    if(mysql_num_rows($sql)>0){
		$sacado_ant="";
		while ($linha=mysql_fetch_array($sql)) {
			$sacado=$linha[sacado];
			if($sacado!=$sacado_ant){
				$imprimir3 .= "<tr  bgcolor='#BEBEBE'>";
				$imprimir3 .= "   <td colspan='11'>";
				$imprimir3 .= "      <b>".$linha[nome]."</b>";	
				$imprimir3 .= "   </td>";
				$imprimir3 .= "</tr>";
				$imprimir3 .= "<tr  bgcolor='#BEBEBE'>";		
				$imprimir3 .= "   <td  align='center'>N&ordm; Documento</td>";
				$imprimir3 .= "   <td  align='center'>Nosso numero</td>";
				$imprimir3 .= "   <td  align='center'>Data emiss&atilde;o</td>";
				$imprimir3 .= "   <td  align='center'>Data vencimento</td>";
				$imprimir3 .= "   <td  align='center'>Valor da fatura</td>";
				$imprimir3 .= "   <td  align='center'>Data Liquida&ccedil;&atilde;o</td>";
				$imprimir3 .= "   <td  align='center'>Data Cr&eacute;dito</td>";
				$imprimir3 .= "   <td  align='center'>Valor Recebido</td>";		
				$imprimir3 .= "   <td  align='center'>Status</td>";					
				$imprimir3 .= "   <td  align='center' colspan='2'>A&ccedil;oes disponiveis</td>";
				$imprimir3 .= "</tr>";
			}
			$sacado_ant=$linha[sacado];
			$documento=$linha[doc];
			$imprimir3 .= "<tr>";		
				$imprimir3 .= "<td  align='right'>$documento/".$linha[sequencia]."</td>";
				$imprimir3 .= "<td  align='right'>".$linha[nossonumero]."</td>";
				$imprimir3 .= "<td  align='right'>".$linha[data_emisao]."</td>";
				$imprimir3 .= "<td  align='right'>".$linha[data_venc]."</td>";
				$imprimir3 .= "<td  align='right'>".$linha['valor']."</td>";
				$imprimir3 .= "<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['st'] == '09' || $linha['st'] == '10')?" -- ":$linha['data_baixa'])."</td>";
				$imprimir3 .= "<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['st'] == '09' || $linha['st'] == '10')?" -- ":$linha['data_credito'])."</td>";
				$imprimir3 .= "<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['st'] == '09' || $linha['st'] == '10')?" -- ":$linha['valor_baixa'])."</td>";
				
				$status = $linha[st];
				$imprimir3 .= "<td  align='right'>".ucwords(strtolower(getStatus($status)))."</td>";
				
				$imprimir3 .= "<td  align='right'>";
				
				$data_emissao=$linha[data_emisao];
				if(empty($data_emissao)):
					$data_emissao="null";
				else:
					list ($dia,$mes,$ano) = split ('/',$data_emissao);
					$data_emissao="$ano-$mes-$dia";
				endif;
				if($linha[data_baixa]=='' and $linha[data_baixa_manual]=='' and $linha[cancelamento]=='' and strlen($linha['nossonumero']) > 10)
				{
					
					$sql_quantidade_boletos = mysql_query("select titulo, documento, sequencia, nossonumero, DATE_FORMAT(data_emisao, '%d/%c/%Y') as data_emisao, DATE_FORMAT(data_venc, '%d/%c/%Y') as data_venc, valor from titulos where sacado='$sacado_ant' and documento='$documento' and cancelamento is null and data_emisao='$data_emissao' and cliente = '$cliente';") or die("Não foi possível conectar ao banco de boletos.");
					$quantidade_boletos = mysql_num_rows($sql_quantidade_boletos);
					$modelo=$linha[modelo];
					if($modelo==1 or empty($modelo)){
						$imprimir3 .= "<input  name= 'imprimir_".$cont."' id= 'imprimir_".$cont."' type='checkbox' value='".$linha[titulo]."' onclick='desmarcar(this)'/>";
						$cont++;
						$imprimir3 .= "</td>";
						$imprimir3 .= "<td  align='left'>";
						$imprimir3 .= "<img src='images/print.jpg' onclick='boleto(".$linha[titulo].",".$linha[sequencia].",".$quantidade_boletos.");' height='20' width='24' /> 2 vias";
						//$imprimir3 .= "<img src='images/print.jpg' height='20' width='24' /> 2 vias";
					}elseif($modelo==2){
						$imprimir3 .= "<input name= 'imprimir_".$cont."' id= 'imprimir_".$cont."' type='checkbox' value='".$linha[titulo]."' onclick='desmarcar(this)' />";
						$cont++;
						$imprimir3 .= "</td>";
						$imprimir3 .= "<td  align='left'>";
						$imprimir3 .= "<img src='images/print.jpg' onclick='boleto3vias(".$linha[titulo].",".$linha[sequencia].",".$quantidade_boletos.", ".$cliente.");' height='20' width='24' />3 vias";
						//$imprimir3 .= "<img src='images/print.jpg' height='20' width='24' />3 vias";
					}elseif($modelo==3){
						$ver = false;
						for($h=0;$h<=$carne_cont;$h++){
							if($carne[$h]==$documento){
								$ver = true;
							}
						}
						if(!$ver){
							$carne[$carne_cont]=$documento;
							$carne_cont++;
							$imprimir3 .= "<input name= 'imprimir_".$cont."' id= 'imprimir_".$cont."' type='checkbox' value='".$linha[titulo]."' onclick='desmarcar(this)' disabled />";
							$cont++;
						}else{
							$imprimir3 .= "&nbsp;&nbsp;";
						}
						$imprimir3 .= "</td>";
						$imprimir3 .= "<td  align='left'>";
						$imprimir3 .= "<img src='images/print.jpg' onclick='boletocarne(\"".$linha[doc]."\", ".$linha[sequencia].", ".$quantidade_boletos.", \"".$linha[sacado]."\");' height='20' width='24' />Carnê";
						//$imprimir3 .= "<img src='images/print.jpg' height='20' width='24' />Carnê";
						
					}
				}
				$imprimir3 .= "</td>";

			$imprimir3 .= "</tr>";
		} 
	}
	
	$imprimir2 .= "<tr bgcolor='#BEBEBE'><td colspan='9' align='center' style='font-size:14px;'><b>Hist&oacute;rico de T&iacute;tulos</b></td>";
	$imprimir2 .= "<td align='right'><input  name= 'imprimir_todos' id= 'imprimir_todos' type='checkbox' value='imprimir_todos' onclick='selecionar(this,".$cont.")' /></td>";
	$imprimir2 .= "<td align='left'><b>TODOS</b><span style='color:red'>*</span></td>";
	$imprimir2 .= "</tr>";
			
	//print $imprimir1;
	if($cont > 0){
		print $imprimir2;
	}
	print $imprimir3;
	
	if($cont > 0){
		if($agencia == '4117' || $agencia == '41')
		{
			print "<tr><td colspan='11' align='center' style='border-top: 1px solid #cccccc'>";
		}
		else
		{
			print "<tr><td colspan='10' align='center' style='border-top: 1px solid #cccccc'>";
		}
		print "<br><input type='button' value='Imprimir Boletos Selecionados' onclick='geraBoletos(".$cont.")'>";
		print "<br><br><span style='color:red'>*N&atilde;o &eacute; permitida a sele&ccedil;&atilde;o dos carn&ecirc;s. Sua impress&atilde;o deve ser individual.</span></td></tr>";
	}
}
		
?>
</form>
</table>
</body>
</html>
