<?php
		header("Cache-Control: no-cache",true);
		header("Pragma: no-cache",true);
		header("Expires: 0",true);

		
		$agencia=$_COOKIE["cokagencia"];
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Untitled Document</title>

<style type="text/css">
* {font: normal 10px Arial, Helvetica, sans-serif; font-weight:bold}
<!--
#aba_clientes_pesq{
font: normal 10px Arial, Helvetica, sans-serif;
font-weight:bold;
color: #333333;
padding:5px; 
width:250px; 
height:450px; 
overflow:auto; 
}

.img_opacity{
	opacity:0.4;
	filter:alpha(opacity=40);
}

.img_normal{
	opacity:"";
	filter:"";
}
-->
</style>
<link rel="stylesheet" href="css/app.css" type="text/css">
<script src="jquery-1.4.1.js" type="text/javascript"></script> 
<script src="js/fun.js" type="text/javascript"></script> 
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.history_remote.pack.js" type="text/javascript"></script>
<script src="jquery.tabs.pack.js" type="text/javascript"></script>
<script type="text/javascript">
			
            $(function() {

                $('#container-1').tabs();
				$('#container-3').tabs();
				$('#container-4').tabs();
				$('#container-2').tabs();
             });

</script>



        <link rel="stylesheet" href="jquery.tabs.css" type="text/css" media="print, projection, screen">

        <!-- Additional IE/Win specific style sheet (Conditional Comments) -->

        <!--[if lte IE 7]>

        <link rel="stylesheet" href="jquery.tabs-ie.css" type="text/css" media="projection, screen">

        <![endif]-->

</head>



<body>

			<div id="container-1">

                <ul>

                    <li><a href="#fragment-1"><span>Tela Principal</span></a></li>

                    <li><a href="#fragment-2"><span>Cadastro de Clientes</span></a></li>
                    <li><a href="#fragment-3" onclick="comboClientes('comboClientes');"    ><span>Cadastro de T&iacute;tulos</span></a></li>
                    <li><a href="#fragment-4" onclick="comboClientes('comboClientesBaixa')"><span>Cancelamento de T&iacute;tulos   </span></a></li>
                    <li><a href="#fragment-5" onclick="comboClientes('comboClientesHistorico')" ><span>Relat&oacute;rios</span></a></li>
                    <li><a href="#fragment-6"><span>Utilit&aacute;rios</span></a></li>
					<li><a href="#fragment-13"><span>Sacador/Avalista</span></a></li>
					
					<?php
					if($agencia == '4117' || $agencia == '41')
					{
					?>
							<li><a href="#fragment-14" onclick="comboClientes('comboClientesInstru')"><span>Instru&ccedil;&otilde;es de T&iacute;tulo</span></a></li>
					<?php
					}
					?>
					
                </ul>

                <div id="fragment-1">
					
                    <?php include('inicial_interno.php'); ?>

                </div>

                <div id="fragment-2">
					<SCRIPT language=javascript>
								 var isNav4, isNav6, isIE4;
								 function setBrowser(){
									 if (navigator.appVersion.charAt(0) == "4"){
										 if (navigator.appName.indexOf("Explorer") >= 0){
											 isIE4 = true;
										 }else{
											 isNav4 = true;
										 }
									 }else if (navigator.appVersion.charAt(0) > "4"){
										 isNav6 = true;
									 }
									 if ((isIE4) || (isNav6)) {
										document.write("<iframe src=clientes.php name=senha width=1000 height=650 scrolling=auto framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
									}
								}
								setBrowser();
					</SCRIPT> 
                    <?php //include('clientes.php'); ?>

                </div>

                <div id="fragment-3">
					<div id="container-3">
						<ul>

							<li><a href="#fragment-15"><span>Cadastro</span></a></li>

							<li><a href="#fragment-16" onclick="atualizaTitulosCad()"><span>T&iacute;tulos Cadastrados</span></a></li>
						
						</ul>
						<div id="fragment-15">
							<?php include('cadastro.php'); ?>
						</div>
						<div id="fragment-16">
							<?php include('cad_tit_hoje.php'); ?>
						</div>
					</div>
                </div>

                <div id="fragment-4">
                    <?php include('baixa.php'); ?>
                </div>

                <div id="fragment-5">
					<div id="container-4">
						<ul>

							<li><a href="#fragment-17" onclick="comboClientes('comboClientesHistorico')" ><span>Hist&oacute;rico de T&iacute;tulos</span></a></li>

							<li><a href="#fragment-18" ><span>Carteira</span></a></li>
							
							<li><a href="#fragment-19" onclick="comboClientes('comboClientesInstrucoes')"><span>Instru&ccedil;&otilde;es</span></a></li>
							
							<li><a href="#fragment-20"><span>Tarifas</span></a></li>
							
						</ul>
						<div id="fragment-17">
							<?php include('boletos.php'); ?>
						</div>
						<div id="fragment-18">
							<?php include('relatorio_carteira.php'); ?>
						</div>
						
						<div id="fragment-19">
							<?php include('relatorio_instrucoes.php'); ?>
						</div>
						
						<div id="fragment-20">
							
							<?php include('relatorio_tarifas.php'); ?>
						</div>
						
					</div>
                </div>
               
                <div id="fragment-6">

            		<div id="container-2">

                        <ul>

                            <li><a href="#fragment-7"><span>Alterar senha de acesso</span></a></li>

                            <li><a href="#fragment-8"><span>Integra&ccedil;&atilde;o via CNAB 400</span></a></li>

                            <li><a href="#fragment-9"><span>Instru&ccedil;&otilde;es</span></a></li>

                            <li><a href="#fragment-10"><span>Parametriza&ccedil;&atilde;o boletos</span></a></li>

                            <li><a href="#fragment-11"><span>Importar sacados</span></a></li>

                            <li><a href="#fragment-12"><span>Observa&ccedil;&atilde;o na 1 via - (Boleto de 3 vias)</span></a></li>			

                        </ul>

                        <div id="fragment-7">

                                 <SCRIPT language=javascript>
								 var isNav4, isNav6, isIE4;
								 function setBrowser(){
									 if (navigator.appVersion.charAt(0) == "4"){
										 if (navigator.appName.indexOf("Explorer") >= 0){
											 isIE4 = true;
										 }else{
											 isNav4 = true;
										 }
									 }else if (navigator.appVersion.charAt(0) > "4"){
										 isNav6 = true;
									 }
									 if ((isIE4) || (isNav6)) {
										document.write("<iframe src=utilitarios/senha.php name=senha width=100% height=350 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
									}
								}
								setBrowser();
								</SCRIPT> 

                        </div>
												
                        <div id="fragment-8">

                                <SCRIPT language=javascript>
								 var isNav4, isNav6, isIE4;
								 function setBrowser(){
									 if (navigator.appVersion.charAt(0) == "4"){
										 if (navigator.appName.indexOf("Explorer") >= 0){
											 isIE4 = true;
										 }else{
											 isNav4 = true;
										 }
									 }else if (navigator.appVersion.charAt(0) > "4"){
										 isNav6 = true;
									 }
									 if ((isIE4) || (isNav6)) {
										document.write("<iframe src=utilitarios/cnab400.php name=senha width=100% height=350 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
									}
								}
								setBrowser();
								</SCRIPT> 

                        </div>

                        <div id="fragment-9">

                                 <SCRIPT language=javascript>
								 var isNav4, isNav6, isIE4;
								 function setBrowser(){
									 if (navigator.appVersion.charAt(0) == "4"){
										 if (navigator.appName.indexOf("Explorer") >= 0){
											 isIE4 = true;
										 }else{
											 isNav4 = true;
										 }
									 }else if (navigator.appVersion.charAt(0) > "4"){
										 isNav6 = true;
									 }
									 if ((isIE4) || (isNav6)) {
										document.write("<iframe src=utilitarios/instrucoes.php name=intrucao width=100% height=350 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
									}
								}
								setBrowser();
								</SCRIPT> 

                        </div>

                        <div id="fragment-10">

                                 <SCRIPT language=javascript>
								 var isNav4, isNav6, isIE4;
								 function setBrowser(){
									 if (navigator.appVersion.charAt(0) == "4"){
										 if (navigator.appName.indexOf("Explorer") >= 0){
											 isIE4 = true;
										 }else{
											 isNav4 = true;
										 }
									 }else if (navigator.appVersion.charAt(0) > "4"){
										 isNav6 = true;
									 }
									 if ((isIE4) || (isNav6)) {
										document.write("<iframe src=utilitarios/boleto_modelo.php name=intrucao width=100% height=350 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
									}
								}
								setBrowser();
								</SCRIPT>

                        </div>

                        <div id="fragment-11">

                             <SCRIPT language=javascript>
							 var isNav4, isNav6, isIE4;
							 function setBrowser(){
								 if (navigator.appVersion.charAt(0) == "4"){
									 if (navigator.appName.indexOf("Explorer") >= 0){
										 isIE4 = true;
									 }else{
										 isNav4 = true;
									 }
								 }else if (navigator.appVersion.charAt(0) > "4"){
									 isNav6 = true;
								 }
								 if ((isIE4) || (isNav6)) {
									document.write("<iframe src=utilitarios/import_sacado.php name=import_sacado width=100% height=350 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
								}
							}
							setBrowser();
							</SCRIPT> 

                        </div>

                        <div id="fragment-12">

                                 <SCRIPT language=javascript>
								 var isNav4, isNav6, isIE4;
								 function setBrowser(){
									 if (navigator.appVersion.charAt(0) == "4"){
										 if (navigator.appName.indexOf("Explorer") >= 0){
											 isIE4 = true;
										 }else{
											 isNav4 = true;
										 }
									 }else if (navigator.appVersion.charAt(0) > "4"){
										 isNav6 = true;
									 }
									 if ((isIE4) || (isNav6)) {
										document.write("<iframe src=utilitarios/instrucoes_1via.php name=instrucoes_1via width=100% height=350 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
									}
								}
								setBrowser();
								</SCRIPT> 

                        </div>

        			</div>

           		</div>
				
				<div id="fragment-13">
                    <?php include('cadastro_sacados.php'); ?>

                </div>
                  
				  <?php
					if($agencia == '4117' || $agencia == '41')
					{
					?>             
						 	<div id="fragment-14">
								
								<?php include('instrucoes.php'); ?>
			
							</div>
					<?php
					}
					?>							
     </div>
</body>

</html>

