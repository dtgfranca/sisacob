// JavaScript Document
// FUNÇÃO RESPONSÁVEL DE CONECTAR A UMA PAGINA EXTERNA NO NOSSO CASO A BUSCA_NOME.PHP
// E RETORNAR OS RESULTADOS

function ajax(url, cNome)
{
    // Ação que ocorre quando inicia a chamada,
    // e quando ela é finalizada
    Ajax.Responders.register({
      onCreate: function(){
        $('loading').className = 'ativo';
        centraliza('loading',100,300);
      }, 
      onComplete: function(){
        $('loading').className = 'oculto';
      }
    });

    // A chamada AJAX propriamente dita
    new Ajax.Request(url,
    {
        method:'get',
        onSuccess: function(transport)
        {
            var response = transport.responseText || "Nada Encontrado";
			
			if(cNome=="alert")
			{
				alert(response);
			}
			else if ((document.getElementById(cNome)!=null) && (document.getElementById(cNome).type == 'text' || document.getElementById(cNome).type == 'hidden'))
			{
				document.getElementById(cNome).value = response;
			}
			else
			{
				if (document.getElementById(cNome)!=null)
				{				
					//Verifica se nada foi encontrado
					if(response != 'Nada Encontrado')
					{	
						document.getElementById(cNome).innerHTML = response;	
					}
					else
					{
						document.getElementById(cNome).innerHTML = '';
					}
				}
			}
          
        },
        onFailure: function(){ alert('Problemas...') }
    });
}

function ajaxSalvar(url)
{
    // Ação que ocorre quando inicia a chamada,
    // e quando ela é finalizada
    Ajax.Responders.register({
      onCreate: function(){
        $('loading').className = 'ativo';
        centraliza('loading',100,300);
      }, 
      onComplete: function(){
        $('loading').className = 'oculto';
      }
    });

    // A chamada AJAX propriamente dita
    new Ajax.Request(url,
    {
        method:'get',
        onSuccess: function(transport) {
          var response = transport.responseText || "";
		  switch(response)
		  {
			case "certo":
				alert("Senha alterada com sucesso!"); 
				break;
		  }
		  
		  if(response.substr(0, 11) == "novochamado")
		  {
			resposta = response.split("-");
			document.getElementById("inputqtdNova").value = resposta[1];
			alert("Atenção!! Novo chamado adicionado a sua lista de 'Meus Chamados'.");
		  }
		  if(response.substr(0, 13) == "nenhumchamado")
		  {
			resposta = response.split("-");
			document.getElementById("inputqtdNova").value = resposta[1];
		  }
		  if(response.substr(0, 12) == "foirealocado")
		  {
			idusuario = response.split("-");
			ajaxSalvar("funcoes/incluir.php?metodo=mudaStatusrealocaUsuario&idusuario="+idusuario[1]);
			alert("O Admin realocou o sistema à você!\n Esta página será atualizada.");
			location.reload(true);
		  }
		  if(response == "desrealocar")
		  {
			 alert("Você foi desrealocado!\n Esta página será atualizada.");
			 location.reload(true);
		  }
        },
        onFailure: function(){ alert('Não conseguiu salvar') }
    });
}


function processReqChange(cCampo)
{
		var cNome = cCampo;
        if (req.readyState == 4) {
			if (document.getElementById(cNome)!=null && document.getElementById(cNome).type == 'text'){						
				document.getElementById(cNome).value = req.responseText;
			}else{
				if (document.getElementById(cNome)!=null){				
					document.getElementById(cNome).innerHTML = req.responseText;
				
				}
			}
		}
}
function processReqChangeSalvar()
{
	   if (req.readyState == 4) {
			//alert(req.responseText);					
		}
}

function setBrowser(cPagina,w,h){
	var isNav4, isNav6, isIE4;
	if (navigator.appVersion.charAt(0) == "4"){
		if (navigator.appName.indexOf("Explorer") >= 0){
			isIE4 = true;
		}else{
			isNav4 = true;
      }
   }else if (navigator.appVersion.charAt(0) > "4"){
      isNav6 = true;
	}
	if ((isIE4) || (isNav6)) {
		document.write("<iframe src="+cPagina+" name='processamento' width="+w+" height="+h+" scrolling=no framespacing=0 frameborder=0 marginheight=0 marginwidth=0></iframe>");
	}
}

function centraliza(id,w,h){
    x = parseInt((screen.width - w)/2);
    y = parseInt((screen.height - h)/2);
    
    document.getElementById(id).style.top = y;
    document.getElementById(id).style.left = x;
}

function centralizaV(id,w){
    x = parseInt((screen.width - w)/2);
    document.getElementById(id).style.left = x;
}


function strcomzero(cString,nQuant,cId){
	var cDados   = cString;
	var cRetorno = ''; 	
	var x = 0;	
	//alert("oi");
	if(cDados!=''){	
	
		for (x = nQuant; x>cDados.length; x--){
				cRetorno+='0';
		}
		cRetorno+=cDados;
		document.getElementById(cId).value = cRetorno;
	} 	 
}

function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;
    if((tecla > 47 && tecla < 58)) return true;
    else{
	    if (tecla != 8 && tecla != 0 && tecla != 13 ) return false;
   	 else return true;
    }
}

function salvarUsuario(){

	var codigo = document.getElementById("cod").value;				
	var nome   = document.getElementById("nom").value;
	var senha  = document.getElementById("snh").value;
	var tipo   = document.getElementById("tip").value;
	var setor  = document.getElementById("set").value;
	var email  = document.getElementById("ema").value;	
	var mytime = '';			
	
	if(codigo == "")
	{
		alert("Informe um código.");
		return false;
	}
	if(nome == "")
	{
		alert("Informe um nome.");
		return false;
	}
	if(senha == "")
	{
		alert("Informe uma senha.");
		return false;
	}
	if(tipo == "")
	{
		alert("Informe o tipo.");
		return false;
	}
	if(setor == "")
	{
		alert("Informe o setor.");
		return false;
	}
	if(!ValidaEmail(email))
	{
		alert("Informe um email válido.");
		return false;	
	}
	
	//Valida e-mail
	/*
	if(!ValidaEmail(email)){
		alert('E-mail inválido!!');
		xSetFocus(document.getElementById("ema"));
		return false;
	}
	*/
	
	ajaxSalvar('funcoes/incluir.php?metodo=salvarUsuario&codigo='+codigo+'&nome='+nome+'&senha='+senha+'&tipo='+tipo+'&setor='+setor+'&email='+email);
	
	abas_atualizaCombos('aba2_comboAlterarAtendente');
	
	document.getElementById("cod").value = '';				
	document.getElementById("nom").value = '';
	document.getElementById("set").value = '';
	document.getElementById("ema").value = '';
	document.getElementById("snh").value = '5';
	document.getElementById("tip").value = '3';	
	
	alert("Cadastro efetuado com sucesso!");

	ajax('funcoes/consultas.php?metodo=sugereNumero', 'cod');
	ajax('funcoes/consultas.php?metodo=listaUsuarios', 'users');	
	MyTime = setTimeout("strcomzero(document.getElementById('cod').value,3,'cod')",500);
	
	xSetFocus('nom');
	
	return false;
}
function salvarTipo(){
	
	var codigo     = document.getElementById("ctc").value;				
	var tipo       = document.getElementById("ttc").value;
	var descricao  = document.getElementById("dtc").value;
	var mytime = '';			
			
	if(codigo == ''){
		alert('** Não há código digitado **');
		document.getElementById("ctc").focus();		
		return;	
	}	

	if(tipo == ''){
		alert('** Verifique o campo tipo **');
		document.getElementById("ttc").focus();		
		return;	
	}
	
	if(document.getElementById("chave_chamado").value != "")
	{
		var chave_chamado = document.getElementById("chave_chamado").value;
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpChamado&codigo='+codigo+'&tipo='+tipo+'&descricao='+descricao+'&id='+chave_chamado);
	}
	else
	{
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpChamado&codigo='+codigo+'&tipo='+tipo+'&descricao='+descricao);
	}
	
	document.getElementById("ctc").value = '';				
	document.getElementById("ttc").value = '';
	document.getElementById("dtc").value = '';
}

function zeraDadostp(){
	
	document.getElementById("chave_chamado").value = '';
	document.getElementById("ctc").value = '';
	document.getElementById("ttc").value = '';
	document.getElementById("dtc").value = '';
}

function salvarNatureza(){

	var codigo     = document.getElementById("ctn").value;				
	var natureza   = document.getElementById("ttn").value;
	var descricao  = document.getElementById("dtn").value;
	var mytime = '';			
			
	if(codigo == ''){
		alert('** Não há código digitado **');
		document.getElementById("ctn").focus();		
		return;	
	}	

	if(natureza == ''){
		alert('** Verifique o campo natureza **');
		document.getElementById("ttn").focus();		
		return;	
	}
	
	if(document.getElementById("chave_natureza").value != "")
	{
		var chave_natureza = document.getElementById("chave_natureza").value;
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpNatureza&codigo='+codigo+'&natureza='+natureza+'&descricao='+descricao+'&id='+chave_natureza);
	}
	else
	{
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpNatureza&codigo='+codigo+'&natureza='+natureza+'&descricao='+descricao);
	}

	document.getElementById("ctn").value = '';				
	document.getElementById("ttn").value = '';
	document.getElementById("dtn").value = '';

}

function zeraDadosnt(){
	
	document.getElementById("chave_natureza").value = '';
	document.getElementById("ctn").value = '';				
	document.getElementById("ttn").value = '';
	document.getElementById("dtn").value = '';
}


function salvarPrioridade(){

	var codigo     = document.getElementById("ctp").value;				
	var prioridade = document.getElementById("ttp").value;
	var descricao  = document.getElementById("dtp").value;
	var mytime = '';			
			
	if(codigo == ''){
		alert('** Não há código digitado **');
		document.getElementById("ctp").focus();		
		return;	
	}	

	if(prioridade == ''){
		alert('** Verifique o campo prioridade **');
		document.getElementById("ttp").focus();		
		return;	
	}
	
	if(document.getElementById("chave_prioridade").value != "")
	{
		var chave_prioridade = document.getElementById("chave_prioridade").value;
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpPrioridade&codigo='+codigo+'&prioridade='+prioridade+'&descricao='+descricao+'&id='+chave_prioridade);
	}
	else
	{
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpPrioridade&codigo='+codigo+'&prioridade='+prioridade+'&descricao='+descricao);
	}

	document.getElementById("ctp").value = '';				
	document.getElementById("ttp").value = '';
	document.getElementById("dtp").value = '';

}

function zeraDadospri(){
	
	document.getElementById("chave_prioridade").value = '';
	document.getElementById("ctp").value = '';				
	document.getElementById("ttp").value = '';
	document.getElementById("dtp").value = '';
}

function salvarSetor(){

	var codigo      = document.getElementById("cte").value;				
	var setor       = document.getElementById("tte").value;
	var responsavel = document.getElementById("dte").value;
	var mytime = '';			
			
	if(codigo == ''){
		alert('** Não há código digitado **');
		document.getElementById("cte").focus();		
		return;	
	}	

	if(setor == ''){
		alert('** Verifique o campo setor **');
		document.getElementById("tte").focus();		
		return;	
	}
	
	if(responsavel == ''){
		alert('** Verifique o campo rsponsavel **');
		document.getElementById("dte").focus();		
		return;	
	}
	
	if(document.getElementById("chave_setor").value != "")
	{
		var chave_setor = document.getElementById("chave_setor").value;
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpSetor&codigo='+codigo+'&setor='+setor+'&resp='+responsavel+'&id='+chave_setor);
	}
	else
	{
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpSetor&codigo='+codigo+'&setor='+setor+'&resp='+responsavel);
	}

	document.getElementById("cte").value = '';				
	document.getElementById("tte").value = '';
	document.getElementById("dte").value = '';

}

function zeraDadosse(){
	
	document.getElementById("chave_setor").value = '';
	document.getElementById("cte").value = '';				
	document.getElementById("tte").value = '';
	document.getElementById("dte").value = '';
}


function salvarStatus(){

	var codigo     = document.getElementById("cts").value;				
	var status     = document.getElementById("tts").value;
	var descricao  = document.getElementById("dts").value;
	var imagem     = 0;
	var mytime     = '';			
	var i=0;

	//Verifica quem é o cara
	for (i=0;i<=8;i++){
		var x = document.getElementById("its"+i);		
		//Verifica se é o cara		
		if(x.checked){
			imagem = x.value;
		}		
	}	
	
	if(codigo == ''){
		alert('** Não há código digitado **');
		document.getElementById("cts").focus();		
		return;	
	}	

	if(status == ''){
		alert('** Verifique o campo natureza **');
		document.getElementById("tts").focus();		
		return;	
	}
	
	if(document.getElementById("chave_status").value != "")
	{
		var chave_status = document.getElementById("chave_status").value;
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpStatus&codigo='+codigo+'&status='+status+'&descricao='+descricao+'&imagem='+imagem+'&id='+chave_status);
	}
	else
	{
		ajaxSalvar('funcoes/incluir.php?metodo=salvarTpStatus&codigo='+codigo+'&status='+status+'&descricao='+descricao+'&imagem='+imagem);
	}
	
	document.getElementById("cts").value = '';				
	document.getElementById("tts").value = '';
	document.getElementById("dts").value = '';
}

function zeraDadosst(){
	
	document.getElementById("chave_status").value = '';
	document.getElementById("cts").value = '';				
	document.getElementById("tts").value = '';
	document.getElementById("dts").value = '';
	document.getElementById("its0").checked = true;
}


function cancelar(){
	document.form.reset();
}

function consertaString(cString,nQuebra){
var cRetorno = '';
var x=1;				
		for (x = 0; x<cString.length; x++){
				if( (x%nQuebra) == 0){
					cRetorno+='<BR>';					
				}
				//cRetorno += cString.substring(x,1);			
				cRetorno += cString.substr(x,1);
		}
	return cRetorno;	
}
function hideShowDiv(cElemento){
	if(document.getElementById(cElemento).style.display == 'none'){
		document.getElementById(cElemento).style.display = '';
	}else{
		document.getElementById(cElemento).style.display = 'none';
	}
}

function xSetFocus(cCampo){
	if (cCampo!=''){
		document.getElementById(cCampo).focus();
	}else{
		alert('Sem parametros');	
	}
}
function atualizaTodosChamados(cId,cDiv,cAcao,cAcao2,cAcao3){
	var mytime = '';					
	var cStr = '';
	var cConsulta = 'funcoes/consultas.php?metodo=todosChamados';

	if(cId!=null && cId!=''){
		cConsulta+='&id='+cId;
	}
	if(cAcao!=null && cAcao!=''){
		cConsulta+='&acao='+cAcao;
	}
	if(cAcao2!=null && cAcao2!=''){
		cConsulta+='&apos='+cAcao2;
	}
	if(cAcao3!=null && cAcao3!=''){
		cConsulta+='&acao2='+cAcao3;
	}
	
	ajax(cConsulta, cDiv);
}
function excluirDados(cTabela, cTipo){
	if(confirm('Deseja excluir este registro?')){
		ajaxSalvar('funcoes/incluir.php?metodo=excluir&tabela='+cTabela+'&tipo='+cTipo);	
	}
}
function alteraResolvido(cValor,cStatus){
	
	if(confirm('Deseja colocar este chamado na lista dos resolvidos?')){
		while(1)
		{
			//grau de satisfação
			//1 - Perfeito
			//2 - Ótimo
			//3 - Bom
			//4 - Regular
			//5 - Ruim
			//6 - Péssimo
			var grau = prompt("Informe-nos o seu grau de satisfação: \n 1-Perfeito   2-Ótimo   3-Bom	4-Regular   5-Ruim   6-Péssimo", "");
			grau = parseInt(grau);
			
			if(isNaN(grau) || grau == 0 || grau > 6)
			{
				alert("Informe apenas número de 1 a 6.");
			}
			else
			{
				alert("Obrigado pela sua participação. Sua nota foi: "+grau);
				ajaxSalvar('funcoes/incluir.php?metodo=alteraResolvido&nValor='+cValor+'&nStatus='+cStatus+'&nGrau='+grau);
				return false;
			}
		}
	}
}
function abrirchamado(cValor,cStatus){
	if(confirm('Deseja abrir este chamado novamente?')){
		ajaxSalvar('funcoes/incluir.php?metodo=alteraResolvido&nValor='+cValor+'&nStatus='+cStatus);	
	}
}
function chamadosGeral(id){
		atualizaTodosChamados(id,'meu_chamados','historico_meu_Chamado','atualizarMeusChamados','responderSolicitanteMeu');
		atualizaTodosChamados(0,'chamados',null,'atualizadoChamadosAbertos','responderSolicitante');
		ajax('funcoes/consultas.php?metodo=chamadosEncerrados', 'chamados_encerrados');
	}
function novoschamados(id,div){
	ajax('funcoes/consultas.php?metodo=exibeNovosChamados&id='+id, div);
	if(document.getElementById(div).style.display == 'none'){
		hideShowDiv(div);	
		//alert("Atenção!! Novo chamado adicionado a sua lista de 'Meus Chamados'.");
	}
}
function visualizado(id){
	ajaxSalvar('funcoes/incluir.php?metodo=visualizado&codigo='+id);
	hideShowDiv('lateral');
}
function IsEmpty(cTexto) {
	   if ( (cTexto == null) || (typeof(cTexto)=="undefined") || (cTexto.length == 0) || (cTexto.value == '')) {
		   return true;
	   }
	   else { 
		   return false; 
	   }
}
function ValidaEmail(obj){
  var txt = obj;

  if( (txt.length != 0) && (txt.indexOf("@") > -1) && (txt.indexOf('.') > -1) )
  {
	  return true;  
  }
  return false;
}

function gerar_senha(num, id){
	
	new Ajax.Request('funcoes/incluir.php?metodo=gerar_senha&num='+num+'&id='+id,
	{
		method:'get',
		onSuccess: function(transport) {
			var response = transport.responseText || "Nada Encontrado";
			if(response != "Nada Encontrado")
			{
				//alert('Nova senha gerada: "'+ response + '"');
				alert("Nova senha enviada com sucesso!");
			}
		},
		onFailure: function(){ alert('Problemas ao gerar senha...') }
	});	
}

function resetData(valores){
	//cria-se uma vetor com os 
	//nomes dos campos que serão resetados
	aValor	= valores.split("-");
	
	//Limpa os campos um por um
	for(i=0;i<aValor.length;i++)
	{
		document.getElementById(aValor[i]).value = '';
	}
}

function editar_parametros(m, e, nome, id)
{
	//elementos que são para mostra ou esconder
	var mostra 	= document.getElementById(m);
	var esconde = document.getElementById(e);
	
	//seus respectivos valores de style
	mostra.style.display = 'block';
	esconde.style.display = 'none';
	
	//variável que separa id de nome
	val = nome.split("-");
	
	//id e nome da aba parâmetro
	id2		= val[0];
	nome2 	= val[1];
	
	//junção do nome e id para criar o nome do método
	var metodo = id2+nome2;
	//ajax necessária para salvar dados da aba parâmetro
	ajax('funcoes/consultas.php?metodo='+metodo+'-'+id, 'salvar_tp_'+nome2);
	atualiza();
}