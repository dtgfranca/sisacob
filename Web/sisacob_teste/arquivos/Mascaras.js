/*----------------------------------------------------------------------------
Formatação para qualquer mascara
-----------------------------------------------------------------------------*/
function formatar(src, mask){
  var i = src.value.length;
  var saida = mask.substring(0,1);
  var texto = mask.substring(i)
if (texto.substring(0,1) != saida)
  {
    src.value += texto.substring(0,1);
  }
}

/**********************************************************************
Função de formatação de valores numéricos na digitação

Objetivos :
	- Inclusão automática de . e ,

Parametros :
	objeto		-> Nome do campo de formulário (Usar this)
	teclapres	-> Tecla pressionada (Usar event)
	tammax		-> Tamanho máximo de caracteres
	decimais	-> Nº de casas decimais

Exemplo:
	OnKeyDown     FormataValor(this,event,17,0);


/**********************************************************************/ 
function FormataValor(objeto,teclapres,tammax,decimais) 
{

	var tecla			= teclapres.keyCode;
	var tamanhoObjeto	= objeto.value.length;

	if ((tecla == 8) && (tamanhoObjeto == tammax))
	{
		tamanhoObjeto = tamanhoObjeto - 1 ;
	}



if (( tecla == 8 || tecla == 88 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105 ) && ((tamanhoObjeto+1) <= tammax))
	{

		vr	= objeto.value;
		vr	= vr.replace( "/", "" );
		vr	= vr.replace( "/", "" );
		vr	= vr.replace( ",", "" );
		vr	= vr.replace( ".", "" );
		vr	= vr.replace( ".", "" );
		vr	= vr.replace( ".", "" );
		vr	= vr.replace( ".", "" );
		tam	= vr.length;
		
		if (tam < tammax && tecla != 8)
		{
			tam = vr.length + 1 ;
		}

		if ((tecla == 8) && (tam > 1))
		{
			tam = tam - 1 ;
			vr = objeto.value;
			vr = vr.replace( "/", "" );
			vr = vr.replace( "/", "" );
			vr = vr.replace( ",", "" );
			vr = vr.replace( ".", "" );
			vr = vr.replace( ".", "" );
			vr = vr.replace( ".", "" );
			vr = vr.replace( ".", "" );
		}
	
		//Cálculo para casas decimais setadas por parametro
		if ( tecla == 8 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105 )
		{
			if (decimais > 0)
			{
				if ( (tam <= decimais) )
				{ 
					objeto.value = ("0," + vr) ;
				}
				if( (tam == (decimais + 1)) && (tecla == 8))
				{
					objeto.value = vr.substr( 0, (tam - decimais)) + ',' + vr.substr( tam - (decimais), tam ) ;	
				}
				if ( (tam > (decimais + 1)) && (tam <= (decimais + 3)) &&  ((vr.substr(0,1)) == "0"))
				{
					objeto.value = vr.substr( 1, (tam - (decimais+1))) + ',' + vr.substr( tam - (decimais), tam ) ;
				}
				if ( (tam > (decimais + 1)) && (tam <= (decimais + 3)) &&  ((vr.substr(0,1)) != "0"))
				{
				    objeto.value = vr.substr( 0, tam - decimais ) + ',' + vr.substr( tam - decimais, tam ) ; 
				}
				if ( (tam >= (decimais + 4)) && (tam <= (decimais + 6)) )
				{
			 		objeto.value = vr.substr( 0, tam - (decimais + 3) ) + '.' + vr.substr( tam - (decimais + 3), 3 ) + ',' + vr.substr( tam - decimais, tam ) ;
				}
			 	if ( (tam >= (decimais + 7)) && (tam <= (decimais + 9)) )
				{
			 		objeto.value = vr.substr( 0, tam - (decimais + 6) ) + '.' + vr.substr( tam - (decimais + 6), 3 ) + '.' + vr.substr( tam - (decimais + 3), 3 ) + ',' + vr.substr( tam - decimais, tam ) ;
				}
				if ( (tam >= (decimais + 10)) && (tam <= (decimais + 12)) )
				{
			 		objeto.value = vr.substr( 0, tam - (decimais + 9) ) + '.' + vr.substr( tam - (decimais + 9), 3 ) + '.' + vr.substr( tam - (decimais + 6), 3 ) + '.' + vr.substr( tam - (decimais + 3), 3 ) + ',' + vr.substr( tam - decimais, tam ) ;
				}
				if ( (tam >= (decimais + 13)) && (tam <= (decimais + 15)) )
				{
			 		objeto.value = vr.substr( 0, tam - (decimais + 12) ) + '.' + vr.substr( tam - (decimais + 12), 3 ) + '.' + vr.substr( tam - (decimais + 9), 3 ) + '.' + vr.substr( tam - (decimais + 6), 3 ) + '.' + vr.substr( tam - (decimais + 3), 3 ) + ',' + vr.substr( tam - decimais, tam ) ;
				}
			}
			else if(decimais == 0)
			{
				if ( tam <= 3 )
				{ 
			 		objeto.value = vr ;
				}
				if ( (tam >= 4) && (tam <= 6) )
				{
					if(tecla == 8)
					{
						objeto.value = vr.substr(0, tam);
						window.event.cancelBubble = true;
						window.event.returnValue = false;
					}
					objeto.value = vr.substr(0, tam - 3) + '.' + vr.substr( tam - 3, 3 ); 
				}
				if ( (tam >= 7) && (tam <= 9) )
				{
					if(tecla == 8)
					{
						objeto.value = vr.substr(0, tam);
						window.event.cancelBubble = true;
						window.event.returnValue = false;
					}
					objeto.value = vr.substr( 0, tam - 6 ) + '.' + vr.substr( tam - 6, 3 ) + '.' + vr.substr( tam - 3, 3 ); 
				}
				if ( (tam >= 10) && (tam <= 12) )
				{
			 		if(tecla == 8)
					{
						objeto.value = vr.substr(0, tam);
						window.event.cancelBubble = true;
						window.event.returnValue = false;
					}
					objeto.value = vr.substr( 0, tam - 9 ) + '.' + vr.substr( tam - 9, 3 ) + '.' + vr.substr( tam - 6, 3 ) + '.' + vr.substr( tam - 3, 3 ); 
				}
				if ( (tam >= 13) && (tam <= 15) )
				{
					if(tecla == 8)
					{
						objeto.value = vr.substr(0, tam);
						window.event.cancelBubble = true;
						window.event.returnValue = false;
					}
					objeto.value = vr.substr( 0, tam - 12 ) + '.' + vr.substr( tam - 12, 3 ) + '.' + vr.substr( tam - 9, 3 ) + '.' + vr.substr( tam - 6, 3 ) + '.' + vr.substr( tam - 3, 3 ) ;
				}			
			}
		}
	}
	else if((window.event.keyCode != 8) && (window.event.keyCode != 9) && (window.event.keyCode != 13) && (window.event.keyCode != 35) && (window.event.keyCode != 36) && (window.event.keyCode != 46))
		{
			window.event.cancelBubble = true;
			window.event.returnValue = false;
		}
}
/********************************************************************** 
Função de formatação de campos tipo texto durante a digitação

Objetivos :
	- Mudar tudo para maiúsculo
	- Não aceitar acentos nem Ç

Parâmetros :
	objeto		-> Nome do campo de formulário (Usar this)

Exemplo : 
	OnKeyPress    FormataTexto(this);
/**********************************************************************/
function FormataTexto(objeto)
{
	var valor = objeto.value;	
	var LetrasArray = new Array();

	LetrasArray[1] = new Array();
	LetrasArray[2] = new Array();
	
	//Primeira Coluna : Letras Acentuadas e Símbolos
	LetrasArray[1][1]='á';
	LetrasArray[1][2]='ã';
	LetrasArray[1][3]='à';
	LetrasArray[1][4]='â';
	LetrasArray[1][5]='ä';
	LetrasArray[1][6]='é';
	LetrasArray[1][7]='è';
	LetrasArray[1][8]='ê';
	LetrasArray[1][9]='ë';
	LetrasArray[1][10]='í';
	LetrasArray[1][11]='ì';
	LetrasArray[1][12]='î';
	LetrasArray[1][13]='ï';
	LetrasArray[1][14]='ó';
	LetrasArray[1][15]='õ';
	LetrasArray[1][16]='ò';
	LetrasArray[1][17]='ô';
	LetrasArray[1][18]='ö';
	LetrasArray[1][19]='ú';
	LetrasArray[1][20]='ù';
	LetrasArray[1][21]='û';
	LetrasArray[1][22]='ü';
	LetrasArray[1][23]='ç';
	LetrasArray[1][24]='Á';
	LetrasArray[1][25]='Ã';
	LetrasArray[1][26]='À';
	LetrasArray[1][27]='Â';
	LetrasArray[1][28]='Ä';
	LetrasArray[1][29]='É';
	LetrasArray[1][30]='È';
	LetrasArray[1][31]='Ê';
	LetrasArray[1][32]='Ë';
	LetrasArray[1][33]='Í';
	LetrasArray[1][34]='Ì';
	LetrasArray[1][35]='Î';
	LetrasArray[1][36]='Ï';
	LetrasArray[1][37]='Ó';
	LetrasArray[1][38]='Õ';
	LetrasArray[1][39]='Ò';
	LetrasArray[1][40]='Ô';
	LetrasArray[1][41]='Ö';
	LetrasArray[1][42]='Ú';
	LetrasArray[1][43]='Ù';
	LetrasArray[1][44]='Û';
	LetrasArray[1][45]='Ü';
	LetrasArray[1][46]='Ç';
	LetrasArray[1][47]='Ñ';
	LetrasArray[1][48]='ñ';
	LetrasArray[1][49]='~';
	LetrasArray[1][50]='^';
	LetrasArray[1][51]='´';
	LetrasArray[1][52]='`';
	
	//Segunda Coluna : Letras Equivalentes sem acentos
	LetrasArray[2][1]='a';
	LetrasArray[2][2]='a';
	LetrasArray[2][3]='a';
	LetrasArray[2][4]='a';
	LetrasArray[2][5]='ä';
	LetrasArray[2][6]='e';
	LetrasArray[2][7]='e';
	LetrasArray[2][8]='e';
	LetrasArray[2][9]='e';
	LetrasArray[2][10]='i';
	LetrasArray[2][11]='i';
	LetrasArray[2][12]='i';
	LetrasArray[2][13]='i';
	LetrasArray[2][14]='o';
	LetrasArray[2][15]='o';
	LetrasArray[2][16]='o';
	LetrasArray[2][17]='o';
	LetrasArray[2][18]='o';
	LetrasArray[2][19]='u';
	LetrasArray[2][20]='u';
	LetrasArray[2][21]='u';
	LetrasArray[2][22]='u';
	LetrasArray[2][23]='c';
	LetrasArray[2][24]='A';
	LetrasArray[2][25]='A';
	LetrasArray[2][26]='A';
	LetrasArray[2][27]='A';
	LetrasArray[2][28]='Ä';
	LetrasArray[2][29]='E';
	LetrasArray[2][30]='E';
	LetrasArray[2][31]='E';
	LetrasArray[2][32]='E';
	LetrasArray[2][33]='I';
	LetrasArray[2][34]='I';
	LetrasArray[2][35]='I';
	LetrasArray[2][36]='I';
	LetrasArray[2][37]='O';
	LetrasArray[2][38]='O';
	LetrasArray[2][39]='O';
	LetrasArray[2][40]='O';
	LetrasArray[2][41]='O';
	LetrasArray[2][42]='U';
	LetrasArray[2][43]='U';
	LetrasArray[2][44]='U';
	LetrasArray[2][45]='U';
	LetrasArray[2][46]='C';
	LetrasArray[2][47]='N';
	LetrasArray[2][48]='n';
	LetrasArray[2][49]='';
	LetrasArray[2][50]='';
	LetrasArray[2][51]='';
	LetrasArray[2][52]='';

	temp = "" + valor + (String.fromCharCode(window.event.keyCode));

	for (i=1; i<=52 ; i++ )
	{
		while (temp.indexOf((LetrasArray[1][i]))>-1)
			{
				pos= temp.indexOf((LetrasArray[1][i]));
				temp = "" + (temp.substring(0, pos) + (LetrasArray[2][i]) + temp.substring((pos + (LetrasArray[1][i].length)), temp.length));
			}
	}
	if((window.event.keyCode) != 13)
		{
			objeto.value = (temp.toUpperCase());
			window.event.cancelBubble = true;
			window.event.returnValue = false;
		}

}

/********************************************************************** 
Função de validação de campos do tipo Data

Objetivos :
	- Aceitar somente datas do tipo : dd/mm/aaaa

Parâmetros :
	objeto		-> Nome do campo de formulário (Usar this)

Exemplo : 
	OnChange    ValidaData(this);
/**********************************************************************/ 
function ValidaData(objeto) 
{

	var DataString	= objeto.value;
	var DataArray	= DataString.split("/");  
	var Flag=true; 

	if (DataArray.length != 3) 
		Flag=false; 
	else 
		{
			if (DataArray.length==3) 
			{
				var dia = DataArray[0], mes = DataArray[1], ano = DataArray[2]; 

				if (((Flag) && (ano<1000) || ano.length>4)) 
					Flag=false; 
				
				if (Flag) 
				{ 
					verifica_mes = new Date(mes+"/"+dia+"/"+ano); 
					if (verifica_mes.getMonth() != (mes - 1)) 
						Flag=false; 
				} 
			} 
			else 
				Flag=false; 
		} 
return Flag;
} 

/********************************************************************** 
Função de formatação de campos do tipo Data

Objetivos :
	- Mascarar a entrada de dados no formato : dd/mm/aaaa

Parâmetros :
	objeto		-> Nome do campo de formulário (Usar this)
	teclapress	-> Tecla pressionada (Usar event)

Exemplo : 
	OnKeyDown    FormataData(this,event);

Requirido :
	Função ValidaData
/**********************************************************************/ 
function FormataData(objeto,teclapress)
{
	var tecla = teclapress.keyCode;

	if(((window.event.keyCode == 13) || (window.event.keyCode == 9))&&objeto.value != "")
	{
		if(!(ValidaData(objeto)))
			{
				window.event.cancelBubble = true;
				window.event.returnValue = false;
				alert("Data Inválida");
				objeto.value = "";
				objeto.focus();
			}
	}

	if (( tecla == 8 || tecla == 88 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105 )&& objeto.value.length < (10))
    {
		vr = objeto.value;
		vr = vr.replace( "/", "" );
		vr = vr.replace( "/", "" );
		tam = vr.length;

		if (tam < 8)
			{
				if (tecla != 8) {tam = vr.length + 1 ;}
			}
		else
			{
				window.event.cancelBubble = true;
				window.event.returnValue = false;
			}
		
		if ((tecla == 8) && (tam > 1))
			{
				tam = tam - 1 ;
				objeto.value = vr.substr(0,tam);
				window.event.cancelBubble = true;
				window.event.returnValue = false;
			}
				if ( tam <= 4 && tecla != 8){ 
			 		objeto.value = vr ; }

				if ( (tam >= 4) && (tam <= 6) ){
			 		objeto.value = vr.substr(0, tam - 4) + '/' + vr.substr( tam - 4, 4 ); }

				if ( (tam >= 6) && (tam <= 8) ){
					objeto.value = vr.substr(0, tam - 6 ) + '/' + vr.substr( tam - 6, 2 ) + '/' + vr.substr( tam - 4, 4 ); }

				if ((tam == (8)) && tecla != 8)
					{
						if(tecla >=96 && tecla <=105)
							{
								tecla = tecla - 48;
							}

						objeto.value = objeto.value + (String.fromCharCode(tecla));
						window.event.cancelBubble = true;
						window.event.returnValue = false;

						if (!(ValidaData(objeto)))
							{
								alert("Data Inválida");
								objeto.value = "";
								objeto.focus();
							}
					}
	}
	else if((window.event.keyCode != 8) && (window.event.keyCode != 9) && (window.event.keyCode != 13) && (window.event.keyCode != 35) && (window.event.keyCode != 36) && (window.event.keyCode != 46))
		{
			event.returnValue = false;
		}
}


/********************************************************************** 
Função de validação de campos do tipo Data Hora

Objetivos :
	- Aceitar somente datas do tipo : dd/mm/aaaa hh:mm:ss

Parâmetros :
	objeto		-> Nome do campo de formulário (Usar this)

Exemplo : 
	OnChange    ValidaDataHora(this);
/**********************************************************************/ 
function ValidaDataHora(objeto) 
{        
	var DataHoraString	= objeto.value;
	var Flag			= true;
	
	if(DataHoraString != "" &&((DataHoraString.indexOf("/")!= -1) && (DataHoraString.indexOf(" ")!= -1) && (DataHoraString.indexOf(":")!= -1)))
	{
//		dd/mm/aaaa hh:mm:ss
		var DataArray	= DataHoraString.split("/");  
//		DataArray[0]	= dd;
//		DataArray[1]	= mm;	
//		DataArray[2]	= aaaa hh:mm:ss	;

		var AuxArray	= DataArray[2].split(" ");
//		AuxArray[0]		= aaaa;
//		AuxArray[1]		= hh:mm:ss;
	
		DataArray[2]	= AuxArray[0];
	
		var HoraArray	= AuxArray[1].split(":");
//		HoraArray[0]	= hh;
//		HoraArray[1]	= mm;
//		HoraArray[2]	= ss;

		if(HoraArray[2] >= 0){Flag = true;} 
	}
	else {Flag= false;}

if(Flag)
	{
		//Valida a data
		if (DataArray.length != 3) 
			Flag=false; 
		else 
			{
				if (DataArray.length==3) 
				{
					var dia = DataArray[0], mes = DataArray[1], ano = DataArray[2]; 
	
					if (((Flag) && (ano<1000) || ano.length>4)) 
						Flag=false; 
					
					if (Flag) 
					{ 
						verifica_mes = new Date(mes+"/"+dia+"/"+ano); 
						if (verifica_mes.getMonth() != (mes - 1)) 
							Flag=false; 
					} 
				} 
				else 
					Flag=false; 
			} 
	
		//Valida a hora
		if (HoraArray.length != 3) 
			Flag=false; 
		else 
			{
				if (HoraArray.length==3) 
				{
					var hora = HoraArray[0], min = HoraArray[1], seg = HoraArray[2]; 
					
					if(!((hora>=0 && hora <=23) && (min>=0 && min<=59) && (seg>=0 && seg<=59)))
						Flag=false;
				} 
				else 
					Flag=false; 
			} 
	}		
return Flag;
} 

/********************************************************************** 
Função de formatação de campos do tipo Data Hora

Objetivos :
	- Mascarar a entrada de dados no formato : dd/mm/aaaa hh:mm:ss

Parâmetros :
	objeto		-> Nome do campo de formulário (Usar this)
	teclapress	-> Tecla pressionada (Usar event)

Exemplo : 
	OnKeyPress    FormataDataHora(this);

Requirido :
	Função ValidaDataHora(objeto);
/**********************************************************************/ 
function FormataDataHora(objeto,teclapress)
{
	var tecla = teclapress.keyCode;

	if(((window.event.keyCode == 13) || (window.event.keyCode == 9))&&objeto.value != "")
	{
		if(!(ValidaDataHora(objeto)))
			{
				window.event.cancelBubble = true;
				window.event.returnValue = false;
				alert("Data Hora Inválida");
				objeto.value = "";
				objeto.focus();
			}
	}

	if (( tecla == 8 || tecla == 88 || tecla >= 48 && tecla <= 57 || tecla >= 96 && tecla <= 105 )&& objeto.value.length <= (19))
    {
		vr = objeto.value;
		vr = vr.replace( "/", "" );
		vr = vr.replace( "/", "" );
		vr = vr.replace( " ", "" );
		vr = vr.replace( ":", "" );
		vr = vr.replace( ":", "" );
		tam = vr.length;

		if (tam < 14)
			{
				if (tecla != 8) {tam = vr.length + 1 ;}
			}
		else
			{
				window.event.cancelBubble = true;
				window.event.returnValue = false;
			}
		
		if ((tecla == 8) && (tam > 1))
			{
				tam = tam - 1 ;
				objeto.value = vr.substr(0,tam);
				window.event.cancelBubble = true;
				window.event.returnValue = false;
			}
				if ( tam <= 2 && tecla != 8){ 
			 		objeto.value = vr ; }

				if ( (tam >= 2) && (tam <= 4) ){
			 		objeto.value = vr.substr(0, tam - 2) + ':' + vr.substr( tam - 2, 2 ); }

				if ( (tam >= 4) && (tam <= 6) ){
					objeto.value = vr.substr(0, tam - 4 ) + ':' + vr.substr( tam - 4, 2 ) + ':' + vr.substr( tam - 2, 2 ); }

				if ( (tam >= 6) && (tam <= 10) ){
					objeto.value = vr.substr(0, tam - 6 ) + ' ' + vr.substr( tam - 6, 2 ) + ':' + vr.substr( tam - 4, 2 ) + ':' + vr.substr( tam - 2, 2 ); }


				if ( (tam >= 10) && (tam <= 12) ){
					objeto.value = vr.substr(0, tam - 10 ) + '/' + vr.substr( tam - 10, 4 ) + ' ' + vr.substr( tam - 6, 2 ) + ':' + vr.substr( tam - 4, 2 ) + ':' + vr.substr( tam - 2, 2 ) ;}			

				if ( (tam >= 12) && (tam <= 14) ){
					objeto.value = vr.substr(0, tam - 12 ) + '/' + vr.substr( tam - 12, 2 ) + '/' + vr.substr( tam - 10, 4 ) + ' ' + vr.substr( tam - 6, 2 ) + ':' + vr.substr( tam - 4, 2 ) + ':' + vr.substr( tam - 2, 2 ) ;}			

				if ((tam == (14)) && tecla != 8)
					{
						if (!(ValidaDataHora(objeto)))
							{
								if(tecla >=96 && tecla <=105){tecla = tecla - 48;}
								objeto.value = objeto.value + (String.fromCharCode(tecla));
								window.event.cancelBubble = true;
								window.event.returnValue = false;
								alert("Data Hora Inválida");
								objeto.value = "";
								objeto.focus();
							}
					}
	}
	else if((window.event.keyCode != 8) && (window.event.keyCode != 9) && (window.event.keyCode != 13) && (window.event.keyCode != 35) && (window.event.keyCode != 36) && (window.event.keyCode != 46))
		{
			event.returnValue = false;
		}
}