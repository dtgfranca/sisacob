<?php
	
header("Cache-Control: no-cache",true);
header("Pragma: no-cache",true);
header("Expires: 0",true);

$cliente=$_COOKIE["cokcliente"];
$agencia=$_COOKIE["cokagencia"];
$conta=$_COOKIE["cokconta"];

function anti_injection($campo, $adicionaBarras = true){
  // remove palavras que contenham sintaxe sql
	$campo = preg_replace("/(from|alter table|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$campo);
	$campo = trim($campo);//limpa espa�os vazio
	$campo = strip_tags($campo);//tira tags html e php
	
	if($adicionaBarras || !get_magic_quotes_gpc())
		$campo = addslashes($campo);//Adiciona barras invertidas a uma string
    
	return $campo;
}

require('config.php');

$tit = $_GET['titulos'];

if ( substr($tit,(strlen($tit)-1),strlen($tit)) == ',')
   $tit = substr($tit,0,(strlen($tit)-1));

$tit = explode(",",$tit);
for ($i=0; $i<count($tit); $i++){
	$titulos .= "'".$tit[$i]."',";
}
$titulos = substr($titulos,0,(strlen($titulos)-1));

$boletos = "";
//recupera informa��es do t�tulo com base no codigo no codigo informado
$cConsulta  = "SELECT titulo, agencia, cliente, sacado, documento, sequencia, nossonumero, seunumero, ";
$cConsulta .= "DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, DATE_FORMAT(data_venc,'%d/%m/%Y') as data_venc, ";
$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') as valor, descontos, ";
$cConsulta .= "REPLACE(multa,'.',',') as multa, REPLACE(juros,'.',',') as juros, protesto, dias_protesto, ";
$cConsulta .= "data_baixa, valor_baixa, codigo_barra, instrucao, data_baixa_manual, cancelamento, ";
$cConsulta .= "criacao, desconto, devolucao, cdd_ndoc, modelo, sacador, registro, so_desconto, intervalo ";
$cConsulta .= "FROM titulos ";
$cConsulta .= "WHERE titulo in (".$titulos.") ";

$selSql = mysql_query($cConsulta);
//$aSel = mysql_fetch_array($selSql);

//pesquisa todos os titulos ligados ao codigo informado anteriormente
while ($aSel = mysql_fetch_array($selSql)){
   
   $cConsulta  = "SELECT titulo, agencia, cliente, sacado, documento, sequencia, nossonumero, seunumero, ";
   $cConsulta .= "DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, DATE_FORMAT(data_venc,'%d/%m/%Y') AS data_venc, ";
   $cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor_f, valor, ";
   $cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(descontos, 2), '.', '|'), ',', '.'), '|', ',')AS descontos , ";
   $cConsulta .= "REPLACE(multa,'.',',') AS multa, REPLACE(juros,'.',',') as juros, protesto, dias_protesto, ";
   $cConsulta .= "data_baixa, valor_baixa, codigo_barra, instrucao, data_baixa_manual, cancelamento, ";
   $cConsulta .= "criacao, desconto, devolucao, cdd_ndoc, modelo, sacador, registro, so_desconto, intervalo ";
   $cConsulta .= "FROM titulos ";
   $cConsulta .= "WHERE documento='".$aSel['documento']."' ";
   $cConsulta .= "AND agencia='".$aSel['agencia']."' ";
   $cConsulta .= "AND cliente='".$aSel['cliente']."' ";
   $cConsulta .= "AND sacado='".$aSel['sacado']."' ";
   $cConsulta .= "AND cancelamento is null ";
   $cConsulta .= "order by sequencia";

   $sql = mysql_query($cConsulta);

   //gera tabela com os dados dos titulos a serem alterados
   $boletos .= '<table border="0" cellpadding="0" cellspacing="1px" width="98%" align="center">';
   $boletos .= 	'<tr bgcolor="#CCCCCC">';
   $boletos .= 		'<td align="center">N&ordm; DOCUMENTO</td>';
   $boletos .= 		'<td align="center">NOSSO N&Uacute;MERO</td>';
   $boletos .= 		'<td align="center">DATA EMISS&Atilde;O</td>';
   $boletos .= 		'<td align="center">DATA VENCIMENTO</td>';
   $boletos .= 		'<td align="center">MULTA</td>';
   $boletos .= 		'<td align="center">JUROS</td>';
   $boletos .= 		'<td align="center">VAL. DESCONTO</td>';
   $boletos .= 		'<td align="center">VALOR FATURA</td>';
   $boletos .= 	'</tr>';
   $i=0;
   $valor_fatura = 0;
   $vencimento1 = "";
   $sequencia = "";

   while($bolSel = mysql_fetch_array($sql)){
	  if($vencimento1 == ""){
		$vencimento1 = $bolSel['data_venc'];
	  }

	  $valor_fatura += $bolSel['valor'];
	
	  if(fmod($i,2)!=0){
		$fundo="bgcolor='#F4F4F4'";
	  }else{
		$fundo= "bgcolor='#FFFFFF'";
	  }
	
	  $boletos .= 	'<tr '.$fundo.'>';
	  $boletos .= 		'<td >'.$bolSel['documento'].'/'.$bolSel['sequencia'].'</td>';
	  $boletos .= 		'<td >'.$bolSel['nossonumero'].'</td>';
	  $boletos .= 		'<td >'.$bolSel['data_emisao'].'</td>';
	  $boletos .= 		'<td >'.$bolSel['data_venc'].'</td>';
	  $boletos .= 		'<td >'.$bolSel['multa'].'%</td>';
	  $boletos .= 		'<td >'.$bolSel['juros'].'%</td>';
	  $boletos .= 		'<td align="right">R$ '.$bolSel['descontos'].'</td>';
	  $boletos .= 		'<td align="right">R$ '.$bolSel['valor_f'].'</td>';
	  $boletos .= 	'</tr>';
	  $i++;
	
	  $sequencia .="&sequencia[".$bolSel['sequencia']."]=".$bolSel['titulo'];
	
   }

   $boletos .= '</table><br><br>';
}   

$data_base = date("d/m/Y",mktime(0,0,0,date("m"),date("d"),date("y"))); 

/** ------------------------------------------------- **/


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">

table, img{behavior: url(iepngfix.htc)}

* {font: normal 10px Arial, Helvetica, sans-serif; font-weight:bold}

#alt_boletos{
font: normal 10px Arial, Helvetica, sans-serif;
font-weight:bold;
color: #333333;
width:100%; 
height:130px; 
overflow:auto; 
}
</style>
<script src="jquery-1.4.1.js" type="text/javascript"></script> 
<script src="js/fun.js" type="text/javascript"></script>
<script src="arquivos/MascaraValidacao.js" type="text/javascript"></script> 
<script src="arquivos/Mascaras.js" type="text/javascript"></script> 
<script src="arquivos/formata_dados.js" type="text/javascript"></script> 
</head>
<script>
	function fechar_altera_tit(){
		//javascript:document.getElementById('fundo').className = 'fimProcesso'; document.getElementById('alterar_tit').className= 'fimProcesso';
		parent.document.getElementById('fundo').className = 'fimProcesso';
		parent.document.getElementById('alterar_tit').className= 'fimProcesso';
	}
</script>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
	<tr>
		<td height="17" align="left" bgcolor="#F4F4F4"><img src="images/ppl3.gif" height="23" />&nbsp;<img src="images/titu28q.png" height="23" /></td>
		<td align="right" bgcolor="#F4F4F4"><img src="images/fechar.png"  onclick="fechar_altera_tit()" /></td>
	</tr>
</table>
<div width="100%" align="center" style="font-size:14px;font-weight:bold;" >BOLETOS</div>
<div id="alt_boletos" style="border:1px solid #CCCCCC;height:315px;">
	<?php echo $boletos; ?>
</div>
<div width="100%" align="center" style="font-size:14px;font-weight:bold;" >
    <input style="float:right;margin-right:20px;margin-top:10px" type="button" value="Confirma Todos" onclick="confirmaCadTitulos('<?php echo str_replace("'", "", $titulos);?>');">
</div>
</body>
</html>