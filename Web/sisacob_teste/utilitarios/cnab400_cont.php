<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<?php
require("config.php");

include("configFTP.php");

$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

	$cliente=$_COOKIE["cokcliente"];
	$agencia=$_COOKIE["cokagencia"];
	$conta=$_COOKIE["cokconta"];
	
	$file_name = $_GET['file_name'];
	
	function zero($casas,$zero){
	    while (strlen($casas)<$zero) {
		   $casas = "0".$casas;
	    }
		return $casas;
	}
	function compareDates($date1,$date2){
		$func_situa = "";
		$date1_array = explode("-",$date1);
		$date2_array = explode("-",$date2);
		$timestamp1  = strtotime($date1);
		$timestamp2  = strtotime($date2);
		if ($timestamp1>$timestamp2){
			$func_situa="maior";
		}elseif($timestamp1<$timestamp2){
			$func_situa="menor";
		}else{
			$func_situa="igual";
		}
		return $func_situa;
	}
	$val_cliente=zero($cliente,4);
	$fd = fopen ("../arquivo_temp/".$file_name, "r");
	while (!feof($fd)){
		$linha = fgets($fd, 9);
		$arq_ident=substr($linha, 0, 1);
		if($arq_ident==9){
			$arq_valido="X";
		}
	}
	fclose($fd);
	
	$fd = fopen ("../arquivo_temp/".$file_name, "r");
	
	while (!feof($fd)){
		$linha = fgets($fd, 4096);
		$linha=str_replace("'", " ", $linha);
		$arq_ident=substr($linha, 0, 1);

		if($arq_ident==0 and $arq_fim!="X" and $arq_valido=="X"){
			
			//HEADER
			//SEQ; INICIO; FINAL; TAM; DESCRIÇÃO; MASCARA; OBSERVAÇÕES
			//$arq_tipo = settype(substr($linha, 1, 1), "integer");//; 002; 002; 01; Tipo do arquivo - Remessa ; 9(001); 1
			//$arq_remessa= settype(substr($linha, 2, 7), "string");//; 003; 009; 07; Nome do Arquivo; A(007); “REMESSA” 
			//; 010; 011; 02; Tipo de Serviço; 9(002); 01
			//; 012; 019; 08; Nome do Serviço; A(008); “COBRANÇA” 

			$arq_valida= substr($linha, 0, 19);

			//; 020; 026; 07; Brancos; A(007); Brancos
			//; 027; 030; 04; Prefixo da Agência; 9(004); Código da Cooperativa
			//; 031; 031; 01; Dígito Verificador; A(001); DV da Cooperativa 
			//; 032; 039; 08; Código do Cedente; 9(008); Identificação do Cedente
			//; 040; 040; 01; Dígito Verificador; A(001); DV de identificação do cedente
			//; 041; 046; 06; Número Convenente – Líder; 9(006); Número do Convênio/ Conta Corrente
			//; 047; 076; 30; Nome do Cedente; A(030); Nome do Cedente

			$ident_banco= substr($linha, 76, 10);//; 077; 094; 18; Identificação do Banco; A(018); 001SISACOB

			//; 095; 100; 06; Data da gravação; 9(006); Data do movimento (ddmmaa)
			//; 101; 107; 07; Seqüencial da Remessa; 9(007); Número seqüencial acrescido de 1 a cada remessa. Inicia com 0000001 
			//; 108; 394; 287; Filler; A(287); Espaços em branco
			//; 395; 400; 06; No. Sequencial do registro; 9(006); Incremento a cada registro = ´000001´
			
			//print gettype($arq_valida);
			//print settype($arq_valida, "string");
			//print "$arq_valida=01REMESSA01COBRANCA";
			
			if($arq_valida!="01REMESSA01COBRANCA" or $ident_banco!="001SISACOB"){
				?>
				<SCRIPT>
					alert('ERRO COD. 01 - O arquivo <? print $file_name;?> não é valido!!!')
					history.back()
				</SCRIPT>
				<?
			}
		}elseif($arq_ident==1){
			
			$n_linhas=$n_linhas+1;
			//DETALHE
			//SEQ; INICIO; FINAL; TAM; DESCRIÇÃO; MASCARA; OBSERVAÇÕES
			$arq_tipo= substr($linha, 1, 2); //; 002; 003; 02; Tipo de inscrição do cliente ; 9(02); 01 – CPJ , 02 – CNPJ  
			$arq_cpf= substr($linha, 3, 14); //*; 004; 017; 14; CPF/CGC Cliente; 9(14); CPF/CGC do cliente sem máscara 
			$arq_agen= substr($linha, 17, 4); //; 018; 021; 04; Agência Cliente; 9(04); Cooperativa do cliente 
			$arq_dv_agen= substr($linha, 21, 1); //; 022; 022; 01; DV Agência Cliente; 9(01); DV cooperativa do cliente 
			$arq_conta= substr($linha, 23, 7); //; 023; 030; 08; Conta Corrente Cliente; 9(08); Conta Corrente do cliente
			$arq_dv_conta= substr($linha, 30, 1); //; 031; 031; 01; DV conta Cliente; X(01); DV Agência do cliente 
			$val_conta=$arq_conta."-".$arq_dv_conta;
			$arq_convenio= substr($linha, 31, 6); //; 032; 037; 06; Número do Convênio; 9(06); Convênio do cliente
			$arq_contr_cliente= substr($linha, 37, 25); //*; 038; 062; 25; Número Controle Cliente; X(25); (Controle do cliente Armazenar em seu_numero)
			$arq_nosso_num= substr($linha, 62, 11); //*; 063; 073; 11; Nosso Número; 9(11); Se comando = 01, preencher com zeros. Caso contrário, preencher com nosso número do título. [[se menor que um gerar do sistema.]]
			$arq_dv_nosso= substr($linha, 73, 1); //; 074; 074; 01; DV Nosso Número; X(01); DV Nosso Número do título no banco                    
			$arq_parcelas= substr($linha, 74, 2); //; 075; 076; 02; Número da Parcela; 9(02); Número da parcela                        
			$arq_valor= substr($linha, 76, 2); //; 077; 078; 02; Indicativo de valor de grupo; 9(02); Indicativo de valor de grupo = “00”                       
			$arq_filler= substr($linha, 78, 3); //; 079; 081; 03; Filler; X(03); 3 Espaços em branco
			$arq_ind_sacado= substr($linha, 81, 1); //; 082; 082; 01; Indicador de Sacador; X(01); A – Coloca-se nome do sacador no campo Observações
			//$arq_= substr($linha, , ); //Em branco – coloca-se a segunda instrução de recebimento no campo Observações
			$arq_prefixo_titulo= substr($linha, 82, 3); //; 083; 085; 03; Prefixo do título; X(03); Preencher com espécie do título
			$arq_variacao= substr($linha, 85, 3); //; 086; 088; 03; Variação ; 9(03); Fornecido pelo banco “000” Contrato
			$arq_conta_caucao= substr($linha, 88, 1); //; 089; 089; 01; Conta Caução; 9(01); Preencher com “0” Contrato
			$arq_cod_resp= substr($linha, 89, 5); //; 090; 094; 05; Código de Responsabilidade; 9(05); Preencher com “00000” Contrato
			$arq_dv= substr($linha, 94, 1); //; 095; 095; 01; DV; X(01); Preencher com “0” Contrato
			$arq_numero_borde= substr($linha, 95, 6); //; 096; 101; 06; Número do borderô; 9(06); Numeração do borderô do cliente
			//$arq_= substr($linha, 101, 5); //; 102; 106; 05; Filler; X(05); 5 Espaços em branco
			$arq_carteira= substr($linha, 106, 2); //; 107; 108; 02; Carteira/Modalidade; 9(02); 01 – Simples
										//02 – Garantida Vinculada
										//03 – Garantida Caucionada
			$arq_comando= substr($linha, 108, 2); //; 109; 110; 02; Comando/Movimento; 9(02); Lista 254
			$arq_seu_numero= substr($linha, 110, 10); //; 111; 120; 10; Seu Número; X(10); Número título dado pelo cedente
			$arq_venc_titulo= "20".substr($linha, 124, 2)."-".substr($linha, 122, 2)."-".substr($linha, 120, 2); //*; 121; 126; 06; Data Vencimento Título; A(06); Normal - Data Vencimento (ddmmaa) A vista – “888888” Contra apresentação – “999999”
			$arq_valor_titulo= trim(substr($linha, 126, 11)).".".substr($linha, 137, 2); //*; 127; 139; 13; Valor Titulo 9(11)V99; 9(13); Valor Nominal do Título. Se moeda diferente de real, informar o valor segundo a cotação do dia.
			$arq_num_banco= substr($linha, 139, 3); //; 140; 142; 03; Número Banco; 9(03); ‘756’ – Bancoob 
			$arq_num_agen= substr($linha, 142, 4); //; 143; 146; 04; Número Agência Cobradora ; 9(04); Número da cooperativa
			$arq_dv_agenc_cobradora= substr($linha, 146, 1); //; 147; 147; 01; DV Agência Cobradora; X(01); DV cooperativa do cliente = “0”
			$arq_especie= substr($linha, 147, 2); //; 148; 149; 02; Espécie do Título ; 9(02); Espécie do Título (lista 61)
			$arq_aceite= substr($linha, 149, 1); //; 150; 150; 01; Aceite do Título ; X(01); 1 = Com aceite, 0 = Sem aceite
			$arq_emissao= "20".substr($linha, 154, 2)."-".substr($linha, 152, 2)."-".substr($linha, 150, 2); //*; 151; 156; 06; Data emissão do Título ; 9(06); Data emissão do Título – DDMMAA
			//$arq_= substr($linha, 156, 2); //; 157; 158; 02; Primeira instrução codificada ; 9(02); Segundo tabela descrita
			//$arq_= substr($linha, 158, 2); //; 159; 160; 02; Segunda instrução ; 9(02); Segundo tabela descrita
			$arq_mora= trim(substr($linha, 160, 2)).".".substr($linha, 162, 4); //*; 161; 166; 07; Taxa de mora mês ; 9(06); 9(02)V9999
			$arq_multa= trim(substr($linha, 166, 2)).".".substr($linha, 168, 4);//*; 167; 172; 06; Taxa de multa ; 9(06); 9(02)V9999
			//$arq_= substr($linha, 172, 1); //; 173; 173; 01; Filler ; 9(01); 1 espaço em branco
			$arq_data_desc= substr($linha, 173, 6); //; 174; 179; 06; Data primeiro desconto ; 9(06); “DDMMAA”. Se for informado a data e não informado o valor, o registro será recusado.
			$arq_valor_desc= trim(substr($linha, 179, 11)).".".substr($linha, 190, 2); //; 180; 192; 13; Valor primeiro desconto ; 9(13); 9(11)V99. Se for informado o valor e não informado a data, o registro será recusado.
			$arq_iof= substr($linha, 192, 13); //; 193; 205; 13; Valor IOF / Quantidade Monetária ; 9(13); 193-193 – Código da moeda 194-205 – Quantidade Monetária   
			$arq_abatimento= trim(substr($linha, 205, 11)).".".substr($linha, 216, 2); //*; 206; 218; 13; Valor Abatimento ; 9(13); 9(11)V99
			$arq_tipo_inscr= substr($linha, 218, 2); //; 219; 220; 02; Tipo de Inscrição; 9(01); 01 – CPF, 02 – CNPJ 
			$arq_numero= substr($linha, 220, 14); //*; 221; 234; 14; Número de Inscrição; 9(14); CPF/CGC do sacado sem máscara
			$arq_sacado= substr($linha, 234, 40); //*; 235; 274; 40; Nome do Sacado; A(40); Nome do Sacado
			$arq_endereco= substr($linha, 274, 37); //*; 275; 311; 37; Endereço do Sacado; A(37); Endereço do Sacado
			$arq_bairro= substr($linha, 311, 15); //*; 312; 326; 15; Bairro do Sacado; X(15); Bairro do Sacado
			$arq_cep= substr($linha, 326, 8); //*; 327; 334; 08; CEP do Sacado; 9(08); CEP do Sacado
			$arq_cidade= substr($linha, 334, 15); //*; 335; 349; 15; Cidade do Sacado; A(15); Cidade do Sacado
			$arq_uf= substr($linha, 349, 2); //*; 350; 351; 02; UF do Sacado; A(02); UF do Sacado
			$arq_obs= substr($linha, 351, 40); //*; 352; 391; 40; Observações; X(40); Observações
			$arq_protesto= substr($linha, 391, 2); //*; 392; 393; 02; Protesto; X(02); Quantidade dias para envio protesto. Se = 0, utilizar dias protesto padrão do cliente. 
			//$arq_= substr($linha, 93, 1); //; 394; 395; 01; Filler ; X(01); Brancos
			$arq_sequencial= substr($linha, 394, 6); //; 395; 400; 06; No. Sequencial do registro; 9(06); Incrementado em 1 a cada registro 
			//print $numero;
			//99.999.999/9999-99 ou 999.999.999-99
			////Valida titulos
			if($arq_valor_titulo<=0){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]=arq_valor_titulo;
			}elseif($arq_numero=="00000000000000" or $arq_sacado==" "){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 02<\b> - Dados do sacado imcompleto.";
			}elseif($arq_agen!=$agencia){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 03<\b> - Agencia não confere.";
			}elseif($val_conta!=$conta){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 04<\b> - Conta corrente não confere.";
			}elseif(!checkdate(substr($linha, 152, 2), substr($linha, 150, 2), "20".substr($linha, 154, 2))){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 05<\b> - Data de emissão não é valida";		
			}elseif(!checkdate(substr($linha, 122, 2), substr($linha, 120, 2), "20".substr($linha, 124, 2))){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 06<\b> - Data de vencimento não é valida";		
			}elseif(compareDates($arq_emissao, $arq_venc_titulo)!="menor"){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 07<\b> - Data de emissão maior do que o vencimento";		
			}elseif($arq_nosso_num=="0000000000" and $arq_contr_cliente==" "){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_seu_numero;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 08<\b> - Controle ou nosso numero vazios";		
			}elseif($arq_nosso_num!="0000000000" and substr($arq_nosso_num, 0, 4)!=$val_cliente){
				$z=$z+1;
				$historico_sacado[$z]=$arq_sacado;
				$historico_numero[$z]=$arq_contr_cliente;
				$historico_nosso_num[$z]=$arq_nosso_num;
				$historico_motivo[$z]="<b>ERRO COD. 09<\b> - Cliente invalido.";		
			}else{
				//Cadastra / valida sacados
				if($arq_tipo_inscr!="02"){
					$arq_tipo_inscr=substr($arq_tipo_inscr, 2, 11);
				}
				
				$sql = mysql_query("select sacado from sacados where cpf='$arq_numero' and agencia='$agencia' and conta='$conta'")or die ("Não foi possível pesquisar no banco de dados");
				if(mysql_num_rows($sql)==0){

					$cliente=$_COOKIE["cokcliente"];
					$agencia=$_COOKIE["cokagencia"];
					$conta=$_COOKIE["cokconta"];

					$sql = mysql_query("insert into sacados (agencia, conta, cliente, nome, cpf, endereco, bairro, cidade, uf, cep)values('$agencia', '$conta', '$cliente', '$arq_sacado', '$arq_numero', '$arq_endereco', '$arq_bairro', '$arq_cidade', '$arq_uf', '$arq_cep')");
					$sql = mysql_query("select sacado from sacados where cpf='$arq_numero' and agencia='$agencia' and conta='$conta'")or die ("Não foi possível cadastrar no banco de dados");

					$sacados=mysql_fetch_array($sql);	
					$cod_sacado=$sacados[sacado];
					$n_clientes=$n_clientes+1;
					
				}else{
					$sacados=mysql_fetch_array($sql);	
					$cod_sacado=$sacados[sacado];
					$sql = mysql_query("update sacados set nome='$arq_sacado', cpf='$arq_numero', endereco='$arq_endereco', bairro='$arq_bairro', cidade='$arq_cidade', uf='$arq_uf', cep='$arq_cep' where sacado=$cod_sacado")or die ("Não foi possível cadastrar no banco de dados");
				}
						
				//validação com base de dados

				if($arq_nosso_num!="0000000000"){
					$sql = mysql_query("select nossonumero from titulos where cliente='$cliente' and nossonumero='$arq_nosso_num' and agencia='$agencia'");
					
					if(mysql_num_rows($sql)!=0){
						$sql = mysql_query("update titulos set sacado='$cod_sacado' where cliente='$cliente' and nossonumero='$arq_nosso_num' and agencia='$agencia' and sacado='0'");
						$z=$z+1;
						$historico_sacado[$z]=$arq_sacado;
						$historico_numero[$z]=$arq_contr_cliente;
						$historico_nosso_num[$z]=$arq_nosso_num;
						$historico_motivo[$z]="<b>ERRO COD. 10<\b> - Registro já enviado.";
						$val_continua="X";	
					}
				}elseif($arq_contr_cliente!=" "){
					$sql = mysql_query("select documento from titulos where cliente='$cliente' and seunumero='$arq_contr_cliente' and $agencia='$agencia'");
					
					if(mysql_num_rows($sql)!=0){
						$z=$z+1;
						$historico_sacado[$z]=$arq_sacado;
						$historico_numero[$z]=$arq_contr_cliente;
						$historico_nosso_num[$z]=$arq_nosso_num;
						$historico_motivo[$z]="<b>ERRO COD. 11<\b> - Numero de controle cadastrado.";
						$val_continua="X";	
					}
				}
				//procesa titulos		

				if($val_continua!="X"){
					$dados = mysql_query("select cliente, nossonumero from clientes where cliente='$cliente'")or die ("Não foi possível realizar a consulta ao banco de dados");
				
					if(mysql_num_rows($dados)>0){   
						$tupla = mysql_fetch_assoc($dados); 
						$nossonumero_base=$tupla["nossonumero"];
						$db_cliente=$tupla["cliente"];
					}

					if($arq_nosso_num=="0000000000"){
						$reg_venc=$vencimento[$cod];
						$reg_descontos=$desconto[$cod];
						$nossonumero_base=$nossonumero_base+1;
						$nossonumero_base=zero($nossonumero_base,6);
						$db_cliente=zero($db_cliente,4);
						$nossonumero=$db_cliente."".$nossonumero_base;

					}else{
						$nossonumero=$arq_nosso_num;
					}
					
					if($cod_sacado>0){
						$insere = mysql_query("insert into titulos (agencia,cliente,sacado,documento,sequencia,nossonumero,data_emisao,data_venc,valor,descontos,multa,juros,protesto,instrucao, seunumero)values('$agencia','$cliente','$cod_sacado','$arq_seu_numero','$arq_sequencial','$nossonumero','$arq_emissao','$arq_venc_titulo','$arq_valor_titulo','$arq_valor_desc','$arq_multa','$arq_mora','$arq_protesto','$arq_obs','$arq_contr_cliente');") or die ("Não foi possível registrar o boleto");
						$atualiza = mysql_query("update clientes set nossonumero= '$nossonumero_base' where cliente='$cliente';")or die ("Não foi possível realizar a atualização do banco de dados");
					}else{
						$historico_motivo[$z]="<b>ERRO COD. 12<\b> - Não foi possivel cadastrar o sacado.";
					}
					
					$arq_valor_titulo=number_format($arq_valor_titulo, 2, '.', '');
					$valor_total=$valor_total+ $arq_valor_titulo;
					$n_titulos=$n_titulos+1;
				}
				$val_continua="";
			}
		}elseif($arq_ident==9){
			
			$arq_fim="X";
		}
	}
	fclose ($fd);
	
	ftp_delete($conn_id, $caminhoTemp.$file_name);
?>



<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
}
-->

<!--
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: italic;
}
-->
</style>
 <?

print "<table width='100%' cellpadding='0' cellspacing='0'>";
print "<tr><td colspan='4' bgcolor='#CCCCCC' align='left'><p class='style1'>";
print $n_linhas." titulo(s) lido(s) do arquivo";
print "</td></tr>";
	
if($n_clientes>0){
	print "<tr> <td colspan='4' bgcolor='#999999' align='left'><p class='style1'>";
	print $n_clientes." novo(s) cliente(s) cadastrados<br />";
	print "</td></tr>";
}

if($n_titulos>0){
	print "<tr> <td colspan='4' bgcolor='#33FF99' align='left'><p class='style1'>";
	print $n_titulos ." titulo(s) processado(s) Totalizando R$ ".number_format($valor_total, 2, ',', '.');
	print "</td></tr>";
}

if(!empty($z)){
	print "<tr> <td colspan='4' bgcolor='#FF3300' align='left'><p class='style1'> $z erro(s) resportado(s)</td></tr>";
	print "<tr bgcolor='#CCCCCC'>";
		print "<td valign='top'><p class='style1'>Sacado</td>";
		print "<td valign='top' align='center'><p class='style1'>Controle</td>";
		print "<td valign='top' align='center'><p class='style1'>Nosso Numero</td>";
		print "<td valign='top' align='center'><p class='style1'>Erro</td>";
	print "</tr>";
	foreach($historico_sacado as $cod => $sacado){
		print "<tr>";
			print "<td valign='top'><p class='style2'>$sacado</td>";
			print "<td valign='top' align='center'><p class='style2'>".$historico_numero[$cod]."</td>";
			print "<td valign='top' align='center'><p class='style2'>".$historico_nosso_num[$cod]."</td>";
			print "<td valign='top'><p class='style2'>".$historico_motivo[$cod]."</td>";
		print "</tr>";
	}
}
print "<table>";
?>
<form>
<input type="button" value="Imprime hitórico" onClick="window.print()" />
</form>
</span></div>











