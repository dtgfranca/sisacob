<?php
require('config.php');
	$cliente=$_COOKIE["cokcliente"];
	//Valoração inicial
	list ($dia,$mes,$ano) = split ('/',$inicio);
	$p1="$dia-$mes-$ano";
	list ($dia,$mes,$ano) = split ('/',$fim);
	$p2="$dia-$mes-$ano";

header('Content-type: application/notepad');
//header('Content-Disposition: attachment; filename="COB-'.$cliente.' periodo '.$p1.' a '.$p2.'.ret"');
header('Content-Disposition: attachment; filename="COB-'.$cliente.'.ret"');

$destino 	 = $_POST["destino"];
$cod_cliente = $_POST["cod_cliente"];
$inicio 	 = $_POST["inicio"];
$fim 		 = $_POST["fim"];
$opcao1 	 = $_POST["opcao1"];
$opcao2 	 = $_POST["opcao2"];
$opcao3 	 = $_POST["opcao3"];
$opcao4 	 = $_POST["opcao4"];
$opcao5 	 = $_POST["opcao5"];

require("config.php");
	$cliente=$_COOKIE["cokcliente"];
	$agencia=$_COOKIE["cokagencia"];
	$conta=$_COOKIE["cokconta"];	

	function zero($casas,$zero){
	   while (strlen($casas)<$zero){
		   $casas = "0".$casas;
	   }
	   return $casas;
	}

	function alfa($casas,$alfa){
	   while (strlen($casas)<$alfa) {
		   $casas = $casas." ";
	   }
	   return $casas;
	}

function heard(){
	$linha="";								//SEQ-INICIO-FINAL-TAM-DESCRIÇÃO-MASCARA-OBSERVAÇÕES
	$linha=$linha."0";				//-001-001-01-Identificação Registro-9(001)-“0” – Registro Header
	$linha=$linha."".zero('2',1);		//-002-002-01-Tipo do arquivo = retorno -9(001)-2
	$linha=$linha.alfa('RETORNO',7);	//-003-009-07-Nome do Arquivo-A(007)-“RETORNO” 
	$linha=$linha."".zero('1',2);//-010-011-02-Tipo do Serviço-9(002)-01
	$linha=$linha."".alfa('COBRANCA',8);//-012-019-08-Nome do Serviço-A(008)-“COBRANCA” 
	$linha=$linha."".alfa('',7);//-020-026-07-Brancos-A(007)-Brancos
	$linha=$linha."".zero('2',4);//-027-030-04-Prefixo da Agência-9(004)-Código da Cooperativa
	$linha=$linha."".alfa('',1);//-031-031-01-Brancos -A(001)-Brancos 
	$linha=$linha."".zero('2',8);//-032-039-08-Código do Cedente-9(008)-Identificação do Cedente
	$linha=$linha."".alfa('',1);//-040-040-01-Dígito Verificador-A(001)-DV de identificação do cedente
	$linha=$linha."".alfa('',6);//-041-046-06-Brancos-A(006)-Brancos
	$linha=$linha."".alfa('',30);//-047-076-30-Nome do Cedente-A(030)-Nome do Cedente
	$linha=$linha."".alfa('001SISACOB',18);//-077-094-18-Identificação do Banco-A(018)-“001SISACOB     ”
	$linha=$linha."".zero('2',06);//-095-100-06-Data da gravação-9(006)-Data do movimento (ddmmaa)
	$linha=$linha."".zero('2',07);//-101-107-07-Seqüencial do RETORNO-9(007)-Número seqüencial acrescido de 1 a cada RETORNO. Inicia com 0000001 
	$linha=$linha."".alfa('COBRANCA',287);//-108-394-287-Filler-A(287)-Espaços em branco
	$linha=$linha."".zero('',6);//-395-400-06-No. Sequencial do registro-9(006)-Incremento a cada registro = ´000001´
	$linha=$linha.chr(13); 
	$linha=$linha.chr(10); 
	return $linha;
}

function trailler($incremental){
	$linha="";
	//SEQ-INICIO-FINAL-TAM-DESCRIÇÃO-MASCARA-OBSERVAÇÕES
	$linha=$linha."".zero('9',1);//-001-001-01-Identificação Registro-9(01)-“9” – Registro Trailler
	$linha=$linha."".zero('',2);//-002-003-02-Identificação Serviço -9(02)-02 – Retorno 
	$linha=$linha."".zero('',3);//-004-006-03-Número do Banco -9(03)-001
	$linha=$linha."".zero('',4);//-007-010-04-Agência -9(04)-Código da Cooperativa Remetente
	$linha=$linha."".alfa('',25);//-011-035-25-Sigla da Agência-A(25)-Sigla da Cooperativa Remetente
	$linha=$linha."".alfa('',50);//-036-085-50-Endereço da Agência-A(50)-Endereço da Cooperativa Remetente
	$linha=$linha."".alfa('',30);//-086-115-30-Bairro Agência-A(30)-Bairro da Cooperativa Remetente
	$linha=$linha."".alfa('',8);//-116-123-08-CEP Agência-A(08)-CEP da Cooperativa Remetente
	$linha=$linha."".alfa('',30);//-124-153-30-Cidade da Agência-A(30)-Cidade da Cooperativa Remetente
	$linha=$linha."".alfa('',2);//-154-155-02-Estado da Agência-A(02)-UF da Cooperativa Remetente
	$linha=$linha."".zero('',8);//-156-163-08-Data do movimento-9(08)-Data do movimento (aaaammdd)
	$linha=$linha."".zero($incremental,8);//-164-171-08-Quantidade de Registros-9(08)-Quantidade de registros detalhe
	$linha=$linha."".alfa('',11);//-172-182-11-Brancos-X(11)-Brancos
	$linha=$linha."".alfa('',212);//-183-394-212-Brancos-X(212)-Brancos
	$linha=$linha."".zero('',6);//-395-400-06-No. Sequencial do registro-9(06)-Incrementado em 1 a cada registro
	$linha=$linha.chr(13); 
	$linha=$linha.chr(10); 
	return $linha;
}

function detalhe($agencia,$conta,$digito,$seunumero,$nossonumero,$tipo,$data_mov,$data_venc,$valor,$data_credito,$valor_rec,$sac_cpf,$incremental){
	$linha="";
	$linha=$linha."".zero('1',1);//-001-001-01-Identificação Registro-9(01)-“1” – Registro Detalhe
	$linha=$linha."".alfa('',2);//-002-003-02-Brancos -X(02)-Brancos  
	$linha=$linha."".zero('',14);//-004-017-14-CPF/CGC Cliente-9(14)-CPF/CGC do cliente sem máscara 
	$linha=$linha."".zero($agencia,4);//-018-021-04-Agência Cliente-9(04)-Cooperativa do cliente 
	$linha=$linha."".alfa('',1);//-022-022-01-Brancos-X(01)-Brancos 
	$linha=$linha."".zero($conta,8);//-023-030-08-Conta Corrente Cliente-9(08)-Conta Corrente do cliente
	$linha=$linha."".alfa($digito,1);//-031-031-01-DV conta Cliente-X(01)-DV Agência do cliente 
	$linha=$linha."".alfa('',6);//-032-037-06-Brancos-X(06)-Brancos
	$linha=$linha."".alfa($seunumero,25);//-038-062-25-Número Controle Cliente-X(25)-Controle do cliente – seunumero
	$linha=$linha."".zero($nossonumero,11);//-063-073-11-Nosso Número-9(11)-Nosso número do título.                       
	$linha=$linha."".alfa('',1);//-074-074-01-Brancos-X(01)-Brancos                        
	$linha=$linha."".alfa('',2);//-075-076-02-Brancos-X(02)-Brancos
	$linha=$linha."".alfa('',4);//-077-080-04-Brancos-X(04)-Brancos
	$linha=$linha."".zero($tipo,2);//-081-082-02-Código do Tipo Lançamento-9(02)-“01” – Inclusão
								//“02” – Baixa no Banco
								//“03” – Baixa no Estabelecimento
								//“04” – Cancelamento do Título
								//“05” – Desconto 
	$linha=$linha."".alfa('',3);//-083-085-03-Brancos-X(03)-Brancos
	$linha=$linha."".alfa('',3);//-086-088-03-Brancos-X(03)-Brancos
	$linha=$linha."".alfa('',1);//-089-089-01-Brancos-X(01)-Brancos
	$linha=$linha."".alfa('',5);//-090-094-05-Brancos-X(05)-Brancos
	$linha=$linha."".alfa('',1);//-095-095-01-Brancos-X(01)-Brancos
	$linha=$linha."".alfa('',5);//-096-100-05-Brancos-X(05)-Brancos
	$linha=$linha."".alfa('',5);//-101-105-05-Brancos-X(05)-Brancos
	$linha=$linha."".alfa('',1);//-106-106-01-Brancos-X(01)-Brancos
	$linha=$linha."".alfa('',2);//-107-108-02-Brancos-X(02)-Brancos
	$linha=$linha."".alfa('',2);//-109-110-02-Brancos-X(02)-Brancos
	$linha=$linha."".zero($data_mov,6);//-111-116-06-Data da Movimentação-9(06)- (ddmmaa)
	$linha=$linha."".alfa('',10);//-117-126-10-Brancos-X(10)-Brancos
	$linha=$linha."".alfa('',20);//-127-146-20-Brancos-X(20)-Brancos
	$linha=$linha."".zero($data_venc,6);//-147-152-06-Data Vencimento Título-A(06)-Ddmmaa
	$linha=$linha."".zero($valor,13);//-153-165-13-Valor Titulo 9(11)V99-9(13)-Valor Nominal do Título. 
	$linha=$linha."".alfa('',3);//-166-168-03-Brancos-X(03)-Brancos 
	$linha=$linha."".alfa('',4);//-169-172-04-Brancos -X(04)-Brancos
	$linha=$linha."".alfa('',1);//-173-173-01-Brancos-X(01)-Brancos
	$linha=$linha."".alfa('',2);//-174-175-02-Brancos -X(02)-Brancos
	$linha=$linha."".zero($data_credito,6);//-176-181-06-Data do crédito -9(06)-Ddmmaa
	$linha=$linha."".alfa('',7);//-182-188-07-Brancos -X(07-Brancos
	$linha=$linha."".alfa('',13);//-189-201-13-Brancos -X(13)-Brancos
	$linha=$linha."".alfa('',13);//-202-214-13-Brancos-X(13)-Brancos
	$linha=$linha."".alfa('',13);//-215-227-13-Brancos-X(13)-Brancos
	$linha=$linha."".zero('',13);//-228-240-13-Abatimento -9(11)V99-Abatimento
	$linha=$linha."".zero($desc_conc,13);//-241-253-13-Desconto Concedido -9(11)V99-Desconto Concedido
	$linha=$linha."".zero($valor_rec,13);//-254-266-13-Valor Recebido-9(11)V99-Valor Recebido
	$linha=$linha."".zero('',13);//-267-279-13-Juros de Mora -9(11)V99-Juros de Mora
	$linha=$linha."".zero('',13);//-280-292-13-Outros recebimentos -9(11)V99-Outros recebimentos
	$linha=$linha."".zero('',13);//-293-305-13-Abatimento não aproveitado-9(11)V99-Abatimento não aproveitado
	$linha=$linha."".zero('',13);//-306-318-13-Valor do Lançamento -9(11)V99-Crédito ou débito
	$linha=$linha."".zero('',1);//-319-319-01-Indicativo débito/crédito -9(01)-Indicativo débito/crédito
	$linha=$linha."".alfa('',1);//-320-320-01-Brancos-X(01)-Brancos
	$linha=$linha."".alfa('',12);//-321-332-12-Brancos-X(12)-Brancos
	$linha=$linha."".alfa('',10);//-333-342-10-Brancos-X(10)-Brancos
	$linha=$linha."".zero($sac_cpf,14);//-343-357-52-CPF/CNPJ -9(14)-CPF/CNPJ do sacado (Pode ser 0)
	$linha=$linha."".zero('',38);//-358-394-38-Zeros -X(52)-Zeros
	$linha=$linha."".zero($incremental,6);//-395-400-06-No. Sequencial do registro-9(06)-Incrementado em 1 a cada registro 
	$linha=$linha.chr(13); 
	$linha=$linha.chr(10); 
	return $linha;
}

//Valoração inicial
list ($dia,$mes,$ano) = split ('/',$inicio);
$inicio="'$ano-$mes-$dia'";
list ($dia,$mes,$ano) = split ('/',$fim);
$fim="'$ano-$mes-$dia'";
$incremental=0;
//Coleta informação do cliente
	$cliente=$_COOKIE["cokcliente"];
	$sql = mysql_query("select * from clientes where cliente='$cliente';")or die ("Não foi ler os dados do cliente");
	$clientes=mysql_fetch_array($sql);
	list ($conta, $digito, $ano) = split ('-', $clientes["conta"]);
//Corpo do arquivo

print heard();
//Tipo Lançamento-9(02)-“01” – Inclusão
if($opcao5=="X"){
$tipo="1";
$cliente=$_COOKIE["cokcliente"];
$sql = mysql_query("select sacado, seunumero, nossonumero, DATE_FORMAT(titulos.data_venc, '%d%m%y') as data_venc, DATE_FORMAT(date(data_credito), '%d%m%y') as data_credito, cast(valor*100 as SIGNED) as valor, cast(valor_baixa*100 as SIGNED) as valor_rec, DATE_FORMAT(date(criacao), '%d%m%y') as data_mov from titulos where cliente='$cliente' and date(criacao)>=$inicio and date(criacao)<=$fim;")or die ("Não foi possível realizar a consulta ao banco de dados");
		if(mysql_num_rows($sql)>0){
			while ($titulos=mysql_fetch_array($sql)) {
				$sacado=$titulos["sacado"];
				$sql2 = mysql_query("select REPLACE(REPLACE(REPLACE(cpf, '.', ''), '-', ''), '/', '') as cpf from sacados where sacado='$sacado';");
				$sacados=mysql_fetch_array($sql2);
				$incremental=$incremental+1;
				print detalhe($agencia,$conta,$digito,$titulos["seunumero"],$titulos["nossonumero"],$tipo,$titulos["data_mov"],$titulos["data_venc"], $titulos["valor"], $titulos["data_credito"],$titulos["valor_rec"],$sacados["cpf"],$incremental);
			} 
		}
}
//Tipo Lançamento-9(02)-“02” – Baixa no Banco
if($opcao1=="X"){
$tipo="2";
$cliente=$_COOKIE["cokcliente"];
$sql = mysql_query("select sacado, seunumero, nossonumero, DATE_FORMAT(titulos.data_venc, '%d%m%y') as data_venc, DATE_FORMAT(date(data_credito), '%d%m%y') as data_credito, cast(valor*100 as SIGNED) as valor, cast(valor_baixa*100 as SIGNED) as valor_rec, DATE_FORMAT(date(data_baixa), '%d%m%y') as data_mov from titulos where cliente='$cliente' and date(data_baixa)>=$inicio and date(data_baixa)<=$fim;")or die ("Não foi possível realizar a consulta ao banco de dados");
    if(mysql_num_rows($sql)>0){
		while ($titulos=mysql_fetch_array($sql)) {
			$sacado=$titulos["sacado"];
			$sql2 = mysql_query("select REPLACE(REPLACE(REPLACE(cpf, '.', ''), '-', ''), '/', '') as cpf from sacados where sacado='$sacado';");
			$sacados=mysql_fetch_array($sql2);
			$incremental=$incremental+1;
			print detalhe($agencia,$conta,$digito,$titulos["seunumero"],$titulos["nossonumero"],$tipo,$titulos["data_mov"],$titulos["data_venc"], $titulos["valor"], $titulos["data_credito"],$titulos["valor_rec"],$sacados["cpf"],$incremental);
		} 
	}
}
//Tipo Lançamento-9(02)-//“03” – Baixa no Estabelecimento

if($opcao2=="X"){
$tipo="3";
$cliente=$_COOKIE["cokcliente"];
$sql = mysql_query("select sacado, seunumero, nossonumero, DATE_FORMAT(titulos.data_venc, '%d%m%y') as data_venc, DATE_FORMAT(date(data_credito), '%d%m%y') as data_credito, cast(valor*100 as SIGNED) as valor, cast(valor_baixa*100 as SIGNED) as valor_rec, DATE_FORMAT(date(data_baixa_manual), '%d%m%y') as data_mov from titulos where cliente='$cliente' and date(data_baixa_manual)>=$inicio and date(data_baixa_manual)<=$fim;")or die ("Não foi possível realizar a consulta ao banco de dados");
    if(mysql_num_rows($sql)>0){
		while ($titulos=mysql_fetch_array($sql)) {
			$sacado=$titulos["sacado"];
			$sql2 = mysql_query("select REPLACE(REPLACE(REPLACE(cpf, '.', ''), '-', ''), '/', '') as cpf from sacados where sacado='$sacado';");
			$sacados=mysql_fetch_array($sql2);
			$incremental=$incremental+1;
			print detalhe($agencia,$conta,$digito,$titulos["seunumero"],$titulos["nossonumero"],$tipo,$titulos["data_mov"],$titulos["data_venc"], $titulos["valor"], $titulos["data_credito"],$titulos["valor_rec"],$sacados["cpf"],$incremental);
		} 
	}
}
//Tipo Lançamento-9(02)-//“04” – Cancelamento do Título

if($opcao3=="X"){
	$tipo="4";
	$cliente=$_COOKIE["cokcliente"];
	$sql = mysql_query("select sacado, seunumero, nossonumero, DATE_FORMAT(titulos.data_venc, '%d%m%y') as data_venc, DATE_FORMAT(date(data_credito), '%d%m%y') as data_credito, cast(valor*100 as SIGNED) as valor, cast(valor_baixa*100 as SIGNED) as valor_rec, DATE_FORMAT(date(cancelamento), '%d%m%y') as data_mov from titulos where cliente='$cliente' and date(cancelamento)>=$inicio and date(cancelamento)<=$fim;")or die ("Não foi possível realizar a consulta ao banco de dados");
    if(mysql_num_rows($sql)>0){
		while ($titulos=mysql_fetch_array($sql)){
			$sacado=$titulos["sacado"];
			$sql2 = mysql_query("select REPLACE(REPLACE(REPLACE(cpf, '.', ''), '-', ''), '/', '') as cpf from sacados where sacado='$sacado';");
			$sacados=mysql_fetch_array($sql2);
			$incremental=$incremental+1;
			print detalhe($agencia,$conta,$digito,$titulos["seunumero"],$titulos["nossonumero"],$tipo,$titulos["data_mov"],$titulos["data_venc"], $titulos["valor"], $titulos["data_credito"],$titulos["valor_rec"],$sacados["cpf"],$incremental);
		} 
	}
}



//Tipo Lançamento-9(02)-///“05” – Desconto 

if($opcao4=="X"){
$tipo="5";
$cliente=$_COOKIE["cokcliente"];
$sql = mysql_query("select sacado, seunumero, nossonumero, DATE_FORMAT(titulos.data_venc, '%d%m%y') as data_venc, DATE_FORMAT(date(data_credito), '%d%m%y') as data_credito, cast(valor*100 as SIGNED) as valor, cast(valor_baixa*100 as SIGNED) as valor_rec, DATE_FORMAT(date(desconto), '%d%m%y') as data_mov from titulos where cliente='$cliente' and date(desconto)>=$inicio and date(desconto)<=$fim;")or die ("Não foi possível realizar a consulta ao banco de dados");
    if(mysql_num_rows($sql)>0){
		while ($titulos=mysql_fetch_array($sql)) {
			$sacado=$titulos["sacado"];
			$sql2 = mysql_query("select REPLACE(REPLACE(REPLACE(cpf, '.', ''), '-', ''), '/', '') as cpf from sacados where sacado='$sacado';");
			$sacados=mysql_fetch_array($sql2);
			$incremental=$incremental+1;
			print detalhe($agencia,$conta,$digito,$titulos["seunumero"],$titulos["nossonumero"],$tipo,$titulos["data_mov"],$titulos["data_venc"], $titulos["valor"], $titulos["data_credito"],$titulos["valor_rec"],$sacados["cpf"],$incremental);
		} 
	}
}

print trailler($incremental);

?>