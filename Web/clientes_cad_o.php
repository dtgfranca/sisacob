<?php

	require('config.php');

	$cookie = unserialize(base64_decode($HTTP_COOKIE_VARS["SISACOB"]));

	$cliente=$cookie["cokcliente"];

	$agencia=$cookie["cokagencia"];
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.autocomplete.css" />
<script type='text/javascript' src='js/autocomplete/lib/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='js/autocomplete/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='js/autocomplete/jquery.autocomplete.js'></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Untitled Document</title>



<style type="text/css">

table, img{behavior: url(iepngfix.htc)}

</style>

</head>

<script language="JavaScript" type="text/javascript" src="arquivos/MascaraValidacao.js"></script> 


<script language="JavaScript" type="text/JavaScript">

<!--

function bt_clientes_novo() {

	botaoNovo();
	limparCampos();
	
}

// -->

</script>



<script language="JavaScript" type="text/JavaScript">

<!--

function bt_clientes(){

		if (document.cad_clientes.nome.value=="") { 
			alert("O campo Nome é obrigatorio"); 
			document.getElementById('nome').focus(); 
			return false;
		}

		if (document.cad_clientes.cep.value=="") { 
			alert("O campo CEP é obrigatorio"); 
			document.getElementById('cep').focus(); 
			return false;
		}else if( !ValidaCep(document.cad_clientes.cep) ){
			document.getElementById('cep').focus(); 
			return false;
		}
		
		var nome = document.getElementById('nome').value;
		var iden = document.getElementById('iden').value;
		var cpf = document.getElementById('cpf').value;
		var cnpj = document.getElementById('cnpj').value;
		var endereco = document.getElementById('endereco').value;
		var bairro = document.getElementById('bairro').value;
		var cidade = document.getElementById('cidade').value;
		var uf = document.getElementById('uf').value;
		var cep = document.getElementById('cep').value;
		var telef_pess = document.getElementById('telef_pess').value;
		var celular = document.getElementById('celular').value;
		var telef_com = document.getElementById('telef_com').value;
		var fax = document.getElementById('fax').value;
		var email = document.getElementById('email').value;
		
		if(cpf == ""){
			cpf = cnpj;
		}
		
		if(cpf != ""){
			if(!valida_cpfcnpj(cpf)){
				return false;
			}
		}
		
		$.ajax
		(
		{
			type: 'GET',
			url: "php/metodos.php",
			data: "metodo=novoCliente&nome="+nome+"&iden="+iden+"&cpf="+cpf+"&endereco="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&cep="+cep+"&telefpess="+telef_pess+"&celular="+celular+"&telefcom="+telef_com+"&fax="+fax+"&email="+email,
			success: function(retorno)
			{
				if(retorno == "ok")
				{
					alert("Cadastro realizada com sucesso!");
					envia_busca("");
					limparCampos();
				}
				else
				{
					alert("Cadastro NÃO realizado.");
				}
				
			}
		}
		);

    }
	
function valida_cpfcnpj(cpf_cnpj)
{
	var tam = cpf_cnpj.length;
	var tamt = -1;
	
	while(tam != tamt){
		tamt = tam;
		cpf_cnpj = cpf_cnpj.replace(".","");
		cpf_cnpj = cpf_cnpj.replace("/","");
		cpf_cnpj = cpf_cnpj.replace("-","");
		tam = cpf_cnpj.length;
	}
	
	if(tam < 11)
	{
		alert("CPF/CNPJ menor que 11 digitos.");
		return false;
	}
	else if(tam == 11)
	{
		if(!valida_cpf(cpf_cnpj))
		{
			alert("CPF Invalido");
			return false;
		}
	}
	else if(tam < 14)
	{
		alert("CPF/CNPJ Didgitos incorretos.");
		return false;
		
	}
	else if(tam == 14)
	{
		if(!valida_cnpj(cpf_cnpj))
		{
			alert("CNPJ Invalido");
			return false;
		}
	}
	return true;
}

function valida_cnpj(cnpj)
{
	var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
	digitos_iguais = 1;
	
	if (cnpj.length < 14 && cnpj.length < 15)
		return false;
		
	for (i = 0; i < cnpj.length - 1; i++)
		if (cnpj.charAt(i) != cnpj.charAt(i + 1))
		{
			digitos_iguais = 0;
			break;
		}
	if (!digitos_iguais)
	{
		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0,tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--)
		{
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;
			
		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--)
		{
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
			pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;
			
		return true;
	}
	else
		return false;
} 

function valida_cpf(cpf)
{
	var numeros, digitos, soma, i, resultado, digitos_iguais;
	digitos_iguais = 1;
	
	if (cpf.length < 11)
		return false;
		
	for (i = 0; i < cpf.length - 1; i++)
		if (cpf.charAt(i) != cpf.charAt(i + 1))
		{
			digitos_iguais = 0;
			break;
		}
	if (!digitos_iguais)
	{
		numeros = cpf.substring(0,9);
		digitos = cpf.substring(9);
		soma = 0;
		for (i = 10; i > 1; i--)
			soma += numeros.charAt(10 - i) * i;
			
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;
			
		numeros = cpf.substring(0,10);
		soma = 0;
		for (i = 11; i > 1; i--)
			soma += numeros.charAt(11 - i) * i;
			
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;
			
		return true;
	}
	else
	return false;
}
</script>



<script language="JavaScript" type="text/JavaScript">

<!--

function bt_clientes_alt(){

		if (document.cad_clientes.nome.value=="") { 
			alert("O campo Nome é obrigatorio"); 
			document.getElementById('nome').focus();
			return false;
		}
		
		if (document.cad_clientes.cep.value=="") { 
			alert("O campo CEP é obrigatorio"); 
			document.getElementById('cep').focus(); 
			return false;
		}else if( !ValidaCep(document.cad_clientes.cep) ){
			document.getElementById('cep').focus(); 
			return false;
		}
		
		
		var sacado = document.getElementById('hid_sacado').value;
		var nome = document.getElementById('nome').value;
		var iden = document.getElementById('iden').value;
		var cpf = document.getElementById('cpf').value;
		var cnpj = document.getElementById('cnpj').value;
		var endereco = document.getElementById('endereco').value;
		var bairro = document.getElementById('bairro').value;
		var cidade = document.getElementById('cidade').value;
		var uf = document.getElementById('uf').value;
		var cep = document.getElementById('cep').value;
		var telef_pess = document.getElementById('telef_pess').value;
		var celular = document.getElementById('celular').value;
		var telef_com = document.getElementById('telef_com').value;
		var fax = document.getElementById('fax').value;
		var email = document.getElementById('email').value;
		
		if(cpf == ""){
			cpf = cnpj;
		}
		
		if(cpf != ""){
			if(!valida_cpfcnpj(cpf)){
				return false;
			}
		}
		
		$.ajax
		(
		{
			type: 'GET',
			url: "php/metodos.php",
			data: "metodo=alterarCliente&sacado="+sacado+"&nome="+nome+"&iden="+iden+"&cpf="+cpf+"&endereco="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&cep="+cep+"&telefpess="+telef_pess+"&celular="+celular+"&telefcom="+telef_com+"&fax="+fax+"&email="+email,
			success: function(retorno)
			{
				if(retorno == "ok")
				{
					alert("Alteração realizada com sucesso!");
					envia_busca("");
					limparCampos();
					botaoNovo();
				}
				else
				{
					alert("Alteração NÃO realizada.");
				}
				
			}
		}
		);
		
    }

</script>



<script language="JavaScript" type="text/JavaScript">

<!--

	function bt_excluir(){

	if(confirm("Deseja realmente excluir o cadastro????")){
		var sacado = document.getElementById('hid_sacado').value;
		$.ajax
		(
		{
			type: 'GET',
			url: "php/metodos.php",
			data: "metodo=excluirCliente&sacado="+sacado,
			success: function(retorno)
			{
				if(retorno == "ok"){
					alert("Cliente excluído com sucesso!");
					envia_busca("");
					limparCampos();
					botaoNovo();
				}else{
					alert("Cliente NÃO excluído.");
				}
			}
		}
		);

	}else{

		return false

	}

}
///////////Rotina par limpar os campos do formulario de cadastro/////////////////
function limparCampos(){
	document.getElementById('nome').value = "";
	document.getElementById('iden').value = "";
	document.getElementById('cpf').value = "";
	document.getElementById('cnpj').value = "";
	document.getElementById('endereco').value = "";
	document.getElementById('bairro').value = "";
	document.getElementById('cidade').value = "";
	document.getElementById('uf').value = "";
	document.getElementById('cep').value = "";
	document.getElementById('telef_pess').value = "";
	document.getElementById('celular').value = "";
	document.getElementById('telef_com').value = "";
	document.getElementById('fax').value = "";
	document.getElementById('email').value = "";
	document.getElementById('cpf').removeAttribute('disabled',false);
	document.getElementById('cpf').style.background = "";
	document.getElementById('cnpj').removeAttribute('disabled',false);
	document.getElementById('cnpj').style.background = "";
}
///////////////////////////////////////////////
function cpfOUcnpj(este,outro){
	if(este.value != ""){
		outro.setAttribute('disabled',true);
		outro.style.background = "#cccccc";
	}else{
		outro.removeAttribute('disabled',false);
		outro.style.background = "";
	}
}

/*----------------------------------------------------------------------------
Formatação para qualquer mascara
-----------------------------------------------------------------------------*/
function formatar(src, mask){
  var i = src.value.length;
  var saida = mask.substring(0,1);
  var texto = mask.substring(i)
if (texto.substring(0,1) != saida)
  {
    src.value += texto.substring(0,1);
  }
}

function botaoAlterar(sacado){

	document.getElementById('hid_sacado').value = sacado;
	
	$.ajax
	(
	{
		type: 'GET',
		url: "php/metodos.php",
		data: "metodo=botaoExcluir&sacado="+sacado,
		success: function(retorno)
		{
			
			var botoes = "";
			botoes += '<table border="0" cellpadding="4" cellspacing="0" width="100%" id="table1" align="center">';
			botoes += '	<tr>';
			botoes += '		<td bgcolor="#F4F4F4"><p align="center">';
			botoes += '			<input type="button" value="Alterar" name="op" onclick="bt_clientes_alt()" style="width: 150; height: 20" />';
			botoes += retorno;
			botoes += '			<input type="button" value="Novo sacado" name="op3" onclick="bt_clientes_novo()" style="width: 150; height: 20" />';
			botoes += '			<input type="hidden"  value="alterar" name="acao" /></p>';
			botoes += '		</td>';
			botoes += '	</tr>';
			botoes += '</table>';
			
			document.getElementById('cadCli_botao').innerHTML = botoes;
			
		}
	}
	);
}

function botaoNovo(){
	var botoes = "";
	botoes += '<table border="0" cellpadding="4" cellspacing="0" width="100%" id="table1" align="center">';
	botoes += '	<tr>';
	botoes += '		<td bgcolor="#F4F4F4">';
	botoes += '			<p align="center">';
	botoes += '				<input type="button" value="Cadastrar Sacado" name="op" onclick="bt_clientes()" style="width: 150; height: 20" />';
	botoes += '			</p>';
	botoes += '		</td>';
	botoes += '	</tr>';
	botoes += '</table>';
	
	document.getElementById('hid_sacado').value = "";
	document.getElementById('cadCli_botao').innerHTML = botoes;
}

/** "Roda" o autocomplete **/
jQuery(document).ready(function() {	
	jQuery("#nome").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'nome'} });
	jQuery("#iden").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'identidade'} });
	jQuery("#cpf").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'cpf'} });
	jQuery("#endereco").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'endereco'} });	
	jQuery("#bairro").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'bairro'} });	
	jQuery("#cidade").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'cidade'} });	
	jQuery("#uf").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'uf'} });	
	jQuery("#cep").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'cep'} });	
	jQuery("#telef_pess").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'telefonePessoal'} });	
	jQuery("#celular").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'celular'} });	
	jQuery("#telef_com").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'telefoneComercial'} });	
	jQuery("#fax").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'fax'} });
	jQuery("#email").autocomplete("inc/funcs_autocomplete.php", { width: 260, selectFirst: false, extraParams : {'parametro' : 'email'} });	
});

</script>
<body>
<div style="background-color: #F4F4F4; width:500px"><img src="images/ppl2.gif" />&nbsp;<img src="images/titu1q.png"/></div>
<form method="post" name="cad_clientes" action="">
<table border="0" cellpadding="0" cellspacing="4" width="70%">
<tr>
    <td colspan="2">Nome</td>
    <td>Identidade</td>
    <td>CPF</td>
	<td>CNPJ</td>
</tr>
<tr>
    <td colspan="2">
		<input type="text" name="nome" id="nome" size="41" maxlength="50" style="text-transform: uppercase;" />
		<input type="hidden" name="hid_sacado" id="hid_sacado" value="" >
	</td>
    <td>
		<input type="text" name="iden" id="iden" value="<? print $id;?>" size="10" style="text-transform: uppercase;" />
	</td>
	<td>
		<input type="text" name="cpf" id="cpf" onkeyup="javascript: cpfOUcnpj(this,document.getElementById('cnpj'));" onkeypress="formataCampo(this, '###.###.###-##', event)"  maxlength="14" size="12" />
	</td>
	<td>
		<input type="text" name="cnpj" id="cnpj" onkeyup="javascript: cpfOUcnpj(this,document.getElementById('cpf'));" onkeypress="formataCampo(this, '##.###.###/####-##', event)" maxlength="18" size="15" />
	</td>
</tr>
<tr>
    <td colspan="2">Endere&ccedil;o</td>
    <td colspan="3">Bairro</td>
</tr>
<tr>
    <td colspan="2">
		<input type="text" name="endereco"  id="endereco" size="40" style="text-transform: uppercase;"/>
	</td>
    <td colspan="3">
		<input type="text" name="bairro" id="bairro" size="26" style="text-transform: uppercase;"/>
	</td>
</tr>
<tr>
    <td>Cidade</td>
    <td>UF</td>
    <td colspan="3">CEP</td>
</tr>
<tr>
    <td>
		<input type="text" name="cidade" id="cidade" size="25" style="text-transform: uppercase;"/>
	</td>
    <td>
		<input type="text" name="uf" id="uf" maxlength="2" size="3" style="text-transform: uppercase;"/>
	</td>
    <td colspan="3">
		<input type="text" onkeypress="formataCampo(this, '#####-###', event)" name="cep" id="cep" size="20" maxlength="9" />
	</td>
</tr>
<tr>
    <td>Telefone Pessoal</td>
    <td>Telefone Celular</td>
    <td>Telefone Comercial</td>
    <td colspan="2">Fax</td>
</tr>
<tr>
    <td>
		<input type="text" name="telef_pess" id="telef_pess" size="18" />
	</td>
    <td>
		<input type="text" name="celular" id="celular" size="18" />
	</td>
    <td>
		<input type="text" name="telef_com" id="telef_com" size="18" />
	</td>
    <td colspan="2">
		<input type="text" name="fax" id="fax" size="18" />
	</td>
</tr>
<tr>
	<td colspan="5">E-mail</td>
</tr>
<tr>
	<td colspan="5">
		<input type="text" name="email" id="email" size="31" />
	</td>
</tr>  
<tr>
    <td colspan="6">
	<div id="cadCli_botao">
	</div>
	<script>
	botaoNovo();
	</script>
    </td>
</tr>
</table>
</form>
</body>
</html>

