<?php
require('config.php');

$cliente = $_COOKIE['cokcliente'];
$conta 	 = $_COOKIE['cokconta'];
$agencia = $_COOKIE['cokagencia'];
$titulo	 = $_GET['titulo'];
$descricao= $_GET['descricao'];

$consulta = "";

$consulta .= "SELECT tit.titulo, tit.agencia, tit.cliente, tit.sacado, ";
$consulta .= "tit.documento, tit.sequencia, tit.nossonumero, DATE_FORMAT(tit.data_emisao,'%d/%m/%Y') as data_emissao, ";
$consulta .= "DATE_FORMAT(tit.data_venc,'%d/%m/%Y') as data_venc, ";
$consulta .= "REPLACE( REPLACE( REPLACE( FORMAT(tit.valor, 2), '.', '|'), ',', '.'), '|', ',') as valor, ";
$consulta .= "DATE_FORMAT(tit.data_baixa,'%d/%m/%Y') as data_baixa, tit.data_credito, ";
$consulta .= "REPLACE( REPLACE( REPLACE( FORMAT(tit.valor_baixa, 2), '.', '|'), ',', '.'), '|', ',') as valor_baixa, ";
$consulta .= "tit.data_baixa_manual, tit.cancelamento, tit.criacao, tit.desconto, ";
$consulta .= "tit.devolucao, tit.so_desconto, tit.status, cli.razao, cli.conta, ";
$consulta .= "sac.nome, sac.cpf, sac.endereco, sac.bairro, sac.cidade, sac.uf, sac.cep  ";
$consulta .= "FROM titulos AS tit ";
$consulta .= "LEFT JOIN clientes AS cli ON tit.cliente=cli.cliente ";
$consulta .= "LEFT JOIN sacados AS sac ON tit.sacado=sac.sacado ";
$consulta .= "WHERE tit.titulo=".$titulo;

$sql = mysql_query($consulta);

$linha=mysql_fetch_array($sql);

$cobranca = $descontada + $simples;
$valor_cobranca = $valor_simples + $valor_descontada;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Comprovante de Instru&ccedil;&atilde;o</title>
</head>
<body>
<font face='Verdana, Arial, Helvetica, sans-serif' size='3'>
<b>
<br />
<br />
<br />
<SCRIPT language=javascript>
 var isNav4, isNav6, isIE4;
 function setBrowser(){
	if (navigator.appVersion.charAt(0) == "4"){
		if (navigator.appName.indexOf("Explorer") >= 0){
			isIE4 = true;
		}else{
			isNav4 = true;
		}
	}else if (navigator.appVersion.charAt(0) > "4"){
		isNav6 = true;
	}
	if ((isIE4) || (isNav6)) {
		document.write("<iframe src=logo.php?agencia=<? print $agencia;?> name=logo width=150 height=50 scrolling=no framespacing=0 frameborder=0 marginheight=0 marginwidth=0></iframe>");
	}
}
setBrowser();
</SCRIPT>
Comprovante de Instru&ccedil;&atilde;o &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<font size='1'><?php echo date("d/m/Y - H:i"); ?></font>
<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
		<td style="border-top:2px solid #9C9C9C">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center" style="font-size:14px;">
			<?php echo utf8_encode($descricao); ?>
		</td>
	</tr>
	<tr>
		<td align="center" >
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="20%" >CEDENTE:</td>
					<td width="80%" ><?php echo $linha['razao']; ?></td>
				</tr>
				<tr>
					<td>AG&Ecirc;NCIA:</td>
					<td><?php echo $linha['agencia']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTA:&nbsp;&nbsp;<?php echo $conta; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-size:14px;border-top:2px solid #9C9C9C">
					Dados do T&iacute;tulo
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<table  width="100%" >
				<tr>
					<td width="10%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width="30%">N&ordm; DOCUMENTO: </td>
					<td width="60%"><?php echo $linha['documento'].'/'.$linha['sequencia']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >NOSSO NUMERO: </td>
					<td ><?php echo $linha['nossonumero']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >DATA EMISS&Atilde;O: </td>
					<td ><?php echo $linha['data_emissao']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >DATA VENCIMENTO:</td>
					<td ><?php echo $linha['data_venc']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >VALOR FATURA:</td>
					<td ><?php echo "R$ ".$linha['valor']; ?></td>
				</tr>
				<!--
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >DATA LIQUIDA&Ccedil;&Atilde;O:</td>
					<td ><?php //echo $linha['data_baixa']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >VALOR RECEBIDO:</td>
					<td ><?php //echo "R$ ".$linha['valor_baixa']; ?></td>
				</tr>
				-->
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-size:14px;border-top:2px solid #9C9C9C">
			Dados do Sacado
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<table  width="100%" >
				<tr>
					<td width="10%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width="30%">NOME: </td>
					<td width="60%"><?php echo $linha['nome']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >CPF: </td>
					<td ><?php echo $linha['cpf']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >ENDERE&Ccedil;O: </td>
					<td ><?php echo $linha['endereco']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >BAIRRO:</td>
					<td ><?php echo $linha['bairro']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >CEP:</td>
					<td ><?php echo $linha['cep']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >CIDADE:</td>
					<td ><?php echo $linha['cidade']; ?></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td >UF:</td>
					<td ><?php echo $linha['uf']; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" style="font-size:14px;border-top:2px solid #9C9C9C">
			&nbsp;
		</td>
	</tr>
</table>
</body>
</html>
