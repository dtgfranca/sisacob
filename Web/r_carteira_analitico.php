<?php
require('config.php');

$cliente = $_COOKIE['cokcliente'];
$conta 	 = $_COOKIE['cokconta'];
$agencia = $_COOKIE['cokagencia'];

$consulta = "";

$consulta .= "SELECT razao ";
$consulta .= "FROM clientes ";
$consulta .= "WHERE cliente=$cliente ";

$sql_cli = mysql_query($consulta);

$linha_cli=mysql_fetch_array($sql_cli)

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio Anal&iacute;tico de Posi&ccedil;&atilde;o das Carteiras</title>
</head>
<body>

<table width="100%">
  <tr>
    <td align="right" width="60%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='3'><b>Relat&oacute;rio Anal&iacute;tico de Posi&ccedil;&atilde;o das Carteiras</b></font>
    </td>
    <td align="right" width="40%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
       <?php
       date_default_timezone_set("Brazil/East");
       echo date("d/m/Y - H:i");
       ?>
       </font>
    </td>
  </tr>
</table>

<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center" style="font-size:14px;border:2px solid #9C9C9C">
			CARTEIRA <?php echo "- ".date("d/m/Y"); ?>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="20%" >CLIENTE:</td>
					<td width="80%" ><?php echo $linha_cli['razao']; ?></td>
				</tr>
				<tr>
					<td>AG&Ecirc;NCIA:</td>
					<td><?php echo $agencia; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTA:&nbsp;&nbsp;<?php echo $conta; ?></td>
				</tr>
			</table>
		</td>
	</tr>
<?php 
$consulta = "";

$consulta .= "SELECT nome, sacado ";
$consulta .= "FROM sacados ";
$consulta .= "WHERE cliente=$cliente ";
$consulta .= "AND (grupo IS NULL OR grupo <> '99999')";

$sql_sac = mysql_query($consulta);

$liquidados_geral = 0;
$valor_liquidados_geral = 0;
$cartorio_geral = 0;
$valor_cartorio_geral = 0;
$baixados_geral = 0;
$valor_baixados_geral = 0;
$cobranca_geral = 0;
$valor_cobranca_geral = 0;
	
while($linha_sac=mysql_fetch_array($sql_sac)){

	$consulta = "";

	$consulta .= "SELECT tit.titulo, tit.agencia, tit.cliente, tit.sacado, ";
	$consulta .= "tit.documento, tit.sequencia, tit.nossonumero, tit.data_emisao, ";
	$consulta .= "tit.data_venc, DATEDIFF(tit.data_venc,DATE_FORMAT(NOW(),'%Y-%m-%d')) AS vencido, ";
	$consulta .= "tit.valor, tit.data_baixa, tit.data_credito, tit.valor_baixa, ";
	$consulta .= "tit.data_baixa_manual, tit.cancelamento, tit.criacao, tit.desconto, ";
	$consulta .= "tit.devolucao, tit.so_desconto, tit.status ";
	$consulta .= "FROM titulos AS tit ";
	$consulta .= "WHERE data_baixa IS NULL and cancelamento IS NULL and tit.cad_completo='S' and tit.status != 3 and tit.sacado=".$linha_sac['sacado'];

	$sql = mysql_query($consulta);

	$razao = "";
	$simples = 0;
	$valor_simples = 0;
	$descontada = 0;
	$valor_descontada = 0;
	$cobranca = 0;
	$valor_cobranca = 0;
	$registro = 0;
	$valor_registro = 0;
	$liquidados = 0;
	$valor_liquidados = 0;
	$cartorio = 0;
	$valor_cartorio = 0;
	$baixados = 0;
	$valor_baixados = 0;
	$vencidos = 0;
	$valor_vencidos = 0;
	$avencer = 0;
	$valor_avencer = 0;

	$nome_sacado = "";

	while($linha=mysql_fetch_array($sql)){

		if($razao==""){
			$razao = $linha['razao'];
		}
		
		if($linha['so_desconto']=="S" && $linha['data_baixa'] == ""){
			$descontada++;		
			$valor_descontada += $linha['valor'];
		}else if($linha['so_desconto']=="N" && $linha['data_baixa'] == ""){
			$simples++;
			$valor_simples += $linha['valor'];
		}
		
		if($linha['registro']!=""){
			$registro++;
			$valor_registro += $linha['valor'];
		}
		
		if($linha['data_baixa'] != "" && $linha['status']!=9 && $linha['devolucao'] == ""){
			$liquidados++;
			$valor_liquidados += $linha['valor'];
		}
		
		if($linha['data_baixa'] != "" && $linha['status']==9){
			$cartorio++;
			$valor_cartorio += $linha['valor'];
		}
		
		if($linha['devolucao'] != "" && $linha['status']!=9){
			$baixados++;
			$valor_baixados += $linha['valor_baixa'];
		}
		
		if($linha['vencido'] < 0 && $linha['data_baixa'] == ""){
			$vencidos++;
			$valor_vencidos += $linha['valor'];
		}
		
		if($linha['vencido'] >= 0 && $linha['data_baixa'] == ""){
			$avencer++;
			$valor_avencer += $linha['valor'];
		}
		
	}

	$cobranca = $descontada + $simples;
	$valor_cobranca = $valor_simples + $valor_descontada;

	if ($simples != 0 || $descontada != 0 || $cobranca != 0 || $registro != 0 || $liquidados != 0 || $cartorio != 0 || $baixados != 0 || $vencidos != 0 || $avencer != 0) {
	?>
		<tr>
			<td>
				<table width="100%" >
					<tr>
						<td align="left" style="font-size:12px;border:2px solid #9C9C9C">
							Sacado: <?php echo $linha_sac['nome']; ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table  width="100%" >
					<tr>
						<td width="50%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
							Descri&ccedil;&atilde;o
						</td>
						<td width="20%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
							N&ordm; de T&iacute;tulos
						</td>
						<td width="30%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
							Valor Total
						</td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos em cobran&ccedil;a:</td>
						<td align="center" ><?php echo $cobranca; ?></td>
						<td align="right" ><?php echo ($valor_cobranca==0?"0,00":number_format($valor_cobranca,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos em cobran&ccedil;a simples:</td>
						<td align="center" ><?php echo $simples; ?></td>
						<td align="right" ><?php echo ($valor_simples==0?"0,00":number_format($valor_simples,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos em cobran&ccedil;a descontada:</td>
						<td align="center" ><?php echo $descontada; ?></td>
						<td align="right" ><?php echo ($valor_descontada==0?"0,00":number_format($valor_descontada,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de entradas confirmadas:</td>
						<td align="center" ><?php echo $registro; ?></td>
						<td align="right" ><?php echo ($valor_registro==0?"0,00":number_format($valor_registro,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos liquidados:</td>
						<td align="center" ><?php echo $liquidados; ?></td>
						<td align="right" ><?php echo ($valor_liquidados==0?"0,00":number_format($valor_liquidados,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos liquidados em cart&oacute;rio:</td>
						<td align="center" ><?php echo $cartorio; ?></td>
						<td align="right" ><?php echo ($valor_cartorio==0?"0,00":number_format($valor_cartorio,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos baixados:</td>
						<td align="center" ><?php echo $baixados; ?></td>
						<td align="right" ><?php echo ($valor_baixados==0?"0,00":number_format($valor_baixados,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos vencidos:</td>
						<td align="center" ><?php echo $vencidos; ?></td>
						<td align="right" ><?php echo ($valor_vencidos==0?"0,00":number_format($valor_vencidos,2,",",".")); ?></td>
					</tr>
					<tr>
						<td align="left" >Total de t&iacute;tulos a vencer:</td>
						<td align="center" ><?php echo $avencer; ?></td>
						<td align="right" ><?php echo ($valor_avencer==0?"0,00":number_format($valor_avencer,2,",",".")); ?></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
	$liquidados_geral += $liquidados;
	$valor_liquidados_geral += $valor_liquidados;
	$cartorio_geral += $cartorio;
	$valor_cartorio_geral += $valor_cartorio;
	$baixados_geral += $baixados;
	$valor_baixados_geral += $valor_baixados;
	$cobranca_geral += $cobranca;
	$valor_cobranca_geral += $valor_cobranca;
} //fim do primeiro while
?>
	<tr>
		<td align="left" style="font-size:12px;border-top:2px solid #9C9C9C">
			Total Geral
		</td>
	</tr>
	<tr>
		<td>
			<table  width="100%" >
				<tr>
					<td width="50%" align="left">
						Total de titulos em cobran&ccedil;a
					<td width="20%" align="center" ><?php echo $cobranca_geral; ?></td>
					<td width="30%" align="right" ><?php echo ($valor_cobranca_geral==0?"0,00":number_format($valor_cobranca_geral,2,",",".")); ?></td>
				</tr>
				<tr>
					<td align="left" >Total de t&iacute;tulos liquidados:</td>
					<td align="center" ><?php echo $liquidados_geral; ?></td>
					<td align="right" ><?php echo ($valor_liquidados_geral==0?"0,00":number_format($valor_liquidados_geral,2,",",".")); ?></td>
				</tr>
				<tr>
					<td align="left" >Total de t&iacute;tulos liquidados em cart&oacute;rio:</td>
					<td align="center" ><?php echo $cartorio_geral; ?></td>
					<td align="right" ><?php echo ($valor_cartorio_geral==0?"0,00":number_format($valor_cartorio_geral,2,",",".")); ?></td>
				</tr>
				<tr>
					<td align="left" >Total de t&iacute;tulos baixados:</td>
					<td align="center" ><?php echo $baixados_geral; ?></td>
					<td align="right" ><?php echo ($valor_baixados_geral==0?"0,00":number_format($valor_baixados_geral,2,",",".")); ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>