<script src="../SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
<div id="CollapsiblePanel1" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit">Alterar senha de acesso</div>
  <div class="CollapsiblePanelContent">
     <SCRIPT language=javascript>
	 var isNav4, isNav6, isIE4;
	 function setBrowser(){
		 if (navigator.appVersion.charAt(0) == "4"){
			 if (navigator.appName.indexOf("Explorer") >= 0){
				 isIE4 = true;
			 }else{
				 isNav4 = true;
			 }
		 }else if (navigator.appVersion.charAt(0) > "4"){
			 isNav6 = true;
		 }
		 if ((isIE4) || (isNav6)) {
		 	document.write("<iframe src=utilitarios/senha.php name=senha width=100% height=200 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
		}
	}
	setBrowser();
	</SCRIPT>    
  </div>
</div>
<div id="CollapsiblePanel2" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit">Integra&ccedil;&atilde;o via CNAB 400</div>
  <div class="CollapsiblePanelContent">
     <SCRIPT language=javascript>
	 var isNav4, isNav6, isIE4;
	 function setBrowser(){
		 if (navigator.appVersion.charAt(0) == "4"){
			 if (navigator.appName.indexOf("Explorer") >= 0){
				 isIE4 = true;
			 }else{
				 isNav4 = true;
			 }
		 }else if (navigator.appVersion.charAt(0) > "4"){
			 isNav6 = true;
		 }
		 if ((isIE4) || (isNav6)) {
		 	document.write("<iframe src=utilitarios/cnab400.php name=senha width=100% height=200 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
		}
	}
	setBrowser();
	</SCRIPT>   
  </div>
</div>
<div id="CollapsiblePanel3" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit">Intru&ccedil;&otilde;es</div>
  <div class="CollapsiblePanelContent">
  
     <SCRIPT language=javascript>
	 var isNav4, isNav6, isIE4;
	 function setBrowser(){
		 if (navigator.appVersion.charAt(0) == "4"){
			 if (navigator.appName.indexOf("Explorer") >= 0){
				 isIE4 = true;
			 }else{
				 isNav4 = true;
			 }
		 }else if (navigator.appVersion.charAt(0) > "4"){
			 isNav6 = true;
		 }
		 if ((isIE4) || (isNav6)) {
		 	document.write("<iframe src=utilitarios/instrucoes.php name=intrucao width=100% height=200 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
		}
	}
	setBrowser();
	</SCRIPT>   
  </div>
</div>
<div id="CollapsiblePanel4" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit">Parametriza&ccedil;&atilde;o boletos</div>
  <div class="CollapsiblePanelContent">
  
     <SCRIPT language=javascript>
	 var isNav4, isNav6, isIE4;
	 function setBrowser(){
		 if (navigator.appVersion.charAt(0) == "4"){
			 if (navigator.appName.indexOf("Explorer") >= 0){
				 isIE4 = true;
			 }else{
				 isNav4 = true;
			 }
		 }else if (navigator.appVersion.charAt(0) > "4"){
			 isNav6 = true;
		 }
		 if ((isIE4) || (isNav6)) {
		 	document.write("<iframe src=utilitarios/boleto_modelo.php name=intrucao width=100% height=200 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
		}
	}
	setBrowser();
	</SCRIPT>
</div>
</div>

<div id="CollapsiblePanel5" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit">Importar sacados</div>
  <div class="CollapsiblePanelContent">
  <SCRIPT language=javascript>
	 var isNav4, isNav6, isIE4;
	 function setBrowser(){
		 if (navigator.appVersion.charAt(0) == "4"){
			 if (navigator.appName.indexOf("Explorer") >= 0){
				 isIE4 = true;
			 }else{
				 isNav4 = true;
			 }
		 }else if (navigator.appVersion.charAt(0) > "4"){
			 isNav6 = true;
		 }
		 if ((isIE4) || (isNav6)) {
		 	document.write("<iframe src=utilitarios/import_sacado.php name=import_sacado width=100% height=200 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
		}
	}
	setBrowser();
	</SCRIPT>     
  </div>
</div>

<div id="CollapsiblePanel6" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-style:inherit">Observa&ccedil;&atilde;o na 1 via - (Boleto de 3 vias)</div>
  <div class="CollapsiblePanelContent">
  
     <SCRIPT language=javascript>
	 var isNav4, isNav6, isIE4;
	 function setBrowser(){
		 if (navigator.appVersion.charAt(0) == "4"){
			 if (navigator.appName.indexOf("Explorer") >= 0){
				 isIE4 = true;
			 }else{
				 isNav4 = true;
			 }
		 }else if (navigator.appVersion.charAt(0) > "4"){
			 isNav6 = true;
		 }
		 if ((isIE4) || (isNav6)) {
		 	document.write("<iframe src=utilitarios/instrucoes_1via.php name=instrucoes_1via width=100% height=200 scrolling=yes framespacing=0 frameborder=0 marginheight=2 marginwidth=2></iframe>");
		}
	}
	setBrowser();
	</SCRIPT>   
  </div>
</div>
<script type="text/javascript">
<!--
var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1", {contentIsOpen:false});
var CollapsiblePanel2 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel2", {contentIsOpen:false});
var CollapsiblePanel3 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel3", {contentIsOpen:false});
var CollapsiblePanel4 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel4", {contentIsOpen:false});
var CollapsiblePanel5 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel5", {contentIsOpen:false});
var CollapsiblePanel6 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel6", {contentIsOpen:false});
//-->
</script>
