<?php
require('config.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relatório de Avisos de Lançamento</title>
<style>
table, img{behavior: url(iepngfix.htc)}

* {font: normal 10px Arial, Helvetica, sans-serif; font-weight:bold}
</style>

<style type="text/css">
<!--
#bol{ 
width:100%; 
height:200px; 
overflow:auto; 
}
-->
</style>
<script>
function gera_relatorio_lancamento(){
	if (document.getElementById('inicio2').value=="") {
		alert("O campo \"Início\" é obrigatório!");
		document.getElementById('inicio2').focus();
		return false;
	}
	if (document.getElementById('fim2').value=="") {
		alert("O campo \"Fim\" é obrigatório!");
		document.getElementById('fim2').focus();
		return false;
	}

	var remote = null
    remote = window.open('','relatorio','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
    if (remote != null) {
       remote.location.href = 'r_lancamentos.php?inicio2='+document.getElementById('inicio2').value+'&fim2='+document.getElementById('fim2').value;
    } 
}
</script>
</head>
<body>
<div align="left" style="padding:10px; background-color:#F4F4F4"><img src="images/ppl18.gif" />&nbsp;<img src="images/titu30q.png" /></div>
<br>
<br>
<br>
<table align="center" border="0" cellpadding="0" cellspacing="4">
   	<tr>
       	<td align="left" bgcolor="#F4F4F4" colspan="2"><img src="images/ppl11.gif" />&nbsp;<img src="images/titu9q.png" /></td>
    </tr>
    <tr>
      	<td align="left">In&iacute;cio</td>
        <td align="left"><input name="inicio2" id="inicio2" OnKeyPress="formatar(this, '##/##/####');" type="text" size="15" maxlength="10" style="text-align:right;"></td>
    </tr>
    <tr>
      	<td align="left">Fim</td>
        <td align="left"><input name="fim2" id="fim2" OnKeyPress="formatar(this, '##/##/####');" type="text" size="15" maxlength="10" style="text-align:right"></td>
    </tr>
    <tr>
        <td>&nbsp</td>
    </tr>
    <tr>
       	<td align="center" colspan="2">
      	   <input type="button" value="Gerar Relat&oacute;rio" onclick="gera_relatorio_lancamento()" width="400">
      	</td>
    </tr>
</table>

</body>
</html>
