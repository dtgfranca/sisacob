<?php

require('config.php');

// Configura um número para reais //
function Reais($num, $cifrao = true)
{
	$retorno = "";
	
	if ($cifrao)
	$retorno .= "R$ ";
	
	$retorno .= number_format($num, 2, ",", ".");
	return $retorno;
}

// Mostra o CPF ou CNPJ com os hífens e pontos //
function formatCPFCNPJ ($string)
{
	$output = preg_replace("[' '-./ t]", '', $string);
	$size = (strlen($output) -2);
	
	if ($size != 9 && $size != 12) return false;
	$mask = ($size == 9) 
		? '###.###.###-##' 
		: '##.###.###/####-##'; 
	$index = -1;
	
	for ($i=0; $i < strlen($mask); $i++):
		if ($mask[$i]=='#') $mask[$i] = $output[++$index];
	endfor;
	return $mask;
}

// Mostra o CEP com os hífens e pontos //
function formatCEP($string)
{
	$output = preg_replace("[' '-./ t]", '', $string);
	$size = (strlen($output));
	
	if ($size != 8) return false;
	$mask = '##.###-###'; 
	$index = -1;
	
	for ($i=0; $i < strlen($mask); $i++):
		if ($mask[$i]=='#') $mask[$i] = $output[++$index];
	endfor;
	return $mask;
}

$cliente = $_COOKIE['cokcliente'];
$conta 	 = $_COOKIE['cokconta'];
$agencia = $_COOKIE['cokagencia'];

$inicio  = substr($_GET['inicio2'],6,4)."-".substr($_GET['inicio2'],3,2)."-".substr($_GET['inicio2'],0,2);
$fim     = substr($_GET['fim2'],6,4)."-".substr($_GET['fim2'],3,2)."-".substr($_GET['fim2'],0,2);

$rs_cli = mysql_query("SELECT cliente, razao, cgc, endereco, bairro, cep, cidade, estado, inscr, agencia FROM clientes WHERE cliente = " . $cliente . " LIMIT 1");
$cli = mysql_fetch_assoc($rs_cli);

$cAgencia = mysql_query("SELECT razao FROM agencias WHERE agencia = " . $cli["agencia"] . " LIMIT 1");
$cAgencia = mysql_result($cAgencia, 0, "razao");

$nTotalValorEntradas = 0;
$nTotalJurosEntradas = 0;
$nTotalMultaEntradas = 0;
$nTotalDescnEntradas = 0;
$nTotalTitulEntradas = 0;

$nTotalValorLiquidac = 0;
$nTotalJurosLiquidac = 0;
$nTotalMultaLiquidac = 0;
$nTotalDescnLiquidac = 0;
$nTotalTitulLiquidac = 0;

$entradas = mysql_query("SELECT
					COALESCE(seunumero, '-') AS seunumero,
					nossonumero,
					DATE_FORMAT(data_emisao, '%d/%m/%Y') AS data_emissao,
					DATE_FORMAT(data_venc, '%d/%m/%Y') AS data_venc,
					DATE_FORMAT(data_baixa, '%d/%m/%Y') AS data_baixa,
					valor,
					multa,
					juros,
					descontos,
					documento
					
				   FROM
				    titulos
					
				   WHERE
				    agencia = '" . $agencia . "' 
					
					AND cliente = '" . $cliente . "'
					
					AND data_emisao BETWEEN '" . $inicio . "' AND '" . $fim . "'
					
					ORDER BY
					 data_emisao, data_venc, data_baixa
				");

$anterior = mysql_query("SELECT SUM(valor) AS soma, COUNT(*) AS cont FROM titulos WHERE cliente = " . $cliente . " AND data_baixa IS NULL AND cancelamento IS NULL AND cad_completo = 'S' AND status != '3' AND registro IS NOT NULL");
$nAnteriorSaldo = mysql_result($anterior, 0, "soma");
$nAnteriorCont = mysql_result($anterior, 0, "cont");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Avisos de Lan&ccedil;amento</title>
<style type="text/css">
html, body, #wrapper
{
	font-size: 8pt;
	font-family: Verdana, Geneva, sans-serif;
}

th
{
	font-weight: normal;
	border-top: thin solid black;
	border-bottom: thin solid black;	
}
</style>
</head>
<body>

<br>

<table align="center" border="0" id="wrapper" width="95%" cellpadding="4" cellspacing="0">
	<tr valign="bottom">
        <td align="center" style="font-weight: bold; border-left: thin solid #000; border-bottom: thin solid #000; border-top: thin solid black" width="25%">
        	<?= $cli["agencia"] ?> - <?= $cAgencia ?>
        </td>
        <td style="font-weight: bold; border-bottom: thin solid #000; border-top: thin solid black" width="50%" align="center">
            SICOOB - Cobrança<br><br>
            Relatório de Aviso de Lançamento<br><br>
            01 - COBRANÇA SIMPLES
        </td>
        <td style="border-bottom: thin solid #000; border-right: thin solid #000; border-top: thin solid black" width="25%">
        	<table width="100%" id="wrapper">
                <tr>
                	<td></td>
                </tr>
            	<tr>
                	<td>Data Processamento:</td>
                    <td><?= date("d/m/Y") ?></td>
                </tr>
            	<tr>
                	<td>Data Emissão:</td>
                    <td><?= date("d/m/Y") ?></td>
                </tr>
            	<tr>
                	<td>Hora Emissão:</td>
                    <td><?= date("H:i:s") ?></td>
                </tr>
                <tr>
                	<td></td>
                </tr>
            </table>
        </td>
	</tr>
</table>

<br>

<table align="center" border="0" id="wrapper" width="95%" cellpadding="4" cellspacing="1">
	<tr>
    	<td>
        	Ao Cliente:
        </td>
    	<td>
        	<?= utf8_encode($cli['cliente']) ?>&nbsp;&nbsp;&nbsp;<?= utf8_encode($cli['razao']) ?>
        </td>
    	<td>&nbsp;
        	        	        	
        </td>
    	<td>&nbsp;
        	        	        	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>
        	CPF/CNPJ:
        </td>
    	<td>
        	<?= formatCPFCNPJ($cli['cgc']) ?>       	
		</td>        
	</tr>
	<tr>
    	<td>
        	Endereço:
        </td>
    	<td>
        	<?= utf8_encode(str_replace(",", ", ", $cli['endereco'])) ?>
        </td>
    	<td>&nbsp;
        	       	        	
        </td>
    	<td>&nbsp;
        	      	        	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>
        	Bairro:        	
        </td>
    	<td> 
        	<?= utf8_encode($cli['bairro']) ?>       	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>
        	Inscrição:
        </td>
    	<td>
        	<?= utf8_encode($cli['inscr'])?>	
		</td>        
	</tr>
	<tr>
    	<td>
        	CEP:
        </td>
    	<td>
        	<?= formatCEP($cli['cep']) ?>
        </td>
    	<td>
        	Cidade:       	        	
        </td>
    	<td>
        	<?= utf8_encode($cli['cidade']) ?>        	        	
        </td>
    	<td>&nbsp;
        	        	
        </td>
    	<td>
        	Estado:        	
        </td>
    	<td> 
        	<?= utf8_encode($cli['estado']) ?>     	
        </td>
    	<td>&nbsp;
        	      	
        </td>
    	<td>&nbsp;
        	
        </td>
    	<td>&nbsp;
        		
		</td>        
	</tr>
</table>

<br>

<table align="center" border="0" id="wrapper" width="95%" cellpadding="4" cellspacing="0">
	<tr>
    	<td colspan="12">
        	<b>Ref.:</b>&nbsp;&nbsp;&nbsp;
        	Entradas ocorridas na carteira no período de processamento acima mencionado
        </td>
        
    <tr><td></td></tr>
    
	<tr>
		<th align="left">
		   Seu N&uacute;mero
		</th>
		<th align="left">
		   Nosso N&uacute;mero
		</th>
		<th>
		  Emiss&atilde;o
		</th>
		<th>
		   Vencimento
		</th>
		<th colspan="2" align="right">
		   Valor Nominal
		</th>
		<th align="right">
		   Multa
		</th>
		<th align="right">
		   Juros
		</th>
		<th align="right">
		   Descontos
		</th>
	</tr>
    
    <tr><td></td></tr>
    
<?php
	
	$x = 0;
		
	if (mysql_num_rows($entradas) > 0)
	{
	   while ($dados = mysql_fetch_assoc($entradas))
	   {		
			if ($x == 0)
			{
			   $cor = "#FFF";
			   $x = 1;
			}  
			
			else
			{
			   $cor = "#F2F2F2";
			   $x = 0;	
			}  
			
			$juros = 0; $multa = 0;
		    $nTotalValorEntradas += $dados['valor'];
			$nTotalMultaEntradas += $nulta;
			$nTotalDescnEntradas += $dados['descontos'];
			$nTotalJurosEntradas += $juros;
			$nTotalTitulEntradas++;

			echo "<tr style='background-color: " . $cor . "'>";				
			echo "<td>" . $dados['seunumero'] . "</td>";
			echo "<td>" . $dados['nossonumero'] . "</td>";
			echo "<td align='center'>" . $dados['data_emissao'] . "</td>";
			echo "<td align='center'>" . $dados['data_venc'] . "</td>";
			echo "<td></td>";
			echo "<td align='right'>" . Reais($dados['valor']) . "</td>";
			echo "<td align='right'>" . Reais($multa) . "</td>";
			echo "<td align='right'>" . Reais($juros) . "</td>";
			echo "<td align='right'>" . Reais($dados['descontos']) . "</td>";
			echo "</tr>";
	   }
	}
	
	else
	{
		echo "<tr>
				<td colspan='6'>&nbsp;</td>
			 </tr>
			 <tr>
				<td colspan='12' align='center'>Nada a mostrar.</td>
			 </tr>
			 <tr>
				<td colspan='6'>&nbsp;</td>
			 </tr>";
	} 
	?>
    
    <tr><td></td></tr>
    <tr><td colspan="12" style="border-top: thin solid black"></td></tr>
    <tr>
    	<td width="30%" colspan="2">
        	Total Histórico:&nbsp;&nbsp;&nbsp;001 - ENTRADA NORMAL
        </td>
    	<td>
        	Tit: <?= $nTotalTitulEntradas ?>
        </td>
    	<td>&nbsp;
        	
        </td>
    	<td>&nbsp;
        	
        </td>
    	<td align="right">
        	<?= Reais($nTotalValorEntradas) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalMultaEntradas) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalJurosEntradas) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalDescnEntradas) ?>
        </td>
    </tr>        
</table>

<br />
<br />

<table align="center" border="0" id="wrapper" width="95%" cellpadding="4" cellspacing="0" style="white-space: nowrap">
	<tr>
    	<td colspan="12">
        	<b>Ref.:</b>&nbsp;&nbsp;&nbsp;
        	Liquidações ocorridas na carteira no período de processamento acima mencionado
        </td>
        
    <tr><td></td></tr>
    
    <tr>
		<th align="left">
		   Seu N&uacute;mero
		</th>
		<th align="left">
		   Nosso N&uacute;mero
		</th>
		<th>
		  Emiss&atilde;o
		</th>
		<th>
		   Vencimento
		</th>
		<th>
		   Liquida&ccedil;&atilde;o
		</th>
		<th align="right">
		   Valor Nominal
		</th>
		<th align="right">
		   Multa
		</th>
		<th align="right">
		   Juros
		</th>
		<th align="right">
		   Descontos
		</th>
		<th align="right">
		   Valor Final
		</th>
	</tr>

    <tr><td></td></tr>
    
<?php

$liquidacoes = mysql_query("SELECT
					COALESCE(seunumero, '-') AS seunumero,
					nossonumero,
					DATE_FORMAT(data_emisao, '%d/%m/%Y') AS data_emissao,
					DATE_FORMAT(data_venc, '%d/%m/%Y') AS data_venc,
					DATE_FORMAT(data_baixa, '%d/%m/%Y') AS data_baixa,
					DATEDIFF(data_baixa, data_venc) - 1 AS atraso,
					valor,
					valor_baixa,
					multa,
					juros,
					descontos,
					documento
					
				   FROM
				    titulos
					
				   WHERE
				    agencia = '" . $agencia . "' 
					
					AND cliente = '" . $cliente . "'
					
					AND data_baixa BETWEEN '" . $inicio . "' AND '" . $fim . "'
										
					ORDER BY
					 data_emisao, data_venc, data_baixa
				");
				
	$x = 0;
	
	if (mysql_num_rows($liquidacoes) > 0)
	{
	   while ($dados = mysql_fetch_assoc($liquidacoes))
	   {		
			if ($x == 0)
			{
			   $cor = "#FFF";
			   $x = 1;
			}  
			
			else
			{
			   $cor = "#F2F2F2";
			   $x = 0;	
			}  
			
			$juros = 0; $multa = 0;
			if ($dados['valor'] != $dados['valor_baixa'])
			{
				if ($dados['atraso'] > 0)
				{
					$juros = $dados['valor'] * ($dados['juros'] / 100) / 30 * $dados['atraso'];
					$multa = $dados['valor'] * ($dados['multa'] / 100);
				}
			}
			
			$nTotalValorLiquidac += $dados['valor'];
			$nTotalMultaLiquidac += $multa;
			$nTotalDescnLiquidac += $dados['descontos'];
			$nTotalJurosLiquidac += $juros;
			$nTotalTitulLiquidac++;
			
			echo "<tr style='background-color: " . $cor . "'>";				
			echo "<td>" . $dados['seunumero'] . "</td>";
			echo "<td>" . $dados['nossonumero'] . "</td>";
			echo "<td align='center'>" . $dados['data_emissao'] . "</td>";
			echo "<td align='center'>" . $dados['data_venc'] . "</td>";
			echo "<td align='center'>" . $dados['data_baixa'] . "</td>";
			echo "<td align='right'>" . Reais($dados['valor']) . "</td>";
			echo "<td align='right'>" . Reais($multa) . "</td>";
			echo "<td align='right'>" . Reais($juros) . "</td>";
			echo "<td align='right'>" . Reais($dados['descontos']) . "</td>";
			echo "<td align='right'>" . Reais($dados['valor_baixa']) . "</td>";
			echo "</tr>";
	   }
	   
	}
	
	else
	{
		echo "<tr>
				<td colspan='6'>&nbsp;</td>
			 </tr>
			 <tr>
				<td colspan='12' align='center'>Nada a mostrar.</td>
			 </tr>
			 <tr>
				<td colspan='6'>&nbsp;</td>
			 </tr>";
	} 
	?>
    
    <tr><td></td></tr>
    <tr><td colspan="12" style="border-top: thin solid black"></td></tr>
    <tr>
    	<td width="30%" colspan="2">
        	Total Histórico:&nbsp;&nbsp;&nbsp;006 - LIQUIDACOES
        </td>
    	<td>
        	Tit: <?= $nTotalTitulLiquidac ?>
        </td>
    	<td>&nbsp;
        	
        </td>
    	<td>&nbsp;
        	
        </td>
    	<td align="right">
        	<?= Reais($nTotalValorLiquidac) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalMultaLiquidac) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalJurosLiquidac) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalDescnLiquidac) ?>
        </td>
    	<td align="right">
        	<?= Reais($nTotalValorLiquidac + $nTotalMultaLiquidac + $nTotalJurosLiquidac - $nTotalDescnLiquidac) ?>
        </td>
    </tr>        
</table>

<br />
<br />
<br />

<table align="center" border="0" id="wrapper" width="50%" cellpadding="4" cellspacing="0" style="white-space: nowrap">
	<tr align="center">
    	<td colspan="3">
        	<table width="67%" style="border: thin solid black; position: relative; top: 5px" id="wrapper" cellpadding="0" cellspacing="">
            	<tr>
                	<td align="center" style="line-height: 20px">
                    	Resumo da Carteira
                    </td>
                </tr>
            </table>        	
        </td>
    </tr>
    <tr align="center" style="background: url(images/dot.png)">
    	<td style="border: thin solid black" width="33%">
        	Saldo
        </td>
    	<td style="border-top: thin solid black; border-bottom: thin solid black" width="33%">
        	Títulos
        </td>
    	<td style="border: thin solid black" width="33%">
        	Valor
        </td>
    </tr>
    <tr align="center">
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	Anterior
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	<?= $nAnteriorCont - $nTotalTitulEntradas + $nTotalTitulLiquidac ?>
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black; border-right: thin solid black">
        	<?= Reais($nAnteriorSaldo - $nTotalValorEntradas + $nTotalValorLiquidac) ?>
        </td>
    </tr>
    <tr align="center">
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	Entradas
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	<?= $nTotalTitulEntradas ?>
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black; border-right: thin solid black">
        	<?= Reais($nTotalValorEntradas) ?>
        </td>
    </tr>
    <tr align="center">
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	Liquidações/Baixas
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	<?= $nTotalTitulLiquidac ?>
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black; border-right: thin solid black">
        	<?= Reais($nTotalValorLiquidac) ?>
        </td>
    </tr>
    <tr align="center">
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	Atual
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black">
        	<?= $nAnteriorCont ?>
        </td>
    	<td style="border-left: thin solid black; border-bottom: thin solid black; border-right: thin solid black">
        	<?php
				echo Reais($nAnteriorSaldo); 
			?>
        </td>
    </tr>
</table>

<br />
<br />

</body>
</html>