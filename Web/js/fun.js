
function comboClientes(div){
	var dados = "";
	if(div == "comboClientesInstru"){
		dados = "metodo=comboClientes&ins="+div;
	}	
	else if (div == "comboClientesInstrucoes"){
		dados = "metodo=comboClientes&ins="+div;
	}else{
		dados = "metodo=comboClientes";
	}
	$.ajax
	(
	{
		type: 'GET',
		url: "php/metodos.php",
		data: dados,
		success: function(retorno)
		{
			document.getElementById(div).innerHTML = retorno;
	    }
	}
	);
}

function atualizaTitulosCad(){
	$.ajax
	(
	{
		type: 'GET',
		url: "cad_tit_hoje.php",
		data: "",
		success: function(retorno)
		{
			document.getElementById('fragment-16').innerHTML = retorno;
	    }
	}
	);
}

function confirmaCad(tit){
	parent.parent.document.getElementById('alterar_tit').innerHTML = '<iframe src="visualiza_tit.php?titulos='+tit+'" name="if_alterar_tit" id="if_alterar_tit" width="800px" height="400px" scrolling=no framespacing=0 frameborder=0 marginheight=0 marginwidth=0>';
	parent.parent.document.getElementById('fundo').className = "fundo";			
	parent.parent.document.getElementById('alterar_tit').className= "div_altera_tit";
}

function atualizaTitulosConfirmados(){
	jQuery.ajax({
		type: 'GET',
		url: "cad_tit_hoje.php",
		data: "",
		success: function(retorno)
		{			
			if (document.all)
			{
				var frame_sistema = parent.document.frames['sistema'].document;
				var frame_principal = frame_sistema.frames['principal'].document;
			}
			else
			{
				var frame_sistema = parent.document.getElementById('sistema').contentDocument;
				var frame_principal = frame_sistema.getElementById('principal').contentDocument;
			}
			
			parent.document.getElementById('fundo').className = "fimProcesso";			
			parent.document.getElementById('alterar_tit').className= "fimProcesso";
			frame_principal.getElementById('fragment-16').innerHTML = retorno;	
		}
	});
}

function confirmaCadTitulos(titulos){
   jQuery.ajax({
	  type: 'GET',
	  url: "php/metodos.php",
	  data: "metodo=confirmaCadTitulos&titulos="+titulos,
	  success: function(retorno){
		 alert(retorno);
		 atualizaTitulosConfirmados();
      }
   });
}   

function altera_tit(tit){

	parent.parent.document.getElementById('alterar_tit').innerHTML = '<iframe src="altera_tit.php?tit='+tit+'" name="if_alterar_tit" id="if_alterar_tit" width="800px" height="400px" scrolling=no framespacing=0 frameborder=0 marginheight=0 marginwidth=0>';
	parent.parent.document.getElementById('fundo').className = "fundo";			
	parent.parent.document.getElementById('alterar_tit').className= "div_altera_tit";

}

function visualiza_tit(){
	var titulos = '';
	$('.conf_titulos:input:checkbox:checked').each(function(){
		titulos += $(this).val() + ",";
	})
	
	if (titulos != ""){ 
	  parent.parent.document.getElementById('alterar_tit').innerHTML = '<iframe src="visualiza_tit.php?titulos='+titulos+'" name="if_alterar_tit" id="if_alterar_tit" width="800px" height="400px" scrolling=no framespacing=0 frameborder=0 marginheight=0 marginwidth=0>';
	  parent.parent.document.getElementById('fundo').className = "fundo";			
	  parent.parent.document.getElementById('alterar_tit').className= "div_altera_tit";
	}
	else{
	  alert("Selecione no minimo um titulo para confirmar!");	
	}
}

function selecionar(campo, cont){
	if(campo.checked){
		for(var i=0;i<cont;i++){
			nome = 'chk_titulos'+i;
			if(!document.getElementById(nome).disabled){
				document.getElementById(nome).checked=true;
			}
		}
	}else{
		for(var i=0;i<cont;i++){
			nome = 'chk_titulos'+i;
			document.getElementById(nome).checked=false;
		}
	}
}