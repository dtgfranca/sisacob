<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Importação de Títulos CNAB</title>
<link href="style/app.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function teste(e){
		var texto = e.value;
		var parte1 = "";
		var parte2 = "";
		
		if(texto.length > 2){
			texto = texto.replace("-", "");
			parte1 = texto.substring(0, texto.length-1);
			parte2 = texto.substring(texto.length-1, texto.length);
			texto = parte1+"-"+parte2;
			e.value = texto;
		}
	}
	
	function SomenteNumero(e){
		var tecla=(window.event)?event.keyCode:e.which;   
		if((tecla>47 && tecla<58)) return true;
		else{
			if (tecla==8 || tecla==0) return true;
		else  return false;
		}
	}
	
</script>
</head>
<body>
	<img id="logo_sisacob" src="imagens/logo_sisacob.png" alt="SISACOB"/>
	<div id="div_login">
	<label class="erro"><?=$_SESSION['mensagemLogin']; ?></label>
    <form name="form_login" method="POST" action="actions/login.php">
        <table>
            <tr><td>Agencia:</td><td><input type="text" name="agencia" value="<?=$_SESSION['agencia']; ?>" maxlength="4" onkeypress='return SomenteNumero(event)'/></td></tr>
            <tr><td>Conta:</td><td><input type="text" name="conta" value="<?=$_SESSION['conta']; ?>" maxlength="20" onkeyup="teste(this)" onkeypress='return SomenteNumero(event)' /></td></tr>
            <tr><td>Login:</td><td><input type="text" name="login" value="<?=$_SESSION['login']; ?>" maxlength="20" /></td></tr>
            <tr><td>Senha:</td><td><input type="password" name="senha" maxlength="20"/></td></tr>
        </table>
        <input type="submit" value="Logar" name="submit"/>
    </form>
    </div>
</body>
</html>