<?php
	session_start();
	require_once('../library/config.php');
	require_once('../library/db.php');
	require_once('../library/param.php');
	
	$param = new Param();
	
	if( $param->get('submit')->present() ){
		$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		$campoVazio = false;
		
		/*echo '<pre>';
			var_dump($param);				
		exit('<pre>');
		*/
		
		$agencia = $param->get('agencia');
		$campoVazio = $campoVazio || $param->get('agencia')->blank();
		$_SESSION['agencia'] = $param->get('agencia')->toHTML();
		
		$login = $param->get('login');
		$campoVazio = $campoVazio || $param->get('login')->blank();
		$_SESSION['login'] = $param->get('login')->toHTML();
		
		$conta = $param->get('conta');
		$campoVazio = $campoVazio || $param->get('conta')->blank();
		$_SESSION['conta'] = $param->get('conta')->toHTML();
		
		$senha = $param->get('senha');
		$campoVazio = $campoVazio || $param->get('senha')->blank();
		
		/*echo '<pre>';
		var_dump($agencia);
		var_dump($login);
		var_dump($conta);
		var_dump($senha);
		exit('<pre>');*/

		if( !$campoVazio ){
			$sql = "SELECT 
						 * 
					FROM 
						clientes 
					WHERE 
						agencia = '".$agencia->toDB()."' AND
						conta like '%".$conta->toDB()."' AND
						usuario = '".$login->toDB()."' AND
						senha = '".$senha->toDB()."'
					LIMIT 1
					";
			$query = $db->query($sql);

			if($query->num_rows > 0){
				//TODO: setar as variaveis com informações do cliente
				$_SESSION['cliente'] = $query->row['cliente'];
				$_SESSION['logado'] = true;
				header("Location: importacao.php");
			}
			else{
				$_SESSION['mensagemLogin'] = 'Erro, usuário não encontrado';
				header("Location: ../index.php");
			}
		}else{
			$_SESSION['mensagemLogin'] = 'Erro, preencha os campos corretamentes';
			header("Location: ../index.php");
		}
	
	}else{
		header("Location: index.php");
	}
?>