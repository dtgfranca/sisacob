<?php

	/**
	 *  Classe que gera o arquivo de log
	 *  @author Luiz Nogueira <luiz.nogueira@skillnet.com.br>	 
	 *  @version 1.0 
	 */

	class Log {
		
		static private $linhas = null;
		static private $caminho = '../logs/LogPHP.txt';
		static private $instancia = null;
		
		public function __construct() {
		}		

		/**
		 * Gera a instancia seguindo o Padr�o de Projeto "Singleton"
		 * @author Luiz Nogueira <luiz.nogueira@skillnet.com.br>		 
		 * @return Log $instancia
		 * @example Log::getInstance()
		 */
		
		static public function getInstance() {
			if(!self::$instancia) {
				self::$instancia = new Log();
			}			
			return self::$instancia;
		}
		
		/**
		 * Seta onde o arquivo ser� gerado
		 * @author Luiz Nogueira <luiz.nogueira@skillnet.com.br>		 
		 * @param String $novocaminho
		 * @return bool
		 * @example Log::setCaminho('C:\\testeLogPHP.txt')
		 */
		static public function setCaminho($novocaminho) {
			if(!empty($novocaminho)) {
				self::limpaLog();
				self::$caminho = $novocaminho;
				return true;
			}	
			return false;	
		}
		
		/**
		 * Escreve uma linha no arquivo que ser� gerado
		 * @author Luiz Nogueira <luiz.nogueira@skillnet.com.br>		 
		 * @param String $msg
		 * @param bool $datahora
		 * @return bool
		 * @example Log::escreve('Erro na query X')
		 */		
		static public function escreve($msg = null, $datahora = true) {
			if(!empty($msg)) {				
				$datahora 
					? self::$linhas[] = '[' . date('d-m-Y') . ']' . '[' . date('H:i:s') . ']' . ' - ' . $msg
					: self::$linhas[] = $msg;
				return true;								
			}
			return false;
		}
		
		/**
		 * Limpa o array do Log
		 * @return bool
		 */
		static public function limpaLog() {
			if(!empty(self::$linhas)) {
				self::$linhas = array();
				return true;
			}
			return false;
		}
		
		/**
		 * Gera o arquivo de log
		 * @author Luiz Nogueira <luiz.nogueira@skillnet.com.br>		 
		 * @return bool
		 * @example Log::geraArquivo()
		 */
		static public function geraArquivo() {
			if(!empty(self::$linhas)) {
				$abreArq = fopen(self::$caminho, 'w+');				
				if($abreArq) {
					$conteudo = implode("\n", self::$linhas);
					$escreveArq = fwrite($abreArq, $conteudo);					
					if($escreveArq) {
						$fechaArq = fclose($abreArq);
						return true;
					}
					return false;
				}
				return false;
			}
			return false;
		}
		 
	}

?>