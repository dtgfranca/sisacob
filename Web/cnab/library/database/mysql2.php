<?php
final class MySQL {
  private $connection;
  private $debug;
  
  public function __construct($hostname, $username, $password, $database, $debug = false) {
    $this->debug = $debug;
    if (!$this->connection = mysql_connect($hostname, $username, $password)) {
      exit('Error: Could not make a database connection' . ( $this->debug ? ' using ' . $username . '@' . $hostname : '' ) );
    }

    if (!mysql_select_db($database, $this->connection)) {
      exit('Error: Could not connect to database ' . ($this->debug ? $database : '' ) );
    }
    
    mysql_query("SET NAMES 'utf8'", $this->connection);
    mysql_query("SET CHARACTER SET utf8", $this->connection);
    mysql_query("SET CHARACTER_SET_CONNECTION=utf8", $this->connection);
    mysql_query("SET SQL_MODE = ''", $this->connection);
  }

  public function query($sql) {
    $resource = mysql_query($sql, $this->connection);
    
    $query = new stdClass();
    $query->sql = $sql;
    $query->row = array();
    $query->rows = array();
    $query->num_rows = 0;
    $query->executed = false;
    $query->message = "";

    if ($resource) {
      
      if (is_resource($resource)) {
        $i = 0;
        $data = array();
    
        while ($result = mysql_fetch_assoc($resource)) {
          $data[$i] = $result;
          $i++;
        }

        mysql_free_result($resource);

        $query->sql = $sql;
        $query->row = isset($data[0]) ? $data[0] : array();
        $query->rows = $data;
        $query->num_rows = $i;
        $query->executed = true;

        unset($data);
      } else {
        $query->message = 'Error: ' . mysql_error($this->connection) . '<br />Error No: ' . mysql_errno($this->connection) . ($this->debug ? '<br />' . $sql : '');
      }
      
    } else {
      $query->message = 'Error: ' . mysql_error($this->connection) . '<br />Error No: ' . mysql_errno($this->connection) . ($this->debug ? '<br />' . $sql : '');
    }
    return $query;  
  }

  public function escape($value) {
    return mysql_real_escape_string($value, $this->connection);
  }

  public function countAffected() {
    return mysql_affected_rows($this->connection);
  }

  public function getLastId() {
    return mysql_insert_id($this->connection);
  }  
  
  public function __destruct() {
    mysql_close($this->connection);
  }
}
?>
