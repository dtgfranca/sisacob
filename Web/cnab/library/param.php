<?php
require_once('text.php');
final class Param {
    private $data;

    public function __construct() {
        $this->initialize();
    }

    private function initialize(){
        $gets  = $this->escape($_GET);
        $posts = $this->escape($_POST);
        $this->data = array_merge($gets, $posts);
    }

    private function escape($array){
        $values = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $values["$key"] = $this->escape($value);
            } else {
                $str = new Text($value, 'UTF-8');
                $values["$key"] = $str->trim();
            }
        }
        return $values;
    }

    public function get($indexes, $other = NULL){
        $result = $this->data;
        foreach ( preg_split ("/\./", $indexes) as $index){
            if (isset($result["$index"])){
                $result = $result["$index"];
            } else {
                return new Text($other);
            }
        }
        return $result;
    }

    public function raw($str) {
        return stripslashes($str);  # retira barras invertidas à uma string.
    }
}
?>
