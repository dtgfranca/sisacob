<?php
	header("Cache-Control: no-cache",true);
	header("Pragma: no-cache",true);
	header("Expires: 0",true);
	
	require('config.php');
	$cliente	=$_COOKIE["cokcliente"];
	$agencia	=$_COOKIE["cokagencia"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
* {font: normal 10px Arial, Helvetica, sans-serif; font-weight:bold}
</style>
<link rel="stylesheet" href="css/app.css" type="text/css">
<script src="js/fun.js" type="text/javascript"></script>
<script type="text/javascript">
function desmarcar(campo){
	if(!campo.checked){
		document.getElementById("checkAll").checked=false;
	}	
}
</script>
</head>
<body>
<table width="100%">
<!--	<tr  bgcolor='#cccccc'>
		<td colspan='7' align="center">
			<b>T&Iacute;TULOS CADASTRADOS NO DIA <?php //echo date("d/m/Y"); ?></b>
		</td>
	</tr> -->

	<?php
	$imprimir = "";
	$consulta = "";
	$documento = "";
	$cont = 0;
	
	$consulta .= "SELECT scd.nome, scd.cpf, tit.documento, tit.sequencia, tit.nossonumero, tit.agencia, tit.cad_completo, tit.titulo, ";
	$consulta .= "DATE_FORMAT(tit.data_emisao, '%d/%m/%Y') AS data_emisao, DATE_FORMAT(tit.data_venc, '%d/%m/%Y') AS data_venc, ";
	$consulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor, tit.criacao, tit.so_desconto ";
	$consulta .= "FROM titulos AS tit ";
	$consulta .= "INNER JOIN sacados AS scd ON scd.sacado=tit.sacado ";
	$consulta .= "WHERE tit.cliente = '".$cliente."' ";
	$consulta .= "AND (scd.grupo <> '99999' OR scd.grupo IS NULL) ";
	$consulta .= "AND registro IS NULL ";
	$consulta .= "AND cancelamento IS NULL ";
	//$consulta .= "AND (tit.criacao >= '".date('Y-m-d')." 00:00:00' AND tit.criacao <= '".date('Y-m-d')." 23:59:59') ";
	$consulta .= "ORDER BY tit.criacao DESC, scd.nome, tit.documento, tit.sequencia ASC";
	
	$sql = mysql_query($consulta);
	$total = mysql_num_rows($sql);	
	?>
	
	<tr  bgcolor='#cccccc' >
		<td width="7%" align='center'><input name='checkAll' id='checkAll' type='checkbox' value='' onclick='selecionar(this,<?php echo $total; ?>)' /><b>TODOS</b></td>
		<td width="18%" align='center'>Cliente</td>
		<td width="10%" align='center'>CPF/CNPJ</td>
		<td width="10%" align='center'>N&ordm; Documento</td>
		<td width="10%" align='center'>Data Emiss&atilde;o</td>
		<td width="10%" align='center'>Data Vencimento</td>
		<td width="10%" align='center'>Valor da Fatura</td>
		<td width="5%" align='center'>P/ Desconto</td>
		<td width="7%" align='center'>A&ccedil;&otilde;es</td>
	</tr>
	
	<?php
	
	while($result = mysql_fetch_array($sql))
	{
		//if($documento != $result['documento']){
			if(fmod($cont,2)!=0){
				$fundo="bgcolor='#F4F4F4'";
			}else{
				$fundo= "";
			}
			$imprimir .= "<tr height='40' ".$fundo.">";
			if($result['cad_completo']!="S"){
			    $imprimir .= 	"<td align='center'><input class='conf_titulos' type='checkbox' name='chk_titulos[]' id='chk_titulos".$cont."' onclick='desmarcar(this)' value='".$result['titulo']."'></td>";	
			}
			else{
				$imprimir .= 	"<td align='center'><input class='conf_titulos' disabled='disabled' type='checkbox' name='chk_titulos[]' id='chk_titulos".$cont."' value='".$result['titulo']."'></td>";
			}			
			$imprimir .= 	"<td align='left'>".$result['nome']."</td>";
			$imprimir .= 	"<td align='left'>".$result['cpf']."</td>";
			$imprimir .= 	"<td align='left'>".$result['documento']."/".$result['sequencia']."</td>";
			$imprimir .= 	"<td align='center'>".$result['data_emisao']."</td>";
			$imprimir .= 	"<td align='center'>".$result['data_venc']."</td>";
			$imprimir .= 	"<td align='right'>".$result['valor']."</td>";
			$imprimir .= 	"<td align='center'>".$result['so_desconto']."</td>";
			$imprimir .= 	"<td align='center' >";
			if($result['cad_completo']!="S"){
				$imprimir .= 	"	<img src='images/editar.png' border='0' onClick='altera_tit(".$result['titulo'].")' onMouseOver='this.className=\"img_opacity\"' onMouseOut='this.className=\"img_normal\"'>";
				$imprimir .= 	"	<img src='images/confirmar.png' border='0' onClick='confirmaCad(".$result['titulo'].")' onMouseOver='this.className=\"img_opacity\"' onMouseOut='this.className=\"img_normal\"' >";
			}else{
				$imprimir .= 	"	<img src='images/ok.png' border='0'>";
			}
			$imprimir .= 	"</td>";
			$imprimir .= "</tr>";
			$cont++;
		//}	
		//$documento = $result['documento'];
	}
	$imprimir .= "
				 <tr>
				    <td colspan='8'></td>
				    <td><input type='button' value='Confirma Selecionados' onclick='visualiza_tit()'></td>
				 </tr>
				 ";
	echo $imprimir;
	?>
</table>
<div id="cad_tit_comfirma" class="fimProcesso" ></div>
<div id="cad_tit_comfirma_p" class="fimProcesso" align="center" >Aguarde... <br><img src="images/processar.gif" border="" ></div>
</body>
</html>