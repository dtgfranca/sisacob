<?
require('config.php');

$cliente=$_COOKIE["cokcliente"];
$agencia=$_COOKIE["cokagencia"];

$rs_cli   = mysql_query("SELECT razao, conta FROM clientes WHERE cliente = $cliente");

$inicio		= $_GET["inicio"];
$fim		= $_GET["fim"];
$tipo		= $_GET["tipo"];
$tipo2		= $_GET["tipo2"];
$sacado		= $_GET["sacado"];
$ordem		= $_GET["ordem"];
$ordem2		= $_GET["ordem2"];
$filtro		= $_GET["filtro"];

$tipoB		= $_GET["tipoB"];
$n_titulo	= $_GET["n_titulo"];
$nosso_n	= $_GET["nosso_n"];

if($tipo=="todos"){ 
   $value_tipo = "RELAT&Oacute;RIO AGRUPADO";
}
if($tipo=="rec"){ 
   $value_tipo = "RECEBIDOS";
}
if($tipo=="baixa"){ 
   $value_tipo = "BAIXADOS";
}
if($tipo=="pend"){ 
   $value_tipo = "EM ABERTO";
}
if($tipo=="venc"){
   $value_tipo = "VENCIDOS";
}
if($tipo=="cancel"){ 
   $value_tipo = "CANCELADOS";
}
if($tipo=="lista_previa"){ 
   $value_tipo = "LISTAGEM PR&Eacute;VIA";
}
if($tipo=="rejeitados"){ 
   $value_tipo = "REJEITADOS";
}
if($tipo=="nosso_n"){ 
   $value_tipo = "NOSSO NUMERO ".$nosso_n;
}

$value_periodo = "";
if(!empty($inicio)){
	$value_periodo = "<font size='1'> - Com ";
	if($filtro=="data_emisao"){
		$value_periodo .= "data da emiss&atilde;o";
	}
	if($filtro=="data_venc"){
		$value_periodo .= "data do vencimento";
	}
	if($filtro=="data_baixa"){
		$value_periodo .= "data da baixa";
	}
	$value_periodo .= " entre o período: ".$inicio." a ".$fim."</font>"; 
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relatório de boletos emitidos</title>
</head>
<body>
<table width="95%">
  <tr>
    <td align="right" width="60%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='3'><b>
       Relatório de boletos
       </b></font>
    </td>
    <td align="right" width="40%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
       <?php
       echo date("d/m/Y - H:i");
       ?>
       </font>
    </td>
  </tr>
</table>
<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
<table border='0' width='95%'>
    <tr>
		<td colspan='10' align="center" style="font-size:14px;border:2px solid #9C9C9C">
			<?php echo $value_tipo . $value_periodo; ?> 
		</td>
	</tr>
	<tr>
		<td colspan='10'>
			<table>
				<tr>
					<td width="20%" >CLIENTE:</td>
					<td width="80%" ><?php echo mysql_result($rs_cli,0,0); ?></td>
				</tr>
				<tr>
					<td>AG&Ecirc;NCIA:</td>
					<td><?php echo $agencia; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTA:&nbsp;&nbsp;<?php echo mysql_result($rs_cli,0,1); ?></td>
				</tr>
			</table>
		</td>
	</tr>
<?

print "<br>";	

	if(!empty($inicio) and !empty($fim)){
		list ($dia,$mes,$ano) = split ('/',$inicio);
		$inicio="'$ano-$mes-$dia'";
		list ($dia,$mes,$ano) = split ('/',$fim);
		$fim="'$ano-$mes-$dia'";
		if($filtro=="data_baixa"){
			$b_data=" and ((titulos.data_baixa>=$inicio and titulos.data_baixa<=$fim) or (titulos.data_baixa_manual>=$inicio and titulos.data_baixa_manual<=$fim)) ";
			$b_data2=" and ((data_baixa>=$inicio and data_baixa<=$fim) or (data_baixa_manual>=$inicio and data_baixa_manual<=$fim)) ";			
		}else{
			$b_data=" and titulos.$filtro>=$inicio and titulos.$filtro<=$fim";
			$b_data2=" and $filtro>=$inicio and $filtro<=$fim";
		}
				
		//$b_data=" and titulos.$filtro>=$inicio and titulos.$filtro<=$fim";
		//$b_data2=" and data_venc>=$inicio and data_venc<=$fim";
	}

	if(!empty($documento)){
		$b_documento=" and titulos.documento='$documento'";
	}
	if($tipo=="rec"){	
		$b_tipo="and (titulos.data_baixa is not null or titulos.data_baixa_manual is not null) and (status = '05' or status = '06' or status = '07' or status = '08' or status = '15')";
	}
	if($tipo=="baixa"){
		$b_tipo="and (titulos.data_baixa is not null or titulos.data_baixa_manual is not null) and titulos.devolucao is not null and (titulos.status = '09'  or titulos.status = '10') ";
	}
	if($tipo=="pend"){
		$b_tipo="and (titulos.data_baixa is null and titulos.data_baixa_manual is null and titulos.cancelamento is null)";
	}
	if($tipo=="venc"){
		$b_tipo="and titulos.data_venc < CURDATE() and titulos.data_baixa is null and titulos.data_baixa_manual is null and titulos.cancelamento is null";
	}
	if($tipo=="cancel"){
		$cancel="and titulos.cancelamento is not null";
	}
	if($tipo=="rejeitados"){
		$b_tipo="and titulos.status='03' and titulos.cancelamento is null";
	}
	if($tipo=="nosso_n"){
		$b_tipo="and titulos.nossonumero like '%".$nosso_n."'";
	}else if($tipo=="cancel"){
		$cancel="and titulos.cancelamento is not null";
	}
	if($tipo=="lista_previa"){
	    $cad_completo = "and cad_completo = 'N' ";
	}
	else{
        $cad_completo = "and cad_completo = 'S' ";	
	}
	
	if(!empty($tipoB)){
		$b_tipo.=" and titulos.documento like '%".$n_titulo."'";
	}
######## Rotina que filtra a pesquisa vinda do Situação2 ##########
	switch($tipo2)
	{
		case "desc":
			if($tipo=="rec"){
				$b_tipo2 = "and titulos.desconto is not null and so_desconto='S'";
			}else if($tipo=="todos"){
				$b_tipo2 = "and so_desconto='S'";
			}else{
				$b_tipo2 = "and titulos.desconto is null and so_desconto='S'";
			}
		break;
		case "cobranca_simples":
			if($tipo=="rec"){
				$b_tipo2 = "so_desconto='N'";
			}else if($tipo=="todos"){
				$b_tipo2 = "and so_desconto='N'";
			}else{
				$b_tipo2 = "so_desconto='N'";
			}
		break;
	}
######## Fim desta rotina ################

	if(!empty($sacado)){
		$b_sacado="and titulos.sacado='$sacado'";
	} 

	$sql = mysql_query("select titulos.titulo,titulos.status, titulos.documento, titulos.devolucao, titulos.nossonumero, DATE_FORMAT(titulos.data_emisao, '%d/%c/%Y') as data_emisao, DATE_FORMAT(titulos.data_venc, '%d/%c/%Y') as data_venc, titulos.valor, titulos.modelo, titulos.sequencia,DATE_FORMAT(coalesce(titulos.data_baixa,titulos.data_baixa_manual), '%d/%c/%Y') as data_baixa, DATE_FORMAT(titulos.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(titulos.data_credito, '%d/%c/%Y') as data_credito,  titulos.valor_baixa, titulos.sacado, sacados.nome, DATE_FORMAT(NOW(), '%d/%c/%Y - %H:%i') as data_atual
					    from titulos 
						left join sacados on titulos.sacado=sacados.sacado 
    					where titulos.cliente='$cliente' and data_emisao is not null $b_sacado $b_data $b_documento $b_tipo $b_tipo2 $cancel $cad_completo order by sacados.nome, $ordem2, titulos.nossonumero;")or die ("");

    if(mysql_num_rows($sql)>0){
		$sacado_ant="";
		while ($linha=mysql_fetch_array($sql)) {
		$sacado=$linha[sacado];
		if($sacado!=$sacado_ant and $x>1){
			print "<tr>";		
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'><b>Total</b></td>";
				print "<td  align='right' bgcolor='#CCCCCC'>".number_format($valor_total, 2, ',', '.')."</td>";
				print "<td  align='right'bgcolor='#CCCCCC'>".number_format($valor_baixa_total, 2, ',', '.')."</td>";
			print "</tr>";
			print "<tr>";		
				print "<td colspan='9'>&nbsp;";
				print "</td>";
			print "</tr>";

		}
		if($sacado!=$sacado_ant){
			$x=$valor_total=$valor_baixa_total=0;
			print "<tr>";		
				print "<td style='font-size:12px;border:2px solid #9C9C9C' colspan='9'>";
						print "<b>".$linha[nome]."</b>";	
				print "</td>";
			print "</tr>";
/*			$total = mysql_query("select titulo
				from titulos
				where cliente='$cliente' and sacado='$sacado' $b_data2;");
			$total2=@mysql_num_rows($total);
*/
			$aberto = mysql_query("select titulo
				from titulos
				where cliente='$cliente' and sacado='$sacado' $b_data2 and (data_baixa is null and cancelamento is null and  data_baixa_manual is null and desconto is null);");
			$abertos=@mysql_num_rows($aberto);
			$baixado = mysql_query("select titulo
				from titulos
				where cliente='$cliente' and sacado='$sacado' $b_data2 and data_baixa is not null;");
			$baixados=@mysql_num_rows($baixado);
			$manual = mysql_query("select titulo
				from titulos
				where cliente='$cliente' and sacado='$sacado' $b_data2 and data_baixa_manual is not null;");
			$manuais=@mysql_num_rows($manual);
			$cancelado = mysql_query("select titulo
				from titulos
				where cliente='$cliente' and sacado='$sacado' $b_data2 and cancelamento is not null;");
			$cancelados=@mysql_num_rows($cancelado);
				
			print "<tr>";		
				print "<td colspan='9' align='right'>";
						//print "<b>Titulos no periodo:</b> ".number_format($total2, 0, ',', '')."<br>";	
						print "<b>Titulos abertos:</b> ".number_format($abertos, 0, ',', '')."<br>";	
						print "<b>Titulos baixados:</b> ".number_format($baixados, 0, ',', '')."<br>";	
						print "<b>Titulos baixados manual:</b> ".number_format($manuais, 0, ',', '')."<br>";
						print "<b>Titulos cancelados:</b> ".number_format($cancelados, 0, ',', '');		
				print "</td>";
			print "</tr>";
			print "<tr>";		
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Nº Documento</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Nosso Numero</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Data Emissão</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Data Vencimento</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Data Pagamento</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Data Pg. Cartório</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Data Crédito</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Valor da Fatura</td>";
				print "<td style='font-size:12px;border:2px solid #9C9C9C' align='center'>Valor Recebido</td>";
			print "</tr>";
		}
		$x=$x+1;
		print "<tr>";		
			print "<td  align='right'>".$linha[documento]."/".$linha[sequencia]."</td>";
			print "<td  align='right'>".$linha[nossonumero]."</td>";
			print "<td  align='right'>".$linha[data_emisao]."</td>";
			print "<td  align='right'>".$linha[data_venc]."</td>";
			if($linha[status]==9){
				print ($linha[cancelamento]==""?"<td  align='right'>&nbsp;":"<td  align='center'>---")."</td>";
				print ($linha[cancelamento]==""?"<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['status'] == '09' || $linha['status'] == '10')?" -- ":$linha[data_baixa]):"<td  align='center'>---")."</td>";
			}else{
				print ($linha[cancelamento]==""?"<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['status'] == '09' || $linha['status'] == '10')?" -- ":$linha[data_baixa]):"<td  align='center'>---")."</td>";
				print ($linha[cancelamento]==""?"<td  align='right'>&nbsp;":"<td  align='center'>---")."</td>";
			}
			print ($linha[cancelamento]==""?"<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['status'] == '09' || $linha['status'] == '10')?" -- ":$linha[data_baixa]):"<td  align='center'>---")."</td>";
			print ($linha[cancelamento]==""?"<td  align='right'>".$linha[valor]:"<td  align='center'>---")."</td>";
			print ($linha[cancelamento]==""?"<td  align='right'>".($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['status'] == '09' || $linha['status'] == '10')?" -- ":$linha[valor_baixa]):"<td  align='center'>---")."</td>";
		print "</tr>";
			if ($linha[cancelamento]==""){
			   $valor_total=$valor_total+$linha[valor];
			}
			if(!($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['status'] == '09' || $linha['status'] == '10'))){
				$valor_baixa_total=$valor_baixa_total+$linha[valor_baixa];
			}
			$sacado_ant=$linha[sacado];
		} 
			print "<tr>";		
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='center'></td>";
				print "<td  align='right' bgcolor='#CCCCCC'>".number_format($valor_total, 2, ',', '.')."</td>";
				print "<td  align='right'bgcolor='#CCCCCC'>".number_format($valor_baixa_total, 2, ',', '.')."</td>";
			print "</tr>";		
	}
?>
</form>
</table>
</body>
</html>
