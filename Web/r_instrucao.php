<?php
require('config.php');

$cliente = $_COOKIE['cokcliente'];
$conta 	 = $_COOKIE['cokconta'];
$agencia = $_COOKIE['cokagencia'];

$sacado  = $_GET['sacado'];

if ($sacado != -1){
   $filtro_sacado1 = "AND titulos.sacado = ".$sacado;
   $filtro_sacado2 = "AND cad_instrucoes.sacado = ".$sacado;
}   
else{ 
   $filtro_sacado1 = "";
   $filtro_sacado2 = "";
}   

$rs_cli   = mysql_query("SELECT razao FROM clientes WHERE cliente = $cliente");

$sql = "
	   SELECT sacados.nome AS nome_geral, CONCAT(titulos.documento,'/',titulos.sequencia) AS documento, titulos.nossonumero, DATE_FORMAT(cad_instrucoes.data_instr, '%d/%c/%Y') AS data_instr, DATE_FORMAT(cad_instrucoes.data_envio, '%d/%c/%Y') AS data_envio, cad_instrucoes.comando, cad_instrucoes.id, cad_instrucoes.sacado, cad_instrucoes.status 
	   FROM cad_instrucoes
			LEFT JOIN titulos ON cad_instrucoes.boleto = titulos.titulo
			LEFT JOIN clientes ON cad_instrucoes.cliente = clientes.cliente
			LEFT JOIN sacados ON titulos.sacado = sacados.sacado
	   WHERE cad_instrucoes.cliente = $cliente
      		 AND cad_instrucoes.agencia = $agencia
      		 $filtro_sacado1
      		 AND cad_instrucoes.data_envio IS NOT NULL
      		 AND cad_instrucoes.boleto IS NOT NULL
      		 AND sacados.nome IS NOT NULL
	   UNION ALL
	   SELECT sacados.nome AS nome_geral, CONCAT(titulos.documento,'/',titulos.sequencia) AS documento, titulos.nossonumero, DATE_FORMAT(cad_instrucoes.data_instr, '%d/%c/%Y') AS data_instr, DATE_FORMAT(cad_instrucoes.data_envio, '%d/%c/%Y') AS data_envio, cad_instrucoes.comando, cad_instrucoes.id, cad_instrucoes.sacado, cad_instrucoes.status
	   FROM cad_instrucoes
			LEFT JOIN titulos ON cad_instrucoes.boleto = titulos.titulo
			LEFT JOIN clientes ON cad_instrucoes.cliente = clientes.cliente
			LEFT JOIN sacados ON cad_instrucoes.sacado = sacados.sacado
	   WHERE cad_instrucoes.cliente = $cliente
      		AND cad_instrucoes.agencia = $agencia
      		$filtro_sacado2
      		AND cad_instrucoes.data_envio IS NOT NULL
      		AND cad_instrucoes.boleto IS NULL
      		AND sacados.nome IS NOT NULL
	   ORDER BY id desc
	   ";

$rs = mysql_query($sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Instru&ccedil;&otilde;es Executadas</title>
</head>
<body>

<table width="100%">
  <tr>
    <td align="right" width="60%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='3'><b>Relat&oacute;rio de Instru&ccedil;&otilde;es Executadas</b></font>
    </td>
    <td align="right" width="40%">
       <font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
       <?php
       date_default_timezone_set("Brazil/East");
       echo date("d/m/Y - H:i");
       ?>
       </font>
    </td>
  </tr>
</table>

<font face='Verdana, Arial, Helvetica, sans-serif' size='1'>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td align="center" style="font-size:14px;border:2px solid #9C9C9C">
			CARTEIRA <?php echo "- ".date("d/m/Y"); ?>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="20%" >CLIENTE:</td>
					<td width="80%" ><?php echo mysql_result($rs_cli,0); ?></td>
				</tr>
				<tr>
					<td>AG&Ecirc;NCIA:</td>
					<td><?php echo $agencia; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTA:&nbsp;&nbsp;<?php echo $conta; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	    <td>
	        <table width="100%">
				<tr>
				   <td width="20%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Cliente
				   </td>
				   <td width="15%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   N&ordm; Documento
				   </td>
				   <td width="15%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Nosso numero
				   </td>
				   <td width="10%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Data Instru&ccedil;&atilde;o
				   </td>
				   <td width="10%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Data Envio
				   </td>
				   <td width="20%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Comando
				   </td>
				   <td width="10%" align="center" style="font-size:12px;border:2px solid #9C9C9C">
					   Status
				   </td>
				</tr>
				<?php
				$x=0;
				while ($dados = mysql_fetch_array($rs)){
					
					switch ($dados['comando']) {
						case '06':
						$comando = "Altera&ccedil;&atilde;o de Vencimento";
						break;
						
						case '10':
						$comando = "Cancelamento/Susta&ccedil;&atilde;o da instru&ccedil;&otilde;es de Pr";
						break;

						case '04':
					 	$comando = "Concess&atilde;o de abatimento";
					 	break;
					 	
					 	case '35':
					 	$comando = "Multa";
					 	break;
					 	
					 	case '09':
					 	$comando = "Protestar";
					 	break;
					 	
					 	case '02':
					 	$comando = "Solicita&ccedil;&atilde;o de Baixa";
					 	break;
					 	
					 	case '05':
					 	$comando = "Cancelamento de Abatimento";
					 	break;
					 	
					 	case '32':
					 	$comando = "Cancelamento de Desconto";
					 	break;
					 	
					 	case '31':
					 	$comando = "Concess&atilde;o de desconto";
					 	break;
					 	
					 	case '12':
					 	$comando = "Cliente-Altera&ccedil;&atilde;o de endere&ccedil;o";
					 	break;
					 	
						default:
						$comando = "Comando desconhecido";
					} 
					
					if ($x==0){
					  $cor = "#E6E6FA";
					  $x=1;
					}  
					else{
					  $cor = "#DCDCDC";
					  $x = 0;	
					}  
					
					echo "<tr style='background-color:$cor'>";
					echo "  <td align='left'>".$dados['nome_geral']."</td>";
					
					if ($dados['documento'] != null)
					   echo"<td align='center'>".$dados['documento']."</td>";
					else  
					   echo"<td align='center'>Altera&ccedil;&atilde;o de endere&ccedil;o</td>";
					
					if ($dados['nossonumero'] != null)      
					   echo"<td align='center'>".$dados['nossonumero']."</td>";
					else  
					   echo"<td align='center'>Altera&ccedil;&atilde;o de endere&ccedil;o</td>";   
					
					echo "  <td align='center'>".$dados['data_instr']."</td>";
					echo "  <td align='center'>".$dados['data_envio']."</td>";
					echo "  <td align='left'>".$comando."</td>";
					if ($dados['status'] == 3)
					   echo "  <td align='center'>Rejeitado</td>";
					else  
					   echo "  <td align='center'>OK</td>";
					echo "</tr>";
				}
				?>
	        </table>
	    </td>
	</tr>
</table>
</font>
</body>
</html>