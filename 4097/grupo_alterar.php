<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	
	include_once('../config.php');
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready( function() {		
		$("#altera_grupo").validate({
			rules:{
				nome:{
					required: true, minlength: 10
				}
			}
		});
	});
function alt_grupo(grupo)
{	
	var nome = document.getElementById("nome").value;
	var tnome = nome.length;
	
	if(tnome < 3){
		alerta('Campo nome é obrigatório (pelo menos 3 caracteres).');
		return false;
	}
	else {
		
		$.ajax({
				type: "GET",
				url: "scripts/grupos_funcoes.php",
			data: "funcao=alt_grupo&grupo="+grupo+"&nome="+nome,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Alteração realizada com sucesso!");
					$("#altera_grupo input#alterar").prop("disabled", true);
					navega('4097/grupo_buscar.php');
				}
				else{
					alerta(retorno);
				}		
			}
		});
		return false;
	}
};
</script>
<?php 
	include('../config.php');
   	$grupo = $_GET['grupo'];
	$sql = "SELECT gsc_nome FROM sc_grupo WHERE gsc_id = '$grupo'";
	$query = mysql_query($sql) or die(mysql_error());
	$linha = mysql_fetch_array($query);
?>
	<div class="titulo">
        <h2>Grupos</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
		<form id="altera_grupo" name="altera_grupo" method="post">
			<fieldset>
				<legend>Alterar  Grupos</legend>
				<table>
					<tr>
						<td colspan="3"><label for="nome">Nome:</label>
					    <input type="hidden" id="grupo" value="<?php echo $grupo; ?>" />						  <input name="nome" type="text" id="nome" value="<?php echo $linha['gsc_nome']; ?>" size="50" maxlength="40" />					    &nbsp;&nbsp;
					</tr>
				</table>
			</fieldset>
            <a class="btn botao margins dir" href="#" onClick="alt_grupo('<?php echo $grupo; ?>'); return false;">Alterar</a>
			<!--<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="alt_grupo()" />-->
			<input class="btn botao margins dir" type="button" name="voltar" id="voltar" value="Voltar" onclick="navega('4097/grupo_buscar.php')" />
        	<br class="clear" />
		</form>
	</div>
</div>