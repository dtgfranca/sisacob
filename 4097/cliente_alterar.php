<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	
	include_once('../config.php');
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready( function() {		
		$("#cpfcnpj").keyup(function(){
			var tam = $(this).val().length+1;
			if(tam >= 15){
				$("#cpfcnpj").removeAttr('onkeypress');
				$("#cpfcnpj").attr('onkeypress','formataCampo(this, "00.000.000/0000-00", event); return SomenteNumero(event)');
			}
			else {
				$("#cpfcnpj").removeAttr('onkeypress');
				$("#cpfcnpj").attr('onkeypress','formataCampo(this, "000.000.000-00", event); return SomenteNumero(event)');
			}
		});
		$("#altera_cliente #uf").keyup(function(){
			if($("#altera_cliente #uf").val()=='SP'){
				$("#altera_cliente #fax").removeAttr('maxlength');
				$("#altera_cliente #fax").attr('maxlength','14');
				$("#altera_cliente #fax").removeAttr('onkeypress');
				$("#altera_cliente #fax").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_pessoal").removeAttr('maxlength');
				$("#altera_cliente #tel_pessoal").attr('maxlength','14');
				$("#altera_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#altera_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #celular").removeAttr('maxlength');
				$("#altera_cliente #celular").attr('maxlength','14');
				$("#altera_cliente #celular").removeAttr('onkeypress');
				$("#altera_cliente #celular").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_comercial").removeAttr('maxlength');
				$("#altera_cliente #tel_comercial").attr('maxlength','14');
				$("#altera_cliente #tel_comercial").removeAttr('onkeypress');
				$("#altera_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
			}
			else {
				$("#altera_cliente #fax").removeAttr('maxlength');
				$("#altera_cliente #fax").attr('maxlength','13');
				$("#altera_cliente #fax").removeAttr('onkeypress');
				$("#altera_cliente #fax").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_pessoal").removeAttr('maxlength');
				$("#altera_cliente #tel_pessoal").attr('maxlength','13');
				$("#altera_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#altera_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #celular").removeAttr('maxlength');
				$("#altera_cliente #celular").attr('maxlength','13');
				$("#altera_cliente #celular").removeAttr('onkeypress');
				$("#altera_cliente #celular").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_comercial").removeAttr('maxlength');
				$("#altera_cliente #tel_comercial").attr('maxlength','13');
				$("#altera_cliente #tel_comercial").removeAttr('onkeypress');
				$("#altera_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
			}
		});
		$("#altera_cliente").validate({
			rules:{
				nome:{
					required: true, minlength: 10
				},
				identidade:{
					required: true, minlength: 8
				},
				cpfcnpj:{
					required: true, minlength: 11
				},
				cep:{
					required: true, minlength: 8
				},
				endereco:{
					required: true, minlength: 2
				},
				bairro:{
					required: true, minlength: 2
				},
				cidade:{
					required: true, minlength: 2
				},
				uf:{
					required: true, minlength: 2
				},
				email:{
					required: true, email: true
				},
				tel_pessoal:{
					required: true, minlength: 8
				},
				fax:{
					required: true, minlength: 8
				},
				tel_comercial:{
					required: true, minlength: 8
				},
				celular:{
					required: true, minlength: 8
				},
			}
		});
	});
function alt_cliente(){
	
	var sacado = document.getElementById("sacado").value;
	var nome = document.getElementById("nome").value;
	var cpfcnpj = document.getElementById("cpfcnpj").value;
	var endereco = document.getElementById("endereco").value;
	var bairro = document.getElementById("bairro").value;
	var cidade = document.getElementById("cidade").value;
	var uf = document.getElementById("uf").value;
	var cep = document.getElementById("cep").value;
	var tel_pessoal = document.getElementById("tel_pessoal").value;
	var celular = document.getElementById("celular").value;
	var tel_comercial = document.getElementById("tel_comercial").value;
	var fax = document.getElementById("fax").value;
	var email = document.getElementById("email").value;
	var grupo = document.getElementById("gsc_id").value;
	var identidade = '';
	var tnome = nome.length;
	var tend = endereco.length;
	var tbai = bairro.length;
	var tcid = cidade.length;
	var tuf = uf.length;
	var tcep = cep.length;
	
	if(tnome < 10){
		alerta('Campo nome é obrigatório (pelo menos 10 caracteres).');
		return false;
	}
	else if(tend < 2){
		alerta('Campo endereço é obrigatório');
		return false;
	}
	else if(tbai < 2){
		alerta('Campo bairro é obrigatório');
		return false;
	}
	else if(tcid < 2){
		alerta('Campo cidade é obrigatório');
		return false;
	}
	else if(tuf < 2){
		alerta('Campo UF é obrigatório');
		return false;
	}
	else if(tcep < 9){
		alerta('Campo CEP é obrigatório');
		return false;
	}
	else {
		
		$.ajax({
				type: "GET",
				url: "scripts/clientes_funcoes.php",
			data: "funcao=alt_cliente&sacado="+sacado+"&nome="+nome+"&identidade="+identidade+"&cpfcnpj="+cpfcnpj+"&endereco="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&email="+email+"&tel_pessoal="+tel_pessoal+"&fax="+fax+"&tel_comercial="+tel_comercial+"&celular="+celular+"&cep="+cep+"&grupo="+grupo,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Alteração realizada com sucesso!");
					$("#altera_cliente input#alterar").prop("disabled", true);
					navega('4097/cliente_buscar.php');
				}
				else{
					alerta(retorno);
				}		
			}
		});
		return false;
	}
};
</script>
<?php 
	include('../config.php');
   	$sacado = $_GET['sacado'];
	$sql = "SELECT nome, cpf, endereco, bairro, cidade, uf, cep, telef_pess, telef_com, celular, fax, email, gsc_id FROM sacados WHERE sacado='$sacado'";
	$query = mysql_query($sql) or die(mysql_error());
	$linha = mysql_fetch_array($query);
	
	if($linha['uf'] == 'SP'){
		$param = 'maxlength="13" onkeypress="formataCampo(this, \'(00)00000-0000\', event);  return SomenteNumero(event)"';
	}
	else{
		$param = 'maxlength="13" onkeypress="formataCampo(this, \'(00)0000-0000\', event);  return SomenteNumero(event)"';
	}
?>
	<div class="titulo">
        <h2>Clientes</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
		<form id="altera_cliente" name="altera_cliente" method="post">
			<fieldset>
				<legend>Alterar  Clientes</legend>
				<table>
					<tr>
						<td colspan="3"><label for="nome">Nome:</label>
					    <input type="hidden" id="sacado" value="<?php echo $sacado; ?>" />						  <input name="nome" type="text" id="nome" value="<?php echo $linha['nome']; ?>" size="50" maxlength="40" />					    &nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    Documento: <input name="cpfcnpj" type="text" id="cpfcnpj" size="18" maxlength="20" value="<?php echo $linha['cpf']; ?>" /></td>
					</tr>
					<tr>
                    <td><label for="gsc_id">Grupo:</label><select name="gsc_id" id="gsc_id" class="">
                          <option value="0">Selecione...</option>
                          <?php
                            $sql = mysql_query("SELECT gsc_id, gsc_nome FROM sc_grupo WHERE agencia = '$agencia' AND cliente = '$cliente' ORDER BY gsc_nome") or die (mysql_error());
							
                            if(mysql_num_rows($sql)>0){
                                while ($linha2=mysql_fetch_array($sql)){
                                    if($linha['gsc_id'] == $linha2["gsc_id"]){
                                        echo "<option value='".$linha2["gsc_id"]."' selected >".$linha2["gsc_nome"]."</option>";
                                    }else{
                                        echo "<option value='".$linha2["gsc_id"]."'>".$linha2["gsc_nome"]."</option>";
                                    }
                                }
                            }
                        ?>
                        </select>
                       </td>
						<td colspan="2"><label for="endereco">Ender.:</label>						  <input name="endereco" type="text" id="endereco" value="<?php echo $linha['endereco']; ?>" size="50" maxlength="100" /></td>
					</tr>
					<tr>
						<td width="34%"><label for="bairro">Bairro:</label>						  <input name="bairro" type="text" id="bairro" size="18" maxlength="20" value="<?php echo $linha['bairro']; ?>" /></td>
						<td><label for="cpfcnpj">Cidade:</label>						  <input name="cidade" type="text" id="cidade" size="18" maxlength="20" value="<?php echo $linha['cidade']; ?>" /></td>
						<td><label for="uf">UF:</label>						  <input type="text" name="uf" id="uf" maxlength="2" size="2" value="<?php echo $linha['uf']; ?>" /></td>
					</tr>
					<tr>
						<td><label for="cep">CEP:</label>						  <input name="cep" type="text" id="cep" maxlength="9" size="10" onkeypress="formataCampo(this, '00000-000', event);  return SomenteNumero(event)" value="<?php echo $linha['cep']; ?>" /></td>
						<td><label for="email">E-mail:</label>						  <input type="text" name="email" id="email" size="15" maxlength="30" value="<?php echo $linha['email']; ?>" /></td>
						<td><label for="fax">Fax:</label>						  <input name="fax" type="text" id="fax" size="12" <?php echo $param; ?> value="<?php echo $linha['fax']; ?>" /></td>
					</tr>
					<tr>
						<td><label for="tel_pessoal">Tel. pessoal:</label>						  <input name="tel_pessoal" type="text" id="tel_pessoal" size="12" <?php echo $param; ?> value="<?php echo $linha['telef_pess']; ?>" /></td>
						<td><label for="celular">Celular:</label>						  <input name="celular" type="text" id="celular" size="12" <?php echo $param; ?> value="<?php echo $linha['celular']; ?>" /></td>
						<td><label for="tel_comercial">Tel. Comercial:</label>						  <input name="tel_comercial" type="text" id="tel_comercial" size="12" <?php echo $param; ?> value="<?php echo $linha['telef_com']; ?>" /></td>
                        
					</tr>
				</table>
			</fieldset>
            <a class="btn botao margins dir" href="#" onClick="alt_cliente(); return false;">Alterar</a>
			<!--<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="alt_cliente()" />-->
			<input class="btn botao margins dir" type="button" name="voltar" id="voltar" value="Voltar" onclick="navega('4097/cliente_buscar.php')" />
        	<br class="clear" />
		</form>
	</div>
</div>