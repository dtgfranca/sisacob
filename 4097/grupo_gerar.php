<?php

	session_start();

	if (empty($_SESSION['userAgencia']))
		header('Location: ../login_mini.php');

	include_once('../config.php');
	include_once('../sql_functions.php');

	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta   = $_SESSION['userConta'];

	// Veem tanto na confirmação quanto na geração
	$grupo    = $_GET["grupo"];
	$sacador  = $_GET["sacador"];
	$venc     = $_GET["venc"];
	$valor    = $_GET["valor"];
	$desconto = $_GET["desconto"];

	$query = mysql_query("SELECT cpf, sacado FROM sacados 
		WHERE agencia = '$agencia' 
		AND conta LIKE '%$conta' 
		AND (grupo != '99999' OR grupo IS NULL) 
		AND gsc_id = '$grupo' 
		AND scd_desativado = '0'");

	if (isset($_GET['confirma']))
	{
		$vencimento  = $venc;
		$valor_total = $valor;
		$intervalo   = '1';

		if (empty($_GET['desconto']) || $_GET['desconto'] == '')
		{
			$desconto = '0,00';
		}
		else
		{
			$desconto = $_GET['desconto'];
		}

		$valor_total = str_replace('.', '', $valor_total); 
		$valor_total = str_replace(',', '.', $valor_total); 

		$qtd_boletos = mysql_num_rows($query);

		if ($qtd_boletos == 0)
			exit("Nenhum boleto a gerar nessas condições...");		

		while ($row = mysql_fetch_object($query))
		{
			$documento = str_replace('.', '', $row->cpf);
			$documento = str_replace('-', '', $documento);
			$documento = str_replace('/', '', $documento).'/'.date('n');
			
			echo '
				<table width="575">
					<tr>
						<td width="45"><span style="font-weight: bold">Fatura:</span></td>
						<td width="75">'.$documento.'</td>
						<td width="40"><span style="font-weight: bold">Valor:</span></td>
						<td width="125">R$ '.$valor.'</td>
						<td width="65"><span style="font-weight: bold">Desconto:</span></td>
						<td width="80">R$ '.$desconto.'</td>
						<td width="75"><span style="font-weight: bold">Vencimento:</span></td>
						<td width="80">'.$vencimento.'</td>
					</tr>
				</table>
			';
		}
		exit();
	}

	// Veem somente para a geração do boleto
	$modelo_boleto     = $_GET["modelo_boleto"];
	$modalidade_boleto = $_GET["modalidade_boleto"];
	$data_emissao      = $_GET["data_emissao"];	
	$protesto          = $_GET["protesto"];
	$dias_protesto     = $_GET["dias_protesto"];	
	$multa             = $_GET["multa"];	
	$data_desconto     = $_GET["data_desconto"];
	$juros             = $_GET["juros"];
	$agencia           = $_GET["agencia"];
	$instrucoes        = $_GET["instrucoes"];

	mysql_query("INSERT INTO bl_grupo 
		(agencia, cliente, gsc_id, blg_sacador, blg_modaliade, blg_modelo, blg_emissao, blg_vencimento, 
		 blg_dias_protesto, blg_tipo_dias_protesto, blg_valor, blg_desconto, blg_data_desconto, blg_multa, 
		 blg_mora, blg_instrucoes, blg_ip) 
	VALUES 
		('$agencia', '$cliente', '$grupo', '$sacador', '$modalidade_boleto', '$modelo_boleto', NOW(), '".to_sql_date($venc)."', 
		 '$protesto', '$dias_protesto', '".to_sql_decimal($valor)."', '".to_sql_decimal($desconto)."', '".to_sql_date($data_desconto)."', '".to_sql_decimal($multa)."', 
		 '".to_sql_decimal($juros)."', '$instrucoes', '".$_SERVER['REMOTE_ADDR']."')");

	echo $blg_id = mysql_insert_id();
	// echo "<br><br>";

	while ($row = mysql_fetch_object($query))
	{
		$documento = str_replace('.', '', $row->cpf);
		$documento = str_replace('-', '', $documento);
		$documento = str_replace('/', '', $documento).'/'.date('n');
		$sacado = $row->sacado;

		// echo "../scripts/titulos_funcoes.php?funcao=reg_titulo&cliente=$sacado&sacador=$sacador&modelo_boleto=$modelo_boleto&modalidade_boleto=$modalidade_boleto&documento=$documento&qtd_boletos=1&data_emissao=$data_emissao&venc=$venc&intervalo=1&protesto=$protesto&dias_protesto=$dias_protesto&valor=$valor&multa=$multa&desconto=$desconto&data_desconto=$data_desconto&juros=$juros<br>";
		// continue;

		$ch = curl_init($_SERVER['HTTP_HOST']."/scripts/titulos_funcoes.php?funcao=reg_titulo&blg_id=$blg_id&agencia=$agencia&usuario=$cliente&cliente=$sacado&sacador=$sacador&modelo_boleto=$modelo_boleto&modalidade_boleto=$modalidade_boleto&documento=$documento&qtd_boletos=1&data_emissao=$data_emissao&venc=$venc&intervalo=1&protesto=$protesto&dias_protesto=$dias_protesto&valor=$valor&multa=$multa&desconto=$desconto&data_desconto=$data_desconto&juros=$juros");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_exec($ch);
		// echo curl_exec($ch);
		curl_close($ch);
	}

	exit();

?>