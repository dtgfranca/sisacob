<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	
	include('../config.php');
	mysql_query("SET NAMES UTF8") or die(mysql_error());
	
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="dialog-confirm" title="Excluir cliente?" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Este cliente será excluído permanentemente. Deseja continuar?</p>
</div>
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready(function(){
    	$('#section').mede();
	});
	function busca(busca){
		$.ajax({
			type: 'GET',
			url: "scripts/clientes_funcoes.php",
			data: "funcao=busca&filtro="+busca+"&grupo="+$('#gsc_id').val(),
			beforeSend: function() {
				$("#busca").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
			},
			success: function(retorno){
				$("#busca").html(retorno);
			}
		});	
	}
	function excluir(sacado){
		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Excluir": function() {
					$.ajax({
						type: 'GET',
						url: "scripts/clientes_funcoes.php",
						data: "funcao=excluir&sacado="+sacado,
						success: function(retorno){
							if(retorno == "ok"){
								alerta("Exclusão realizada com sucesso!");
								navega('cliente_buscar.php');
							}
							else {
								alerta ("Erro ao excluir.");
							}
						}					
					});
					$(this).dialog("close");
				},
				"Cancelar": function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
</script>
	<div class="titulo">
        <h2>Clientes</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
   	  <fieldset class="busca">
   	    <legend>Buscar Clientes</legend>
    		<form id="busca_cliente" name="busca_cliente" method="post" action="" target="busca">
            	<table>
                	<tr>
                    	<td width="180"><label for="buscar">Pesquisar <span style="font-size:10px;">(cliente ou CPF/CNPJ)</span>:</label></td>
                        <td><input class="largo" name="buscar" type="text" id="buscar" size="50" /></td>
                        <td>&nbsp;&nbsp;&nbsp;<label for="scd_grupo">Grupo</label>
                        <select name="gsc_id" id="gsc_id">
                              <option value="0">Selecione um grupo</option>
                              <?php
                                    $sql = "SELECT gsc_id, gsc_nome FROM sc_grupo WHERE agencia = '$agencia' AND cliente = '$cliente' ORDER BY gsc_nome";
                                    $query = mysql_query($sql) or die(mysql_error());
                                    while($linha = mysql_fetch_array($query)){
                                        echo '
                                            <option value="'.$linha['gsc_id'].'">'.$linha['gsc_nome'].'</option>
                                        ';
                                    }
                                ?>
                          </select>
              			</td>
                        <!--<td><input class="largo" name="buscar" type="text" id="buscar" size="50"  onKeyUp="busca(this.value)" /></td>-->
                        <td width="80"><input class="botao top dir" type="button" name="pesq" id="pesq" onclick="busca($('#buscar').val())" value="Pesquisar"/></td>
                	</tr>
              	</table>
   		  </form>
        <div id="busca">
        	<table>
            	<tr class="cinza">
                	<td class="destaque borda">NOME</td>
                    <td width="25%" class="destaque borda">CPF/CNPJ/RG</td>
                    <td width="10%" class="destaque borda">	AÇÕES</td>
                </tr>
                <?php

					$sql = "SELECT nome, cpf, sacado FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND (grupo <> '99999' or grupo is null) ORDER BY UPPER(nome)";
					$query = mysql_query ($sql) or die(mysql_error());
					while($linha = mysql_fetch_array($query)){
						echo '
							<tr>
								<td class="borda">'.$linha['nome'].'</td>
								<td class="borda">'.$linha['cpf'].'</td>
								<td class="borda centro">
						';
						if($_SESSION['userMaster'] == true){
							echo '
									<a href="javascript:excluir('.$linha['sacado'].');" title="Excluir"><img src="img/cancelar.png" /></a>&nbsp;&nbsp;';
						}
						echo '
									<a href="javascript:navega(\'4097/cliente_alterar.php?sacado='.$linha['sacado'].'\');" title="Alterar"><img src="img/editar.png" /></a>
								</td>
							</tr>
						';
					}				
				?>
            </table>
        </div>
   	  </fieldset>
    </div>
</div>