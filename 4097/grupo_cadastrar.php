<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	
	include_once('../config.php');
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready( function() {
		$("#novo").click(function(){
			$("#cadastra_grupo")[0].reset();
			$("#cadastra_grupo input").prop('disabled', false);
			$("#cadastra_grupo #novo").attr('class','oculto');
		});
	});
	function limparChar(string){
		return string.replace(/á/g, "a").replace(/é/g, "e").
		replace(/í/g, "i").replace(/ó/g, "o").replace(/ú/g, "u").
		replace(/à/g, "a").replace(/è/g, "e").replace(/ì/g, "i").
		replace(/ò/g, "o").replace(/ù/g, "u").replace(/ã/g, "a").
		replace(/õ/g, "o").replace(/â/g, "a").replace(/ê/g, "e").
		replace(/î/g, "i").replace(/ô/g, "o").replace(/û/g, "u").
		replace(/ç/g, "c").replace(/ä/g, "a").replace(/ë/g, "e").
		replace(/ï/g, "i").replace(/ö/g, "o").replace(/ü/g, "u").
		replace(/Á/g, "A").replace(/É/g, "E").replace(/Í/g, "I").
		replace(/Ó/g, "O").replace(/Ú/g, "U").replace(/À/g, "A").
		replace(/È/g, "E").replace(/Ì/g, "I").replace(/Ò/g, "O").
		replace(/Ù/g, "U").replace(/Â/g, "A").replace(/Ê/g, "E").
		replace(/Ô/g, "O").replace(/Ã/g, "A").replace(/Õ/g, "O").
		replace(/Ç/g, "C");
	}
	function cad_grupo(){
		var nome = document.getElementById('nome').value;
		var tnome = nome.length;
		
		nome = limparChar(nome);
	
		if(tnome < 3){
			alerta('Campo nome é obrigatório! (Pelo menos 3 caracteres)');
			return false;
		}
		else{		
			$.ajax({
				type: "GET",
				url: "scripts/grupos_funcoes.php",
				data: "funcao=cad_grupo&nome="+nome,
				success: function(retorno){
					if(retorno == "ok"){
						alerta("Cadastro realizado com sucesso!");
						$("#cadastra_grupo input").prop('disabled', true);
						$("#cadastra_grupo #novo").attr('class','btn botao margins dir');
						$("#cadastra_grupo #novo").prop('disabled', false);
					}
					else{
						alerta(retorno);
					}		
				}
			});
			return false;
		}
	}
</script>
	<div class="titulo">
        <h2>Grupos</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="cadastra_grupo" name="cadastra_grupo" method="post" action="">
  		<fieldset>
    	  <legend>Cadastrar  Grupos</legend>
          <table>
          	<tr>
            	<td>
                	<label for="nome">Nome:&nbsp;&nbsp;</label><input name="nome" type="text" id="nome" size="50" maxlength="40" />&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<label id="ncpf">
    	          	</label>
				</td>
            </tr>
          </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:cad_grupo()">Cadastrar</a>
  		<!--<input class="btn botao margins dir" type="button" name="cadastrar" id="cadastrar" value="Cadastrar" onclick="cad_grupo()" />-->
        <input class="btn botao margins dir" type="reset" name="limpar" id="limpar" value="Limpar" /> 
        <input class="oculto" type="reset" name="novo" id="novo" value="Novo" />
        <br class="clear" />
	  </form>
    </div>
</div>