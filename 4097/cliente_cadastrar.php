<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	
	include_once('../config.php');
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready( function() {
		$("#cadastra_cliente #cep").attr('onkeypress', "formataCampo(this, '00000-0', event); return SomenteNumero(event)");
		$("#novo").click(function(){
			$("#cadastra_cliente")[0].reset();
			$("#cadastra_cliente input").prop('disabled', false);
			$("#cadastra_cliente #novo").attr('class','oculto');
		});
		$('#cadastra_cliente #cpfcnpj').change(function(){
			if($("#cadastra_cliente #cpfcnpj").val() == 'cpf'){
				$("#cadastra_cliente #cnpj").attr('style','display:none');
				$("#cadastra_cliente #rg").attr('style','display:none');
				$("#cadastra_cliente #outro").attr('style','display:none');
				$("#cadastra_cliente #cpf").removeAttr('style');
				$("#cadastra_cliente #cpf").attr('onkeypress', "formataCampo(this, '000.000.000-00', event); return SomenteNumero(event)");
			}
			else if($("#cadastra_cliente #cpfcnpj").val() == 'cnpj'){
				$("#cadastra_cliente #cpf").attr('style','display:none');
				$("#cadastra_cliente #rg").attr('style','display:none');
				$("#cadastra_cliente #outro").attr('style','display:none');
				$("#cadastra_cliente #cnpj").removeAttr('style');
				$("#cadastra_cliente #cnpj").attr('onkeypress', "formataCampo(this, '00.000.000/0000-00', event); return SomenteNumero(event)");
			}
			else if($("#cadastra_cliente #cpfcnpj").val() == 'rg'){
				$("#cadastra_cliente #cpf").attr('style','display:none');
				$("#cadastra_cliente #cnpj").attr('style','display:none');
				$("#cadastra_cliente #outro").attr('style','display:none');
				$("#cadastra_cliente #rg").removeAttr('style');
			}
			else if($("#cadastra_cliente #cpfcnpj").val() == 'outro'){
				$("#cadastra_cliente #cpf").attr('style','display:none');
				$("#cadastra_cliente #cnpj").attr('style','display:none');
				$("#cadastra_cliente #rg").attr('style','display:none');
				$("#cadastra_cliente #outro").removeAttr('style');
			}
			else {
				$("#cadastra_cliente #cnpj").attr('style','display:none');
				$("#cadastra_cliente #cpf").attr('style','display:none');
				$("#cadastra_cliente #rg").attr('style','display:none');
				$("#cadastra_cliente #outro").attr('style','display:none');
			}
		});
		$("#cadastra_cliente #uf").change(function(){
			if($("#cadastra_cliente #uf").val()=='SP'){
				$("#cadastra_cliente #fax").removeAttr('maxlength');
				$("#cadastra_cliente #fax").attr('maxlength','14');
				$("#cadastra_cliente #fax").removeAttr('onkeypress');
				$("#cadastra_cliente #fax").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_pessoal").removeAttr('maxlength');
				$("#cadastra_cliente #tel_pessoal").attr('maxlength','14');
				$("#cadastra_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #celular").removeAttr('maxlength');
				$("#cadastra_cliente #celular").attr('maxlength','14');
				$("#cadastra_cliente #celular").removeAttr('onkeypress');
				$("#cadastra_cliente #celular").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_comercial").removeAttr('maxlength');
				$("#cadastra_cliente #tel_comercial").attr('maxlength','14');
				$("#cadastra_cliente #tel_comercial").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
			}
			else {
				$("#cadastra_cliente #fax").removeAttr('maxlength');
				$("#cadastra_cliente #fax").attr('maxlength','13');
				$("#cadastra_cliente #fax").removeAttr('onkeypress');
				$("#cadastra_cliente #fax").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_pessoal").removeAttr('maxlength');
				$("#cadastra_cliente #tel_pessoal").attr('maxlength','13');
				$("#cadastra_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #celular").removeAttr('maxlength');
				$("#cadastra_cliente #celular").attr('maxlength','13');
				$("#cadastra_cliente #celular").removeAttr('onkeypress');
				$("#cadastra_cliente #celular").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_comercial").removeAttr('maxlength');
				$("#cadastra_cliente #tel_comercial").attr('maxlength','13');
				$("#cadastra_cliente #tel_comercial").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
			}
		});
	});
	function limparChar(string){
		return string.replace(/á/g, "a").replace(/é/g, "e").
		replace(/í/g, "i").replace(/ó/g, "o").replace(/ú/g, "u").
		replace(/à/g, "a").replace(/è/g, "e").replace(/ì/g, "i").
		replace(/ò/g, "o").replace(/ù/g, "u").replace(/ã/g, "a").
		replace(/õ/g, "o").replace(/â/g, "a").replace(/ê/g, "e").
		replace(/î/g, "i").replace(/ô/g, "o").replace(/û/g, "u").
		replace(/ç/g, "c").replace(/ä/g, "a").replace(/ë/g, "e").
		replace(/ï/g, "i").replace(/ö/g, "o").replace(/ü/g, "u").
		replace(/Á/g, "A").replace(/É/g, "E").replace(/Í/g, "I").
		replace(/Ó/g, "O").replace(/Ú/g, "U").replace(/À/g, "A").
		replace(/È/g, "E").replace(/Ì/g, "I").replace(/Ò/g, "O").
		replace(/Ù/g, "U").replace(/Â/g, "A").replace(/Ê/g, "E").
		replace(/Ô/g, "O").replace(/Ã/g, "A").replace(/Õ/g, "O").
		replace(/Ç/g, "C");
	}
	function cad_cliente(){
		var nome = document.getElementById('nome').value;
		var endereco = document.getElementById('endereco').value;
		var bairro = document.getElementById('bairro').value;
		var cidade = document.getElementById('cidade').value;
		var uf = document.getElementById('uf').value;
		var cep = document.getElementById('cep').value;
		var tel_pessoal = document.getElementById('tel_pessoal').value;
		var celular = document.getElementById('celular').value;
		var tel_comercial = document.getElementById('tel_comercial').value;
		var fax = document.getElementById('fax').value;
		var grupo = document.getElementById('gsc_id').value;
		var email = document.getElementById('email').value;
		var identidade = '';
		var tnome = nome.length;
		var tend = endereco.length;
		var tbai = bairro.length;
		var tcid = cidade.length;
		var tcep = cep.length;
		nome = limparChar(nome);
		var tipo =  document.getElementById('cpfcnpj').value;
		if(tipo == 'cpf'){
			var cpfcnpj = document.getElementById('cpf').value;
			var tcpf = cpfcnpj.length;
			if(tcpf != 14){
				alerta('CPF inválido');
			}
		}
		else if(tipo == 'cnpj'){
			var cpfcnpj = document.getElementById('cnpj').value;
			var tcpf = cpfcnpj.length;
			if(tcpf != 18){
				alerta('CNPJ inválido');
			}
		}
		else if(tipo == 'rg'){
			var cpfcnpj = document.getElementById('rg').value;
		}
		else if(tipo == 'outro'){
			var cpfcnpj = document.getElementById('outro').value;
		}
		else if(tipo == '0'){
			alerta('Insira um documento válido');
		}
		if(tnome < 10){
			alerta('Campo nome é obrigatório');
			return false;
		}
		else if(tend < 2){
			alerta('Campo endereço é obrigatório');
			return false;
		}
		else if(tbai < 2){
			alerta('Campo bairro é obrigatório');
			return false;
		}
		else if(tcid < 2){
			alerta('Campo cidade é obrigatório');
			return false;
		}
		else if(uf == '0'){
			alerta('Selecione a UF');
			return false;
		}
		else if(tcep < 9){
			alerta('Campo cep é obrigatório');
			return false;
		}
		else{		
			$.ajax({
				type: "GET",
				url: "scripts/clientes_funcoes.php",
				data: "funcao=cad_cliente&nome="+nome+"&identidade="+identidade+"&cpfcnpj="+cpfcnpj+"&endereco="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&email="+email+"&tel_pessoal="+tel_pessoal+"&fax="+fax+"&tel_comercial="+tel_comercial+"&celular="+celular+"&cep="+cep+"&grupo="+grupo,
				success: function(retorno){
					if(retorno == "ok"){
						alerta("Cadastro realizado com sucesso!");
						$("#cadastra_cliente input").prop('disabled', true);
						$("#cadastra_cliente #novo").attr('class','btn botao margins dir');
						$("#cadastra_cliente #novo").prop('disabled', false);
					}
					else{
						alerta(retorno);
					}		
				}
			});
			return false;
		}
	}
</script>
	<div class="titulo">
        <h2>Clientes</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="cadastra_cliente" name="cadastra_cliente" method="post" action="">
  		<fieldset>
    	  <legend>Cadastrar  Clientes</legend>
          <table>
          	<tr>
            	<td>
                	<label for="nome">Nome:&nbsp;&nbsp;</label><input name="nome" type="text" id="nome" size="50" maxlength="40" />&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<label id="ncpf">
    	          	</label>
                  <label for="cpfcnpj"></label>
                    <select name="cpfcnpj" id="cpfcnpj">
                      <option value="0">Selecione</option>
                      <option value="cpf">CPF</option>
                      <option value="cnpj">CNPJ</option>
                      <option value="rg">RG</option>
                      <option value="outro">Outros</option>
                    </select>
                  <input style="display:none" name="cpf" type="text" id="cpf" size="12" maxlength="14" onkeypress="formataCampo(this, '000.000.000-00', event); return SomenteNumero(event)" />
                    <input style="display:none" name="cnpj" type="text" id="cnpj" size="16" maxlength="18" onkeypress="formataCampo(this, '00.000.000/0000-00', event); return SomenteNumero(event)" />
                    <input style="display:none" name="rg" type="text" id="rg" size="14" maxlength="13" />
                    <input style="display:none" name="outro" type="text" id="outro" size="20" maxlength="50" onkeypress="" />
                </td>
            </tr>
          </table>
    	  <table>
    	    <tr>
    	      <td colspan="2"><label for="endereco">Ender.:&nbsp;&nbsp;</label>    	        <input name="endereco" type="text" id="endereco" size="70" maxlength="40" /></td>
    	      <td>
              		<label for="gsc_id">Grupo:&nbsp;&nbsp;</label><select name="gsc_id" id="gsc_id">
    	          <option value="0">Selecione um grupo</option>
    	          <?php
						$sql = "SELECT gsc_id, gsc_nome FROM sc_grupo WHERE agencia = '$agencia' AND cliente = '$cliente' ORDER BY gsc_nome";
						$query = mysql_query($sql) or die(mysql_error());

						while($linha = mysql_fetch_assoc($query)){
							echo '
								<option value="'.$linha['gsc_id'].'">'.$linha['gsc_nome'].'</option>
							';
						}
					?>
  	          </select></td>
   	        </tr>
    	    <tr>
    	      <td width="34%"><label for="bairro">Bairro:&nbsp;&nbsp;</label>    	        <input name="bairro" type="text" id="bairro" size="18" maxlength="20"/></td>
    	      <td width="32%"><label for="cidade">Cidade:&nbsp;&nbsp;</label>    	        <input name="cidade" type="text" id="cidade" size="18" maxlength="20"/></td>
    	      <td><label for="uf">UF:&nbsp;&nbsp;</label>
              	<select name="uf" id="uf">
                	<option value="0" selected="selected">--</option>
	         	  	<option value="AC">AC</option>
	         	  	<option value="AL">AL</option>
	         	  	<option value="AP">AP</option>
	         	  	<option value="AM">AM</option>
	         	  	<option value="BA">BA</option>
	         	  	<option value="CE">CE</option>
	         	  	<option value="DF">DF</option>
	         	  	<option value="ES">ES</option>
	         	  	<option value="GO">GO</option>
	         	  	<option value="MA">MA</option>
	         	  	<option value="MT">MT</option>
	         	  	<option value="MS">MS</option>
	         	  	<option value="MG">MG</option>
	         	  	<option value="PA">PA</option>
	         	  	<option value="PB">PB</option>
	         	  	<option value="PE">PE</option>
	         	  	<option value="PI">PI</option>
	         	  	<option value="PR">PR</option>
	         	  	<option value="RJ">RJ</option>
	         	  	<option value="RN">RN</option>
	         	  	<option value="RO">RO</option>
	         	  	<option value="RR">RR</option>
	         	  	<option value="RS">RS</option>
	         	  	<option value="SC">SC</option>
	         	  	<option value="SE">SE</option>
	         	  	<option value="SP">SP</option>
	         	  	<option value="TO">TO</option>
   	  	      	</select>              </td>
   	        </tr>
          <tr>
    	      <td><label for="cep">CEP:&nbsp;&nbsp;</label>    	        <input name="cep" type="text" id="cep" maxlength="9" size="10" onkeypress="formataCampo(this, '00000-000', event);  return SomenteNumero(event)" /></td>
    	      <td><label for="email">E-mail:&nbsp;&nbsp;</label>    	        <input type="text" name="email" id="email" size="15" maxlength="50" /></td>
    	      <td><label for="fax">Fax:&nbsp;&nbsp;</label>    	        <input name="fax" type="text" id="fax" size="12" maxlength="13" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /></td>
   	        </tr>
    	   <tr>
    	      <td><label for="tel_pessoal">Tel. pessoal:&nbsp;&nbsp;</label>    	        <input name="tel_pessoal" type="text" id="tel_pessoal" maxlength="13" size="12" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /></td>
    	      <td><label for="celular">Celular:</label>    	        <input name="celular" type="text" id="celular" maxlength="13" size="12" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /> </td>
    	      <td><label for="tel_comercial">Tel. Comercial:</label>    	        <input name="tel_comercial" type="text" id="tel_comercial" size="12" maxlength="13" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /></td>
   	        </tr>
  	    </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:cad_cliente()">Cadastrar</a>
  		<!--<input class="btn botao margins dir" type="button" name="cadastrar" id="cadastrar" value="Cadastrar" onclick="cad_cliente()" />-->
        <input class="btn botao margins dir" type="reset" name="limpar" id="limpar" value="Limpar" /> 
        <input class="oculto" type="reset" name="novo" id="novo" value="Novo" />
        <br class="clear" />
	  </form>
    </div>
</div>