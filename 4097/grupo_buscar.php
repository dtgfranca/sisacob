<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	
	include_once('../config.php');
	mysql_query("SET NAMES UTF8") or die(mysql_error());
	
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="dialog-confirm" title="Excluir grupo?" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Este grupo será excluído permanentemente. Deseja continuar?</p>
</div>
<div id="dialog-mes" title="Selecionar Mês" style="display:none">
	<p><b>Qual seria o mês de referência para impressão dos boletos do grupo?</b>
	<br><br><center>
	<select name="mes" id="mes" style="text-align: center; width: 150px">
	<?php 
	$count = -1; 
	while ($count < 5) 
	{ 
		$mes = date('m/Y', strtotime('+'.$count.' months', strtotime(date('Y-m-d')))); 
		echo '<option value="'.str_replace('/', '-', $mes).'">'.$mes.'</option>';	
		$count++; 
	} 
	?>
	</select></center>
</div>
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready(function(){
    	$('#section').mede();
	});
	function busca(busca){
		$.ajax({
			type: 'GET',
			url: "scripts/grupos_funcoes.php",
			data: "funcao=busca&filtro="+busca,
			beforeSend: function() {
				$("#busca").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
			},
			success: function(retorno){
				$("#busca").html(retorno);
			}
		});	
	}
	function excluir(grupo){
		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Excluir": function() {
					$.ajax({
						type: 'GET',
						url: "scripts/grupos_funcoes.php",
						data: "funcao=excluir&grupo="+grupo,
						success: function(retorno){
							if(retorno == "ok"){
								alerta("Exclusão realizada com sucesso!");
								navega('grupo_buscar.php');
							}
							else {
								alerta ("Erro ao excluir.");
							}
						}					
					});
					$(this).dialog("close");
				},
				"Cancelar": function() {
					$(this).dialog( "close" );
				}
			}
		});
	}

	function selecionaMes(grupo)
	{
		$("#dialog-mes").html("<p><b>Qual seria o mês de referência para impressão dos boletos do grupo?</b><br><br><center><select name=\"mes\" id=\"mes\" style=\"text-align: center; width: 150px\"><?php $count = -1; while ($count < 5) { $mes = date('m/Y', strtotime('+'.$count.' months', strtotime(date('Y-m-d')))); echo '<option value=\"'.str_replace('/', '-', $mes).'\">'.$mes.'</option>'; $count++; } ?></select></center>");
		$("#dialog-mes").dialog({
			resizable: false,
			height:150,
			width:320,
			modal: true,
			buttons: {
				"Imprimir": function(){
					$(this).dialog("close");
					var remote = null;
					remote = window.open('','fatura_'+grupo,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=850,height=400,left=20,top=20');

					if (remote != null)
						remote.location.href = '../boletophp/imprimir.php?base=boletophp/carne.php&grupo='+grupo+'&mes='+$('#mes').val();
				},
				"Cancelar": function(){
					$(this).dialog( "close" );
				}
			}
		});
	}
</script>
	<div class="titulo">
        <h2>Grupos</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
   	  <fieldset class="busca">
   	    <legend>Buscar Grupos</legend>
    		<form id="busca_grupo" name="busca_grupo" method="post" action="" target="busca">
            	<table>
                	<tr>
                    	<td width="180"><label for="buscar">Pesquisar <span style="font-size:10px;">(nome)</span>:</label></td>
                        <td><input class="largo" name="buscar" type="text" id="buscar" size="50" /></td>
                        <!--<td><input class="largo" name="buscar" type="text" id="buscar" size="50"  onKeyUp="busca(this.value)" /></td>-->
                        <td width="80"><input class="botao top dir" type="button" name="pesq" id="pesq" onclick="busca($('#buscar').val())" value="Pesquisar"/></td>
                	</tr>
              	</table>
   		  </form>
        <div id="busca">
        	<table>
            	<tr class="cinza">
                	<td class="destaque borda">NOME</td>
                    <td width="10%" class="destaque borda">	AÇÕES</td>
                </tr>
                <?php

					$sql = "SELECT gsc_id, gsc_nome FROM sc_grupo WHERE agencia = '$agencia' 
					AND cliente = '$cliente' ORDER BY gsc_nome";

					$query = mysql_query ($sql) or die(mysql_error());
					while($linha = mysql_fetch_array($query)){
						echo '
							<tr>
								<td class="borda">'.$linha['gsc_nome'].'</td>
								<td class="borda centro">';

						if ($_SESSION['userMaster'] == true)
							echo '<a href="javascript:excluir('.$linha['gsc_id'].');" title="Excluir"><img src="img/cancelar.png" /></a>&nbsp;&nbsp;';

						echo '<a href="javascript:navega(\'4097/grupo_alterar.php?grupo='.$linha['gsc_id'].'\');" title="Alterar"><img src="img/editar.png" /></a>&nbsp;
							  <a href="javascript:selecionaMes(\''.$linha['gsc_id'].'\');" title="Imprimir"><img src="img/boleto.png" /></a>
							</td>
						</tr>';
					}				
				?>
            </table>
        </div>
   	  </fieldset>
    </div>
</div>