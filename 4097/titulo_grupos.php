<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	include('../config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
	$data_base = date("d/m/Y");
?>
<div id="section" class="sect">
<script type="text/javascript">
	$(document).ready(function () {
		var data_base = '<?php echo $data_base; ?>';
		$('#valor').blur(function(){
			var valor = document.getElementById('valor').value;
			var boletos = document.getElementById('qtd_boletos').value;
			valor = valor.replace('.','');
			valor = valor.replace(',','.');
			var parcelas = valor/boletos;
			if(parcelas == 'Infinity'){
				var parcelas = '';
			}
			else {
				var parcelas = parcelas;
			}
			$('#fatura').attr('value',parcelas);
		});
		$('#qtd_boletos').blur(function(){
			var boletos = document.getElementById('qtd_boletos').value;
			if(boletos == ''){
				$('#qtd_boletos').attr('value','1')
			}
		});
		$('#modalidade_boleto_0').change(function(){
			if($(this).is(':checked')){
				$('#modalidade_boleto_1').attr('disabled','disabled');
				$('#protesto').attr('value','5');
				$('#protesto').attr('disabled','disabled');
				$('#dias_protesto_1').attr('checked','checked');
				$('#dias_protesto_0').attr('disabled','disabled');
				$('#dias_protesto_1').attr('disabled','disabled');
				$('#juros').attr('value','10,00');
				$('#juros').attr('disabled','disabled');
				$('#qtd_boletos').attr('value','1');
				$('#qtd_boletos').attr('disabled','disabled');
				$('#intervalo').attr('value','1');
				$('#intervalo').attr('disabled','disabled');
				$('#multa').attr('disabled','disabled');
			}
			else{
				$('#protesto').removeAttr('value');
				$('#protesto').removeAttr('disabled');
				$('#dias_protesto_0').removeAttr('disabled');
				$('#dias_protesto_1').removeAttr('disabled');
				$('#juros').removeAttr('value');
				$('#juros').removeAttr('disabled');
				$('#qtd_boletos').removeAttr('value');
				$('#qtd_boletos').removeAttr('disabled');
				$('#intervalo').removeAttr('value');
				$('#intervalo').removeAttr('disabled');
				$('#multa').removeAttr('disabled');
			}
		});
		$('#modalidade_boleto_1').change(function(){
			if($(this).is(':checked')){
				$('#modalidade_boleto_0').attr('disabled','disabled');
			}
			else{
				$('#modalidade_boleto_0').removeAttr('disabled');
			}
		});
		$('#qtd_boletos').blur(function(){
			if($('#qtd_boletos').val() == "1"){
				$('#intervalo').attr('value','1');
				$('#intervalo').attr('disabled','disabled');
			}
			else {
				$('#intervalo').removeAttr('value');
				$('#intervalo').removeAttr('disabled');
			}
		});
		$('#desconto').blur(function(){
			if($('#desconto').val() < "0,01"){
				$('#data_desconto').attr('disabled','disabled');
				$('#data_desconto').removeAttr('onblur');
			}
			else {
				$('#data_desconto').removeAttr('value');
				$('#data_desconto').removeAttr('disabled');
				$('#data_desconto').attr('onblur','verificaDesc(this.value)');
			}
		});
		$('#limpar').click(function(){
			$('#protesto').removeAttr('value');
			$('#protesto').removeAttr('disabled');
			$('#dias_protesto_0').removeAttr('disabled');
			$('#dias_protesto_1').removeAttr('disabled');
			$('#juros').removeAttr('value');
			$('#juros').removeAttr('disabled');
		});
		$("#novo").click(function(){
			$("#cadastra_titulo")[0].reset();
			$("#cadastra_titulo input").prop('disabled', false);
			$("#cadastra_titulo select").prop('disabled', false);
			$("#cadastra_titulo #novo").attr('class','oculto');
			$("#cadastra_titulo #fatura").attr('disabled','disabled');
			$("#cadastra_titulo #data_emissao").attr('disabled','disabled');
			$("#cadastra_titulo #data_emissao").attr('value',data_base);
			$('#qtd_boletos').removeAttr('value');
			$('#intervalo').removeAttr('value');
			$('#fatura').removeAttr('value');
			$('#protesto').removeAttr('value');

			$('#juros').removeAttr('value');
		});
	});
	function prepara_gerar(){
		var vencimento = document.getElementById('venc').value;
		var protesto = document.getElementById('protesto').value;
		var total = document.getElementById('valor').value;
		var multa = document.getElementById('multa').value;
		var juros = document.getElementById('juros').value;
		var desconto = document.getElementById('desconto').value;
		var datadesconto = document.getElementById('data_desconto').value;
		var tvenc = vencimento.length;
		var tpro = protesto.length;
		var ttot = total.length;
		var tdesc = desconto.length;
		var tdata = datadesconto.length;

		if (document.cadastra_titulo.grupo.value=="0"){
			alerta("Favor selecionar o grupo.");
			return false;
		}
		else if (document.cadastra_titulo.valor.value < 1){
			alerta("O valor total dos boletos não pode ser menor que R$ 1,00.");
			return false;
		}
		else if(tvenc < 10){
			alerta("Data de vencimento inválida.");
			return false;
		}
		else if(ttot < 4){
			alerta("Campo valor total é obrigatório.");
			return false;
		}
		else {
			if(document.cadastra_titulo.modalidade_boleto.checked){
				$.ajax({ 
					type: 'GET',
					url: "scripts/titulos_funcoes.php",
					data: "funcao=verificarHora&agencia=<?php echo $agencia;?>",
					success: function(retorno){
						if(retorno == "1"){
							alerta("Horário para cadastro de títulos para desconto encerrado.");
							return false;
						}
						else if(retorno == "ok"){ // SÓ DESCONTO - HORARIO OK
									cad_titulo();
								}
						}
				});
			}
			else {	
				cad_titulo();
			}
		}
	}
	function cad_titulo(){
		var vencimento = document.getElementById('venc').value;
		var valor = document.getElementById('valor').value;
		var desconto = document.getElementById('desconto').value;
		var grupo = document.getElementById('grupo').value;
		var sacador = document.getElementById('sacador').value;
				
		$.ajax({
			type: "GET",
			url: "4097/grupo_gerar.php",
			data: "&venc="+vencimento+"&valor="+valor+"&desconto="+desconto+"&grupo="+grupo+"&sacador="+sacador+"&confirma=",
			success: function(retorno){
				$('#dialog').html('<div id="dialog-confirm" title="Cadastrar títulos?" style="display:none">'+retorno+'</div>');
				// cria janela de confirmação
				$( "#dialog-confirm" ).dialog({
					resizable: false,
					height:200,
					width: 585,
					modal: true,
					buttons: {
						"Cadastrar": function() {
							reg_titulo();	
							$(this).dialog("close");
						},
						"Editar": function() {
							$(this).dialog("close");
						},
						"Cancelar": function() {
							$(this).dialog("close");
							navega('principal.php');
						}
					}
				});
			}
		});		
	}
	function reg_titulo(){
		var grupo = document.getElementById('grupo').value;
		var sacador = document.getElementById('sacador').value;
		var modelo_boleto = document.getElementById('modelo_boleto').value;
		var data_emissao = document.getElementById('data_emissao').value;
		var vencimento = document.getElementById('venc').value;
		var protesto = document.getElementById('protesto').value;
		var valor = document.getElementById('valor').value;
		var multa = document.getElementById('multa').value;
		var desconto = document.getElementById('desconto').value;		
		var juros = document.getElementById('juros').value;
		var data_desconto = document.getElementById('data_desconto').value;
		var instrucoes = document.getElementById('textarea').value;
		var agencia = '<?php echo $agencia; ?>';

		if (protesto == ''){
			protesto = 0;
			var dias_protesto = null;
		}
		else{
			var dias_protesto = $('[name=dias_protesto]:checked').val();
		}	
		if($('#modalidade_boleto_0').is(':checked')){
			var modalidade_boleto = document.getElementById('modalidade_boleto_0').value;
		}
		else if($('#modalidade_boleto_1').is(':checked')){
			var modalidade_boleto = document.getElementById('modalidade_boleto_1').value;
		}
		else {
			var modalidade_boleto = "N";
		}
						
		$.ajax({
			type: "GET",
			url: "4097/grupo_gerar.php",
			data: "grupo="+grupo+"&sacador="+sacador+"&modelo_boleto="+modelo_boleto+"&modalidade_boleto="+modalidade_boleto+"&data_emissao="+data_emissao+"&venc="+vencimento+"&protesto="+protesto+"&dias_protesto="+dias_protesto+"&valor="+valor+"&multa="+multa+"&desconto="+desconto+"&data_desconto="+data_desconto+"&juros="+juros+"&agencia="+agencia+"&instrucoes="+instrucoes,
			success: function(retorno){

				if (retorno == '' || retorno == '0' || retorno.length == 0)
					alerta('Erro ao tentar cadastrar!');

				else
				{
					if (modalidade_boleto == 'R')
					{
						var msg_confirm = '<div id="dialog-confirm" title="Impressão" style="display:none">Faturas cadastrads com sucesso!<br><br><b>Deseja imprimir seu(s) boleto(s)?</b></div>'
						$('#dialog').html(msg_confirm);

						// Cria janela de confirmação
						$( "#dialog-confirm" ).dialog(
						{
							resizable: false,
							height: 135,
							width: 220,
							modal: true,
							buttons:
							{
								"Imprimir": function()
								{
									var url = '../boletophp/imprimir.php?base=boletophp/carne.php&blg_id='+retorno;
									var propriedades = 'toolbar=yes,location=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=800';
									window.open(url, 'faturas_'+retorno, propriedades);
										
									$(this).dialog("close");
								},
								
								"Cancelar": function()
								{
									$(this).dialog("close");
									//navega('principal.php');
								}
							}
						});
					}
					alerta('Faturas cadastradas com sucesso!');
				}
				
				$("#cadastra_titulo input").prop('disabled', true);
				$("#cadastra_titulo select").prop('disabled', true);
				$("#cadastra_titulo #novo").attr('class','btn botao margins dir');
				$("#cadastra_titulo #novo").prop('disabled', false);
				
				if (false)//(retorno == 'boleto')
				{
					alerta("Número do documento já existente. Cadastre outro número.");
				}
				
				else if(retorno == 'erro')
				{
					alerta('Erro ao cadastrar.');
				}
				
				else
				{
					//alerta(retorno);
				}
			}
		});
	}
</script>
<div id="dialog"></div>
	<div class="titulo">
        <h2>GRUPOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="cadastra_titulo" name="cadastra_titulo" method="post">
  		<fieldset>
    	  <legend>Gerar TÍTULOS por grupo
    	  </legend>
    	  <table>
    	    <tr>
    	      <td colspan="3">Grupo:&nbsp;&nbsp;&nbsp;
    	        <select name="grupo" id="grupo">
    	          <option value="0">Selecione um grupo</option>
    	          <?php
						$sql = "SELECT gsc_id, gsc_nome FROM sc_grupo WHERE agencia = '$agencia' 
							AND cliente = '$cliente' ORDER BY gsc_nome";

						$query = mysql_query($sql) or die(mysql_error());
						while($linha = mysql_fetch_array($query)){
							echo '
								<option value="'.$linha['gsc_id'].'">'.$linha['gsc_nome'].'</option>
							';
						}
					?>
  	          </select></td>
    	      <td colspan="2">Sacador/Avalista:&nbsp;&nbsp;&nbsp;
    	        <select name="sacador" id="sacador">
    	          <option value="0">Selecione...</option>
    	          <?php
					$sql2 = "SELECT nome, sacado FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND grupo='99999' ORDER BY UPPER(nome)";
					$query2 = mysql_query($sql2) or die(mysql_error());
					while($linha2 = mysql_fetch_array($query2)){
						echo '
							<option value="'.$linha2['sacado'].'">'.$linha2['nome'].'</option>
						';
					}
				?>
  	          </select></td>
  	      </tr>
    	    <tr>
    	      <td width="24%">
              	<?php
					$qPara = "SELECT boleto_padrao, modal_padrao FROM clientes WHERE cliente='$cliente'";
					$sPara = mysql_query($qPara) or die(mysql_error());
					$aPara = mysql_fetch_array($sPara);
				?>
              	Modelo Boleto:&nbsp;&nbsp;&nbsp;
    	        <select name="modelo_boleto" id="modelo_boleto">
                	<?php						
						if($aPara['boleto_padrao'] == 1){
							echo "
							<option value='1' selected>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3'>Carnê</option>0
							";
						}
						else if($aPara['boleto_padrao']==2){
							echo "
							<option value='1'>2 Vias</option>
							<option value='2' selected>3 Vias</option>
							<option value='3'>Carnê</option>
							";
						}
						else if($aPara['boleto_padrao']==3){
							echo "
							<option value='1'>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3' selected>Carnê</option>
							";
						}
						else {
							echo "
							<option value='1' selected>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3'>Carnê</option>0
							";
						}
					?>
              </select></td>
    	      <td colspan="2">Modal. Boleto:&nbsp;&nbsp;&nbsp;
    	        <!--<input type="checkbox" name="modalidade_boleto" id="modalidade_boleto" value="S" />-->
    	        <input type="checkbox" name="modalidade_boleto" value="S" id="modalidade_boleto_0" <?php if ($aPara['modal_padrao'] == 'S') echo 'checked="checked"'; ?> />&nbsp;Desconto
    	        <input type="checkbox" name="modalidade_boleto" value="R" id="modalidade_boleto_1" <?php if ($aPara['modal_padrao'] == 'R') echo 'checked="checked"'; ?> />&nbsp;Direto  	        
              </td>
    	      <td width="18%">&nbsp;</td>
    	      <td width="34%">&nbsp;</td>
  	      </tr>
    	    <tr>
    	      <td colspan="2">Data Emissão:&nbsp;&nbsp;&nbsp;
    	        <input name="data_emissao" type="text" id="data_emissao" value="<?php echo $data_base; ?>" size="8" maxlength="10" readonly /></td>
    	      <td width="27%">Vencimento:&nbsp;&nbsp;&nbsp;
    	        <input name="venc" type="text" id="venc" size="8" maxlength="10" onkeypress="formataCampo(this, '00/00/0000', event)" onblur="verificaVenc(this.value)" /></td>
    	      <td>&nbsp;</td>
    	      <td>Dias protesto:&nbsp;&nbsp;&nbsp;
    	        <input name="protesto" type="text" id="protesto" style="width:20px" size="1" maxlength="2" />
    	        <label>
    	          <input type="radio" name="dias_protesto" value="corridos" id="dias_protesto_0" />
    	          Corridos</label>
    	        <label>
    	          <input name="dias_protesto" type="radio" id="dias_protesto_1" value="uteis" checked="checked" />
    	          Úteis</label></td>
  	      </tr>
    	    <tr>
    	      <td colspan="2">Valor Total<span style="font-size:10px;">(R$)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="valor" type="text" id="valor" size="7" maxlength="25" onkeydown="FormataValor(this,event,17,2);" /></td>
    	      <td>&nbsp;</td>
    	      <td>Multa<span style="font-size:10px;">(%)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="multa" type="text" id="multa" size="3" maxlength="5" onkeydown="FormataValor(this,event,5,2);" /></td>
    	      <td><label for="juros">Juros mora Mensal<span style="font-size:10px;">(%)</span>:&nbsp;&nbsp;&nbsp;</label>
    	        <input name="juros" type="text" id="juros" size="3" maxlength="5" onkeydown="FormataValor(this,event,5,2);" /></td>
  	      </tr>
    	    <tr>
    	      <td colspan="2">Valor Desconto<span style="font-size:10px;">(R$)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="desconto" type="text" id="desconto" size="7" maxlength="10" onkeydown="FormataValor(this,event,17,2);" /></td>
    	      <td>Data Lim. Desconto:&nbsp;&nbsp;&nbsp;
    	        <input name="data_desconto" type="text" id="data_desconto" onblur="verificaDesc(this.value)" onkeypress="formataCampo(this, '00/00/0000', event)" size="10" maxlength="10" /></td>
    	      <td colspan="2">Instruções:&nbsp;&nbsp;&nbsp;
    	        <label for="textarea"></label>
    	        <textarea name="textarea" cols="35" rows="2" id="textarea" style="resize:none;"></textarea></td>
  	      </tr>
  	    </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:prepara_gerar()">Gerar</a>
  		<!--<input class="btn botao margins dir" type="button" name="gerar" id="gerar" value="Gerar" onclick="prepara_gerar()" />-->
        <input class="btn botao margins dir" type="reset" name="limpar" id="limpar" value="Limpar" />
        <input class="oculto" type="button" name="novo" id="novo" value="Novo" onclick="" />
        <br class="clear" />
	  </form>
    </div>
</div>