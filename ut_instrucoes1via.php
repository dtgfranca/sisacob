<?php
	include('config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login.php?res=1');
	}
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
	mysql_query("SET NAMES UTF8") or die(mysql_error());
	$qInst = "SELECT instrucao_1via FROM clientes WHERE cliente='$cliente'";
	$sInst = mysql_query($qInst) or die(mysql_error());
	$aInst = mysql_fetch_array($sInst);
?>
<script type="text/javascript">
	function altera(){
		var instrucao = document.getElementById('inst').value;
		$.ajax({
			type: "GET",
			url: "scripts/utilit_funcoes.php",
			data: "funcao=inst1&instruc="+instrucao,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Instrução alterada com sucesso!");
					navega("principal.php");
				}
				else{
					alerta(retorno);
				}		
			}
		});
	}	
	function contarCaracteres(box,valor,campospan){
		var conta = valor - box.length;
		document.getElementById(campospan).innerHTML = "Você ainda pode digitar " + conta + " caracteres";
		if(box.length >= valor){
			document.getElementById(campospan).innerHTML = "Opss.. você não pode mais digitar..";
			document.getElementById("campo").value = document.getElementById("campo").value.substr(0,valor);
		}
	}
</script>
<div id="section" class="quarto">
	<div class="titulo">
        <h2>INSTRUÇÃO NA 1ª VIA DE BOLETO</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  	<form id="instrucao" name="instrucao" method="post" action="">
      		<fieldset>
            	<legend>Instrução:</legend>
                <textarea name="inst" id="inst" rows="5" class="largo" onkeyup="contarCaracteres(this.value,160,'sprestante')" style="text-transform: uppercase;"><?php echo $aInst['instrucao_1via']; ?></textarea><br />
				<span id="sprestante">Caracteres disponíveis: <?php echo 160-strlen($aInst['instrucao_1via']); ?></span>
			</fieldset>
  			<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="altera()" />
        <br class="clear" />
	  </form>
    </div>
</div>