<?php
	include('config.php');
	session_start();
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];	
	
	$qPara = "SELECT boleto_padrao, nome_fantasia FROM clientes WHERE cliente='$cliente'";
	$sPara = mysql_query($qPara) or die(mysql_error());
	$aPara = mysql_fetch_array($sPara);
?>
<script type="text/javascript" >
	$(function(){
		var btnUpload=$('#upload');
		var status=$('#status');
		new AjaxUpload(btnUpload, {
			// Arquivo que fará o upload
			action: 'scripts/recebe-sacado.php',
			//Nome da caixa de entrada do arquivo
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(txt)$/.test(ext))){ 
                    // verificar a extensão de arquivo válido
					status.text('Somente arquivos txt são permitidos');
					return false;
				}
				status.text('Enviando...');
			},
			onComplete: function(file, response){
				//Limpamos o status
				status.text('');
				//Adicionar arquivo carregado na lista
				if(response==="success"){
					$('<li></li>').appendTo('#files').html('<img src="./uploads/'+file+'" alt="" /><br />'+file).addClass('success');
				} 
				else{
					status.text('Erro ao fazer o upload');
				}
			}
		});
		
	});
</script>
<script type="text/javascript">
	function importa(){
		var boleto = document.getElementById('boleto').value;		
		var nome = document.getElementById('nome').value;
		$.ajax({
			type: "GET",
			url: "scripts/utilit_funcoes.php",
			data: "funcao=para&boleto="+boleto+"&nome="+nome,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Parametrização alterada com sucesso!");
				}
				else{
					alerta(retorno);
				}		
			}
		});
	}
</script>
<div id="section" class="duplo">
	<div class="titulo">
        <h2>IMPORTAR ARQUIVO DE SACADO</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
      	<fieldset>
           	<table>
                <tr>
                    <td class="destaque">
                    	<b>OBSERVAÇÃO: Os campos no arquivo de importação devem ser separados por ponto e vírgula (;).</b>
                    </td>
                </tr>
              	<tr>
                	<td><div id="upload" class="botao">Selecionar Arquivo</div><span id="status" style="color: #f00"></span></td>
                </tr>
                	<td><ul id="files"></ul></td>
                <tr>
                	<td class="destaque">LAYOUT:</td>
                </tr>
                <tr>
                	<td> Nome | identidade | CPF/CNPJ | endereco | bairro | cep | cidade | UF | telefone pessoal | telefone comercial | celular | fax | email</td>
                </tr>
            </table>
		</fieldset>
  		<!--<input class="btn botao margins dir" type="button" name="importar" id="importar" value="Importar" onclick="importa()" />      -->  
        <br class="clear" />
    </div>
</div>