/**
 * Created with JetBrains PhpStorm.
 * User: Skill
 * Date: 07/12/12
 * Time: 10:54
 * To change this template use File | Settings | File Templates.
 */

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

jQuery.fn.ajusta = function () {
    this.css("height", $(document).height() - ($('#header').height() + $('#footer').height()) - 60);
    this.css("width", $(document).width());
    return this;
}

jQuery.fn.mede = function () {
    this.css("max-height", $(document).height() - ($('#header').height() + $('#footer').height()) - 0);
	this.find(".corpo").css("max-height", $(document).height() - ($('#header').height() + $('#footer').height()) - 0);
	this.find("fieldset").css("max-height", $(document).height() - ($('#header').height() + $('#footer').height()) - 0);
	this.find("#busca").css("max-height", $(document).height() - ($('#header').height() + $('#footer').height()) - 220);
    return this;
}

jQuery.fn.mede2 = function () {
    this.css("max-height", $(window).height() - ($('#header').height() + $('#footer').height()) - 20);
	this.find(".corpo").css("max-height", parseFloat($("#section").css("max-height")) - 90);
	this.find("fieldset").css("max-height", parseFloat($(".corpo").css("max-height")) - 25);
	this.find("div form").css("max-height", parseFloat($("fieldset").css("max-height")) - 15);
	this.find("form #busca2").css("max-height", parseFloat($("fieldset").css("max-height")) - 32);
    return this;
}

function alerta(texto) {
    $('#message_content').html('<div style="background-color: #666; color: white; padding: 5px; height: 13px; border-bottom: 1px solid black; border-top-left-radius: 3px; border-top-right-radius: 3px;"><div style="float: left">Atenção!</div><div style="float: right"><img style="cursor: pointer" onClick="$(\'#message\').fadeOut(\'fast\');" src="img/fechar.png"></div></div><br><div width="100%" style="padding: 0px 25px; min-width: 60px">' + texto + '</div><br><input type="button" value="OK" class="botao mbotao" onClick="$(\'#message\').fadeOut(\'fast\');"/><br><br>');
    $('#message').fadeIn('fast');
    $('#message_content').center();
}

function navega(url)
{
	$('#main').fadeOut('fast', function() {
		$('#main').html('<center><img src="img/loader.gif" width="64" height="64"></center>').fadeIn('slow', function() {
			$.get(url, {}, function(data) {
				$('#main').fadeOut('fast', function() {
					$('#main').html(data).fadeIn('slow');
				});
			});
		});
	});
}

function navega2(url)
{
	$('.corpo').fadeOut('fast', function() {
		$('.corpo').html('<center><img src="img/loader.gif" width="64" height="64"></center>').fadeIn('slow', function() {
			$.get(url, {}, function(data) {
				$('.corpo').fadeOut('fast', function() {
					$('.corpo').html(data).fadeIn('slow');
				});
			});
		});
	});
}

// MENU

$(document).ready(function(){
	$(".menu li").has("ul").hover(function(){
		$(this).addClass("current").children("ul").slideDown(100);
	}, function() {
		$(this).removeClass("current").children("ul").slideUp(100);
	});
	$(".menu li ul li").click().parent("ul").slideUp(100);
});

// checkbox

$(document).ready(function() {
	$('.chk_boxes').click(function(){
		var chk = $(this).attr('checked')?true:false;
		$('.chk_boxes1').attr('checked',chk);
	});
});