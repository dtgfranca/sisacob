<?php

session_start();
$userAgencia = isset($_SESSION['userAgencia']) ? $_SESSION['userAgencia'] : '';
$userCliente = isset($_SESSION['userCliente']) ? $_SESSION['userCliente'] : '';
$userConta = isset($_SESSION['userConta']) ? $_SESSION['userConta'] : '';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=9" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SisaCob</title>
	<link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
    <link href="js/upload/uploadify.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="js/ui/jquery-ui.css">
    <script language="javascript" src="js/validacao.js" type="text/javascript"></script>
    <script language="javascript" src="js/Mascaras.js" type="text/javascript"></script>
    <script language="javascript" src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script language="javascript" src="js/ui/external/jquery/jquery.js" type="text/javascript"></script>
     <script language="javascript" src="js/ui/jquery-ui.js" type="text/javascript"></script>
    
    <script language="javascript" src="js/jquery.validate.js" type="text/javascript"></script>
    <script language="javascript" src="js/script.js" type="text/javascript"></script>
    <script>
	$(function() {
		$(document).tooltip();
	});
	</script>
	<script type="text/javascript">
	var url = new String(window.location.href);
	var res = url.split("res=");
	if(res[1]==0){
		window.onload = function(){
			alerta('Dados de login inválidos!');
            $("#senha").focus();
		}
	}
	else if(res[1]==1){
		window.onload = function(){
			alerta('Sessão expirada. Faça login novamente.');
		}
	}
	else if(res[1]==2){
		window.onload = function(){
			alerta('Você não tem permissão para acessar o sistema.');
		}
	}
</script>
<!--[if lt IE 10]>
    <script type="text/javascript" src="js/IE9.js"></script>
    <link href="css/ie9.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/html5.js"></script>
<![endif]-->

</head>

<body>

<!-- CABEÇALHO -->
<script type="text/javascript">
	$(document).ready(function(){
    	$('#main').ajusta();
        $('body').fadeIn('fast');
        <?php if (isset($_GET['res'])) 
        echo '$("#senha").focus();'; 
        ?>
	});
</script>
    </div>
	<div id="header">
    	<div class="quarto esq altura">
        </div>
        <div class="min esq altura">
        </div>
        <div class="duplo esq altura">
        	<img class="top" src="img/proposta.png" style="cursor: pointer" onClick="window.open('http://www.skillnet.com.br')" />
        </div>
        <div class="quarto esq altura">
          	</div>
        </div>    	      
    </div>
    <?php /*if (!in_array($_SERVER['REMOTE_ADDR'], array('X201.16.252.81', 'X179.192.148.5'))) die("<center><span style='font-size: 24px; color: red; text-align: center'>O SisaCob está offline para manutenção até 23:59 do dia 20/01/2014. Pedimos desculpas pelo inconveniente.</span></center>");*/ ?>
<!-- FIM DO CABEÇALHO -->
<div id="message">
        <div id="message_content">
           Erro desconhecido.<br>
            <input type="button" value="OK" class="botao mbotao" onClick="$('#message').fadeOut('fast');" />
        </div>
    </div>

	<div id="main" style="display: table-cell; vertical-align: middle; width: 100%">  
    
    	<div id="section" class="small">
            <h2>LOGIN</h2>
            	<div class="corpo">
               		<form id="login" name="login" method="post" action="valida.php">
            			<fieldset>
            				<table>
                				<tr>
                    				<td><label for="agencia">Agência:</label></td>
                   					<td><input name="agencia" type="text" id="agencia" size="3" maxlength="4" value="<?php echo $userAgencia; ?>" /></td>
                    			</tr>
                                <tr>
                    				<td><label for="cliente">Cliente:</label></td>
                   					<td><input name="cliente" type="text" id="cliente" size="12" maxlength="20" value="<?php echo $userCliente; ?>" /></td>
                    			</tr>
                                <tr>
                    				<td><label for="conta">Conta:</label></td>
                   					<td><input name="conta" type="text" id="conta" size="11" maxlength="11" value="<?php echo $userConta; ?>" onKeyDown="FormataDado(10,1,event)" /></td>
                    			</tr>
                    			<tr>
                    				<td><label for="senha">Senha:</label></td>
                        			<td><input name="senha" type="password" id="senha" size="15" maxlength="15" /></td>
                    			</tr>
              			</table>
              		</fieldset>
           		      <input class="botao dir margins" type="reset" name="limpar" id="limpar" value="Limpar" />
           		      <input class="botao dir margins" type="submit" name="entrar" id="entrar" value="Entrar" />
              		<br class="clear" />
				</form>
			</div>          		
		</div>
        <div align ="center">
            <a target="_blank" href="http://ssa.sisacob.com.br/Cliente/login.php"> Clique aqui para ter acesso ao sistema de pagamento online</a> 
        </div>
	</div>    
<script language="JavaScript" src="js/pers.js" type="text/javascript"></script>
</body>
</html>