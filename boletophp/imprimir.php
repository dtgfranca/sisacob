<?php 

require_once('../config.php');

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);

function get_content($titulo)
{
	global $base, $seq, $quant, $cliente, $sacado;

	$ch = curl_init($base.".php?titulo=".$titulo."&seq=".$seq."&quant=".$quant."&cliente=".$cliente."&sacado=".$sacado);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	echo curl_exec($ch);
	curl_close($ch);
}

// die(print_r($_GET));
$base = $_GET['base'];
$base = str_replace("boletophp", "", $base);
$base = str_replace(".php", "", $base);
$base = str_replace("/", "", $base);
$base = str_replace("/", "", $base);
$base = $_SERVER['HTTP_HOST'].'/boletophp/'.$base;

if (isset($_GET['grupo']))
{
	$grupo = $_GET['grupo'];
	$mes = $_GET['mes'];

	session_start();
	// die(print_r($_SESSION));

	$query = mysql_query("SELECT titulo, documento, sacador, sequencia, cliente, sacado FROM titulos WHERE agencia = '4097' 
		AND cliente = '".$_SESSION['userCliente']."' AND sacado IN (SELECT sacado FROM sacados WHERE gsc_id = '".$grupo."') 
		AND CONCAT( LPAD(MONTH(data_venc), 2, '0'), '-', YEAR(data_venc) ) = '".$mes."'");

	if (mysql_num_rows($query) == 0)
		die("Nenhum boleto encontrado com estes parâmetros!");

	while ($linha = mysql_fetch_object($query))
	{
		$sql = mysql_query("SELECT titulo FROM titulos 
			WHERE cliente = '".$linha->cliente."' AND sacado = '".$linha->sacado."' AND documento = '".$linha->documento."' AND sacador = '".$linha->sacador."' 
			AND cancelamento IS NULL");

		$seq = $linha->sequencia;
		$quant = mysql_num_rows($sql);
		$cliente = $linha->cliente;
		$sacado = $linha->sacado;
		get_content($linha->titulo);
	}
}

elseif (isset($_GET['blg_id']))
{
	$request = $_GET['blg_id'];
	$query =  mysql_query("SELECT CONCAT(LPAD(MONTH(blg_emissao), 2, '0'), '-', YEAR(blg_emissao)) AS mes, gsc_id FROM bl_grupo WHERE blg_id = '{$request}'");
	$mes = mysql_result($query, 0, 'mes');
	$grupo = mysql_result($query, 0, 'gsc_id');

	session_start();
	// die(print_r($_SESSION));

	$query = mysql_query("SELECT titulo, documento, sacador, sequencia, cliente, sacado FROM titulos WHERE titulo IN (SELECT tit_id FROM bl_titulos WHERE blg_id = '".$request."')");

	if (mysql_num_rows($query) == 0)
		die("Nenhum boleto encontrado com estes parâmetros!");

	while ($linha = mysql_fetch_object($query))
	{
		$sql = mysql_query("SELECT titulo FROM titulos 
			WHERE cliente = '".$linha->cliente."' AND sacado = '".$linha->sacado."' AND documento = '".$linha->documento."' AND sacador = '".$linha->sacador."' 
			AND cancelamento IS NULL");

		$seq = 1;
		$quant = 1;
		$cliente = $linha->cliente;
		$sacado = $linha->sacado;
		get_content($linha->titulo);
		echo '<br>';
	}
}

else
{
	$titulo    = $_GET['titulo'];
	$seq       = $_GET['seq'];
	$quant     = $_GET['quant'];

	# Pega dados extras do cliente {
	$query = mysql_query("SELECT banco, documento, sacado, cliente, sacador 
		FROM titulos WHERE titulo = '".$titulo."' LIMIT 1");

	$assoc = mysql_fetch_object($query);
	$documento = $assoc->documento;
	$sacado  = isset($_GET['sacado']) ? $_GET['sacado'] : $assoc->sacado;
	$cliente = isset($_GET['cliente']) ? $_GET['cliente'] : $assoc->cliente;
	$sacador = isset($_GET['sacador']) ? $_GET['sacador'] : $assoc->sacador;
	# } Pega dados extras do cliente

	if ($quant > 1)
	{
		# Pega ID dos demais boletos {
		$sql = "SELECT titulo FROM titulos 
			WHERE cliente = '".$cliente."' AND sacado = '".$sacado."' AND documento = '".$documento."' AND sacador = '".$sacador."' 
			AND cancelamento IS NULL";
		$query = mysql_query($sql) or die(mysql_error());
		# } Pega ID dos demais boletos

		// die("*".mysql_num_rows($query)."|".$quant."*");
		if (mysql_num_rows($query) != $quant)
			die("H&aacute; um conflito com a quantidade de boletos a gerar. Entre em contato para mais detalhes.");

		elseif (mysql_num_rows($query) == 0)
			die("N&atilde;o foram encontrados quaisquer boletos nessas condi&ccedil;&otilde;es.");
		
		if (isset($_GET['capa']))
		{
			session_start();
			if ($assoc->banco = '756')
			{
				include('include/capa_bancoob.php');
				echo "<br><br><div style='border-bottom: 1px #000 dashed'></div><br><br>";
				include('include/verso_bancoob.php');
			}
			echo "<br><div style='page-break-before:always;'></div><br>";
		}

		$seq = 1;
		while ($linha = mysql_fetch_object($query))
		{
			get_content($linha->titulo);
			$seq++;
		}
	}
	else
		get_content($titulo);
}

?>