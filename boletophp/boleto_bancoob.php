<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Você deve ter recebido uma cópia da GNU Public License junto com     |
// | esse pacote; se não, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de João Prado Maia e Pablo Martins F. Costa				        |
// | 														                                   			  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +--------------------------------------------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>              		             				|
// | Desenvolvimento Boleto Banco do Brasil: Daniel William Schultz / Leandro Maniezo / Rogério Dias Pereira|
// +--------------------------------------------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
require('../config.php');

if (!function_exists('zero'))
{
	function zero($casas,$zero)
	{
	   while (strlen($casas)<$zero) {
	       $casas = "0".$casas;
	   }
	   return $casas;
	}
}

// print_r($_GET);
$titulo = $_GET['titulo'];

if(!empty($v_titulo)){
	foreach ($v_titulo as $cod => $valor) {
		if(empty($titulo)){
			 $titulo="$cod";
		}
		else{
			$titulo=$titulo."','$cod";
		}
	}
}
mysql_query("SET NAMES UTF8") or die(mysql_error());
$titulos = mysql_query("select *, DATE_FORMAT(data_emisao, '%d/%m/%Y') as data_emisao2, DATE_FORMAT(data_venc, '%d/%m/%Y') as data_venc2 from titulos where titulo in ('$titulo');")or die ("Não foi possível realizar a consulta ao banco de dados titulos");
if(mysql_num_rows($titulos)>0){
	while ($db_titulos=mysql_fetch_array($titulos)) {
	$cliente = $db_titulos['cliente'];
	$sacado=$db_titulos['sacado'];
	$sacados = mysql_query("select * from sacados where sacado='$sacado';")or die ("Não foi possível realizar a consulta ao banco de dados");
	
  //############## modificacao 17-12-2009, inserindo o nome do sacodor avalista - Daniel ###########//	
	$sacador=$db_titulos['sacador'];
	$saca_aval = mysql_query("select nome, cpf from sacados where sacado='$sacador';")or die ("Não foi possível realizar a consulta ao banco de dados");
	$res_sacador = mysql_fetch_array($saca_aval);
	$sacador_nome = $res_sacador['nome'];
	$sacador_cpf =$res_sacador['cpf'];
	
	if(mysql_num_rows($sacados)>0){
		$db_sacados=mysql_fetch_array($sacados);
		$cliente=$db_sacados['cliente'];
		$clientes = mysql_query("select *, coalesce(nome_fantasia,razao) as razao2 from clientes where cliente='$cliente';")or die ("Não foi possível realizar a consulta ao banco de dados clientes");
		if(mysql_num_rows($clientes)>0){
			$db_clientes=mysql_fetch_array($clientes);
			$agencia_logo=$agencia=$db_clientes['agencia'];
			$agencias = mysql_query("select * from agencias where agencia='$agencia';")or die ("Não foi possível realizar a consulta ao banco de dados agencias");

			if ($agencia == '4117' && substr($db_titulos["nossonumero"], 0, 7) != '1677072')
				die('O nosso n&uacute;mero deste boleto ainda n&atilde;o foi processado. Tente novamente mais tarde ou entre em contato com a Cooperativa.');

			if(mysql_num_rows($agencias)>0){
			$db_agencias=mysql_fetch_array($agencias);
			$db_razao=$db_agencias['razao'];


$dias_de_prazo_para_pagamento = $db_titulos['protesto'];
$taxa_boleto = 2.95;
$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = "2950,00"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$db_titulos['valor']);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');
$dadosboleto["numero_documento"] = $db_titulos['documento']."/".$db_titulos['sequencia'];	// Num do pedido ou nosso numero
$dadosboleto["data_vencimento"] = $db_titulos['data_venc2']; // Data de Vencimento do Boleto
$dadosboleto["data_documento"] = $db_titulos['data_emisao2']; // Data de emissão do Boleto 
$dadosboleto["data_processamento"] = ""; // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = number_format($db_titulos['valor'], 2, ',', ''); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// CALCULA NOSSONUMERO
if(false){//($db_titulos['so_desconto']=='R'){
	$num = mysql_query("SELECT nosso_numero_bco FROM clientes WHERE cliente='$cliente'") or die(mysql_error());
	$anum = mysql_fetch_array($num);
	$dadosboleto["nosso_numero"] = $anum['nosso_numero_bco'];
}
else{
	$dadosboleto["nosso_numero"] = substr($db_titulos['nossonumero'], -10);
}

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $db_sacados["nome"];
$dadosboleto["endereco1"] = $db_sacados["endereco"]." - ".$db_sacados["bairro"];
$dadosboleto["endereco2"] = "CEP ".$db_sacados["cep"]." - ".$db_sacados["cidade"]." - ".$db_sacados["uf"];

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "";
$dadosboleto["demonstrativo2"] = "";
if($db_titulos['descontos']>0){
	$dadosboleto["demonstrativo3"] = "- Conceder desconto de R$ ".$db_titulos['descontos']. " até o vencimento.<br>";
}
if($db_titulos['multa']>0){
	$dadosboleto["instrucoes1"] = "- Cobrar multa de ".$db_titulos['multa']."% após o vencimento.<br>";
}
if($db_titulos['juros']>0){
	$juros=(($db_titulos['valor']*$db_titulos['juros'])/100)/30;
	$dadosboleto["instrucoes2"] = "- Cobrar juros de R$ ".number_format($juros, 2, ',', '')." ao dia após o vencimento.<br>";
}
if($db_titulos['protesto']>0){
	$dadosboleto["instrucoes3"] = "- Protesto a partir do ".$db_titulos['protesto']."º dia ".$db_titulos['dias_protesto']." após o vencimento. A partir dessa, consulte o banco para pagamento.<br>";
}
if(!empty($db_titulos['instrucao'])){
	$dadosboleto["instrucoes4"] = "- ".$db_titulos['instrucao'];
}

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = $_GET['seq']."/".$_GET['quant'];
$dadosboleto["seq"] = $_GET['seq'];
$dadosboleto["valor_unitario"] = "10";
$dadosboleto["aceite"] = "N";		
$dadosboleto["uso_banco"] = ""; 	
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";



// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - BANCO DO BRASIL
$dadosboleto["agencia"] = zero($db_agencias['cod_agen'],4); // Num da agencia, sem digito
$dadosboleto["conta"] = substr($db_clientes['conta'], -8,-1); 	// Num da conta, sem digito

// DADOS PERSONALIZADOS - BANCO DO BRASIL
$dadosboleto["convenio"] = $db_agencias['convenio'];  // Num do convênio - REGRA: 6 ou 7 ou 8 dígitos
$dadosboleto["contrato"] = $db_agencias['contrato']; // Num do seu contrato
$dadosboleto["carteira"] = $db_agencias['carteira'];
$dadosboleto["variacao_carteira"] = $db_agencias['variacao_carteira'];  // Variação da Carteira, com traço (opcional)

// TIPO DO BOLETO
$dadosboleto["formatacao_convenio"] = "7"; // REGRA: 8 p/ Convênio c/ 8 dígitos, 7 p/ Convênio c/ 7 dígitos, ou 6 se Convênio c/ 6 dígitos
$dadosboleto["formatacao_nosso_numero"] = "2"; // REGRA: Usado apenas p/ Convênio c/ 6 dígitos: informe 1 se for NossoNúmero de até 5 dígitos ou 2 para opção de até 17 dígitos

/*
#################################################
DESENVOLVIDO PARA CARTEIRA 18

- Carteira 18 com Convenio de 8 digitos
  Nosso número: pode ser até 9 dígitos

- Carteira 18 com Convenio de 7 digitos
  Nosso número: pode ser até 10 dígitos

- Carteira 18 com Convenio de 6 digitos
  Nosso número:
  de 1 a 99999 para opção de até 5 dígitos
  de 1 a 99999999999999999 para opção de até 17 dígitos

#################################################
*/


// SEUS DADOS
$dadosboleto["identificacao"] = $db_clientes["razao2"];
$dadosboleto["cpf_cnpj"] = $db_clientes['cgc'];
$dadosboleto["endereco"] = $db_clientes["endereco"];
$dadosboleto["cidade_uf"] = $db_clientes["cidade"]." - ".$db_clientes["estado"];
$dadosboleto["cedente"] = $db_clientes["razao2"];
$dadosboleto["cod_cedente"] = str_replace('-', '', $db_clientes["ate_codigo"]);

//die(print_r($dadosboleto));

// N�O ALTERAR!
include("include/funcoes_bancoob.php");
include("include/layout_bancoob.php");
echo '<div style="page-break-after: always;"></div>';

// echo "<script type='text/javascript'>window.print();</script>";
						}
		}
	}
	}
}


?>
