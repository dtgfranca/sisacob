<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META http-equiv=Content-Type content=text/html charset=ISO-8859-1>
<title><?php echo $dadosboleto["identificacao"]; ?></title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 8px;
}
.boleto {
	height: 255px;
	width: 750px;
}
.lateral {
	clear: right;
	float: left;
	height: 255px;
	width: 120px;
}
.corpo {
	float: right;
	height: 255px;
	width: 610px;
}
</style>
</head>

<body>
<div class="boleto">
  <div class="lateral">
    	<table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td style="border-bottom:1px black solid;"><img src="imagens/logobancoob.jpg" width="90" border="0" /></td>
            </tr>
            <tr>
              <td>Parc/Plano</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;"><center><b><? echo $db_titulos['sequencia'].'/'.$quant;?></b></center></td>
            </tr>
            <tr>
              <td>N&ordm; Documento</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;" align="right"><b><?php echo $dadosboleto["numero_documento"]?></b></td>
            </tr>
            <tr>
              <td>Vencimento</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;"><center><b><?php echo $dadosboleto["data_vencimento"]?></b></center></td>
            </tr>
            <tr>
              <td>Ag./C&oacute;digo Beneficiário</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;"><center><b><?php echo $dadosboleto["agencia_codigo"]?></b></center></td>
            </tr>
            <tr>
              <td>Nosso N&uacute;mero</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;" align="right"><b><?php echo $dadosboleto["nosso_numero"].'-'.$dadosboleto["nosso_numero_dv"];?></b></td>
            </tr>
            <tr>
              <td>(=) Valor do Documento</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;" align="right"><b><?php echo $dadosboleto["valor_boleto"]?></b></td>
            </tr>
            <tr>
              <td>(-) Desconto/Abatimento</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;">&nbsp;</td>
            </tr>
            <tr>
              <td>(+) Mora/Multa</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;">&nbsp;</td>
            </tr>
            <tr>
              <td>(=) Valor Cobrado</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;">&nbsp;</td>
            </tr>
            <tr>
              <td>Pagador</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;">&nbsp;</td>
            </tr>
            <tr>
              <td>Beneficiário</td>
            </tr>
            <tr>
              <td style="border-bottom:1px black solid;">&nbsp;</td>
            </tr>
            <tr>
              <td><b>Autenticar no Verso</b></td>
            </tr>
          </table>    
    </div>
  <div class="corpo">
    <table width="620" cellspacing="0" cellpadding="0">
      <tr>
        <td width="144" style="border-bottom: 2px #000 solid"><img src="imagens/logobancoob.jpg" width="140" /></td>
        <td width="54" style="border-bottom: 2px #000 solid; border-left: 2px #000 solid"><center><span style="font-weight: bold; font-size: 12px;"><?php echo $dadosboleto["codigo_banco_com_dv"]?></span></center></td>
        <td colspan="2" align="right" style="border-bottom: 2px #000 solid; border-left: 2px #000 solid"><span style="font-weight: bold; font-size: 11px;"><?php echo $dadosboleto["linha_digitavel"]?></span></td>
      </tr>
      <tr>
        <td colspan="3" align="left" style="border-left: 4px #000 solid">&nbsp;Local de Pagamento</td>
        <td width="150" align="left" style="border-left: 4px #000 solid; background-color: #edf9f1">&nbsp;Vencimento</td>
      </tr>
      <tr>
        <td colspan="3" align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;<b><?php echo $db_agencias['mensagem_local']; ?></b></td>
        <td width="155" align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid; background-color: #edf9f1">&nbsp;<b><?php echo $dadosboleto["data_vencimento"]?></b></td>
      </tr>
      <tr>
        <td colspan="3" align="left" style="border-left: 4px #000 solid">&nbsp;Beneficiário</td>
        <td width="145" align="left" style="border-left: 4px #000 solid">&nbsp;Ag./C&oacute;digo Beneficiário</td>
      </tr>
      <tr>
        <td colspan="3" align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;<b><?php echo $dadosboleto["cedente"]?></b></td>
        <td width="145" align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;<b><?php echo $dadosboleto["agencia_codigo"]?></b></td>
      </tr>
    </table>
    <table width="620" cellspacing="0" cellpadding="0">
      <tr>
        <td width="95" align="left" style="border-left: 4px #000 solid"><center>Data Documento</center></td>
        <td width="95" align="left" style="border-left: 4px #000 solid"><center>N&ordm; Documento</center></td>
        <td width="83" align="left" style="border-left: 4px #000 solid"><center>Esp&eacute;cie Doc.</center></td>
        <td width="67" align="left" style="border-left: 4px #000 solid"><center>Aceite</center></td>
        <td width="90" align="left" style="border-left: 4px #000 solid"><center>Data Process.</center></td>
        <td width="150" align="left" style="border-left: 4px #000 solid">&nbsp;Nosso N&uacute;mero</td>
      </tr>
      <tr>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["data_documento"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["numero_documento"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["especie_doc"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["aceite"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["data_processamento"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;<b><?php echo $dadosboleto["nosso_numero"].'-'.$dadosboleto["nosso_numero_dv"];?></b></td>
      </tr>
      <tr>
        <td align="left" style="border-left: 4px #000 solid; background-color: #edf9f1"><center>Uso do Banco</center></td>
        <td align="left" style="border-left: 4px #000 solid"><center>Carteira</center></td>
        <td align="left" style="border-left: 4px #000 solid"><center>Esp&eacute;cie</center></td>
        <td align="left" style="border-left: 4px #000 solid"><center>Quantidade</center></td>
        <td align="left" style="border-left: 4px #000 solid"><center>X Valor</center></td>
        <td align="left" style="border-left: 4px #000 solid">&nbsp;(=) Valor Documento</td>
      </tr>
      <tr>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid; background-color: #edf9f1">&nbsp;</td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["carteira"]?> <?php echo isset($dadosboleto["variacao_carteira"]) ? $dadosboleto["variacao_carteira"] : '&nbsp;' ?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["especie"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php echo $dadosboleto["seq"]."/".$dadosboleto["quantidade"]; ?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid"><center><b><?php //echo $dadosboleto["valor_unitario"]?></b></center></td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;<b><?php echo $dadosboleto["valor_boleto"]?></b></td>
      </tr>
      <tr>
        <td colspan="5" align="left" style="border-left: 4px #000 solid">&nbsp;Instru&ccedil;&otilde;es (Texto de responsabilidade do beneficiário)</td>
        <td align="left" style="border-left: 4px #000 solid">&nbsp;(-) Desconto/Abatimento</td>
      </tr>
      <tr>
        <td colspan="5" rowspan="5"style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">
        	<?
					$intrucao=$db_clientes['instrucao'];
                    if(!empty($intrucao)){
						print "<p>".nl2br($intrucao)."</p>";
					}
					?>
					<p><?php if(!empty($dadosboleto["demonstrativo1"])){echo $dadosboleto["demonstrativo1"];} ?></p>		
					<p><?php if(!empty($dadosboleto["demonstrativo2"])){echo $dadosboleto["demonstrativo2"];} ?></p>
					<p><?php if(!empty($dadosboleto["demonstrativo3"])){echo $dadosboleto["demonstrativo3"];} ?></p>
					<p><?php if(!empty($dadosboleto["instrucoes1"])){echo $dadosboleto["instrucoes1"];} ?></p>
					<p><?php if(!empty($dadosboleto["instrucoes2"])){echo $dadosboleto["instrucoes2"];} ?></p>
					<p><?php if(!empty($dadosboleto["instrucoes3"])){echo $dadosboleto["instrucoes3"];} ?></p>
					<p><?php if(!empty($dadosboleto["instrucoes4"])){echo nl2br($dadosboleto["instrucoes4"]);} ?></p>
        </td>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" style="border-left: 4px #000 solid">&nbsp;(+) Mora/Multa</td>
      </tr>
      <tr>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" style="border-left: 4px #000 solid; background-color: #edf9f1">&nbsp;(=) Valor Cobrado</td>
      </tr>
      <tr>
        <td align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid; background-color: #edf9f1">&nbsp;</td>
      </tr>
      <tr>
        <td colspan=6 align="left" style="border-left: 4px #000 solid">&nbsp;Pagador: <b><?php echo $dadosboleto["sacado"]; ?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CPF: <b><?php echo $db_sacados['cpf']?></b></td>
      </tr>
      <tr>
        <td colspan=6 align="left" style="border-bottom: 1px #000 solid; border-left: 4px #000 solid">
        	<b>&nbsp;<?php echo $dadosboleto["endereco1"]?><br />&nbsp;<?php echo $dadosboleto["endereco2"]?></b></td>
      </tr>
       <tr>
        <td colspan=5 rowspan=2 align="left" style="border-left: 4px #000 solid"><center><?php fbarcode($dadosboleto["codigo_barras"]); ?></center></td>
        <td align="left">Autentica&ccedil;&atilde;o Mec&acirc;nica</td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
