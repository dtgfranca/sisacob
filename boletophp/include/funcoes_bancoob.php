<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa                |
// |                                                                      |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto BANCOOB/SICOOB: Marcelo de Souza              |
// | Ajuste de algumas rotinas: Anderson Nuernberg                        |
// +----------------------------------------------------------------------+

require_once('../scripts/funcoes_gera.php');

$codigobanco = "756";
$codigo_banco_com_dv = geraCodigoBanco($codigobanco);
$nummoeda = "9";
$fator_vencimento = fator_vencimento($dadosboleto["data_vencimento"]);

//valor tem 10 digitos, sem virgula
$valor = formata_numero($dadosboleto["valor_boleto"],10,0,"valor");
//agencia � sempre 4 digitos
$agencia = formata_numero($dadosboleto["agencia"],4,0);
//conta � sempre 8 digitos
$conta = formata_numero($dadosboleto["conta"],8,0);

$carteira = $dadosboleto["carteira"];

//Zeros: usado quando convenio de 7 digitos
$livre_zeros='000000';
//$modalidadecobranca = $dadosboleto["modalidade_cobranca"];
//$numeroparcela      = $dadosboleto["numero_parcela"];

if ($dadosboleto["agencia"] == 4097)
	$convenio = formata_numero($dadosboleto["cod_cedente"],7,0);
	
else
	$convenio = formata_numero($dadosboleto["convenio"],7,0);
//agencia e conta
$agencia_codigo = $agencia ." / ". $convenio;

// Nosso n�mero de at� 8 d�gitos - 2 digitos para o ano e outros 6 numeros sequencias por ano 
// deve ser gerado no programa boleto_bancoob.php
$nossonumero = formata_numero($dadosboleto["nosso_numero"],8,0);
$dadosboleto["nosso_numero"] = $nossonumero;
//$campolivre  = "$modalidadecobranca$convenio$nossonumero$numeroparcela";
//$nossonumero = $dadosboleto["nosso_numero"];
$dadosboleto["nosso_numero_dv"] = modulo_11($nossonumero);
$campolivre  = "$convenio$nossonumero";


$dv=modulo_11("$codigobanco$nummoeda$fator_vencimento$valor$carteira$agencia$campolivre");

//$linha="$codigobanco$nummoeda$dv 5 $fator_vencimento 10/15 $valor 10/25 $carteira 2/27 $agencia 4/31 $campolivre";

if ($agencia == '4097')
{
	$carteira = '1';
	$modalidade = '02';
	$parcela = str_pad($dadosboleto['seq'], 3, '0', STR_PAD_LEFT);
	
	$nossonumero = str_pad(ltrim($dadosboleto['nosso_numero'], '0'), 7, '0', STR_PAD_LEFT);
	$nossonumero_dv = nosso_numero_dv($agencia.str_pad($convenio, 10, '0', STR_PAD_LEFT).$nossonumero);
	$nossonumero .= $nossonumero_dv;
	$dv = modulo_11("$codigobanco$nummoeda$fator_vencimento$valor$carteira$agencia$modalidade$convenio$nossonumero$parcela");

	$linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$carteira$agencia$modalidade$convenio$nossonumero$parcela";
	$dadosboleto["linha_digitavel"] = monta_linha_digitavel3($linha);
}

else
{
	$linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$carteira$agencia$campolivre";
	$dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
}
$dadosboleto["codigo_barras"] = $linha;
$dadosboleto["agencia_codigo"] = $agencia_codigo;
$dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;

?>