<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<HTML>
<HEAD>
<TITLE><?php echo $dadosboleto["identificacao"]; ?></TITLE>
<META http-equiv=Content-Type content=text/html charset=ISO-8859-1>
<meta name="Generator" content="Boleto Bradesco" />
<style type='text/css'>
	* {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 8px;
		font-weight: bold;
	}
	td {
		padding-bottom: 1px;
	}
	td .cl {
		padding-bottom: 0px;
		padding-top: 0px;
		background: #000;
	}
	.dado {
		font-family: "Courier New", Courier, monospace;
		font-size: 9px;
	}
	.campotitulo {
		font-family: "Courier New", Courier, monospace;
		font-size: 11px;
		line-height: 27px;
	}
	.cp {  
		font: bold 10px Arial; color: black
	}
	.ti {  
		font: 9px Arial, Helvetica, sans-serif
	}
	.ld { 
		font: bold 15px Arial; color: #000000
	}
	.ct { 
		FONT: 9px "Arial Narrow"; COLOR: #000033
	}
	.cn { 
		FONT: 9px Arial; COLOR: black 
	}
	.bc { 
		font: bold 20px Arial; color: #000000 
	}
	.ld2 { 
		font: bold 12px Arial; color: #000000 
	}
</style> 
</head>

<BODY text='#000000' bgColor='#ffffff' topMargin=0 rightMargin=0>
	<table border=0 cellspacing="0" cellpadding="0">
    	<tr>
        	<td width="110" valign="top">
            	<table border=0 cellspacing="0" cellpadding="0" width="110">
                	<tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	<IMG src="imagens/logocaixa.jpg" width="100" height="27" border=0>
                            <br><br>RECIBO DO PAGADOR
                      	</td>
                   	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	N&ordm; Documento<br />
							<span class="dado"><?php echo $dadosboleto["numero_documento"]?></span>
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Vencimento<br />
							<span class="dado"><?php echo $dadosboleto["data_vencimento"]?></span>
                       	</td>
                 	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Ag/Cod. Beneficiário<br />
							<span class="dado"><?php echo $dadosboleto["agencia_codigo"]?></span>
                       	</td>
                   	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Nosso Numero<br />
                            <span class="dado"><?php echo $dadosboleto["nosso_numero"]?></span>
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Valor Documento<br />
							<center><span class="dado"><?php echo $dadosboleto["valor_boleto"]?></span></center>
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Desconto<br>
                            &nbsp;
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Outras Deduc./Abat.<br>
                            &nbsp;
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Mora/Multa<br>
                            &nbsp;
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Outros Acres.<br>
                            &nbsp;
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Vl. Cobrado<br>
                            &nbsp;
                       	</td>
                  	</tr>
                    <tr>
                    	<td style="border-bottom:1px black solid" align="left" valign="top">
                        	Pagador<br />
							<span class="dado"><?php echo $dadosboleto["sacado"];?></span>
                       	</td>
                  	</tr>
   				</table>
          	</td> <!-- barra lateral -->
       		<td width="10" rowspan="5">&nbsp;</td>
  	  	  <td>
            <table cellspacing=0 cellpadding=0 border=0>
                    <tr>
                        <td width=110 colspan="2">
                            <center><IMG src="imagens/logocaixa.jpg" width="100" height="27" border=0></center>
                        </td>
                        <td class='cpt' width=70 valign='bottom' style="border-left: 2px #000 solid">
                            <div align='center'>
                                <font class="campotitulo" style="font-size:18px"><?php echo $dadosboleto["codigo_banco_com_dv"]?></font>
                            </div>
                        </td>
                        <td class='ld' align='right' width=420 valign='bottom' style="border-left: 2px #000 solid">
                            <center><span class="campotitulo">
                                <?php echo $dadosboleto["linha_digitavel"]?>
                            </span></center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=6 height=2 class="cl"></td>
                    </tr>                    
              </table>
              <table cellspacing=0 cellpadding=0 border=0>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">
                          Local de pagamento
                      </td>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">
                          Vencimento
                      </td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                            <span class="dado">CASAS LOTERICAS, AG.CAIXA E REDE BANCARIA</span>
                      </td>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                            <span class="dado"><?php echo $dadosboleto["data_vencimento"]?></span>
                      </td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">Beneficiário</td>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Ag/Cod. Beneficiário</td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["cedente"]?></span>
                      </td>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["agencia_codigo"]?></span>
                      </td>
                  </tr>
                  <tr>
                      <td width="85" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">Dt. Emissão</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Nº Documento</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Esp. Doc.</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Aceite</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Dt. Proc</td>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Nosso Numero</td>
                  </tr>
                  <tr>
                      <td width="85" style="border-left: 1px #000 dashed; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["data_documento"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["numero_documento"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["especie_doc"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["aceite"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["data_processamento"]?></span>
                      </td>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["nosso_numero"]?></span>
                      </td>
                  </tr>
                  <tr>
                      <td width="85" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">Uso do Banco</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Carteira</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Esp. Moeda.</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Qtde Moeda</td>
                      <td width="85" style="border-left: 1px #000 solid; padding-left: 5px; padding-top: 1px;">Valor Moeda</td>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Valor Doc.</td>
                  </tr>
                  <tr>
                      <td width="85" style="border-left: 1px #000 dashed; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["carteira"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["especie"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["quantidade"]?></span>
                      </td>
                      <td width="85" style="border-left: 1px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["valor_unitario"]?></span>
                      </td>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                          <span class="dado"><?php echo $dadosboleto["valor_boleto"]?></span>
                      </td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">Texto de responsabilidade do beneficiário</td>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Desconto</td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" rowspan="7" style="border-left: 1px #000 dashed; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">
                      	<span class="dado">
                        	<?php if(!empty($dadosboleto["instrucoes1"])){echo $dadosboleto["instrucoes1"];} ?><br>
							<?php if(!empty($dadosboleto["instrucoes2"])){echo $dadosboleto["instrucoes2"];} ?><br>
                            <?php if(!empty($dadosboleto["instrucoes3"])){echo $dadosboleto["instrucoes3"];} ?><br>
                            <?php if(!empty($dadosboleto["instrucoes4"])){echo $dadosboleto["instrucoes4"];} ?>
                      </span>
                      </td>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Outras Deduc./Abat.</td>
                  </tr>
                  <tr>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Mora/Multa</td>
                  </tr>
                  <tr>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Outros Acres.</td>
                  </tr>
                  <tr>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">
                      	Pagador: <span class="dado"><?php echo $dadosboleto["sacado"]?></span> 
                      </td>
                      <td width="130" style="border-left: 2px #000 solid; padding-left: 5px; padding-top: 1px;">Vl. Cobrado</td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">
                          <span class="dado">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dadosboleto["endereco1"]?></span>
                      </td>
                      <td width="130" align="right" style="border-left: 2px #000 solid; border-bottom: 1px #000 solid; padding-left: 5px; padding-top: 1px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;">Sacador/Avalista: </td>
                      <td width="130">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" style="border-left: 1px #000 dashed; border-bottom: 2px #000 solid; padding-left: 5px; padding-top: 1px;">&nbsp;</td>
                      <td width="130" align="right" style="border-bottom: 2px #000 solid; padding-left: 5px; padding-top: 1px;"></td>
                  </tr>
                  <tr>
                      <td width="460" colspan="5" rowspan="4" style="border-left: 1px #000 dashed; padding-left: 5px; padding-top: 1px;"><?php fbarcode($dadosboleto["codigo_barras"]); ?></td>
                      <td width="130">&nbsp;</td>
                  </tr>
                  <tr>
                      <td width="130">Ficha de Compensa&ccedil;&atilde;o</td>
                  </tr>
                  <tr>
                      <td width="130">Autenticação no verso</td>
                  </tr><tr>
                      <td width="130">&nbsp;</td>
                  </tr>
              </table>            	
           	</td>
      	</tr>
   	</table>
</BODY>
</HTML>