<?php include_once "seguranca.php";
$cooperativa = parse_ini_file("cooperativas.ini");
protege();?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SisaCob</title>
	<link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="js/ui/jquery-ui.css">
    <script language="javascript" src="js/validacao.js" type="text/javascript"></script>
    <script language="javascript" src="js/Mascaras.js" type="text/javascript"></script>
    <script language="javascript" src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script language="javascript" src="js/ui/external/jquery/jquery.js" type="text/javascript"></script>
     <script language="javascript" src="js/ui/jquery-ui.js" type="text/javascript"></script>

    <script language="javascript" src="js/jquery.validate.js" type="text/javascript"></script>
    <script language="javascript" src="js/jquery.maskedinput.js" type="text/javascript"></script>
    <script language="javascript" src="js/script.js" type="text/javascript"></script>
    <script>
	$(function() {
		$(document).tooltip();
	});
	</script>
    <!--<script language="javascript" src="js/pers.js" type="text/javascript"></script>-->
</head>

<body>
    <div id="wrap">
        <div id="header">
            <div class="quarto min1 esq altura">
                <div class="interno">
                    <b>Usuário: </b><?php echo $_SESSION['userNome'];?><br />
                    <b>Último Acesso: </b><?php echo $_SESSION['userConec'];?><br />
                    <b>IP da Última Conexão: </b><?php echo $_SESSION['userIp'];?>

  </b>

                </div>
            </div>
            <div class="min-cabecalho esq altura">
                <img class="top" src="img/proposta.png" style="cursor: pointer" onClick="window.open('http://www.skillnet.com.br')" />
            </div>
            <div class="duplo esq altura">
                <img class="top" src="img/logo/logo_<?php echo $_SESSION['userAgencia'];?>.jpg" width="<?php if ($_SESSION['userAgencia'] == '4117') {echo '250';} else {echo '130';}
?>" style="position:relative; left:15px;" />

		   </div>
            <div class="quarto esq min-cabecalho altura">
                <div class="interno tdir">
                    v2.0<br>
                    <span style="cursor: pointer" onClick="window.location='scripts/logout.php'"><b>Encerrar (x)</b></span>
                </div>
            </div>
        </div>

<!-- MENU -->



    <div id="nav">
    	<ul class="menu">
        	<li><a href="javascript:navega('principal.php');">A - Principal</a></li>
            <?php
          
if ($_SESSION['userAgencia'] == "4097") {
	echo '
            <li><a href="javascript:navega(\'4097/cliente_buscar.php\');">B -  Clientes</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega(\'4097/cliente_buscar.php\');">1 - Busca</a></li>
					<li><a href="javascript:navega(\'4097/cliente_cadastrar.php\');">2 - Cadastro</a></li>
					<li style="max-height: 20px">
						<a href="javascript:navega(\'4097/grupo_buscar.php\');">3 - Grupos</a>
						<ul class="sub-menu" style="position: relative; top: -20px; left: 150px">
							<li><a href="javascript:navega(\'4097/grupo_buscar.php\');">1 - Busca</a></li>
							<li><a href="javascript:navega(\'4097/grupo_cadastrar.php\');">2 - Cadastro</a></li>
						</ul>
					</li>';
} elseif (in_array($_SESSION['userAgencia'],$cooperativa))  {
	echo '
                    <li >B -  Clientes</a>
                            <ul class="sub-menu "  readonly="readonly">
                                <li><a >1 - Busca</a></li>
                                <li><a >2 - Cadastro</a></li>';

} else {
	echo '
			<li><a href="javascript:navega(\'cliente_buscar.php\');">B -  Clientes</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega(\'cliente_buscar.php\');">1 - Busca</a></li>
					<li><a href="javascript:navega(\'cliente_cadastrar.php\');">2 - Cadastro</a></li>';
}
?>
               	</ul>
           	</li>
            <li>
                <?php
if ($_SESSION['userAgencia'] == "4097") {
	echo '
				<a href="javascript:navega(\'4097/titulo_buscar.php\');">C - Títulos</a>
				<ul class="sub-menu">
                	<li><a href="javascript:navega(\'4097/titulo_buscar.php\');">1 - Busca</a></li>
                    <li style="max-height: 20px">
                        <a href="javascript:navega(\'4097/titulo_cadastrar.php\');">2 - Cadastro</a>
                        <ul class="sub-menu" style="position: relative; top: -20px; left: 150px">
                            <li><a href="javascript:navega(\'4097/titulo_cadastrar.php\');">1 - Individual</a></li>
                            <li><a href="javascript:navega(\'4097/titulo_grupos.php\');">2 - Por Grupo</a></li>
                        </ul>
                    </li>
					';
} else if ($_SESSION['userAgencia'] == "4030") {
	echo '
				<a href="javascript:navega(\'4030/titulo_buscar.php\');">C - Títulos</a>
				<ul class="sub-menu">
                	<li><a href="javascript:navega(\'4030/titulo_buscar.php\');">1 - Busca</a></li>
					<li><a href="javascript:navega(\'4030/titulo_cadastrar.php\');">2 - Cadastro</a></li>
					';
} elseif (in_array($_SESSION['userAgencia'],$cooperativa)) {
	echo '
               <li><a>C - Títulos</a>
                <ul class="sub-menu">
                    <li><a >1 - Busca</a></li>
                    <li><a >2 - Cadastro</a></li>
                    ';
} else {
	echo '
				<a href="javascript:navega(\'titulo_buscar.php\');">C - Títulos</a>
				<ul class="sub-menu">
                	<li><a href="javascript:navega(\'titulo_buscar.php\');">1 - Busca</a></li>
                    <li><a href="javascript:navega(\'titulo_cadastrar.php\');">2 - Cadastro</a></li>
					';
}
?>
               	</ul>
           	</li>
            <?php
if (in_array($_SESSION['userAgencia'],$cooperativa)) {

	echo '<li><a href="javascript:navega(\'rel_historico.php\');">D - Relatórios</a>
                <ul class="sub-menu">
                    <li><a href="javascript:navega(\'rel_historico.php\');">1 - Histórico Títulos</a></li>

                </ul>
            </li>';
} else {

	echo '    <li><a href="javascript:navega(\'rel_historico.php\');">D - Relatórios</a>
                <ul class="sub-menu">
                    <li><a href="javascript:navega(\'rel_historico.php\');">1 - Histórico Títulos</a></li>
                    <li><a href="javascript:navega(\'rel_carteira.php\');">2 - Carteira</a></li>
                    <li><a href="javascript:navega(\'rel_instrucoes.php\');">3 - Instruções</a></li>
                    <li><a href="javascript:navega(\'rel_tarifas.php\');">4 - Tarifas</a></li>
                    <li><a href="javascript:navega(\'rel_lancamentos.php\');">5 - Lançamentos</a></li>
                    <li><a href="extrato.php" target="_blank" >6 - Extrato</a></li>
                    <li><a href="rel/informe.php" target="_blank">7 - Aviso de lançamentos</a></li>

                   
                ';
            if( $_SESSION['userMaster']){
                echo ' <li><a href="scripts/relatorios/r_titulos_a_vencer.php"  target="_blank"> 8 - Títulos a vencer</a></li>';

            }
           echo  " </ul>
                 </li>";
}

?>

            <?php
if (in_array($_SESSION['userAgencia'],$cooperativa)) {

	echo '  <li><a >E - Sacador/Avalista</a>
                <ul class="sub-menu">
                    <li><a >1 - Busca</a></li>
                    <li><a >2 - Cadastro</a></li>
                </ul>
            </li>';
} else {
	echo '  <li><a href="javascript:navega(\'sacadoravalista_buscar.php\');">E - Sacador/Avalista</a>
                <ul class="sub-menu">
                    <li><a href="javascript:navega(\'sacadoravalista_buscar.php\');">1 - Busca</a></li>
                    <li><a href="javascript:navega(\'sacadoravalista_cadastrar.php\');">2 - Cadastro</a></li>
                </ul>
            </li>';
}
?>
           <?php
if(in_array($_SESSION['userAgencia'],$cooperativa)) {

	echo ' <li><a href="javascript:navega(\'cadastrar_usuario.php\');">F - Utilitários</a>
                <ul class="sub-menu">
                    <li><a href="javascript:navega(\'ut_senha.php\');">1 - Alterar Senha</a></li>
                 <li><a href="javascript:navega(\'cnab/inicio.php\');">2 - Integração CNAB 400</a></li>
                 <li><a href="arquivos/CNAB400_EXPORTACAO.pdf" target="_blank">3 - Layout de Exportação</a></li>
                    ';

} else {
	echo ' <li><a href="javascript:navega(\'cadastrar_usuario.php\');">F - Utilitários</a>
                <ul class="sub-menu">
                    <li><a href="javascript:navega(\'ut_senha.php\');">1 - Alterar Senha</a></li>
                    <!--<li><a href="javascript:navega(\'cnab/inicio_240.php\');">2 - Integração CNAB 240</a></li>-->
                 <li><a href="javascript:navega(\'cnab/inicio.php\');">2 - Integração CNAB 400</a></li>
                    <li><a href="javascript:navega(\'ut_instrucoes.php\');">3 - Instruções</a></li>';

}

?>
                    <?php
if ($_SESSION['userAgencia'] == "4097") {
	echo '<li><a href="javascript:navega(\'4097/ut_parametriza.php\');">4 - Parametrização Boletos</a></li>';
} elseif ($_SESSION['userAgencia'] != "4097" && !in_array($_SESSION['userAgencia'],$cooperativa)) {
	echo '<li><a href="javascript:navega(\'ut_parametriza.php\');">4 - Parametrização Boletos</a></li>';
}

?>
                 <?php
if (!in_array($_SESSION['userAgencia'],$cooperativa)) {
	echo '   <li><a href="javascript:navega(\'ut_importar.php\');">5 - Importar Sacados</a></li>
                    <li><a href="javascript:navega(\'ut_instrucoes1vi\.php\');">6 - Obs. 1ª via</a></li>
                    <li><a href="arquivos/LAYOUT_CNAB400_CBR641_REM.pdf" target="_blank">7 - Layout de Importação</a></li>
                    <li><a href="arquivos/CNAB400_EXPORTACAO.pdf" target="_blank">8 - Layout de Exportação</a></li>';
}
?>
               	</ul>
           	</li>
        </ul>
    </div>

<!-- FIM DO MENU -->
    <?php if ($_SESSION['userAgencia'] == "4097") {?>
		<center><span style="color: red">  
            <b>A partir do dia 01/11/2018 haverá alterações nas tarifas referentes a cobrança.
            <br> Para mais informações entrar em contato através do 3226-9700.
        </b>
</span></center> 
    <?php }?>
        <div id="main" style="display: table-cell; vertical-align: middle; width: 100%">
            <center><img src="img/loader.gif" width="64" height="64"></center>
        </div>
    </div>

    <div id="message">
        <div id="message_content">
           Erro desconhecido.<br>
            <input type="button" value="OK" class="botao mbotao" onClick="$('#message').fadeOut('fast');" />
        </div>
    </div>

    <div id="nova">
        <div id="nova_content">
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#main').ajusta();
            $('body').fadeIn('fast');

            $('#message_content').click(function(event) { event.stopPropagation(); });
			$('#main').load('principal.php');
        });
	</script>


</body>
</html>