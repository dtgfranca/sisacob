<?php
	include('config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
	//Limpando o erro de fontes do relatório
	if (file_exists('scripts/font/unifont/dejavusans-bold.mtx.php')) {
		unlink('scripts/font/unifont/dejavusans-bold.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusans-bold.cw.dat')) {
		unlink('scripts/font/unifont/dejavusans-bold.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat');
	}
	$agencia = $_SESSION['userAgencia'];
	$conta = $_SESSION['userConta'];
?>
<script type="text/javascript">
	function r_simples(){
		var inicio = document.getElementById('inicio').value;
		var tini = inicio.length;
		var fim = document.getElementById('fim').value;
		var tfim = fim.length;
		var tipo = $('input[name=tipo]:checked').val();
		if(tipo == 'nosso_n'){
			var nosso = document.getElementById('nosso_n').value;
			var tnum = nosso.length;
			if(tnum < 6){
				alerta('Nosso Número inválido');
			}
			else {
				document.forms["relatorio"].action="scripts/relatorios/r_simples.php";
				document.forms["relatorio"].submit();
			}
		}
		else if(tipo == 'n_doc'){
			var doc = document.getElementById('n_titulo').value;
			var tdoc = doc.length;
			if(tdoc < 2 ){
				alerta('Número do documento inválido');
			}
			else {
				document.forms["relatorio"].action="scripts/relatorios/r_simples.php";
				document.forms["relatorio"].submit();
			}
		}
		else{
			document.forms["relatorio"].action="scripts/relatorios/r_simples.php";
			document.forms["relatorio"].submit();
		}
		/*else if(tipo == 'lista_previa'){
			document.forms["relatorio"].action="scripts/relatorios/r_simples.php";
			document.forms["relatorio"].submit();
		}
		else {
			if(tini != 10 || inicio == ''){
				alerta('Data inicial inválida');
			}
			else{
				var inicial = 'ok';
			}
			if(tfim != 10 || fim == ''){
				alerta('Data final inválida');
			}
			else{
				var final = 'ok';
			}
			if(inicial == 'ok' && final == 'ok'){
				document.forms["relatorio"].action="scripts/relatorios/r_simples.php";
				document.forms["relatorio"].submit();
			}
		}*/
	}
    function r_simples_outros(){
        var inicio = document.getElementById('inicio').value;
        var tini = inicio.length;
        var fim = document.getElementById('fim').value;
        var tfim = fim.length;
        var tipo = $('input[name=tipo]:checked').val();
        if(tipo == 'nosso_n'){
            var nosso = document.getElementById('nosso_n').value;
            var tnum = nosso.length;
            if(tnum < 6){
                alerta('Nosso Número inválido');
            }
            else {
                document.forms["relatorio"].action="scripts/relatorios/r_simples_outros.php";
                document.forms["relatorio"].submit();
            }
        }
        else if(tipo == 'n_doc'){
            var doc = document.getElementById('n_titulo').value;
            var tdoc = doc.length;
            if(tdoc < 2 ){
                alerta('Número do documento inválido');
            }
            else {
                document.forms["relatorio"].action="scripts/relatorios/r_simples_outros.php";
                document.forms["relatorio"].submit();
            }
        }
        else{
            document.forms["relatorio"].action="scripts/relatorios/r_simples_outros.php";
            document.forms["relatorio"].submit();
        }

    }
	function r_complementos(){
		var inicio = document.getElementById('inicio').value;
		var tini = inicio.length;
		var fim = document.getElementById('fim').value;
		var tfim = fim.length;
		var tipo = $('input[name=tipo]:checked').val();
		if(tipo == 'nosso_n'){
			var nosso = document.getElementById('nosso_n').value;
			var tnum = nosso.length;
			if(tnum < 6){
				alerta('Nosso Número inválido');
			}
			else {
				document.forms["relatorio"].action="scripts/relatorios/r_complementos.php";
				document.forms["relatorio"].submit();
			}
		}
		else if(tipo == 'n_doc'){
			var doc = document.getElementById('n_titulo').value;
			var tdoc = doc.length;
			if(tdoc < 2 ){
				alerta('Número do documento inválido');
			}
			else {
				document.forms["relatorio"].action="scripts/relatorios/r_complementos.php";
				document.forms["relatorio"].submit();
			}
		}
		else {
			document.forms["relatorio"].action="scripts/relatorios/r_complementos.php";
			document.forms["relatorio"].submit();
		}
		/*else if(tipo == 'lista_previa'){
			document.forms["relatorio"].action="scripts/relatorios/r_complementos.php";
			document.forms["relatorio"].submit();
		}
		else {
			if(tini != 10 || inicio == ''){
				alerta('Data inicial inválida');
			}
			else{
				var inicial = 'ok';
			}
			if(tfim != 10 || fim == ''){
				alerta('Data final inválida');
			}
			else{
				var final = 'ok';
			}
			if(inicial == 'ok' && final == 'ok'){
				document.forms["relatorio"].action="scripts/relatorios/r_complementos.php";
				document.forms["relatorio"].submit();
			};
		}*/
	}
    function r_agrupado_outros(){
        var inicio = document.getElementById('inicio').value;
        var tini = inicio.length;
        var fim = document.getElementById('fim').value;
        var tfim = fim.length;
        var tipo = $('input[name=tipo]:checked').val();
        if(tipo == 'nosso_n'){
            var nosso = document.getElementById('nosso_n').value;
            var tnum = nosso.length;
            if(tnum < 6){
                alerta('Nosso Número inválido');
            }
            else {
                document.forms["relatorio"].action="scripts/relatorios/r_agrupado_outros.php";
                document.forms["relatorio"].submit();
            }
        }
        else if(tipo == 'n_doc'){
            var doc = document.getElementById('n_titulo').value;
            var tdoc = doc.length;
            if(tdoc < 2 ){
                alerta('Número do documento inválido');
            }
            else {
                document.forms["relatorio"].action="scripts/relatorios/r_agrupado_outros.php";
                document.forms["relatorio"].submit();
            }
        }
        else {
            document.forms["relatorio"].action="scripts/relatorios/r_agrupado_outros.php";
            document.forms["relatorio"].submit();
        }

    }
	function r_agrupado(){
		var inicio = document.getElementById('inicio').value;
		var tini = inicio.length;
		var fim = document.getElementById('fim').value;
		var tfim = fim.length;
		var tipo = $('input[name=tipo]:checked').val();
		if(tipo == 'nosso_n'){
			var nosso = document.getElementById('nosso_n').value;
			var tnum = nosso.length;
			if(tnum < 6){
				alerta('Nosso Número inválido');
			}
			else {
				document.forms["relatorio"].action="scripts/relatorios/r_agrupado.php";
				document.forms["relatorio"].submit();
			}
		}
		else if(tipo == 'n_doc'){
			var doc = document.getElementById('n_titulo').value;
			var tdoc = doc.length;
			if(tdoc < 2 ){
				alerta('Número do documento inválido');
			}
			else {
				document.forms["relatorio"].action="scripts/relatorios/r_agrupado.php";
				document.forms["relatorio"].submit();
			}
		}
		else {
			document.forms["relatorio"].action="scripts/relatorios/r_agrupado.php";
			document.forms["relatorio"].submit();
		}
		/*else if(tipo == 'lista_previa'){
			document.forms["relatorio"].action="scripts/relatorios/r_agrupado.php";
			document.forms["relatorio"].submit();
		}
		else {
			if(tini != 10 || inicio == ''){
				alerta('Data inicial inválida');
			}
			else{
				var inicial = 'ok';
			}
			if(tfim != 10 || fim == ''){
				alerta('Data final inválida');
			}
			else{
				var final = 'ok';
			}
			if(inicial == 'ok' && final == 'ok'){
				document.forms["relatorio"].action="scripts/relatorios/r_agrupado.php";
				document.forms["relatorio"].submit();
			}
		}*/
	}
	$(function() {
		$("#inicio").datepicker({
			numberOfMonths: 2,
			onClose: function(selectedDate) {
				$("#fim").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#fim").datepicker({
			numberOfMonths: 2,
			onClose: function(selectedDate) {
				$("#inicio").datepicker("option", "maxDate", selectedDate);
			}
		});
	});
</script>
<div id="section" class="duplo">
	<div class="titulo">
        <h2>RELATÓRIOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    	<form id="relatorio" name="relatorio" method="post" action="" target="_blank">
            <fieldset>
                <legend>HISTÓRICO DE TÍTULOS</legend>
                <p>CLIENTE:&nbsp;&nbsp;
                <input type="hidden" value="1" name="flag">
                <select name="cliente" id="cliente" style="width: 300px;">
          	    	<option value="0">Selecione o cliente</option>
          	    	<?php
						mysql_query("SET NAMES UTF8") or die(mysql_error());
						$sql = "SELECT nome, sacado, cpf FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND (grupo <> '99999' OR grupo IS NULL) ORDER BY UPPER(nome)";
						$query = mysql_query($sql) or die(mysql_error());
						while($linha = mysql_fetch_array($query)){
							echo '
								<option value="'.$linha['sacado'].'">'.$linha['nome'].' - '.$linha['cpf'].'</option>
							';
						}
					?>
       	    	</select>
                </p>
                <br />
              <table>
                	<tr>
                   	  <td width="50%"><p>Situação 1</p></td>
                      <td width="50%" colspan="2"><p>Situação 2</p></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="todos" checked>&nbsp;Todos</td>
                        <td colspan="2"><input type="radio" name="tipo2" checked value="" >&nbsp;Todos</td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="rec" />&nbsp;Recebidos</td>
                        <td colspan="2"><input type="radio" name="tipo2" value="cobranca_simples">&nbsp;Cobrança Simples</td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="baixa" />&nbsp;Baixados</td>
                        <td colspan="2"><input type="radio" name="tipo2" value="desc">&nbsp;Descontados</td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="baixa_protesto" />&nbsp;Baixado/Protesto</td>                        
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="pend" />&nbsp;Em Aberto</td>
                        <td colspan="2"><p>Período</p></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="venc" />&nbsp;Vencidos</td>
                        <td>Filtro:
                        </td>
                      <td><select name="filtro">
                        <option value="data_emisao">Data da Emissão</option>
                        <option value="data_venc">Data do Vencimento</option>
                        <option value="data_baixa">Data da Liquidação</option>
                      </select></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="cancel" />&nbsp;Cancelados</td>
                        <td>Início:</td>
                        <td><input name="inicio" type="text" id="inicio" size="7" maxlength="10" onkeypress="formataCampo(this, '00/00/0000', event); return SomenteNumero(this)" /></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="lista_previa" />&nbsp;Listagem Prévia</td>
                        <td>Fim:</td>
                        <td><input name="fim" type="text" id="fim" size="7" maxlength="10" onkeypress="formataCampo(this, '00/00/0000', event); return SomenteNumero(this)" /></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="rejeitados" />&nbsp;Rejeitados</td>
                        <td colspan="2"><p>Ordenar</p></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="nosso_n" />&nbsp;Nosso Número</td>
                        <td colspan="2"><select name="ordem">
                          <option value="t.data_venc">Vencimento</option>
                          <option value="s.nome">Nome</option>
                          <option value="t.data_emisao">Emissão</option>
                          <option value="t.data_baixa">Liquida&ccedil;&atilde;o</option>
                        </select></td>
                    </tr>
                    <tr id="op_nnumero">
                        <td><input name="nosso_n" id="nosso_n" type="text" size="15" maxlength="17" /></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="tipo" value="n_doc" />&nbsp;Nº Documento</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="op_ntitulo">
                        <td><input name="n_titulo" id="n_titulo" type="text" size="15" maxlength="8" /></td>
                        <td colspan="2"></td>
                    </tr>
                </table>
            </fieldset>
            <a class="btn botao margins dir" href="javascript:r_complementos()">Complementos</a>
            <?php if($agencia ==4117):?><a class="btn botao margins dir" href="javascript:r_simples()">Relatório Simples</a><?php else:?>

                <a class="btn botao margins dir" href="javascript:r_simples_outros()">Relatório Simples</a>
                <?php endif;?>

             <?php if($agencia ==4117):?> <a class="btn botao margins dir" href="javascript:r_agrupado()">Relatório Agrupado</a><?php else:?>
                 <a class="btn botao margins dir" href="javascript:r_agrupado_outros()">Relatório Agrupado</a> <?php endif;?>
            <!--<input class="btn botao margins dir" type="button" name="simples" id="simples" value="Complementos" onclick="r_complementos()" />
            <input class="btn botao margins dir" type="button" name="simples" id="simples" value="Relatório Simples" onclick="r_simples()" />
            <input class="btn botao margins dir" type="button" name="agrupado" id="agrupado" value="Relatório Agrupado" onclick="r_agrupado()" /> -->
            <br class="clear" />
        </form>
    </div>
</div>
