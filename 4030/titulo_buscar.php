<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php 
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
?>
<div id="dialog-confirm" title="Excluir título?" style="display:none">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Este título será excluído permanentemente. Deseja continuar?</p>
</div>
<div id="dialog">
</div>
<div id="load">
	<div id="load_content">
      	<div width="100%" style="padding: 0px 25px; min-width: 60px"><br /><center><img src="img/loader.gif" width="64" height="64"></center><br /></div>
    </div>
</div>	
<div id="section" class="largo">
<script type="text/javascript">
	$(document).ready(function(){
    	$('#section').mede();
		$('#load_content').center();
		$('#filt').hide();
		$('#filtros').click(function() {
			$('#filt').slideToggle('fast',function(){
			if($('#filt').is(':visible')) {
				$('#busca').css("max-height", $("#busca").height() - 25);
			}
			else{
				$('#busca').css("max-height", $("#busca").height() + 25);
			}
			});
        });
		$('#chk_boxes').click(function(){
			var chk = $(this).prop('checked')?true:false;
			$('.chk_boxes1').prop('checked',chk);
		});
		var i = 0;
		$('#busca').bind('scroll', function(){
        	if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
				i++;
           		carregar(i);
            }
  		});
		$("#buscar").keypress(function(e) {
			if(e.which == 13) {
				event.preventDefault();
				event.returnValue=false;
				busca();
			}
		});
		$("#buscar").keyup(function(){
			var busca = $(this).val();
			var tam = busca.length;
			if(tam > 0){
				$("#pesq").removeAttr('class');
				$("#pesq").attr('class','botao top dir');
				$("#pesq").attr('onclick','busca()');
			}
			else{
				$("#pesq").removeAttr('class');
				$("#pesq").attr('class','bdesab top dir');
				$("#pesq").removeAttr('onclick');
			}
		});	
		$("input[name='radio_filtro']").change(function(){
			var filtro = $('input[name=radio_filtro]:checked').val();
			if(filtro != 'nenhum'){
				$("#pesq").removeAttr('class');
				$("#pesq").attr('class','botao top dir');
				$("#pesq").attr('onclick','busca()');
			}
			else{
				$("#pesq").removeAttr('class');
				$("#pesq").attr('class','bdesab top dir');
				$("#pesq").removeAttr('onclick');
			}
		});
		$('#acao').change(function(){
			camposMarcados = new Array();
			$("input[type=checkbox][name='box[]']:checked").each(function(){
				camposMarcados.push($(this).val());
			});
			if(camposMarcados.length === 0){
				alerta('Favor selecionar no mínimo um título');
			}
			else {
				var ids = "";
				$.each(camposMarcados, function() {
					ids += (this)+',';
				});
				if($(this).val()=='excluir'){
					$.ajax({
						type: 'GET',
						url: "scripts/titulos_funcoes.php",
						data: "funcao=gexcluir&id="+ids,
						success: function(retorno){
							$("#dialog").html('<div id="dialog-boletos" title="Boletos" width="250px"><p><b>Tem certeza de que deseja cancelar os boletos selecionados?</b></p><br>'+retorno+'</div>');
							$("#dialog-boletos").dialog({
								resizable: false,
								height:200,
								width:700,
								modal: true,
								buttons: {
									"Excluir": function() {
										gexcluir(ids);
										$(this).dialog("close");
									},
									"Cancelar": function() {
										$(this).dialog( "close" );
									}
								}
							});
						}
					});
				}
				else if($(this).val()=='confirmar'){
					$.ajax({
						type: 'GET',
						url: "scripts/titulos_funcoes.php",
						data: "funcao=gconfirma&id="+ids,
						success: function(retorno){
							$("#dialog").html('<div id="dialog-boletos" title="Boletos" width="250px"><p><b>Tem certeza de que deseja confirmar os boletos selecionados?</b></p><br>'+retorno+'</div>');
							$("#dialog-boletos").dialog({
								resizable: false,
								height:200,
								width:700,
								modal: true,
								buttons: {
									"Confirmar": function() {
										gconfirmar(ids);
										$(this).dialog("close");
									},
									"Cancelar": function() {
										$(this).dialog( "close" );
									}
								}
							});
						}
					})
				}
			}
		});
	});
	function gexcluir(titulos){
		$.ajax({
			type: 'GET',
			url: "scripts/titulos_funcoes.php",
			data: "funcao=cgexcluir&id="+titulos,
			success: function(retorno){
				if(retorno == 'ok'){
					alerta('Títulos cancelados com sucesso.');
					navega('titulo_buscar.php');
				}
				else if(retorno == 'erro'){
					alerta('Erro ao cadastrar.');
				}
			}
		});
	}
	function gconfirmar(titulos){
		$.ajax({
			type: 'GET',
			url: "scripts/titulos_funcoes.php",
			data: "funcao=cgconfirma&id="+titulos,
			success: function(retorno){
				if(retorno == 'ok'){
					alerta('Títulos confirmados com sucesso.');
					navega('titulo_buscar.php');
				}
				else if(retorno == 'erro'){
					alerta('Erro ao cadastrar.');
				}
			}
		});
	}
	function carregar(i){
		$.ajax({
			type: 'GET',
			url: "scripts/titulos_funcoes.php",
			data: "funcao=carrega&i="+i,
			beforeSend: function() {
				$('#load').fadeIn('fast');
			},
			success: function(retorno){
				if(retorno != 'f'){
					$("#lista").append(retorno);
				}
			},
			complete: function() {
				$('#load').fadeOut('fast');				
			}
		});
	}
	function busca(){
		var busca = $("#buscar").val();
		var filtro = $('input[name=radio_filtro]:checked').val();
		$.ajax({
			type: 'GET',
			url: "scripts/titulos_funcoes.php",
			data: "funcao=busca&busca="+busca+"&filtro="+filtro,
			//beforeSend: function() {
				//$("#busca").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
			//},
			success: function(retorno){
				$("#busca").html(retorno);
			}
		});	
	}
	function confirma(titulo){
		$.ajax({
			type: 'GET',
			url: "scripts/titulos_funcoes.php",
			data: "funcao=confirma&titulo="+titulo,
			success: function(retorno){
				if(retorno=='ok'){
					alerta('Título confirmado com sucesso.');
					navega('titulo_buscar.php');					
				}
				else{
					alerta(retorno)
				}
			}
		});
	}
	function excluir(titulo){
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Excluir": function() {
					$.ajax({
						type: 'GET',
						url: "scripts/titulos_funcoes.php",
						data: "funcao=excluir&titulo="+titulo,
						success: function(retorno){
							if(retorno == "ok"){
								alerta("Exclusão realizada com sucesso!");
								navega('titulo_buscar.php');
							}
							else {
								alerta (retorno);
							}
						}					
					});
					$(this).dialog("close");
				},
				"Cancelar": function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
	function instrucao(titulo){
		var acao = document.getElementById(titulo).value;
		if(acao=='06'){ // ALTERAÇÃO DE VENCIMENTO
			$.ajax({
				type: 'GET',
				url: "scripts/titulos_funcoes.php",
				data: "funcao=altvenc&id="+titulo,
				success: function(retorno){
					$("#dialog").html('<div id="dialog-alt" title="Alterar Vencimento">'+retorno+'</div>');
					$("#dialog-alt").dialog({
						resizable: false,
						height:170,
						width:300,
						modal: true,
						buttons: {
							"Confirmar": function() {
								var venc = document.getElementById('venc').value;
								$.ajax({
									type: 'GET',
									url: "scripts/titulos_funcoes.php",
									data: "funcao=altervenc&id="+titulo+"&acao="+acao+"&data="+venc,
									success: function(retorno){
										if(retorno == "ok"){
											alerta("Alteração de vencimento realizada com sucesso!");
											navega('titulo_buscar.php');
											var remote = null;	
											remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
											if(remote != null){
												remote.location.href = 'titulos_comprovante.php?titulo='+titulo+'&descricao=ALTERACÃO DE VENCIMENTO';
											}
										}
										else if(retorno == "erro1"){
											alerta("Campos vazios, operação impedida.");
										}
										else if(retorno == "erro2"){
											alerta("Erro ao inserir os dados.");
										}
										else if(retorno == "erro3"){
											alerta("Instrução já comandada hoje.");
										}
										else {
											alerta (retorno);
										}
									}
								});
								$(this).dialog("close");
							},
							"Cancelar": function() {
								$(this).dialog( "close" );
							}
						}
					});
				}
			});			
		}
		else if(acao == '02' || acao == '05' ||acao == '09' || acao== '10' || acao == '32'){// BAIXA, CANCELAMENTO/SUSTAÇAO, PROTESTO, CANCELAMENTO DE DESCONTO E ABATIMENTO
			if(acao=='02'){
				var tit = "Solitação de baixa";
				var msg = "Solitação de baixa realizada com sucesso!";
				var desc = "SOLICITACÃO DE BAIXA";
			}
			else if(acao=='05'){
				var tit = "Cancelamento de abatimento";
				var msg = "Cancelamento de abatimento realizado com sucesso!";
				var desc = "SOLICITACÃO DE CANCELAMENTO DE ABATIMENTO";
			}
			else if(acao=='09'){
				var tit = "Protestar";
				var msg = "Instrução de protesto realizada com sucesso!";
				var desc = "SOLICITACÃO DE PROTESTO";
			}
			else if(acao=='10'){
				var tit = "Cancelamento/Sustação de Instrução de PR";
				var msg = "Cancelamento/Sustação de instrução realizado com sucesso!";
				var desc = "SOLICITACÃO DE CANCELAMENTO/SUSTACÃO DE ISNTRUCÃO DE PR";
			}
			else if(acao=='32'){
				var tit = "Cancelamento de desconto";
				var msg = "Cancelamento de desconto realizado com sucesso!";
				var desc = "SOLICITACÃO DE CANCELAMENTO DE DESCONTO";
			}
			$("#dialog").html('<div id="dialog-alt" title="'+tit+'"><p><b>Deseja continuar?</b></p></div>');
			$("#dialog-alt").dialog({
				resizable: false,
				height:100,
				width:300,
				modal: true,
				buttons: {
					"Confirmar": function() {
						$.ajax({
							type: 'GET',
							url: "scripts/titulos_funcoes.php",
							data: "funcao=alter&id="+titulo+"&acao="+acao,
							success: function(retorno){
								if(retorno == "ok"){
									alerta(msg);
									navega('titulo_buscar.php');									
									var remote = null;	
									remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
									if(remote != null){
										remote.location.href = 'titulos_comprovante.php?titulo='+titulo+'&descricao='+desc;
									}
								}
								else if(retorno == "erro1"){
									alerta("Campos vazios, operação impedida.");
								}
								else if(retorno == "erro2"){
									alerta("Erro ao inserir os dados.");
								}
								else if(retorno == "erro3"){
									alerta("Instrução já comandada hoje.");
								}
								else {
									alerta (retorno);
								}
							}
						});
						$(this).dialog("close")
					},
					"Cancelar": function() {
						$(this).dialog( "close" );
					}
				}
			});
		}
		else if(acao == '04' || acao=='31' || acao=='35'){// CONCESSÃO DE ABATIMENTO, DESCONTO OU MULTA
			if(acao=='04'){
				var tit = "Concessão de abatimento";
				var legend = "Valor do abatimento: ";
				var msg = "Concessão de abatimento realizada com sucesso!";
				var desc = "SOLICITACÃO DE CONCESSÃO DE ABATIMENTO";
			}
			else if(acao=='31'){
				var tit = "Concessão de desconto";
				var legend = "Valor do desconto: ";
				var msg = "Concessão de desconto realizada com sucesso!";
				var desc = "SOLICITACÃO DE CONCESSÃO DE DESCONTO";
			}
			else if(acao=='35'){
				var tit = "Multa";
				var legend = "Valor da multa: ";
				var msg = "Multa aplicada com sucesso!";
				var desc = "SOLICITACÃO DE APLICACÃO DE MULTA";
			}
			$("#dialog").html('<div id="dialog-alt" title="'+tit+'"><p><b>Deseja continuar?</b></p><br><label for="vvalor">'+legend+'</label><input type="text" name="vvalor" id="vvalor" size="10" maxlength="10" onkeydown="FormataValor(this,event,17,2);" /></div>');
			$("#dialog-alt").dialog({
				resizable: false,
				height:130,
				width:300,
				modal: true,
				buttons: {
					"Confirmar": function() {
						var valor = document.getElementById('vvalor').value;
						$.ajax({
							type: 'GET',
							url: "scripts/titulos_funcoes.php",
							data: "funcao=altervalor&id="+titulo+"&acao="+acao+"&valor="+valor,
							success: function(retorno){
								if(retorno == "ok"){
									alerta(msg);
									navega('titulo_buscar.php');
									var remote = null;	
									remote = window.open('','relatorio_sim','toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20');
									if(remote != null){
										remote.location.href = 'titulos_comprovante.php?titulo='+titulo+'&descricao='+desc;
									}
								}
								else if(retorno == "erro1"){
									alerta("Campos vazios, operação impedida.");
								}
								else if(retorno == "erro2"){
									alerta("Erro ao inserir os dados.");
								}
								else if(retorno == "erro3"){
									alerta("Instrução já comandada hoje.");
								}
								else {
									alerta (retorno);
								}
							}
						});
						$(this).dialog("close")
					},
					"Cancelar": function() {
						$(this).dialog( "close" );
					}
				}
			});
		}
		else if(acao == '12'){ // ALTERAÇÃO DE ENDEREÇO
			$.ajax({
				type: 'GET',
				url: "scripts/titulos_funcoes.php",
				data: "funcao=altinfo&id="+titulo,
				success: function(retorno){
					$("#dialog").html('<div id="dialog-alt" title="Alterar Endereço - Cliente">'+retorno+'</div>');
					$("#dialog-alt").dialog({
						resizable: false,
						height:165,
						width:400,
						modal: true,
						buttons: {
							"Confirmar": function() {
								var endereco = document.getElementById('endereco').value;
								var bairro = document.getElementById('bairro').value;
								var cidade = document.getElementById('cidade').value;
								var uf = document.getElementById('uf').value;
								var cep = document.getElementById('cep').value;
								alerta(endereco+"|"+bairro+"|"+cidade+"|"+uf+"|"+cep);
								/*$.ajax({
									type: 'GET',
									url: "scripts/titulos_funcoes.php",
									data: "funcao=alterinfo&id="+titulo+"&acao="+acao+"&end="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&cep="+cep,
									success: function(retorno){
										if(retorno == "ok"){
											alerta("Alteração de endereço realizada com sucesso!");
											navega('titulo_buscar.php');
											//popup comprovante
										}
										else if(retorno == "erro1"){
											alerta("Campos vazios, operação impedida.");
										}
										else if(retorno == "erro2"){
											alerta("Erro ao inserir os dados.");
										}
										else if(retorno == "erro3"){
											alerta("Instrução já comandada hoje.");
										}
										else {
											alerta (retorno);
										}
									}
								});*/
								$(this).dialog("close");
							},
							"Cancelar": function() {
								$(this).dialog( "close" );
							}
						}
					});
				}
			});
		}
	}
	// IMPRIMIR BOLETO
	function boleto(cod,seq,quant) {
		$.ajax({
			type: 'GET',
			url: "scripts/boletos_funcoes.php",
			data: "id="+cod,
			success: function(retorno){
				var remote = null
				remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
				if (remote != null) {				
					remote.location.href = retorno+'?titulo='+cod+'&seq='+seq+'&quant='+quant;
				}
			}
		});
	}
	function boleto3vias(cod,seq,quant,clien) {
		$.ajax({
			type: 'GET',
			url: "scripts/boletos_funcoes.php",
			data: "id="+cod,
			success: function(retorno){
				var remote = null
				remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=700,height=300,left=20,top=20')
				if (remote != null) {				
					remote.location.href = retorno+'?titulo='+cod+'&seq='+seq+'&quant='+quant+'&cliente='+clien;
				}
			}
		});
	}
	function boletocarne(cod,seq,quant,sac) {
		$.ajax({
			type: 'GET',
			url: "scripts/boletos_funcoes.php",
			data: "id="+cod,
			success: function(retorno){
				var remote = null
				remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=850,height=400,left=20,top=20')
				if (remote != null) {				
					remote.location.href = retorno+'?titulo='+cod+'&seq='+seq+'&quant='+quant+'&sac='+sac;
				}
			}
		});
	}
	function boletocarnetudo(cod,seq,quant,sac,doc) {
		$("#dialog").html('<div id="dialog-alt" title="Impressão de Boleto"><p><b>Deseja imprimir o carnê completo?</b></div>');
			$("#dialog-alt").dialog({
				resizable: false,
				height:100,
				width:300,
				modal: true,
				buttons: {
					"Imprimir carnê": function(){
						$(this).dialog( "close" );
						window.open('scripts/gera_boletos.php?doc='+doc+'&quant='+quant+'&sac='+sac,'_blank');
						/*$.ajax({
							type: 'GET',
							url: "scripts/gera_boletos.php",
							data: "doc="+doc+"&sac="+sac,
							success: function(retorno){
								alerta(retorno);*/
								
							/*type: 'GET',
							url: "scripts/boletos_funcoes.php",
							data: "id="+cod,
							success: function(retorno){
								var remote = null
								remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=850,height=400,left=20,top=20')
								if (remote != null) {				
									remote.location.href = retorno+'?titulo='+cod+'&seq='+seq+'&quant='+quant+'&sac='+sac;
								}*/
							/*}
						});*/
					},
					"Imprimir folha": function(){
						$(this).dialog( "close" );
						$.ajax({
							type: 'GET',
							url: "scripts/boletos_funcoes.php",
							data: "id="+cod,
							success: function(retorno){
								var remote = null
								remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=850,height=400,left=20,top=20')
								if (remote != null) {				
									remote.location.href = retorno+'?titulo='+cod+'&seq='+seq+'&quant='+quant+'&sac='+sac;
								}
							}
						});
					},
					"Cancelar": function(){
						$(this).dialog( "close" );
					}
				}
			});
		/*$.ajax({
			type: 'GET',
			url: "scripts/boletos_funcoes.php",
			data: "id="+cod,
			success: function(retorno){
				var remote = null
				remote = window.open('','fatura_'+cod,'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=850,height=400,left=20,top=20')
				if (remote != null) {				
					remote.location.href = retorno+'?titulo='+cod+'&seq='+seq+'&quant='+quant+'&sac='+sac;
				}
			}
		});*/
	}
</script>
<div class="titulo">
    <h2>TÍtulos</h2>
       	<a href="javascript:navega('../principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
   	  <fieldset class="busca">
   	    <legend>Buscar TÍtulos</legend>
    		<form id="busca_cliente" name="busca_cliente" method="post" action="" target="busca">
            	<table>
                	<tr>
                    	<td width="180"><label for="buscar">Pesquisar <span style="font-size:10px;">(cliente ou CPF/CNPJ)</span>:</label></td>
                        <td><input class="largo" name="buscar" type="text" id="buscar" size="50" /></td>
                        <td width="160">
                        	<input class="bdesab top dir" type="button" name="pesq" id="pesq" value="Pesquisar" />
                            <input class="botao top dir" type="button" name="filtros" id="filtros" value="Filtros" />
                       </td>
                	</tr>
                    <tr id="filt">
                    	<td colspan="3">                
                            <input style="display:none" type="radio" name="radio_filtro" value="nenhum" id="radio_filtro_5" checked="checked" />
                          	<label style="float:right">
                                <input type="radio" name="radio_filtro" value="cancelados" id="radio_filtro_4" />
                                Cancelados
                           	</label>                           
                          	<label style="float:right">
                                <input type="radio" name="radio_filtro" value="baixados" id="radio_filtro_3" />
                                Baixados&nbsp;&nbsp;
                            </label>                           
                          	<label style="float:right">
                                <input type="radio" name="radio_filtro" value="liquidados" id="radio_filtro_2" />
                                Liquidados&nbsp;&nbsp;
                           	</label>
            	  			<label style="float:right">
            	    			<input type="radio" name="radio_filtro" value="aberto" id="radio_filtro_1" />
           	      				Em Aberto&nbsp;&nbsp;
                           	</label>
                        	<label style="float:right">
                            	<input type="radio" name="radio_filtro" value="n_confirmados" id="radio_filtro_0" />
            	    			Não Confirmados&nbsp;&nbsp;
                          	</label>   
                    	</td>
                   	</tr>
              	</table>
	      </form>
        <div id="busca">
        	<table id="lista">
            	<tr class="cinza">
                	<td width="2%" class="destaque borda centro"><input type="checkbox" id="chk_boxes"/><!--<a href="#" class="tip_trigger"><a href="#" title="Selecionar todos"><input type="checkbox" class="chk_boxes"/></a><span class="tip" style="top: 400px; left: 500px; display: none; ">Selecionar tudo</span></a>--></td>
                    <td width="4%" class="destaque borda centro">ID</td>
                    <td width="16%" class="destaque borda centro">CLIENTE</td>
                    <td width="12%" class="destaque borda centro">CPF/CNPJ</td>
                    <td width="7%" class="destaque borda centro">DOCUMENTO</td>
                    <td width="7%" class="destaque borda centro">NOSSO NUMERO</td>
                    <td width="6%" class="destaque borda centro">EMISSÃO</td>
                    <td width="6%" class="destaque borda centro">VENCIMENTO</td>
                    <td width="7%" class="destaque borda centro">VALOR</td>
                    <td width="4%" class="destaque borda centro">DESC.</td>
                    <td width="11%" class="destaque borda centro">AÇÕES</td>
                    <td width="18%" class="destaque borda centro">INSTRUÇÕES</td>
                </tr>
                <?php 
					include('../config.php');
					$agencia = $_SESSION['userAgencia'];
					$cliente = $_SESSION['userCliente'];
					$conta = $_SESSION['userConta'];
					$master = $_SESSION['userMaster'];
					mysql_query("SET NAMES UTF8") or die(mysql_error());
					$cont = 0;
					$busca  = "SELECT scd.nome, scd.cpf, tit.documento, tit.sequencia, tit.nossonumero, tit.agencia, tit.cad_completo, tit.titulo, ";
					$busca .= "DATE_FORMAT(tit.data_emisao, '%d/%m/%Y') AS data_emisao, DATE_FORMAT(tit.data_venc, '%d/%m/%Y') AS data_venc, ";
					$busca .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor, tit.criacao, tit.so_desconto, ";
					$busca .= "tit.devolucao, tit.data_baixa, tit.registro, tit.status, tit.sacado, tit.modelo ";
					$busca .= "FROM titulos AS tit ";
					$busca .= "INNER JOIN sacados AS scd ON scd.sacado=tit.sacado ";
					$busca .= "WHERE tit.cliente = '".$cliente."' ";
					$busca .= "AND (scd.grupo <> '99999' OR scd.grupo IS NULL) ";
					$busca .= "AND cancelamento IS NULL ";
					$busca .= "ORDER BY tit.criacao DESC, scd.nome, tit.documento, tit.sequencia ASC LIMIT 25";
					$query = mysql_query($busca) or die(mysql_error());
					while($result = mysql_fetch_array($query)){
						if($result["cad_completo"]!="S"){
							$check = '<input type="checkbox" class="chk_boxes1" name="box[]" value="'.$result['titulo'].'">';
						}
						else {
							$check = '';
						}
						$venc = explode('/',$result['data_venc']);
						$venc = $venc[2].'-'.$venc[1].'-'.$venc[0];
						if($venc > date('Y-m-d')){
							$venc = 'S';
						}
						else{
							$venc = 'N';
						}
						echo '
						<tr>
							<td class="borda centro">'.$check.'</td>
							<td class="borda centro">'.$result['titulo'].'</td>
							<td class="borda">&nbsp;&nbsp;<span style="font-size:10px;">'.$result['nome'].'</span></td>
							<td class="borda centro">'.$result['cpf'].'</td>
							<td class="borda centro">'.$result['documento']."/".$result['sequencia'].'</td>
							<td class="borda centro">'.$result['nossonumero'].'</td>
							<td class="borda centro">'.$result['data_emisao'].'</td>
							<td class="borda centro">'.$result['data_venc'].'</td>
							<td class="borda" style="text-align: right">R$ '.$result['valor'].'&nbsp;&nbsp;</td>
							<td class="borda centro">'.$result['so_desconto'].'</td>
							<td class="borda centro">';
							if($result["cad_completo"]!="S" && $result["status"]!="03"){
								echo '
									<a href="javascript:excluir('.$result['titulo'].');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;
								';
								if($agencia == '4030'){
									echo '
									<a href="javascript:navega(\'4030/titulo_alterar.php?titulo='.$result['titulo'].'\');" title="Alterar"><img src="img/editar.png" /></a>
									';
								}
								else{
									echo '
									<a href="javascript:navega(\'titulo_alterar.php?titulo='.$result['titulo'].'\');" title="Alterar"><img src="img/editar.png" /></a>
									';
								}
								echo '
									&nbsp;
									<a href="javascript:confirma('.$result['titulo'].');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<select name="instrucoes" disabled="disabled">
										<option value="0">Selecione a Instrução</option>
									</select>
								</td>';
							}
							else if($result["cad_completo"]!="S" && $result["status"]=="03" && $result["so_desconto"]=="S"){
								echo '
									<a href="javascript:navega(\'titulo_alterar.php?titulo='.$result['titulo'].'\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma('.$result['titulo'].');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<b><font color="#F00">Título Devolvido</font></b>
								</td>';
							}
							else if($result["cad_completo"]!="S" && $result["status"]=="03" && $result["so_desconto"]=="N"){
								echo '
									<a href="javascript:excluir('.$result['titulo'].');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;
									<a href="javascript:navega(\'titulo_alterar.php?titulo='.$result['titulo'].'\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma('.$result['titulo'].');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<b><font color="#F00">Título Devolvido</font></b>
								</td>';
							}
							else if($result["cad_completo"]=="S" && $result["devolucao"]==NULL && $result["data_baixa"]==NULL && $result["registro"]!=NULL && $result['so_desconto']=='N') {								
								$titulo = $result['titulo'];
								$seq = $result['sequencia'];
								$doc = $result['documento'];
								$sac = $result['sacado'];
								$modelo = $result['modelo'];
								$link = '../boletophp/';
								//define o arquivo de acordo com o modelo
								if($modelo == '1'){// boleto normal
									$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
									$qt = mysql_num_rows($qQtd);
									$base = 'onclick="boleto('.$titulo.','.$seq.','.$qt.')"';
								}
								else if($modelo == '2'){// boleto 3 vias
									$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
									$qt = mysql_num_rows($qQtd);
									$base = 'onclick="boleto3vias('.$titulo.','.$seq.','.$qt.','.$cliente.')"';
								}
								else if($modelo == '3'){//carne
									$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
									$qt = mysql_num_rows($qQtd);
									$base = 'onclick="boletocarne('.$titulo.','.$seq.','.$qt.','.$sac.')"';
								}
								echo '
									<img src="img/ok.png" />
									&nbsp;
									<img src="img/boleto.png" title="Imprimir Boleto" style="cursor: pointer;" '.$base.' />
								</td>
								<td class="borda centro">
									<select name="instrucoes" id="'.$result['titulo'].'" onchange="javascript: instrucao('.$result['titulo'].');">
										<option value="0">Selecione a Instrução</option>
										<option value="06">Alteração de Vencimento</option>
										<option value="10">Cancel.Sustação Instr. Pr</option>
										<option value="04">Concessão de Abatimento</option>
										<option value="31">Concessão de Desconto</option>
										<option value="35">Multa</option>
										<option value="09">Protestar</option>
										<option value="02">Solicitação de Baixa</option>
										<option value="05">Cancelamento Abatimento</option>
										<option value="32">Cancelamento Desconto</option>
									</select>
								</td>';
								//<option value="12">Alteração Endereço(Cliente)</option>;
							}
							else if($result["cad_completo"]=="S" && $result["devolucao"]==NULL && $result["data_baixa"]==NULL && $result["registro"]!=NULL && $result['so_desconto']=='S'){
									$titulo = $result['titulo'];
									$seq = $result['sequencia'];
									$doc = $result['documento'];
									$sac = $result['sacado'];
									$modelo = $result['modelo'];
									$link = '../boletophp/';
									//define o arquivo de acordo com o modelo
									if($modelo == '1'){// boleto normal
										$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
										$qt = mysql_num_rows($qQtd);
										$base = 'onclick="boleto('.$titulo.','.$seq.','.$qt.')"';
									}
									else if($modelo == '2'){// boleto 3 vias
										$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
										$qt = mysql_num_rows($qQtd);
										$base = 'onclick="boleto3vias('.$titulo.','.$seq.','.$qt.','.$cliente.')"';
									}
									else if($modelo == '3'){//carne
										if($seq == 1){
											$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
											$qt = mysql_num_rows($qQtd);
											$base = 'onclick="boletocarnetudo('.$titulo.','.$seq.','.$qt.','.$sac.','.$doc.')"';
										}
										else {
											$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='$doc' AND sacado='$sac'") or die(mysql_error());
											$qt = mysql_num_rows($qQtd);
											$base = 'onclick="boletocarne('.$titulo.','.$seq.','.$qt.','.$sac.')"';
										}
									}
									echo '
										<img src="img/ok.png" />
										&nbsp;
										<img src="img/boleto.png" title="Imprimir Boleto" style="cursor: pointer;" '.$base.' />
									</td>
									<td class="borda centro">
									';
									if($master == true){
										echo '
										<select name="instrucoes" id="'.$result['titulo'].'" onchange="javascript: instrucao('.$result['titulo'].');">
											<option value="0">Selecione a Instrução</option>
											<option value="06">Alteração de Vencimento</option>
											<option value="10">Cancel.Sustação Instr. Pr</option>
											<option value="04">Concessão de Abatimento</option>
											<option value="31">Concessão de Desconto</option>
											<option value="35">Multa</option>
											<option value="09">Protestar</option>
											<option value="02">Solicitação de Baixa</option>
											<option value="05">Cancelamento Abatimento</option>
											<option value="32">Cancelamento Desconto</option>
										</select>
										';
									}
									else{
										echo '
										<select name="instrucoes" disabled="disabled">
											<option value="0">Selecione a Instrução</option>
										</select>
										';
									}
									echo '</td>';
								}
							else {
								echo '
									<img src="img/ok.png" />
								</td>
								<td class="borda centro">
									<select name="instrucoes" disabled="disabled">
										<option value="0">Selecione a Instrução</option>
									</select>
								</td>';
							}
						echo'							
						</tr>
						';
					}			
				?>
            </table>
       	</div>
   	  	</fieldset>
        <div class="acao">
            <label for="acao"><b>Selecionados:</b></label>
            <select name="acao" id="acao">
              <option>Selecione uma ação</option>
              <option value="excluir">Excluir</option>
              <option value="confirmar">Confirmar</option>
</select>
      	</div>
   	  <br class="clear" />
  </div>
</div>