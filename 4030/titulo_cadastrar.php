<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	include('../config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: ../login_mini.php');
	}
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
	$data_base = date("d/m/Y");
	mysql_query("SET NAMES UTF8") or die(mysql_error());
?>
<div id="section" class="sect">
<script type="text/javascript">
	$(document).ready(function () {
		var data_base = '<?php echo $data_base; ?>';
		$('#valor').blur(function(){
			var valor = document.getElementById('valor').value;
			var boletos = document.getElementById('qtd_boletos').value;
			valor = valor.replace('.','');
			valor = valor.replace(',','.');
			var parcelas = valor/boletos;
			if(parcelas == 'Infinity'){
				var parcelas = '';
			}
			else {
				var parcelas = parcelas;
			}
			$('#fatura').attr('value',parcelas);
		});
		$('#qtd_boletos').blur(function(){
			var boletos = document.getElementById('qtd_boletos').value;
			if(boletos == ''){
				$('#qtd_boletos').attr('value','1')
			}
		});
		// $('#modalidade_boleto').change(function(){
		// 	if($(this).is(':checked')){
		// 		$('#protesto').attr('value','5');
		// 		$('#protesto').attr('disabled','disabled');
		// 		$('#dias_protesto_1').attr('checked','checked');
		// 		$('#dias_protesto_0').attr('disabled','disabled');
		// 		$('#dias_protesto_1').attr('disabled','disabled');
		// 		$('#juros').attr('value','7,50');
		// 		$('#juros').attr('disabled','disabled');
		// 		$('#qtd_boletos').attr('value','1');
		// 		$('#qtd_boletos').attr('disabled','disabled');
		// 		$('#intervalo').attr('value','1');
		// 		$('#intervalo').attr('disabled','disabled');
		// 		$('#multa').attr('disabled','disabled');
		// 		$('#multa').val('0,00');
		// 	}
		// 	else{
		// 		$('#protesto').removeAttr('value');
		// 		$('#protesto').removeAttr('disabled');
		// 		$('#dias_protesto_0').removeAttr('disabled');
		// 		$('#dias_protesto_1').removeAttr('disabled');
		// 		$('#juros').removeAttr('value');
		// 		$('#juros').removeAttr('disabled');
		// 		$('#qtd_boletos').removeAttr('disabled');
		// 		$('#intervalo').removeAttr('value');
		// 		$('#intervalo').removeAttr('disabled');
		// 		$('#multa').removeAttr('disabled');
		// 	}
		// });
		$('#qtd_boletos').blur(function(){
			if($('#qtd_boletos').val() == "1"){
				$('#intervalo').attr('value','1');
				$('#intervalo').attr('disabled','disabled');
			}
			else {
				$('#intervalo').removeAttr('value');
				$('#intervalo').removeAttr('disabled');
			}
		});
		$('#desconto').blur(function(){
			if($('#desconto').val() < "0,01"){
				$('#data_desconto').attr('disabled','disabled');
				$('#data_desconto').removeAttr('onblur');
			}
			else {
				$('#data_desconto').removeAttr('value');
				$('#data_desconto').removeAttr('disabled');
				$('#data_desconto').attr('onblur','verificaDesc(this.value)');
			}
		});
		$('#limpar').click(function(){
			$('#protesto').removeAttr('value');
			$('#protesto').removeAttr('disabled');
			$('#dias_protesto_0').removeAttr('disabled');
			$('#dias_protesto_1').removeAttr('disabled');
			$('#juros').removeAttr('value');
			$('#juros').removeAttr('disabled');
		});
		$("#novo").click(function(){
			$("#cadastra_titulo")[0].reset();
			$("#cadastra_titulo input").prop('disabled', false);
			$("#cadastra_titulo select").prop('disabled', false);
			$("#cadastra_titulo #novo").attr('class','oculto');
			$("#cadastra_titulo #fatura").attr('disabled','disabled');
			$("#cadastra_titulo #data_emissao").attr('disabled','disabled');
			$("#cadastra_titulo #data_emissao").attr('value',data_base);
			$('#qtd_boletos').removeAttr('value');
			$('#intervalo').removeAttr('value');
			$('#fatura').removeAttr('value');
			$('#protesto').removeAttr('value');
			$('#juros').removeAttr('value');
		});
	});
	function prepara_gerar(){
		var documento = document.getElementById('documento').value;
		var boletos = document.getElementById('qtd_boletos').value;var vencimento = document.getElementById('venc').value;
		var intervalo = document.getElementById('intervalo').value;
		var protesto = document.getElementById('protesto').value;
		var total = document.getElementById('valor').value;
		var multa = document.getElementById('multa').value;
		var juros = document.getElementById('juros').value;
		var desconto = document.getElementById('desconto').value;
		var datadesconto = document.getElementById('data_desconto').value;
		var tdoc = documento.length;
		var tbol = boletos.length;
		var tvenc = vencimento.length;
		var tinter = intervalo.length;
		var tpro = protesto.length;
		var ttot = total.length;
		var tdata = datadesconto.length; 
		if(document.cadastra_titulo.cliente.value=="0"){
			alerta("Favor selecionar o cliente.");
		}
		else if (document.cadastra_titulo.sacador.value=="0"){
			alerta("Favor selecionar o sacador/avalista.");
		}
		else if (document.cadastra_titulo.qtd_boletos.value<1){
			alerta("A quantidade de boletos não pode ser menor que 1.");
		}
		else if (document.cadastra_titulo.valor.value<1){
			alerta("O valor total dos boletos não pode ser menor que 1.");
		}
		else if(tdoc < 1){
			alerta("Campo documento inválido. Mínimo 1 caracteres.");
		}
		else if(tbol < 1){
			alerta("Campo boleto é obrigatório.");
		}
		else if(tvenc < 10){
			alerta("Data de vencimento inválida.");
		}
		else if(tinter < 1){
			alerta("Campo intervalo é obrigatório.");
		}
		/*else if(tpro < 1){
			alerta("Campo dias para protesto é obrigatório.");
		}*/
		else if(ttot < 4){
			alerta("Campo valor total é obrigatório.");
		}
		else if(desconto != "" && tdata < 10){
			alerta("Campo data limite do desconto é obrigatório.");
		}
		else {
			if(document.cadastra_titulo.modalidade_boleto.checked){
				$.ajax({ 
					type: 'GET',
					url: "scripts/titulos_funcoes.php",
					data: "funcao=verificarHora&agencia=<?php echo $agencia;?>",
					success: function(retorno){
						if(retorno == "1"){
							alerta("Horário para cadastro de títulos para desconto encerrado.");
						}
						else if(retorno == "ok"){ // SÓ DESCONTO - HORARIO OK
							$.ajax({
								type: 'GET',
								url: "scripts/titulos_funcoes.php",
								data: "funcao=verificarDocumento&documento="+zero(document.cadastra_titulo.documento.value,8)+"&agencia=<?php echo $agencia;?>&cliente=<?php echo $cliente;?>",
								success: function(retorno1){
									if(retorno1 == "1"){
										alerta("Número do documento já existente. Cadastre outro número.");
									}
									else if (retorno1 == "ok"){
										cad_titulo();
									}
								}
							});
						}
					}
				});
			}
			else {
				$.ajax({
					type: 'GET',
					url: "scripts/titulos_funcoes.php",
					data: "funcao=verificarDocumento&documento="+zero(document.cadastra_titulo.documento.value,8)+"&agencia=<?php echo $agencia;?>&cliente=<?php echo $cliente;?>",
					success: function(retorno1){
						if(retorno1 == "1"){
							alerta("Número do documento já existente. Cadastre outro número.");
						}
						else if (retorno1 == "ok"){
							cad_titulo();
						}
					}
				});
			}
		}
	}
	function cad_titulo(){
		var documento = document.getElementById('documento').value;
		var qtd_boletos = document.getElementById('qtd_boletos').value;
		var vencimento = document.getElementById('venc').value;
		var intervalo = document.getElementById('intervalo').value;
		var valor = document.getElementById('valor').value;
		var desconto = document.getElementById('desconto').value;
				
		$.ajax({
			type: "GET",
			url: "scripts/titulos_funcoes.php",
			data: "funcao=cad_titulo&documento="+documento+"&qtd_boletos="+qtd_boletos+"&venc="+vencimento+"&intervalo="+intervalo+"&valor="+valor+"&desconto="+desconto,
			success: function(retorno){
				$('#dialog').html('<div id="dialog-confirm" title="Cadastrar títulos?" style="display:none">'+retorno+'</div>');
				// cria janela de confirmação
				$( "#dialog-confirm" ).dialog({
					resizable: false,
					height:200,
					width: 585,
					modal: true,
					buttons: {
						"Cadastrar": function() {
							reg_titulo();	
							$(this).dialog("close");
						},
						"Editar": function() {
							$(this).dialog("close");
						},
						"Cancelar": function() {
							$(this).dialog("close");
							navega('principal.php');
						}
					}
				});
			}
		});		
	}
	function reg_titulo(){
		var cliente = document.getElementById('cliente').value;
		var sacador = document.getElementById('sacador').value;
		var modelo_boleto = document.getElementById('modelo_boleto').value;
		var documento = document.getElementById('documento').value;
		var qtd_boletos = document.getElementById('qtd_boletos').value;
		var data_emissao = document.getElementById('data_emissao').value;
		var vencimento = document.getElementById('venc').value;
		var intervalo = document.getElementById('intervalo').value;
		var protesto = document.getElementById('protesto').value;
		var valor = document.getElementById('valor').value;
		var multa = document.getElementById('multa').value;
		var desconto = document.getElementById('desconto').value;	
		var datadesconto = document.getElementById('data_desconto').value;	
		var juros = document.getElementById('juros').value;
		var modalidade_boleto = document.getElementById('modalidade_boleto');
		if (modalidade_boleto.checked){
			var modalidade_boleto = document.getElementById('modalidade_boleto').value;
		}
		else {
			var modalidade_boleto = "N";
		}
		var dias_protesto = $('[name=dias_protesto]:checked').val();
						
		$.ajax({
			type: "GET",
			url: "scripts/titulos_funcoes.php",
			data: "funcao=reg_titulo&cliente="+cliente+"&sacador="+sacador+"&modelo_boleto="+modelo_boleto+"&modalidade_boleto="+modalidade_boleto+"&documento="+documento+"&qtd_boletos="+qtd_boletos+"&data_emissao="+data_emissao+"&venc="+vencimento+"&intervalo="+intervalo+"&protesto="+protesto+"&dias_protesto="+dias_protesto+"&valor="+valor+"&multa="+multa+"&desconto="+desconto+"&data_desconto="+datadesconto+"&juros="+juros,
			success: function(retorno){
				if(retorno == 'boleto'){
					alerta('Este número de documento já existe!');
				}
				else if(retorno == 'ok'){
					alerta('Faturas cadastradas com sucesso!');
					$("#cadastra_titulo #documento").val('');
					$("#cadastra_titulo #modalidade_boleto").removeAttr('checked');
					$("#cadastra_titulo #venc").val('');
					$("#cadastra_titulo #valor").val('');
					$("#cadastra_titulo #desconto").val('');
					$("#cadastra_titulo #data_desconto").val('');
					$("#cadastra_titulo #qtd_boletos").removeAttr('disabled');
					$("#cadastra_titulo #intervalo").removeAttr('disabled');
					$("#cadastra_titulo #protesto").removeAttr('disabled');
					$("#cadastra_titulo #multa").removeAttr('disabled');
					$("#cadastra_titulo #juros").removeAttr('disabled');
					$("#cadastra_titulo #dias_protesto_0").removeAttr('disabled');
					$("#cadastra_titulo #dias_protesto_1").removeAttr('disabled');
					$("#cadastra_titulo #data_desconto").removeAttr('disabled');
					$("#cadastra_titulo #fatura").attr('value','');
				}
				else if(retorno == 'erro'){
					alerta('Erro ao cadastrar.');
				}
				else{
					alerta(retorno);
				}
			}
		});
	}
</script>
<div id="dialog"></div>
	<div class="titulo">
        <h2>TÍTULOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="cadastra_titulo" name="cadastra_titulo" method="post">
  		<fieldset>
    	  <legend>Cadastrar  TÍTULOS
    	  </legend>
    	  <table>
    	    <tr>
    	      <td colspan="2">Cliente:&nbsp;&nbsp;&nbsp;
    	        <select name="cliente" id="cliente">
    	          <option value="0">Selecione um sacado</option>
    	          <?php
						$sql = "SELECT nome, sacado FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND (grupo <> '99999' or grupo is null) ORDER BY UPPER(nome)";
						$query = mysql_query($sql) or die(mysql_error());
						while($linha = mysql_fetch_array($query)){
							echo '
								<option value="'.$linha['sacado'].'">'.$linha['nome'].'</option>
							';
						}
					?>
  	          </select></td>
    	      <td colspan="2">Sacador/Avalista:&nbsp;&nbsp;&nbsp;
    	        <select name="sacador" id="sacador">
    	          <option value="0">Selecione...</option>
    	          <?php
					$sql2 = "SELECT nome, sacado FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND grupo='99999' ORDER BY UPPER(nome)";
					$query2 = mysql_query($sql2) or die(mysql_error());
					while($linha2 = mysql_fetch_array($query2)){
						echo '
							<option value="'.$linha2['sacado'].'">'.$linha2['nome'].'</option>
						';
					}
				?>
  	          </select></td>
  	      </tr>
    	    <tr>
    	      <td width="26%">
              	<?php
					$qPara = "SELECT boleto_padrao FROM clientes WHERE cliente='$cliente'";
					$sPara = mysql_query($qPara) or die(mysql_error());
					$aPara = mysql_fetch_array($sPara);
				?>
              	Modelo Boleto:&nbsp;&nbsp;&nbsp;
    	        <select name="modelo_boleto" id="modelo_boleto">
                	<?php						
						if($aPara['boleto_padrao'] == 1){
							echo "
							<option value='1' selected>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3'>Carnê</option>0
							";
						}
						else if($aPara['boleto_padrao']==2){
							echo "
							<option value='1'>2 Vias</option>
							<option value='2' selected>3 Vias</option>
							<option value='3'>Carnê</option>
							";
						}
						else if($aPara['boleto_padrao']==3){
							echo "
							<option value='1'>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3' selected>Carnê</option>
							";
						}
						else {
							echo "
							<option value='1' selected>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3'>Carnê</option>0
							";
						}
					?>
  	          </select></td>
    	      <td width="27%">Modal. Boleto:&nbsp;&nbsp;&nbsp;
    	        <input type="checkbox" name="modalidade_boleto" id="modalidade_boleto" value="S" />
    	        Exclusivo Desconto</td>
    	      <td width="18%">Documento:&nbsp;&nbsp;&nbsp;
    	        <input name="documento" type="text" id="documento" size="6" maxlength="13" /></td>
    	      <td width="34%">Qtd. Boletos:&nbsp;&nbsp;&nbsp;
    	        <input name="qtd_boletos" type="text" id="qtd_boletos" value="1" size="1" maxlength="2" readonly /></td>
  	      </tr>
    	    <tr>
    	      <td>Data Emissão:&nbsp;&nbsp;&nbsp;
    	        <input name="data_emissao" type="text" id="data_emissao" value="<?php echo $data_base; ?>" size="8" maxlength="10" readonly /></td>
    	      <td>Vencimento:&nbsp;&nbsp;&nbsp;
    	        <input name="venc" type="text" id="venc" size="8" maxlength="10" onkeypress="formataCampo(this, '00/00/0000', event)" onblur="verificaVenc(this.value)" /></td>
    	      <td>Intervalo:&nbsp;&nbsp;&nbsp;
    	        <input name="intervalo" type="text" id="intervalo" size="1" maxlength="2" /></td>
    	      <td>Dias protesto:&nbsp;&nbsp;&nbsp;
    	        <input name="protesto" type="text" id="protesto" style="width:20px" size="1" maxlength="2" />
    	        <label>
    	          <input type="radio" name="dias_protesto" value="corridos" id="dias_protesto_0" />
    	          Corridos</label>
    	        <label>
    	          <input name="dias_protesto" type="radio" id="dias_protesto_1" value="uteis" checked="checked" />
    	          Úteis</label></td>
  	      </tr>
    	    <tr>
    	      <td>Valor Total<span style="font-size:10px;">(R$)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="valor" type="text" id="valor" size="7" maxlength="25" onkeydown="FormataValor(this,event,17,2);" /></td>
    	      <td>Valor Faturas<span style="font-size:10px;">(R$)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="fatura" type="text" disabled="disabled" id="fatura" size="7" maxlength="25" /></td>
    	      <td>Multa<span style="font-size:10px;">(%)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="multa" type="text" id="multa" size="3" maxlength="5" onkeydown="FormataValor(this,event,5,2);" /></td>
    	      <td><label for="juros">Juros mora Mensal<span style="font-size:10px;">(%)</span>:&nbsp;&nbsp;&nbsp;</label>
    	        <input name="juros" type="text" id="juros" size="3" maxlength="5" onkeydown="FormataValor(this,event,5,2);" /></td>
  	      </tr>
    	    <tr>
    	      <td>Valor Desconto<span style="font-size:10px;">(R$)</span>:&nbsp;&nbsp;&nbsp;
    	        <input name="desconto" type="text" id="desconto" size="10" maxlength="10" onkeydown="FormataValor(this,event,17,2);" /></td>
    	      <td>Data Lim. Desconto:&nbsp;&nbsp;&nbsp;
    	        <input name="data_desconto" type="text" id="data_desconto" onblur="verificaDesc(this.value)" onkeypress="formataCampo(this, '00/00/0000', event)" size="10" maxlength="10" /></td>
    	      <td colspan="2">Instruções:&nbsp;&nbsp;&nbsp;
    	        <label for="textarea"></label>
    	        <textarea name="textarea" cols="35" rows="2" id="textarea" style="resize:none;"></textarea></td>
  	      </tr>
  	    </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:prepara_gerar()">Gerar</a>
        <input class="btn botao margins dir" type="reset" name="limpar" id="limpar" value="Limpar" />
        <input class="oculto" type="button" name="novo" id="novo" value="Novo" onclick="" />
        <br class="clear" />
	  </form>
    </div>
</div>