<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="dialog-confirm" title="Excluir cliente?" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Este cliente será excluído permanentemente. Deseja continuar?</p>
</div>
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready(function(){
    	$('#section').mede();
		$("#buscar").keypress(function(e) {
			if(e.which == 13) {
				event.preventDefault();
				busca();
			}
		});
	});	
	function busca(){
		var busca = $("#buscar").val();
		$.ajax({
			type: 'GET',
			url: "scripts/clientes_funcoes.php",
			data: "funcao=busca&filtro="+busca,
			//beforeSend: function() {
				//$("#busca").html('<center><img src="img/loader.gif" width="64" height="64"></center>');
			//},
			success: function(retorno){
				$("#busca").html(retorno);
			}
		});	
	}
	function excluir(sacado){
		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Excluir": function() {
					$.ajax({
						type: 'GET',
						url: "scripts/clientes_funcoes.php",
						data: "funcao=excluir&sacado="+sacado,
						success: function(retorno){
							if(retorno == "ok"){
								alerta("Exclusão realizada com sucesso!");
								navega('cliente_buscar.php');
							}
							else {
								alerta ("Erro ao excluir.");
							}
						}					
					});
					$(this).dialog("close");
				},
				"Cancelar": function() {
					$(this).dialog( "close" );
				}
			}
		});
	}
</script>
	<div class="titulo">
        <h2>Clientes</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
   	  <fieldset class="busca">
   	    <legend>Buscar Clientes</legend>
    		<form id="busca_cliente" name="busca_cliente" method="post" action="" target="busca">
            	<table>
                	<tr>
                    	<td width="180"><label for="buscar">Pesquisar <span style="font-size:10px;">(cliente ou CPF/CNPJ)</span>:</label></td>
                        <td><input class="largo" name="buscar" type="text" id="buscar" size="50" /></td>
                        <!--<td><input class="largo" name="buscar" type="text" id="buscar" size="50"  onKeyUp="busca(this.value)" /></td>-->
                        <td width="80"><input class="botao top dir" type="button" name="pesq" id="pesq" onclick="busca(this.value)" value="Pesquisar"/></td>
                	</tr>
              	</table>
   		  </form>
        <div id="busca">
        	<table>
            	<tr class="cinza">
                	<td class="destaque borda">NOME</td>
                    <td width="25%" class="destaque borda">CPF/CNPJ</td>
                    <td width="10%" class="destaque borda">	AÇÕES</td>
                </tr>
                <?php
					include('config.php');
					$agencia = $_SESSION['userAgencia'];
					$cliente = $_SESSION['userCliente'];
					$conta = $_SESSION['userConta'];
					$sql = "SELECT nome, cpf, sacado FROM sacados WHERE agencia='$agencia' AND conta LIKE '%$conta' AND (grupo <> '99999' or grupo is null) ORDER BY UPPER(nome)";
					$query = mysql_query ($sql) or die(mysql_error());
					while($linha = mysql_fetch_array($query)){
						echo '
							<tr>
								<td class="borda">'.$linha['nome'].'</td>
								<td class="borda">'.$linha['cpf'].'</td>
								<td class="borda centro">
						';
						if($_SESSION['userMaster'] == true){
							echo '
									<a href="javascript:excluir('.$linha['sacado'].');" title="Excluir"><img src="img/cancelar.png" /></a>&nbsp;&nbsp;';
						}
						echo '
									<a href="javascript:navega(\'cliente_alterar.php?sacado='.$linha['sacado'].'\');" title="Alterar"><img src="img/editar.png" /></a>
								</td>
							</tr>
						';
					}				
				?>
            </table>
        </div>
   	  </fieldset>
    </div>
</div>