<?php 
session_start(); 
header('Content-Type: text/html; charset=latin1');
//error_reporting(0);
if(!isset($_SESSION['userAgencia']) || !isset($_SESSION['userAteCodigo']) ){
    header("Location: http://sisacob.com.br/index.php");
}
?>

<html>
    <head>
        <title>Extrato</title>
        <style>
            *{
                margin:0px;
            }
            .pag{
                    margin-left: 10px;
                    margin-bottom: 3px;
                    float: left;
                    width: 17px;
                    text-align: center;
                    padding: 5px;
                    border: 1px solid #5E755A;
                    background: #709e69;
                    border-radius: 3px;
                    transition: 0.5s;
                    text-decoration: none;
                    color: white;
                    cursor: pointer;
            }
            .pag a{
                text-decoration: none;
                color: white;
            }
            .pag:hover{
                transform: scale(1.2);
            }
            .invisible{
                display: none;
            }
            .visible{
                display: block;
            }
            
            #antpaginas{
                 margin:5px auto;
                 width: 500px;
                 height: 46px;
                padding: 1px;
                border-radius: 3px;
                border-bottom: 1px solid #B3B3B3;
            }

            #formdata{
                position: absolute;
                background: rgba(0, 0, 0, 0.66);
            }

            #formdata>div{
                    width: 190px;
                    min-height: 23px;
                    border: 1px solid black;
                    background: white;
                    margin-top: 188px;
                    border-radius: 10px;
                    margin-left: auto;
                    margin-right: auto;
                    padding: 10px;
                    box-shadow: 1px 1px 5px black;
            }
            #formdata>span{
                color: white;
                float: right;
                margin-right: 13px;
                margin-top: 5px;
                text-shadow: 1px 1px 3px black;
                cursor: pointer;
            }
            
        </style>
        <script src="moment.js" ></script>
        <script>
            window.onload = function(){
                
                total = document.getElementsByClassName('extrato').length;
                
                document.getElementById('close').addEventListener('click',function(){
                    document.getElementById('formdata').className='invisible';
                });
                
                document.getElementsByName('data')[0].value = moment().format('YYYY-MM-DD');
                document.getElementsByName('data')[0].addEventListener('blur',function(){
                    if(this.value == ''){
                        this.value =  moment().format('YYYY-MM-DD');
                    }
                });
                
                
                
                document.getElementById('selData').addEventListener('click',function(){
                    
                    var formData = document.getElementById('formdata');
                    formData.className='visible';
                    
                    
                    formData.style.height = document.body.scrollHeight;
                    formData.style.width = document.body.scrollWidth;
                });
                
                
                if(total <= 0){
                    alert('Voc\u00ea nao possui extrato nesta data!');
                    
                    document.getElementById('close').className='invisible';
                    
                    var formData = document.getElementById('formdata');
                    formData.className='visible';
                    formData.style.height = document.body.scrollHeight;
                    formData.style.width = document.body.scrollWidth;
                    
                    var span = li = document.createElement('span');
                    span.innerHTML = 'Selecione outra data!';
                    span.style.color = 'red';
                    span.id='erro';
                    document.getElementsByName('data')[0].addEventListener('focus',function(){
                        document.getElementById('erro').className='invisible';
                    });
                    
                    document.getElementById('beforeformdata').appendChild(span);
                    //location ='http://sisacob.com.br';
                }else{
                    
                    var extratos = document.getElementsByClassName('extrato');
                    var pagina = document.getElementById('paginasul');
                    
                    document.get
                    
                    
                    var li,link;
                    document.getElementById('ext0').className = 'visible extrato';
                    for(var a=0;extratos.length > a ; a++){
                        li = document.createElement('LI');

                        li.className = 'pag';

                        li.extratoId='ext'+a;
                        li.innerHTML = a+1;
                        li.addEventListener('click', function(){

                            var ext = document.getElementsByClassName('extrato');

                            for(var b= 0; ext.length > b ;b++){

                                ext[b].className = "invisible extrato";
                            }
                            document.getElementById(this.extratoId).className = "visible extrato";
                        });


                       pagina.appendChild(li);
                    }
                }
            };
        </script>
    </head>
    <body>
        <div id="formdata" class="invisible" >
            <span id='close'>X</span>
            <div id='beforeformdata'>
                <form >
                    <input type="date" name="data" />
                    <input type="submit" value="OK"/>
                </form>
            </div>
        </div>
        <div id="antpaginas">
            <div id="paginas">
                <ul id = "paginasul">
                   
                </ul>
            </div>
        </div>
        <div style="margin:2px auto;width: 100px;">
            <form >
                <input id="selData" type="button" value="Selecionar Data">
            </form>    
        </div>
<?php

function getArquivosExtratos($agencia,$ateCodigo,$data){
    $pathExtratos = "arquivos/extratos/";
    $sigla = 'cob';
    $aTemp= [];
    $alfabeto = ['','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','z'];
    
    foreach($alfabeto as $value){
        if(file_exists($pathExtratos.$sigla.$agencia.'_'.$ateCodigo.'_'.$data.$value.'.html')){
            $aTemp[] = $pathExtratos.$sigla.$agencia.'_'.$ateCodigo.'_'.$data.$value.'.html';
            
        }
        //echo $pathExtratos.$sigla.$agencia.'_'.$conta.$value.'.html';
    }
    return $aTemp;
    
}

function formataHTMLExtrato($extrato){
    
    $nFim =  strripos($extrato, '<CENTER>') ;
    // var_dump($nFim);
    $extrato = substr($extrato, 87, $nFim - 87);
    
    return $extrato;
    
}



$agencia = $_SESSION['userAgencia'];
$ateCodigo = str_pad($_SESSION['userAteCodigo'],9,0,STR_PAD_LEFT);
$data;
if( isset($_GET['data']) ){
    if( empty($_GET['data']) ){
        $data = date('dmY');
    }else{
        $data = implode('', array_reverse( explode('-', $_GET['data']) ) );
    }
}else{
    $data = date('dmY');
}

//$agencia = '4117';
//$ateCodigo = '00941-5';

$arquivos = getArquivosExtratos($agencia, $ateCodigo,$data);



$id = 0;
foreach ( $arquivos as $value){
    echo '<div id="ext'.$id.'" class="extrato invisible">';
    echo formataHTMLExtrato(file_get_contents( $value) );
    echo '</div>';
    $id++;
    
    
}
