<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="meio">
<script type="text/javascript">
	$(document).ready( function() {
		$("#cpfcnpj").keyup(function(){
			var tam = $(this).val().length+1;
			if(tam >= 15){
				$("#cpfcnpj").removeAttr('onkeypress');
				$("#cpfcnpj").attr('onkeypress','formataCampo(this, "00.000.000/0000-00", event); return SomenteNumero(event)');
			}
			else {
				$("#cpfcnpj").removeAttr('onkeypress');
				$("#cpfcnpj").attr('onkeypress','formataCampo(this, "000.000.000-00", event); return SomenteNumero(event)');
			}
		});
	});
	function alt_sacador(){
		var sacado = document.getElementById("sacado").value;
		var nome = document.getElementById('nome').value;
		var cpfcnpj = document.getElementById('cpfcnpj').value;
		
		$.ajax({
			type: "GET",
			url: "scripts/sacador_funcoes.php",
			data: "funcao=alt_sacador&sacado="+sacado+"&nome="+nome+"&cpfcnpj="+cpfcnpj,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Alteração realizada com sucesso!");
					navega("sacadoravalista_buscar.php");
				}
				else{
					alerta(retorno);
				}				
			}
		});
	};
	$(document).ready(function () {
		$("#novo").click(function(){
			$("#altera_sacador")[0].reset();
			$("#altera_sacador input").prop('disabled', false);
			$("#altera_sacador #novo").attr('class','oculto');
		});
	});
</script>
<?php 
	include('config.php');
   	$sacado = $_GET['sacado'];
	$sql = "SELECT nome, cpf FROM sacados WHERE sacado='$sacado'";
	$query = mysql_query($sql) or die(mysql_error());
	$linha = mysql_fetch_array($query);
?>
	<div class="titulo">
        <h2>Sacador/Avalista</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="altera_sacador" name="altera_sacador" method="post" action="">
  		<fieldset>
    	  <legend>Alterar Sacador/Avalista
    	  </legend><table>
    	    <tr>
    	      <td width="11%"><label for="nome">Nome:</label></td>
    	      <td width="57%"><input id="sacado" type="hidden" value="<?php echo $sacado; ?>" /><input name="nome" type="text" class="largo" id="nome" maxlength="40" value="<?php echo $linha['nome']; ?>" /></td>
    	      <td width="13%"><label for="cpfcnpj">CPF/CNPJ:</label></td>
    	      <td width="19%"><input name="cpfcnpj" type="text" id="cpfcnpj" size="16" maxlength="18" value="<?php echo $linha['cpf']; ?>" /></td>
  	      </tr>
  	    </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:alt_sacador()">Alterar</a>
  		<!--<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="alt_sacador()" />-->
        <a class="btn botao margins dir" href="javascript:navega('sacadoravalista_buscar.php')">Voltar</a>
        <br class="clear" />
	  </form>
    </div>
</div>