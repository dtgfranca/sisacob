<?php
	include('../config.php');
	include('limpa.php');
	include('auditoria.php');
	session_start();
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	$_GET = sanitize($_GET);
	if(empty($_SESSION['userAgencia'])){
		header('location: login.php?res=1');
	}
	$funcao = $_GET['funcao'];


	//CADASTRO DE SACADOR
	if($funcao == 'cad_sacador'){
		$nome           = $_GET["nome"];
		$cpfcnpj        = $_GET["cpfcnpj"];

		$inserir  = "INSERT INTO sacados (agencia, conta, cliente, nome, cpf, grupo, scd_cadastro) ";
		$inserir .= "VALUES ('$agencia', '$conta', '$cliente', UPPER('$nome'), '$cpfcnpj', '99999', NOW())";
		auditoria($_SESSION['userLogin'],$agencia, $conta, $_SERVER['REMOTE_ADDR'], "CADASTRO DE SACADOR", mysql_real_escape_string($inserir));
		$sql = mysql_query($inserir) or die(mysql_error());
		if($sql) {
			echo 'ok';
		}
		else {
			echo 'Erro!<br>';
			mysql_error();
		}
	}

	// BUSCA SACADOR

	else if($funcao == 'busca'){
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda">NOME</td>
                    <td width="25%" class="destaque borda">CPF/CNPJ</td>
                    <td width="8%" class="destaque borda">	AÇÕES</td>
                </tr>
		';
		$filtro = $_GET['filtro'];
		if(empty($filtro)){
			$busca = '';
		}
		else {
			$busca=" AND (nome LIKE '%$filtro%' OR cpf LIKE '%$filtro%')";
		}
        $sql = "SELECT nome, cpf, sacado FROM sacados WHERE agencia='$agencia' $busca AND conta LIKE '%$conta' AND grupo='99999' ORDER BY UPPER(nome)";
		$query = mysql_query ($sql) or die(mysql_error());
    	if(mysql_num_rows($query)>0){
			while($linha = mysql_fetch_array($query)){
				echo '
					<tr>
						<td class="borda">'.$linha['nome'].'</td>
						<td class="borda">'.$linha['cpf'].'</td>
						<td class="borda">
							<a href="javascript:excluir('.$linha['sacado'].');" title="Excluir"><img src="img/cancelar.png" /></a>
							&nbsp;&nbsp;
							<a href="javascript:navega(\'sacadoravalista_alterar.php?sacado='.$linha['sacado'].'\');" title="Alterar"><img src="img/editar.png" /></a>
						</td>
					</tr>
				';
			}
		}
		echo '
		    </table>
		';
	}

	//ALTERA SACADOR

	else if($funcao == 'alt_sacador'){
		$sacado         = $_GET["sacado"];
		$nome           = $_GET["nome"];
		$cpfcnpj        = $_GET["cpfcnpj"];;

		$alterar = "UPDATE sacados SET nome=UPPER('$nome'), cpf='$cpfcnpj' WHERE sacado='$sacado'";
		auditoria($_SESSION['userLogin'],$agencia, $conta, $_SERVER['REMOTE_ADDR'], "ALTERAÇÃO DE SACADO", mysql_real_escape_string($alterar));
		$sql = mysql_query($alterar) or die(mysql_error());
		if($sql) {
			echo 'ok';
		}
		else {
			echo 'Erro!<br>';
			mysql_error();
		}
	}

	// EXCLUIR CLIENTE

	else if ($funcao=='excluir') {
		$sacado = $_GET['sacado'];
		$excluir = "DELETE FROM sacados WHERE sacado='$sacado'";
		auditoria($_SESSION['userLogin'],$agencia, $conta, $_SERVER['REMOTE_ADDR'], "Exclusao de sacado", mysql_real_escape_string($excluir));
		$sql = mysql_query($excluir);
		if($sql) {
			echo 'ok';
		}
		else {
			echo 'Erro!<br>';
			mysql_error();
		}
	}
?>
