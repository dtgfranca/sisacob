﻿<?php

// Prepara conexão com banco
require_once("../config.php");

// Evita timeout e mostra erros
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
ini_set('max_execution_time', 0);

// Formata página para código e instancia variáveis
echo "<pre>Sacados/Sacadores com mesmo CPF/CNPJ e nomes diferentes:<br><br>";
$tab = "&nbsp;&nbsp;&nbsp;&nbsp;";

function echoSeExiste($titulo, $vetor, $chave)
{
	global $tab;
	echo $tab.$titulo.": ";
	
	if (isset($vetor[$chave]))
	echo $vetor[$chave];
	
	echo "<br>";
}

$query = mysql_query("SELECT 
		  agencia, cliente, cpf,
		  GROUP_CONCAT(conta) AS contas,
		  GROUP_CONCAT(nome) AS nomes,
		  GROUP_CONCAT(identidade) AS identidades,
		  GROUP_CONCAT(endereco) AS enderecos,
		  GROUP_CONCAT(bairro) AS bairros,
		  GROUP_CONCAT(cep) AS ceps,
		  GROUP_CONCAT(cidade) AS cidades,
		  GROUP_CONCAT(uf) AS ufs,
		  GROUP_CONCAT(telef_pess) AS telefones_pessoais,
		  GROUP_CONCAT(telef_com) AS telefones_comerciais,
		  GROUP_CONCAT(fax) AS fax,
		  GROUP_CONCAT(email) AS emails, 
		  COUNT(*) AS qtde
		FROM sacados 
		WHERE grupo IS NULL AND cpf != '' 
		GROUP BY cpf, cliente, agencia 
		HAVING COUNT(*) > 1");

$count = 0;
$total = 0;
$array = array();

while ($row = mysql_fetch_object($query))
{	
	$array[$count]['agencia'] = $row->agencia;
	$array[$count]['cliente'] = $row->cliente;
	$array[$count]['cpf'] = $row->cpf;
	$array[$count]['qtde'] = $row->qtde;

	$array[$count]['contas'] = explode(",", $row->contas);
	$array[$count]['nomes'] = explode(",", $row->nomes);
	$array[$count]['identidades'] = explode(",", $row->identidades);
	$array[$count]['enderecos'] = explode(",", $row->enderecos);
	$array[$count]['ceps'] = explode(",", $row->ceps);
	$array[$count]['cidades'] = explode(",", $row->cidades);
	$array[$count]['telefones_pessoais'] = explode(",", $row->telefones_pessoais);
	$array[$count]['telefones_comerciais'] = explode(",", $row->telefones_comerciais);
	$array[$count]['fax'] = explode(",", $row->fax);
	$array[$count]['emails'] = explode(",", $row->emails);
	
	$similaridade = 0;
	similar_text($array[$count]['nomes'][0], $array[$count]['nomes'][1], $similaridade);
	$array[$count]['similaridade'] = number_format($similaridade, 0);
	
	if ($similaridade > 50)
		unset($array[$count]);
		
	$count++;	
}

$count = 0;
foreach ($array as $registro)
{
	echo "Contagem: ".($count + 1)." | Agência: ".$registro['agencia']." | Cliente: ".$registro['cliente']." | CPF/CNPJ: ".$registro['cpf']." | ";
	echo "Similaridade do campo nome: ".$registro['similaridade']."%<br>";
	
	for ($subcount = 0; $subcount < $registro['qtde']; $subcount++)
	{
		echo $tab."Registro ".($subcount + 1).":<br>";
		echoSeExiste("Nome", $registro['nomes'], $subcount);
		echoSeExiste("Identidade", $registro['identidades'], $subcount);
		//echoSeExiste("Endereço", $registro['enderecos'], $subcount);
		echoSeExiste("CEP", $registro['ceps'], $subcount);
		echoSeExiste("Cidade", $registro['cidades'], $subcount);
		echoSeExiste("Tel. Pessoal", $registro['telefones_pessoais'], $subcount);
		echoSeExiste("Tel. Comercial", $registro['telefones_comerciais'], $subcount);
		echoSeExiste("Fax", $registro['fax'], $subcount);
		echoSeExiste("Email", $registro['emails'], $subcount);
		echo "<br>";
		
		$total++;
	}
	$count++;
}

// Finaliza a execução e mostra o resultado
echo "<br>Total de registros duplicados: {$total}</pre>";

?>