<?php
require_once '../config.php';
session_start();
// Instancia as variáveis //
$message = '';
$messageAlt = isset($_SESSION['messageAlt']) ? $_SESSION['messageAlt'] :'';
$result = "";
$account = "";
$bExibeDuplicata = false;
$bExibeCheques = false;
$bLogado = false;
$bSucess = isset($_SESSION['bSucess']) ? $_SESSION['bSucess'] :false;
$bAltSenha = isset($_SESSION['bAltSenha']) ? $_SESSION['bAltSenha'] :false;

$_SESSION['messageAlt'] = '';
$_SESSION['bSucess'] = false;
$_SESSION['bAltSenha'] = false;

$action = filter_input(INPUT_POST, 'action');
$dados = [];


// Conexão ao banco de dados //
$conPdo = new Pdo(
		"mysql:host={$server};dbname={$dbname}",
		$user,
		$password,
		array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		)
	);

//Dados da ssessão
if( $action == 'logout'){
	unset($_SESSION['LOGIN']);
	session_destroy();
	header('Location: http://www.sicoobdivicred.com.br');
	die;
}else if( isset($_SESSION['LOGIN']) ){
	$sQuery = "SELECT agencia, usuario, conta, razao, cliente, senha FROM clientes WHERE agencia =4364 AND cliente = ? LIMIT 1";
	$stm = $conPdo->prepare($sQuery);
	$stm->execute([
			$_SESSION['LOGIN']['cliente']
		]);
	$dados = $stm->fetch(PDO::FETCH_ASSOC);
	

	// Aleração de senha
	if($action == 'alterar'){
		$oldPass = filter_input(INPUT_POST, 'oldPass');
		$newPass = filter_input(INPUT_POST, 'newPass');
		$conNewPass = filter_input(INPUT_POST, 'conNewPass');

		if($dados['senha'] != $oldPass){
			$_SESSION['bAltSenha'] = true;
			$_SESSION['messageAlt'] = 'Senha Atual incorreta!';
		}else if(strlen($newPass) < 5 ){
			$_SESSION['bAltSenha'] = true;
			$_SESSION['messageAlt'] = 'Nova Senha deve ter mais de 4 caracteres!';
		}else if( $newPass != $conNewPass ){
			$_SESSION['bAltSenha'] = true;
			$_SESSION['messageAlt'] = 'Nova Senha e Confirmação de Senha estão diferentes!';
		}else{

			$sQuery = 'UPDATE clientes  SET senha = ? WHERE agencia = 4364 AND cliente = ?';
			$stm = $conPdo->prepare($sQuery);
			$stm->execute([
				$newPass,
				$dados['cliente']
			]);
			$_SESSION['bSucess'] = true;
			$_SESSION['messageAlt'] = 'Senha Alterada com Sucesso!';
			
		}
		Header('Location: '.$_SERVER['PHP_SELF']);
		die;
	}

	isset($dados['senha']);
	$_SESSION['LOGIN']=$dados;

//Envio de formulário no login
}else if( $action == 'login' ){

	$sUser = filter_input(INPUT_POST, 'tuser');
	$sPassword = filter_input(INPUT_POST, 'tpassword');
	$sConta = filter_input(INPUT_POST,'taccount');

	if(!strstr($sConta, "-"))
		$sConta = substr($sConta, 0, strlen($sConta) - 1)."-".substr($sConta, -1 );
	
	// Adiciona zeros à esquerda //
	$sConta = str_pad($sConta, 10, '0', STR_PAD_LEFT);
	
	// Consulta de usuário e senha //
	$sQuery = "SELECT agencia, usuario, conta, razao, cliente FROM clientes WHERE agencia =4364 AND usuario = ? AND conta = ? AND senha = ? LIMIT 1";
	$stm = $conPdo->prepare($sQuery);
	$stm->execute([
		$sUser,
		$sConta,
		$sPassword
		]);
	$dados = $stm->fetch(PDO::FETCH_ASSOC);

	if(!empty($dados) ){
		$_SESSION['LOGIN']=$dados;
		//recarregar para limpar os dados enviados de formulário, problema ao voltar página
		Header('Location: '.$_SERVER['PHP_SELF']);
		die;
	}

	$message = "LOGIN INVÁLIDO!";

//finaliza a sessão
}

if(!empty($dados)){

	$bLogado = true;

	$razao = $dados['razao'];
	
	// Consulta na tabela de limites //
	$sQuery = "SELECT lim_data,lim_hora,lim_data_plan FROM limites WHERE  agencia = ? AND conta = ? LIMIT 1";

	$stm = $conPdo->prepare($sQuery);

	$stm->execute([
			$_SESSION['LOGIN']['agencia'],
			$dados['conta']
		]);

	$result = $stm->fetch(PDO::FETCH_ASSOC);

	$lim_data = $result['lim_data'] == null ? "-":$result['lim_data'] ;
    $lim_hora = $result['lim_hora'] == null ? "-":$result['lim_hora'] ;
    $lim_data_plan = "SALDO DIA". implode('/',array_reverse(explode("-",$result['lim_data_plan'])));

	$sQuery = "SELECT * FROM limites WHERE agencia = ? AND conta = ? AND lim_data = CURDATE() ORDER BY tipo ";
	$stm = $conPdo->prepare($sQuery);
	$stm->execute([
		$_SESSION['LOGIN']['agencia'],
		$dados['conta']
	]);

	$aLimites = $stm->fetchAll(PDO::FETCH_ASSOC);

	// Usuário existe, mas não constam dados na tabela //
	if( empty($result) ){
		$message = "No momento não existem limites cadastrados para a Conta Corrente  informada, procure sua agência.";
	}else if(empty($aLimites)){
		$message = "Limites não disponíveis para consulta neste momento, tente mais tarde.";
	}else{
		foreach ($aLimites as $ind => $aLimite){
			if($aLimite['tipo'] == 6){
				$aLimites[] = $aLimite;
				unset($aLimites[$ind]);
				break;
			}
		}
	}
}else{
	session_destroy();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Consulta de Limite Disponível</title>
		<style type="text/css">
			body{
				font-family: Tahoma, Geneva, sans-serif;
				font-size: 13px;

			}


			input[type=text], input[type=password]{
				width: 110px;	
				font-size: 13px;	
			}

			input[type=button]{
				font-family: Tahoma, Geneva, sans-serif;
				font-size: 13px;
				background: #fff;
				height: 30px;
				width: 100px;
				color: #695E4A;
				font-weight: 600;
			}

			.uppercase{
				text-transform: uppercase;
			}

			.green{
				text-align: center;
				color: #006600;
			}

			.red{
				text-align: center;
				color: red;
			}
		</style>
		<script type="text/javascript">
			function contains(char){
				if (document.getElementById('taccount').value.indexOf(char) != -1)
					return true;

				else
					return false;
			}

			function validateNumber(event){
				// Verifica evento //
				var key = window.event ? event.keyCode : event.which;
				//alert(key);

				// Caracteres não permitidos //
				if ((key >= 65 && key <= 90) || key >= 146 || (key >= 106 && key <= 110 && key != 109)) 
					return false;
					
				// Permite o caractere //
				else
				{
					// Adiciona hífen a número da conta //
					//if (document.getElementById('account').value.length == 6)
						//document.getElementById('account').value += '-';

					return true;		
				}
			}

			function alterarSenha(){

				if (document.getElementById('oldPass').value.length < 3){
					alert('O campo "Senha Atual" deve ter mais de 2 caracteres!');
					return false;
				}
				if (document.getElementById('newPass').value.length < 5){
					alert('O campo "Nova Senha" deve ter mais de 4 caracteres!');
					return false;
				}
				if (document.getElementById('conNewPass').value != document.getElementById('newPass').value ){
					alert('"Nova Senha" e "Confirmação de Senha" são diferentes!');
					return false;
				}

				document.getElementById('frmAlteraSenha').submit();

			}

			function validateForm(){
				// Variável de backup de segurança //
				abort = false;

				// Valida a conta //
				if (document.getElementById('taccount').value.length <= 2){
					alert('Preencha corretamente o campo "Conta"!');
					abort = true;
					return false;
				}
				// Valida o dígito //
				/*else if (document.getElementById('account').value.indexOf("-") == -1)
				{
					alert('Preencha corretamente o campo dígito da conta!');
					abort = true;
					return false;
				}*/
				
				// Valida caracteres especiais //
				else if (contains('~') || contains('[') || contains(']') || contains(',') || contains('.') || contains(';') || contains(':') || contains('/') || contains('\\') || contains('^') || contains('´') || contains('`')){
					alert('Caracteres não permitidos no campo "Conta"!');
					abort = true;
					return false;
				}
				// Valida o usuário //
				else if (document.getElementById('tuser').value.length <= 2){
					alert('Preencha corretamente o campo "Usuário"!');
					abort = true;
					return false;
				}
				// Valida a senha //
				else if (document.getElementById('tpassword').value.length <= 2){
					alert('Preencha corretamente o campo "Senha"!');
					abort = true;
					return false;
				}
				// Tudo validado, prossiga...  //
				else{
					// Verifica variável de backup  //
					if (abort == false){
						// Configura o valor de controle de formulário enviado  //
						document.getElementById('action').value = "login";
						
						// Envia o formulário  //
						document.getElementById('frmLogin').submit();
						
						return true;
					}
				}	
			}

			function exibeFrmAlterar(){
				document.getElementById('frmAlteraSenha').hidden = false;
				document.getElementById('frmLogin').hidden = true;
			}

			function cancelarAlteracao(){
				document.getElementById('frmAlteraSenha').hidden = true;
				document.getElementById('frmLogin').hidden = false;
			}

			function logout(){
				document.getElementById('action').value = "logout";
				document.getElementById('frmLogin').submit();
			}
		</script>
	</head>
	<body onload="document.getElementById('agencia').focus()">
	<!-- Formulário -->
	<form id="frmAlteraSenha" <?php echo $bAltSenha?'':'hidden="true"';  ?>" action="" method="POST" >
		<input type="hidden" name="action" value="alterar" />
		<table border="0" align="center" style="width: 100%; height: 100%">
			<table align="center" style="width: 624px; height: 264px; border: #006600 thin solid; color: #666; background-color: #ffffff">
				<tr align="center"><td><img  align="center" src="http://www.sisacob.com.br/scripts/imagens/logo_4364.png"></td></tr>
				<tr><td>
				<table align="center" style="width: 250px">
					<tr><td colspan="2" class="red"> <?php echo !$bSucess ? $messageAlt:''; ?></td></tr>
					<tr>
				    	<td colspan="2" class="green">Alteração de Senha:</td>
				    </tr>
				    <tr><td>&nbsp;</td></tr>
				  
				    <tr>
				    	<td>Senha Atual:</td><td><input id="oldPass" type="password" name="oldPass" placeholder="****" minlength="3"  maxlength="20"/></td>
				    </tr>
				    <tr>
				        <td>Nova Senha:</td><td><input id="newPass" type="password" name="newPass"  placeholder="****" minlength="3" maxlength="20"/></td>
				    </tr>
				    <tr>   
				        <td>Confirmar Nova Senha:</td><td><input id="conNewPass" type="password" name="conNewPass" placeholder="****"  minlength="4" maxlength="20" /></td>        
				    </tr>
				    <tr><td>&nbsp;</td></tr>
				    <tr align="center">
				    	<td colspan="2">
				    	<input type="button" name="alterar" value="ALTERAR" onclick="alterarSenha()"/>
				    	<input type="button" name="alterar" value="CANCELAR" onclick="cancelarAlteracao()"/>
				    	</td>
				    </tr>
				    <tr><td>&nbsp;</td></tr>
			    </table>
			    </tr></td>
			 </table>
		</table>
	</form>

	<form id="frmLogin" action="" <?php echo $bAltSenha?'hidden="true"':'';  ?>" method="POST" onkeypress="if (event.keyCode == 13) validateForm()">
		<input id="action" type="hidden" name="action" value="login" />

		<!-- Tabela exterior -->
		<table border="0" align="center" style="width: 100%; height: 100%">
			<table align="center" style="width: 624px; height: 264px; border: #006600 thin solid; color: #666; background-color: #ffffff">
				<tr align="center"><td><img  align="center" src="http://www.sisacob.com.br/scripts/imagens/logo_4364.png"></td></tr>
				<tr id="verifique" align="center"class="capitalize"><td style="color: #695E4A; padding: 7px">Vefique aqui seu limite para desconto de cheques e desconto de duplicatas.</td></tr>
				<tr><td>
				<!-- Tabela interior  -->
				<table id="table" align="center" style="width: 250px">
					<tr><td>&nbsp;</td></tr>
		    
				    <!-- Se houver erro ou não houver dado, mostra formulário -->
				    
					<?php if(!$bLogado) { ?>
					<tr><td colspan="2" class="red"> <?php echo $message; ?></td></tr>
					<tr>
				    	<td colspan="2" class="green">Insira os dados abaixo:</td>
				    </tr>
				    <tr><td>&nbsp;</td></tr>
				  
				    <tr>
				    	<td>Conta:</td><td><input id="taccount" type="text" name="taccount" placeholder="EX.: 123456-7" onkeydown="return validateNumber(event)" maxlength="10"/></td>
				    </tr>
				    <tr>
				        <td>Usuário:</td><td><input id="tuser" type="text" name="tuser" class="uppercase" placeholder="Ex.: JOSE" maxlength="20"/></td>
				    </tr>
				    <tr>   
				        <td>Senha:</td><td><input id="tpassword" type="password" name="tpassword" placeholder="****"  maxlength="20"/></td>        
				    </tr>
				    <tr><td>&nbsp;</td></tr>
				    <tr align="center">
				    	<td colspan="2"><input type="button" name="logar" value="ENTRAR" onclick="validateForm()"/></td>
				    </tr>
				    <tr><td>&nbsp;</td></tr>
					
					<?php } else { ?>

					<?php if(!empty($messageAlt) && $bSucess ){ ?>
					<script type="text/javascript">alert("<?php echo $messageAlt?>")</script>
				    <?php } ?>
				    <!-- Aumenta tamanho da tabela -->	
				    <script type="text/javascript">document.getElementById('table').style.width = "560px"; document.getElementById('verifique').style.display='none'</script>
				    
				    <!-- Nenhum errro encontrado, mostrar resultado -->	
				   
				 
				   
				    <tr align="center" class="uppercase">  
				        <td align="center" colspan="2"><strong  style="margin-right: 10px;"><?= $razao ?> </strong>&nbsp;</td>
				        <!-- td align="left"  ></td -->
				        <td align="left"></td> 
						<tr><td>&nbsp;</td></tr>
				    </tr>
				    <tr class="uppercase">
				    	<td align="left" ><strong>Conta : </strong><?= $dados['conta'] ?></td>
				    	<td><strong>Última Atualização</strong> : <?php echo implode('/',array_reverse(explode("-",$lim_data))); ?><strong> Hora :</strong> <?php echo $lim_hora; ?>
				    	</td>
				    	<td></td>
				    </tr>
				    <tr><td>&nbsp;</td></tr>
					<?php foreach($aLimites as $aLimite){ ?>
					<tr class="uppercase">
				    	<td colspan="2" class="green"> <img src="imagens/ico.png"/><?php
					    	 switch ($aLimite['tipo']){
					    	 	case 3:
					    	 		echo "SICOOBCRED CHEQUES {$lim_data_plan}:";
					    	 		break;
					    	 	case 4:
					    	 		echo "SICOOBCRED DUPLICATAS  {$lim_data_plan}:";
					    	 		break;
					    	 	case 5:
					    	 		echo "SICOOBCRED DUPLICATAS RURAL {$lim_data_plan}:";
					    	 		break;
					    	 	case 6:
					    	 		echo "SICOOBCRED CHEQUES PLUS {$lim_data_plan}:";
					    	 		break;
					    	 	case 7:
					    	 		echo "SICOOBCRED CHEQUES NORMAL 1 {$lim_data_plan}:";
					    	 		break;
					    	 }
				    	 ?>
				    	 </td>
				    </tr>
				    <?php if($aLimite["tipo"] == 6 ){ ?>
					<tr align="center" class="uppercase">
				        <td align="right" colspan="2" style="color:red; text-align: center;font-size:10px">*Limite Plus condicionado à utilização total do Limite Normal.</td>
				    </tr>
					<?php } ?>
					<tr><td>&nbsp;</td></tr>
				    <tr align="center" class="uppercase">   
				        <td align="right"><strong>Vencimento:&nbsp;&nbsp;</strong></td><td align="left"><?= date("d/m/Y", strtotime($aLimite["vencimento"])) ?></td>        
				    </tr>
				    <tr align="center" class="uppercase">
				    	<td align="right"><strong>Limite:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($aLimite["limite"], 2, ",", ".") ?></td>
				    </tr>

				    <tr align="center" class="uppercase">
				        <td align="right"><strong>Utilizado:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($aLimite["utilizado"], 2, ",", ".") ?></td>
				    </tr>
				    
				    <tr align="center" class="uppercase">
				        <td align="right"><strong>Disponível:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format(($aLimite["limite"] - $aLimite["utilizado"]), 2, ",", ".") ?></td>
				    </tr>
				     <!--<tr align="center" class="uppercase">
				        <td align="right"><strong>Taxa:&nbsp;&nbsp;</strong></td><td align="left">1.32%</td>
				    </tr>-->
				   <tr><td>&nbsp;</td></tr>
					<?php }?>
					<?php if(empty($aLimites)){ ?>
					<tr align="center" class="uppercase">
				        <td align="right" colspan="2" style="color:red; text-align: center;font-size:11px"><?php echo $message?></td>
				    </tr>
					<?php } ?>
				   
				    <tr align="center" class="uppercase" >
				    	<td colspan="2" ><input style ="width: 123px;" type="button" name="alterar" value="ALTERAR SENHA" onclick="exibeFrmAlterar()"/>
				    	<input type="button" name="voltar" value="VOLTAR" onclick="logout()"/></td>
				    </tr>
					
				    <!-- Encerra emissão do resultado  -->
					<?php } ?>

				<!-- Fecha tabela interior -->
				</table>
				</td></tr>
				<tr><td style="color: #695E4A; padding: 7px; text-align: center; font-weight: bold"></td></tr><tr><td>&nbsp;</td></tr>
		</table>

		<!-- Fecha tabela exterior -->
		</td></tr>
		</table>
		<!-- Fecha formulário e página -->
		</form>
	</body>
</html>