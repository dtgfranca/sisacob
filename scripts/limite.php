<?php

session_start();
// Instancia as variáveis //
$message = "";
$result = "";
$account = "";


// Verifica se o botão "Entrar" foi pressionado //
if (isset($_POST['submitted']) && $_POST['submitted'] == "yes")
{
	// Atualiza variável //
	$account = $_POST['account'];
	
	// Adiciona hífen se não houver //
	if (!strstr($account, "-"))
		$account = substr($_POST['account'], 0, strlen($_POST['account']) - 1)."-".substr($_POST['account'], strlen($_POST['account']) - 1, 1);
	
	// Adiciona zeros à esquerda //
	$account = str_pad($account, 10, '0', STR_PAD_LEFT);
	
	// Conexão ao banco de dados //
	mysql_connect("localhost", "root", "Skillnet69547") or die("Não foi possível se conectar ao servidor!");
	mysql_select_db("skill_sisacob") or die("Não foi possível selecionar a fonte dos dados!");
	
	// Consulta de usuário e senha //
	$query = mysql_query("SELECT agencia, usuario, conta, senha, razao FROM clientes WHERE  agencia ={$_POST['agencia']} AND usuario = '".$_POST['user']."' AND conta = '".$account."' AND senha = '".mysql_real_escape_string($_POST['password'])."' LIMIT 1");
	
	

	// Não há cliente com estes dados //
	if (mysql_num_rows($query) != 1)
	$message = "LOGIN INVÁLIDO!";
	
	else
	{
		$dados = mysql_fetch_assoc($query);

		$_SESSION['agencia'] =$dados['agencia'];

		$logo = function () {
	           $query = "SELECT razao FROM agencias WHERE agencia ={$_SESSION['agencia']}";
	           $cmd = mysql_query($query);
	           $array = mysql_fetch_assoc($cmd);
	          return str_replace("SICOOB", "", $array['razao']);
           };	

		$razao = mysql_result($query, 0, "razao");
		
		// Consulta na tabela de limites //
		$query = mysql_query("SELECT * FROM limites WHERE  agencia = {$_SESSION['agencia']} AND conta = '".$account."' LIMIT 1");
		$result = mysql_fetch_assoc($query);
		$lim_data = $result['lim_data'] == null ? "-":$result['lim_data'] ;
        $lim_hora = $result['lim_hora'] == null ? "-":$result['lim_hora'] ;

		$limiteDuplicata = mysql_query("SELECT * FROM limites WHERE agencia ={$_SESSION['agencia']} and tipo = 4 AND conta = '".$account."' LIMIT 1");
        $duplicatas = mysql_fetch_assoc($limiteDuplicata);

        $limiteCheque = mysql_query("SELECT * FROM limites WHERE agencia = {$_SESSION['agencia']} and tipo = 3 AND conta = '".$account."' LIMIT 1");
        $desCheque = mysql_fetch_assoc($limiteCheque);
		// Usuário existe, mas não constam dados na tabela //
		if (mysql_num_rows($query) != 1)
			$message = "NENHUM LIMITE ENCONTRADO!";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Limite Disponível</title>

<style type="text/css">
body
{
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 13px;
	text-transform: uppercase;
}

input[type=text], input[type=password]
{
	width: 110px;	
	font-size: 13px;	
}

input[type=button]
{
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 13px;
	background: #fff;
	height: 30px;
	width: 100px;
	color: #695E4A;
	font-weight: 600;
}

.uppercase
{
	text-transform: uppercase;
}

.green
{
	text-align: center;
	color: #006600;
}

.red
{
	text-align: center;
	color: red;
}
</style>
<script type="text/javascript">
function contains(char)
{
	if (document.getElementById('account').value.indexOf(char) != -1)
		return true;

	else
		return false;
}

function validateNumber(event)
{
	// Verifica evento //
	var key = window.event ? event.keyCode : event.which;
	//alert(key);

	// Caracteres não permitidos //
	if ((key >= 65 && key <= 90) || key >= 146 || (key >= 106 && key <= 110 && key != 109)) 
		return false;
		
	// Permite o caractere //
	else
	{
		// Adiciona hífen a número da conta //
		//if (document.getElementById('account').value.length == 6)
			//document.getElementById('account').value += '-';

		return true;		
	}
}

function validateForm()
{
	// Variável de backup de segurança //
	abort = false;

	// Valida a conta //
	if (document.getElementById('account').value.length <= 2)
	{
		alert('Preencha corretamente o campo "Conta"!');
		abort = true;
		return false;
	}
	
	// Valida o dígito //
	/*else if (document.getElementById('account').value.indexOf("-") == -1)
	{
		alert('Preencha corretamente o campo dígito da conta!');
		abort = true;
		return false;
	}*/
	
	// Valida caracteres especiais //
	else if (contains('~') || contains('[') || contains(']') || contains(',') || contains('.') || contains(';') || contains(':') || contains('/') || contains('\\') || contains('^') || contains('´') || contains('`'))
	{
		alert('Caracteres não permitidos no campo "Conta"!');
		abort = true;
		return false;
	}

	// Valida o usuário //
	else if (document.getElementById('user').value.length <= 2)
	{
		alert('Preencha corretamente o campo "Usuário"!');
		abort = true;
		return false;
	}
	
	// Valida a senha //
	else if (document.getElementById('password').value.length <= 2)
	{
		alert('Preencha corretamente o campo "Senha"!');
		abort = true;
		return false;
	}
	
	// Tudo validado, prossiga...  //
	else
	{
		// Verifica variável de backup  //
		if (abort == false)
		{
			// Configura o valor de controle de formulário enviado  //
			document.getElementById('hidden').value = "yes";
			
			// Envia o formulário  //
			document.getElementById('myform').submit();
			
			return true;
		}
	}
	
}
</script>
</head>

<body onload="document.getElementById('agencia').focus()">
<!-- Formulário -->
<form id="myform" action="" method="post" onkeypress="if (event.keyCode == 13) validateForm()">
<input id="hidden" type="hidden" name="submitted" value="no" />

<!-- Tabela exterior -->
<table border="0" align="center" style="width: 100%; height: 100%"><tr><td>
<table align="center" style="width: 624px; height: 264px; border: #006600 thin solid; color: #666; background-color: #fafafa">
<tr align="center"> <td><img  align="center" src="http://www.sisacob.com.br/scripts/imagens/logo.png"><br>
 <?php if($logo !=''):?><font style ="margin-left:180px"color="green"><?php echo $logo();?></font></td></tr><?php endif;?>

<tr id="verifique"><td style="color: #695E4A; padding: 7px">VERIFIQUE AQUI SEU LIMITE PARA DESCONTO DE CHEQUES E DESCONTO DE DUPLICATAS E AINDA FAÇA SUA CUSTÓDIA.</td></tr><tr><td>

<!-- Tabela interior  -->
<table id="table" align="center" style="width: 250px">
	<tr><td>&nbsp;</td></tr>
    
    <!-- Se houver erro ou não houver dado, mostra formulário -->
	<?php if ($result == "" || $message != "") { ?>
	<tr><td colspan="2" class="red"><?= $message ?></td></tr>
	<tr>
    	<td colspan="2" class="green">Insira os dados abaixo:</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td>Agência:</td><td><input id="agencia" type="text" placeholder="Agência"name="agencia"  maxlength="6"/></td>
    </tr>
    <tr>
    	<td>Conta:</td><td><input id="account" type="text" name="account" placeholder="EX.: 123456-7" onkeydown="return validateNumber(event)" maxlength="10"/></td>
    </tr>
    <tr>
        <td>Usuário:</td><td><input id="user" type="text" name="user" class="uppercase" placeholder="Ex.: JOSE" maxlength="20"/></td>
    </tr>
    <tr>   
        <td>Senha:</td><td><input id="password" type="password" name="password" placeholder="****"  maxlength="20"/></td>        
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td colspan="2"><input type="button" name="logar" value="ENTRAR" onclick="validateForm()"/></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
	
	<?php } else { ?>
    <!-- Aumenta tamanho da tabela -->	
    <script type="text/javascript">document.getElementById('table').style.width = "560px"; document.getElementById('verifique').style.display='none'</script>
    
    <!-- Nenhum errro encontrado, mostrar resultado -->	
   
   <tr><td>&nbsp;</td></tr>
   
    <tr align="center">  
    
    
    		
    	
        <td align="left" colspan="2"><strong  style="margin-right: 10px;">Nome:</strong><?= $razao ?>&nbsp;</td>
        <!-- td align="left"  ></td -->
        <td align="left"></td> 

       
    </tr>
    <tr>
    	<td align="left" ><strong>Conta : </strong><?= $account ?></td>
    	<td> <strong>Última Atualização</strong> : <?php echo implode('/',array_reverse(explode("-",$lim_data))); ?></td>
    	<td> <strong>Hora :</strong> <?php echo $lim_hora; ?></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <?php if(mysql_num_rows($limiteDuplicata)>0):?>
	<tr>
    	<td colspan="2" class="green">Informações referentes ao limite Desconto de Duplicatas : </td>
    </tr>    
    
<tr><td>&nbsp;</td></tr>
    <tr align="center">   
        <td align="right"><strong>Vencimento:&nbsp;&nbsp;</strong></td><td align="left"><?= date("d/m/Y", strtotime($duplicatas["vencimento"])) ?></td>        
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td align="right"><strong>Limite:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($duplicatas["limite"], 2, ",", ".") ?></td>
    </tr>
    <tr align="center">
        <td align="right"><strong>Utilizado:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($duplicatas["utilizado"], 2, ",", ".") ?></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
        <td align="right"><strong>Disponível:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format(($duplicatas["limite"] - $duplicatas["utilizado"]), 2, ",", ".") ?></td>
    </tr>
<?php endif;?>
   <tr><td>&nbsp;</td></tr>
   
    <!-- alterando -->
    <?php if(mysql_num_fields($limiteCheque)>0):  ?>
    <tr>
    	<td colspan="2" class="green">Informações referentes ao limite Desconto de Cheques : </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
   
<tr><td>&nbsp;</td></tr>
    <tr align="center">   
        <td align="right" ><strong>Vencimento:&nbsp;&nbsp;</strong></td><td align="left"><?= date("d/m/Y", strtotime($desCheque["vencimento"])) ?></td>        
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td align="right"><strong>Limite:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($desCheque["limite"], 2, ",", ".") ?></td>
    </tr>
    <tr align="center">
        <td align="right"><strong>Utilizado:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($desCheque["utilizado"], 2, ",", ".") ?></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
        <td align="right"><strong>Disponível:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format(($desCheque["limite"] - $desCheque["utilizado"]), 2, ",", ".") ?></td>
    </tr>

   <?php endif;?>
    <!-- #### -->
	<tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td colspan="2"><input type="button" name="voltar" value="VOLTAR" onclick="document.location.href='http://www.sisacob.com.br'"/></td>
    </tr>
	
    <!-- Encerra emissão do resultado  -->
	<?php } ?>

<!-- Fecha tabela interior -->
</table></td></tr><tr><td style="color: #695E4A; padding: 7px; text-align: center; font-weight: bold">Para mais informações, ligue:  (37) 3226-9700.</td></tr><tr><td>&nbsp;</td></tr></table>

<!-- Fecha tabela exterior -->
</td></tr></table>

<!-- Fecha formulário e página -->
</form></body></html>