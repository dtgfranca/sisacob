<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Consulta de Limite Disponível</title>
		<style type="text/css">
			body{
				font-family: Tahoma, Geneva, sans-serif;
				font-size: 13px;

			}


			input[type=text], input[type=password]{
				width: 110px;	
				font-size: 13px;	
			}

			input[type=button]{
				font-family: Tahoma, Geneva, sans-serif;
				font-size: 13px;
				background: #fff;
				height: 30px;
				width: 100px;
				color: #695E4A;
				font-weight: 600;
			}

			.uppercase{
				text-transform: uppercase;
			}

			.green{
				text-align: center;
				color: #006600;
			}

			.red{
				text-align: center;
				color: red;
			}
		</style>
		<script type="text/javascript">
			function contains(char){
				if (document.getElementById('taccount').value.indexOf(char) != -1)
					return true;

				else
					return false;
			}

			function validateNumber(event){
				// Verifica evento //
				var key = window.event ? event.keyCode : event.which;
				//alert(key);

				// Caracteres não permitidos //
				if ((key >= 65 && key <= 90) || key >= 146 || (key >= 106 && key <= 110 && key != 109)) 
					return false;
					
				// Permite o caractere //
				else
				{
					// Adiciona hífen a número da conta //
					//if (document.getElementById('account').value.length == 6)
						//document.getElementById('account').value += '-';

					return true;		
				}
			}

			function alterarSenha(){

				if (document.getElementById('oldPass').value.length < 3){
					alert('O campo "Senha Atual" deve ter mais de 2 caracteres!');
					return false;
				}
				if (document.getElementById('newPass').value.length < 5){
					alert('O campo "Nova Senha" deve ter mais de 4 caracteres!');
					return false;
				}
				if (document.getElementById('conNewPass').value != document.getElementById('newPass').value ){
					alert('"Nova Senha" e "Confirmação de Senha" são diferentes!');
					return false;
				}

				document.getElementById('frmAlteraSenha').submit();

			}

			function validateForm(){
				// Variável de backup de segurança //
				abort = false;

				// Valida a conta //
				if (document.getElementById('taccount').value.length <= 2){
					alert('Preencha corretamente o campo "Conta"!');
					abort = true;
					return false;
				}
				// Valida o dígito //
				/*else if (document.getElementById('account').value.indexOf("-") == -1)
				{
					alert('Preencha corretamente o campo dígito da conta!');
					abort = true;
					return false;
				}*/
				
				// Valida caracteres especiais //
				else if (contains('~') || contains('[') || contains(']') || contains(',') || contains('.') || contains(';') || contains(':') || contains('/') || contains('\\') || contains('^') || contains('´') || contains('`')){
					alert('Caracteres não permitidos no campo "Conta"!');
					abort = true;
					return false;
				}
				// Valida o usuário //
				else if (document.getElementById('tuser').value.length <= 2){
					alert('Preencha corretamente o campo "Usuário"!');
					abort = true;
					return false;
				}
				// Valida a senha //
				else if (document.getElementById('tpassword').value.length <= 2){
					alert('Preencha corretamente o campo "Senha"!');
					abort = true;
					return false;
				}
				// Tudo validado, prossiga...  //
				else{
					// Verifica variável de backup  //
					if (abort == false){
						// Configura o valor de controle de formulário enviado  //
						document.getElementById('action').value = "login";
						
						// Envia o formulário  //
						document.getElementById('frmLogin').submit();
						
						return true;
					}
				}	
			}

			function exibeFrmAlterar(){
				document.getElementById('frmAlteraSenha').hidden = false;
				document.getElementById('frmLogin').hidden = true;
			}

			function cancelarAlteracao(){
				document.getElementById('frmAlteraSenha').hidden = true;
				document.getElementById('frmLogin').hidden = false;
			}

			function logout(){
				document.getElementById('action').value = "logout";
				document.getElementById('frmLogin').submit();
			}
		</script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body onload="document.getElementById('agencia').focus()">