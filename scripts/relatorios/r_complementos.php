<?php

ini_set("max_execution_time", 3600);
include('../../config.php');
include('../limpa.php');
require('../tfpdf.php');	
$arquivo = parse_ini_file('../../cooperativas.ini');
session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}
$_GET = sanitize($_POST);

function Reais($num, $cifrao = true){
	$retorno = "";
	
	if ($cifrao)
	$retorno .= "R$ ";
	
	$retorno .= number_format($num, 2, ",", ".");
	return $retorno;
}
	
class PDF_MC_Table extends tFPDF{
	function Header(){	
		$agencia = $_SESSION['userAgencia'];			
		$cliente = $_SESSION['userCliente'];
		$conta = $_SESSION['userConta'];
		$razao = $_SESSION['userNome'];
		$tipo = $_POST['tipo'];
		$filtro = $_POST['filtro'];
		
		if($filtro=="data_emisao"){
			$value_periodo = "EMISSÃO DE ".$_POST['inicio']." A ".$_POST['fim'];
		}
		else if($filtro=="data_venc"){
			$value_periodo = "VENCIMENTO DE ".$_POST['inicio']." A ".$_POST['fim'];
		}
		else if($filtro=="data_baixa"){
			$value_periodo = "BAIXA DE ".$_POST['inicio']." A ".$_POST['fim'];
		}
		
		if($tipo=="todos"){ 
   			$value_tipo = "RELATÓRIO SIMPLES";
		}
		else if($tipo=="rec"){ 
			$value_tipo = "RECEBIDOS";
		}
		else if($tipo=="baixa"){ 
		   	$value_tipo = "BAIXADOS";
		}
		else if($tipo=="pend"){ 
		   	$value_tipo = "EM ABERTO";
		}
		else if($tipo=="venc"){
		   	$value_tipo = "VENCIDOS";
		}
		else if($tipo=="cancel"){ 
		   	$value_tipo = "CANCELADOS";
		}
		else if($tipo=="lista_previa"){ 
			$value_tipo = "LISTAGEM PRÉVIA";
		}
		else if($tipo=="rejeitados"){ 
			$value_tipo = "REJEITADOS";
		}
		else if($tipo=="nosso_n"){ 
			$nosso_n = $_POST['nosso_n'];
			$value_tipo = "NOSSO NUMERO ".$nosso_n;
			$value_periodo = '';
		}
		else if($tipo=="n_doc"){ 
			$nosso_n = $_POST['n_titulo'];
			$value_tipo = "Documento ".$nosso_n;
			$value_periodo = '';
		}
			
		if($agencia == '4117'){
			$tam = '42,15';
		}
		else{
			$tam ='42,15';
		}
			
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',16);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,$tam); // logo do relatório
		$this->Cell(61);
		$this->Cell(153,10,'RELATÓRIO COMPLEMENTAR DE BOLETOS',0,0,'C'); // título do relatório
		$this->Cell(61,10,'',0,1);		
		$this->Cell(61);
		$this->SetFont('DejaVu','B',13);
		$this->Cell(153,10,$value_tipo,0,0,'C');
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(61,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(214);			
		$this->Cell(61,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(61);
		$this->SetTextColor(0,127,0);			
		$this->SetFont('DejaVu','B',10);
		$this->Cell(153,10,$value_periodo,0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(61,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);	
		$this->Cell(275,5,'Cliente: '.$razao,0,1);			
		$this->Cell(30,5,'Agência: '.$agencia,0,0);			
		$this->Cell(245,5,'Conta: '.$conta,0,1);
		$this->Ln(2);
		$this->SetFont('DejaVu','B',8);
		$this->SetFillColor(226,226,226);
		$this->Cell(28,6,'Documento',1,0,'C',true);
		$this->Cell(35,6,'Nosso Número',1,0,'C',true);
		$this->Cell(18,6,'Emissão',1,0,'C',true);
		$this->Cell(18,6,'Venc.',1,0,'C',true);
		$this->Cell(92,6,'Sacador/Avalista',1,0,'C',true);
		$this->Cell(18,6,'Juros',1,0,'C',true);
		$this->Cell(18,6,'Multa',1,0,'C',true);
		$this->Cell(18,6,'Protesto',1,0,'C',true);
		$this->Cell(30,6,'Valor',1,1,'C',true);
	}
	var $widths;
	var $aligns;

	function SetWidths($w){
	//Set the array of column widths
		$this->widths=$w;
	}
	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}
	function Row($data){
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function CheckPageBreak($h){
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
	function NbLines($w,$txt){
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n"){
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax){
				if($sep==-1){
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}

$pdf = new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4');

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

if(empty($_POST['inicio']) || empty($_POST['fim'])){
	$b_data = "";
}
else {
	$inicio = explode('/',$_POST['inicio']);
	$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
	$fim = explode('/',$_POST['fim']);
	$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];
}
if(!empty($_POST['cliente'])){$sacado = $_POST['cliente'];}
$tipo   = $_POST["tipo"];
$tipo2  = $_POST["tipo2"];
$ordem  = $_POST["ordem"];
$filtro = $_POST["filtro"];

if($filtro == "data_baixa"){
	$b_data = "AND ((t.data_baixa>='$inicio' AND t.data_baixa<='$fim') OR (t.data_baixa_manual>='$inicio' AND t.data_baixa_manual<='$fim'))";
}

else {
	$b_data = "";
}
if($tipo == "rec"){
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND (status='05' OR status='06' OR status='07' OR status='08' OR status='15')";
}
else if($tipo == "baixa"){
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND t.devolucao IS NOT NULL AND (t.status='09' OR t.status='10')";
}
else if($tipo=="pend"){
	$b_tipo="AND (t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL)";
}
else if($tipo=="venc"){
	$b_tipo="AND t.data_venc < CURDATE() AND t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL";
}
else if($tipo=="rejeitados"){
	$b_tipo="AND t.status='03' AND t.cancelamento IS NULL";
}
else if($tipo=="nosso_n"){
	$nosso_n = $_POST['nosso_n'];
	$b_tipo="AND t.nossonumero LIKE '%".$nosso_n."%'";
}
else if($tipo=="n_doc"){
	$n_titulo = $_POST['n_titulo'];
	$b_tipo="AND t.documento LIKE '%".$n_titulo."%'";
}
else if($tipo=="cancel"){
	$b_tipo="AND t.cancelamento IS NOT NULL";
}
else if($tipo=="lista_previa"){
    $b_tipo = "AND cad_completo = 'N' AND status <> '99' ";
}
else {
	$b_tipo = "AND cad_completo = 'S' ";	
}
if(empty($_POST['tipo2'])){
	$b_tipo2 = "";
}
else{
	$tipo2	= $_POST['tipo2'];
	switch($tipo2){
		case "desc":
			if($tipo == "rec"){
				$b_tipo2 = "AND t.desconto IS NOT NULL AND so_desconto='S'";
			}
			else if($tipo == "todos"){
				$b_tipo2 = "AND so_desconto='S'";
			}
			else{
				$b_tipo2 = "AND so_desconto='S'";
			}
		break;
		case "cobranca_simples":
			if($tipo == "rec"){
				$b_tipo2 = "AND so_desconto='N'";
			}else if($tipo == "todos"){
				$b_tipo2 = "AND so_desconto='N'";
			}else{
				$b_tipo2 = "AND so_desconto='N'";
			}
		break;
	}
}
if(!empty($sacado)){
	$b_sacado = "AND t.sacado='$sacado'";
}
else{ 
	$b_sacado="";
}
if(in_array($_SESSION['userAgencia'],$arquivo)){
$query = "select t.titulo, t.status, t.documento, t.sequencia, t.multa, t.juros, t.protesto, t.nossonumero, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, ";
$query .= "t.valor_baixa, t.sacador, s.nome ";
$query .= "from titulos AS t left join sacados AS s on t.sacador=s.sacado ";
$query .= "where t.cliente='$cliente' data_emisao is not null  $b_data $b_tipo $b_tipo2 order by $ordem;";
}else{
	$query = "select t.titulo, t.status, t.documento, t.sequencia, t.multa, t.juros, t.protesto, t.nossonumero, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, ";
$query .= "t.valor_baixa, t.sacador, s.nome ";
$query .= "from titulos AS t left join sacados AS s on t.sacador=s.sacado ";
$query .= "where t.cliente='$cliente' and data_emisao is not null $b_sacado $b_data $b_tipo $b_tipo2 order by $ordem;";
	
}

$sql = mysql_query($query)or die (mysql_error());
	
if(mysql_num_rows($sql)>0){
	$pdf->SetWidths(array(28,35,18,18,92,18,18,18,30));
	$pdf->SetAligns(array('C','C','C','C','L','R','R','R','R'));
	$sacado_ant="";
	$titulos = 0;
	while ($linha=mysql_fetch_array($sql)) {
		$sacado=$linha['sacador'];	
		$titulos++;
		$valor_total = 0;
		if($linha['cancelamento']==""){
			$valor_total=$valor_total+$linha['valor_baixa'];
		}	
		$pdf->SetFont('Arial','',8);
		if($agencia=='4030'){
			$pdf->Row(array($linha['documento'],$linha['nossonumero'],$linha['data_emisao'],$linha['data_venc'],$linha['nome'],str_replace('.',',',$linha['juros'])."%",str_replace('.',',',$linha['multa'])."%",$linha['protesto'].' dias',Reais($linha['valor_baixa'])));
		}
		else{
			$pdf->Row(array($linha['documento']."/".$linha['sequencia'],$linha['nossonumero'],$linha['data_emisao'],$linha['data_venc'],$linha['nome'],str_replace('.',',',$linha['juros'])."%",str_replace('.',',',$linha['multa'])."%",$linha['protesto'].' dias',Reais($linha['valor_baixa'])));
		}
	}	
	$pdf->Ln(2);
	$pdf->SetFont('DejaVu','B',8);
	$pdf->SetFillColor(226,226,226);
	$pdf->Cell(28,6,'Totais =>',1,0,'L',true);
	$pdf->Cell(35,6,$titulos.' títulos',1,0,'R',true);
	$pdf->Cell(182,6,'',1,0,'R',true);
	$pdf->Cell(30,6,Reais($valor_total),1,1,'R',true);
}
else{
	$pdf->Cell(275,15,'Nenhum lançamento',1,1,'C');
}

$pdf->Output();
?>