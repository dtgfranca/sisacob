<?php

ini_set("max_execution_time", 3600);
include('../../config.php');
include('../limpa.php');
require('../tfpdf.php');	
session_start();
mysql_query("SET NAMES UTF8") or die(mysql_error());
$_GET = sanitize($_POST);
	
class PDF_MC_Table extends tFPDF{
	function Header(){	
		$agencia = $_SESSION['userAgencia'];
		$conta = $_SESSION['userConta'];	
		$razao = $_SESSION['userNome'];			
			
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',16);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,42,15); // logo do relatório
		$this->Cell(42);
		$this->Cell(105,10,'RELATÓRIO DE INSTRUÇÕES',0,0,'C'); // título do relatório
		$this->Cell(42,10,'',0,1);		
		$this->Cell(42);
		$this->Cell(105,10,'EXECUTADAS',0,0,'C');
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(40,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(147);			
		$this->Cell(40,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(42);
		$this->SetTextColor(0,127,0);			
		$this->SetFont('DejaVu','B',12);
		$this->Cell(105,10,'CARTEIRA - '.date('d/m/Y'),0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(40,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);
		$this->Cell(187,5,'Cliente: '.$razao,0,1);			
		$this->Cell(30,5,'Agência: '.$agencia,0,0);			
		$this->Cell(157,5,'Conta: '.$conta,0,1);	
		$this->Ln(2);
		$this->SetFont('DejaVu','B',9);
		$this->SetFillColor(226,226,226);
		$this->SetTextColor(0,0,0);
		$this->Cell(43,6,'Cliente',1,0,'L',true);
		$this->Cell(28,6,'Documento',1,0,'L',true);
		$this->Cell(28,6,'Nosso Número',1,0,'L',true);
		$this->Cell(18,6,'Instr.',1,0,'L',true);
		$this->Cell(18,6,'Envio',1,0,'L',true);
		$this->Cell(37,6,'Comando',1,0,'L',true);
		$this->Cell(15,6,'Status',1,1,'L',true);
	}
	var $widths;
	var $aligns;

	function SetWidths($w){
	//Set the array of column widths
		$this->widths=$w;
	}
	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}
	function Row($data){
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function CheckPageBreak($h){
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
	function NbLines($w,$txt){
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n"){
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax){
				if($sep==-1){
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}

$pdf = new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];
$sacado = $_POST['cliente'];

if ($sacado == '0'){
   $filtro1 = "";
   $filtro2 = "";
}   
else{ 
   $filtro1 = "AND titulos.sacado = ".$sacado;
   $filtro2 = "AND cad_instrucoes.sacado = ".$sacado;
}

$sDados = "SELECT sacados.nome AS nome_geral, CONCAT(titulos.documento,'/',titulos.sequencia) AS documento, titulos.nossonumero, ";
$sDados .= "DATE_FORMAT(cad_instrucoes.data_instr, '%d/%c/%Y') AS data_instr, DATE_FORMAT(cad_instrucoes.data_envio, '%d/%c/%Y') AS data_envio, ";
$sDados .= "cad_instrucoes.comando, cad_instrucoes.id, cad_instrucoes.sacado, cad_instrucoes.status ";
$sDados .= "FROM cad_instrucoes	LEFT JOIN titulos ON cad_instrucoes.boleto=titulos.titulo ";
$sDados .= "LEFT JOIN clientes ON cad_instrucoes.cliente=clientes.cliente ";
$sDados .= "LEFT JOIN sacados ON titulos.sacado=sacados.sacado ";
$sDados .= "WHERE cad_instrucoes.cliente='$cliente' AND cad_instrucoes.agencia='$agencia' ";
$sDados .= "$filtro1 AND cad_instrucoes.data_envio IS NOT NULL AND cad_instrucoes.boleto IS NOT NULL AND sacados.nome IS NOT NULL ";
$sDados .= "UNION ALL ";
$sDados .= "SELECT sacados.nome AS nome_geral, CONCAT(titulos.documento,'/',titulos.sequencia) AS documento, titulos.nossonumero,  ";
$sDados .= "DATE_FORMAT(cad_instrucoes.data_instr, '%d/%c/%Y') AS data_instr, DATE_FORMAT(cad_instrucoes.data_envio, '%d/%c/%Y') AS data_envio, ";
$sDados .= "cad_instrucoes.comando, cad_instrucoes.id, cad_instrucoes.sacado, cad_instrucoes.status ";
$sDados .= "FROM cad_instrucoes LEFT JOIN titulos ON cad_instrucoes.boleto=titulos.titulo ";
$sDados .= "LEFT JOIN clientes ON cad_instrucoes.cliente=clientes.cliente ";
$sDados .= "LEFT JOIN sacados ON cad_instrucoes.sacado=sacados.sacado ";
$sDados .= "WHERE cad_instrucoes.cliente='$cliente' AND cad_instrucoes.agencia='$agencia' ";
$sDados .= "$filtro2 AND cad_instrucoes.data_envio IS NOT NULL AND cad_instrucoes.boleto IS NULL AND sacados.nome IS NOT NULL ";
$sDados .= "ORDER BY id DESC";
$qDados = mysql_query($sDados) or die(mysql_error());
if(mysql_num_rows($qDados) > 0){ // maior que um para não retornar resultado vazio
	while($aDados = mysql_fetch_array($qDados)){
		
		switch ($aDados['comando']) {
			case '06':
			$comando = "Alteração de Vencimento";
			break;
			
			case '10':
			$comando = "Cancelamento/Sustação da instruções de Pr";
			break;

			case '04':
		 	$comando = "Concessão de abatimento";
		 	break;
					 	
		 	case '35':
		 	$comando = "Multa";
		 	break;
					 	
		 	case '09':
		 	$comando = "Protestar";
		 	break;
					 	
		 	case '02':
		 	$comando = "Solicitação de Baixa";
		 	break;
					 	
			case '05':
		 	$comando = "Cancelamento de Abatimento";
		 	break;
					 	
			case '32':
		 	$comando = "Cancelamento de Desconto";
		 	break;
					 	
			case '31':
		 	$comando = "Concessão de desconto";
		 	break;
					 	
		 	case '12':
		 	$comando = "Cliente-Alteração de endereço";
			break;
				 	
			default:
			$comando = "Comando desconhecido";
		}
		
		if($aDados['status'] == 3){
			$status = "Rej.";
		}
		else {
			$status = "OK";
		}
		$pdf->SetWidths(array(43,28,28,18,18,37,15));
		$pdf->SetFont('Arial','',8);
		$pdf->Row(array($aDados['nome_geral'],$aDados['documento'],$aDados['nossonumero'],$aDados['data_instr'],$aDados['data_envio'],$comando,$status));
	}
}
else{
	$pdf->SetFont('DejaVu','',10);
	$pdf->Cell(187,15,'Cliente não possui instruções',0,1,'C');
}

$pdf->Output();
?>