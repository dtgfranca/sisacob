<?php
ini_set("max_execution_time", 3600);
include('../../config.php');
include('../limpa.php');
require('../tfpdf.php');
session_start();
mysql_query("SET NAMES UTF8") or die(mysql_error());
function Reais($num, $cifrao = true){
	$retorno = "";
	
	if ($cifrao)
	$retorno .= "R$ ";
	
	$retorno .= number_format($num, 2, ",", ".");
	return $retorno;
}

class PDF_MC_Table extends tFPDF{
	function Header(){	
		$dData = $_POST['data'];
		$agencia = $_SESSION['userAgencia'];
		$conta = $_SESSION['userConta'];			
		$razao = $_SESSION['userNome'];			
			
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',20);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,42,15); // logo do relatório
		$this->Cell(42);
		$this->Cell(105,20,'RELATÓRIO DE TARIFAS',0,0,'C'); // título do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);
		$this->Cell(40,10,'',0,1);
		$this->Cell(147);			
		$this->Cell(40,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(147);			
		$this->Cell(40,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(42);
		$this->SetTextColor(0,127,0);			
		$this->SetFont('DejaVu','B',12);
		$this->Cell(105,5,'TARIFAS DE '.$dData,0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(40,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);
		$this->Cell(187,5,'Cliente: '.$razao,0,1);			
		$this->Cell(30,5,'Agência: '.$agencia,0,0);			
		$this->Cell(157,5,'Conta: '.$conta,0,1);	
		$this->Ln(2);
		$this->SetFont('DejaVu','B',9);
		$this->SetFillColor(226,226,226);
		$this->SetTextColor(0,0,0);
		$this->Cell(30,6,'Nosso Número',1,0,'L',true);
		$this->Cell(46,6,'Sacado',1,0,'L',true);
		$this->Cell(28,6,'Vencimento',1,0,'L',true);
		$this->Cell(28,6,'Valor Título',1,0,'L',true);
		$this->Cell(35,6,'Descrição',1,0,'L',true);
		$this->Cell(20,6,'Tarifa',1,1,'L',true);
	}
	
	var $widths;
	var $aligns;

	function SetWidths($w){
	//Set the array of column widths
		$this->widths=$w;
	}
	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}
	function Row($data){
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function CheckPageBreak($h){
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
	function NbLines($w,$txt){
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n"){
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax){
				if($sep==-1){
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}


$pdf=new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];
$dData = $_POST['data'];
$aData = explode('/',$dData);
$dIData = $aData[2].'-'.$aData[1].'-'.$aData[0];
$nTotal = 0.00;

$sDados = "SELECT tit.nossonumero, s.nome, DATE_FORMAT(tit.data_venc, '%d/%m/%Y') AS data_venc, tit.valor, ";
$sDados .= "tar.valor_tarifa, tar.descricao_tarifa ";
$sDados .= "FROM titulos tit, sacados s, inf_tarifa tar ";
$sDados .= "WHERE tar.numero_titulo = tit.titulo AND tit.sacado=s.sacado AND tit.agencia='$agencia' ";
$sDados .= "AND tit.cliente='$cliente' AND tar.datatarifa='$dIData'";
$qDados = mysql_query($sDados) or die(mysql_error());
if(mysql_num_rows($qDados) > 1){ // maior que um para não retornar resultado vazio
$pdf->SetWidths(array(30,46,28,28,35,20));
	while($aDados = mysql_fetch_array($qDados)){
		$nTotal += $aDados['valor_tarifa'];
		
		$pdf->SetFont('Arial','',8);
		$pdf->Row(array($aDados['nossonumero'],$aDados['nome'],$aDados['data_venc'],Reais($aDados['valor']),$aDados['descricao_tarifa'],Reais($aDados['valor_tarifa'])));
	}
}
else{
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(187,15,'Cliente não possui tarifas',0,1,'C');
}
$pdf->Ln(2);
$pdf->SetFillColor(226,226,226);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(30,6,'Total Tarifas ==>',1,0,'L',true);
$pdf->Cell(157,6,Reais($nTotal),1,0,'R',true);
$pdf->Output();
?>
