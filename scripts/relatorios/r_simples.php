<?php

ini_set("max_execution_time", 3600);
include('../../config.php');
include('../limpa.php');
require('../tfpdf.php');

session_start();
mysql_query("SET NAMES UTF8") or die(mysql_error());
$_GET = sanitize($_POST);

function Reais($num, $cifrao = true){
	$retorno = "";

	if ($cifrao)
	$retorno .= "R$ ";

	$retorno .= number_format($num, 2, ",", ".");
	return $retorno;
}
function getStatus($status,$nossonumero =""){

	$cmd=	mysql_query("select compl_status, cancelamento from titulos where nossonumero ='$nossonumero'");
    $dados = mysql_fetch_assoc($cmd);

	if ($status == '02' || $status == '12' || $status == '13' || $status == '14' || $status == '16' || $status == '19' || $status == '21' || $status == '22' || $status == '25' || $status == '28' || $status == '31' || $status == '32' || $status == '33' || $status == '34' || $status == '35' || $status == '36' || $status == '37' || $status == '38' || $status == '39' || $status == '44' || $status == '46' || $status == '76'  ){
		$status = '11';
	}elseif ($status == '20'){
		$status = '06';
	}elseif ($status == '98'){
		$status = '23';
	}elseif($status =='96'){

        if( $dados['compl_status'] ==null){
        	$status  ='15';
        }elseif($dados['compl_status'] =="15"){
        	$status ='110';

        }


	}elseif($status =="15" && $dados['compl_status']=="15"){
		$status="110";
	}elseif($status =="01" && ($dados['cancelamento'] != null )){
			$status ="99";
	}elseif($status =="01" && ($dados['cancelamento'] == null) ){
			$status ="01";
	}


    $desc['01']='ANDAMENTO';
	$desc['02']='REGISTRO DE TITULO';
	$desc['03']='COMANDO RECUSADO';
	$desc['05']='LIQUIDADO SEM REGISTRO';
	$desc['06']='LIQUIDACAO NORMAL';
	$desc['07']='LIQUIDADO POR CONTA';
	$desc['08']='LIQUIDADO POR SALDO';
	$desc['09']='BAIXA DO TITULO';
	$desc['10']='BAIXA SOLICITADA';
	$desc['11']='TITULO EM SER';
	$desc['12']='ABATIMENTO CONCEDIDO';
	$desc['13']='ABATIMENTO CANCELADO';
	$desc['14']='ALTERACAO VENCIMENTO';
	$desc['15']='LIQUIDADO EM CARTORIO';
	#$desc['15']='BAIXADO/PROTESTO';
	$desc['16']='ALTERACAO JUROS DE MORA';
	$desc['19']='INSTRUCAO PROTESTO';
	$desc['20']='DEBITO EM CONTA';
	$desc['21']='ALTERACAO DO NOME DO SACADO';
	$desc['22']='ALTERACAO ENDERECO';
	$desc['23']='ENCAMINHAMENTO A CARTORIO';
	$desc['24']='SUSTACAO PROTESTO';
	$desc['25']='DISPENSA JUROS DE MORA';
	$desc['28']='MANUTENCAO DE TITULO VENCIDO';
	$desc['31']='CONCESSAO DESCONTO';
	$desc['32']='CANCELAMENTO CONCESSAO DESCONTO';
	$desc['33']='RETIFICAR DESCONTO';
	$desc['34']='ALTERAR DATA PARA DESCONTO';
	$desc['35']='COBRAR MULTA';
	$desc['36']='DISPENSAR MULTA';
	$desc['37']='DISPENSAR INDEXADOR';
	$desc['38']='DISPENSAR PRAZO LIMITE PARA RECEBIMENTO';
	$desc['39']='ALTERAR PRAZO LIMITE PARA RECEBIMENTO';
	$desc['44']='TITULO PAGO COM CHEQUE DEVOLVIDO';
	$desc['46']='TITULO PAGO COM CHEQUE AGUARDANDO COMPENSACAO';
	$desc['76']='ALTERCAO DE TIPO DE COBRANCA';
	$desc['96']='DEPESAS DE PROTESTO';
	$desc['97']='DESPESA SUSTACAO PROTESTO';
	$desc['98']='DEBITO DE CUSTAS ANTECIPADAS';
	$desc['88']='RECUSADO P/ DESCONTO';
	$desc['99']='CANCELADO';
	$desc['110']='BAIXADO/PROTESTO';
	if (array_key_exists($status, $desc)){

		return $desc[$status];
	}
	else{
		return "DESCONHECIDO";
	}
}

class PDF_MC_Table extends tFPDF{
	function Header(){
		$agencia = $_SESSION['userAgencia'];
		$cliente = $_SESSION['userCliente'];
		$conta = $_SESSION['userConta'];
		$razao = $_SESSION['userNome'];
		$tipo = $_POST['tipo'];
		$filtro = $_POST['filtro'];

		// Remove o HTML da cor da fonte
		$razao = str_replace('<b><font color="#f00">', '', $razao);
		$razao = str_replace('</font></b>', '', $razao);

		if ($_POST['inicio'] != '' && $_POST['fim'] != '')
		{
			if($filtro=="data_emisao"){
				$value_periodo = "EMISSÃO DE ".$_POST['inicio']." A ".$_POST['fim'];
			}
			else if($filtro=="data_venc"){
				$value_periodo = "VENCIMENTO DE ".$_POST['inicio']." A ".$_POST['fim'];
			}
			else if($filtro=="data_baixa"){
				$value_periodo = "BAIXA DE ".$_POST['inicio']." A ".$_POST['fim'];
			}
		}
		else
			$value_periodo = '';

		if($tipo=="todos"){
   			$value_tipo = "RELATÓRIO SIMPLES";
		}
		else if($tipo=="rec"){
			$value_tipo = "RECEBIDOS";
		}
		else if($tipo=="baixa"){
		   	$value_tipo = "BAIXADOS";
		}
		else if($tipo=="baixa_protesto"){
		   	$value_tipo = "BAIXADO/PROTESTO";
		}
		else if($tipo=="pend"){
		   	$value_tipo = "EM ABERTO";
		}
		else if($tipo=="venc"){
		   	$value_tipo = "VENCIDOS";
		}
		else if($tipo=="cancel"){
		   	$value_tipo = "CANCELADOS";
		}
		else if($tipo=="lista_previa"){
			$value_tipo = "LISTAGEM PRÉVIA";
		}
		else if($tipo=="rejeitados"){
			$value_tipo = "REJEITADOS";
		}
		else if($tipo=="nosso_n"){
			$nosso_n = $_POST['nosso_n'];
			$value_tipo = "NOSSO NUMERO ".$nosso_n;
			$value_periodo = '';
		}
		else if($tipo=="n_doc"){
			$nosso_n = $_POST['n_titulo'];
			$value_tipo = "Documento ".$nosso_n;
			$value_periodo = '';
		}

		if($agencia == '4117'){
			$tam = '42,15';
		}
		else{
			$tam ='42,15';
		}

		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',16);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,$tam); // logo do relatório
		$this->Cell(61);
		$this->Cell(153,10,'RELATÓRIO DE BOLETOS',0,0,'C'); // título do relatório
		$this->Cell(61,10,'',0,1);
		$this->Cell(61);
		$this->SetFont('DejaVu','B',13);
		$this->Cell(153,10,$value_tipo,0,0,'C');
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);
		$this->Cell(61,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(214);
		$this->Cell(61,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(61);
		$this->SetTextColor(0,127,0);
		$this->SetFont('DejaVu','B',10);
		$this->Cell(153,10,$value_periodo,0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);
		$this->Cell(61,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);
		$this->Cell(275,5,'Cliente: '.$razao,0,1);
		$this->Cell(30,5,'Agência: '.$agencia,0,0);
		$this->Cell(245,5,'Conta: '.$conta,0,1);
		$this->Ln(2);
		$this->SetFont('DejaVu','B',8);
		$this->SetFillColor(226,226,226);
		$this->Cell(62,6,'Nome',1,0,'C',true);
		$this->Cell(20,6,'Documento',1,0,'C',true);
		$this->Cell(33,6,'Nosso Número',1,0,'C',true);
		$this->Cell(17,6,'Emissão',1,0,'C',true);
		$this->Cell(17,6,'Venc.',1,0,'C',true);
		$this->Cell(17,6,'Pag.',1,0,'C',true);
		$this->Cell(17,6,'Dt.Baixa',1,0,'C',true);
		$this->Cell(32,6,'Status',1,0,'C',true);
		$this->Cell(30,6,'Valor',1,0,'C',true);
		$this->Cell(30,6,'Recebido',1,1,'C',true);
	}
	var $widths;
	var $aligns;

	function SetWidths($w){
	//Set the array of column widths
		$this->widths=$w;
	}
	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}
	function Row($data){
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function CheckPageBreak($h){
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
	function NbLines($w,$txt){
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n"){
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax){
				if($sep==-1){
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}

$pdf = new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4');

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

if(empty($_POST['inicio']) || empty($_POST['fim'])){
	$b_data = "";
}
else {
	$inicio = explode('/',$_POST['inicio']);
	$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
	$fim = explode('/',$_POST['fim']);
	$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];
}

if(!empty($_POST['cliente'])){$sacado = $_POST['cliente'];}
$tipo   = $_POST["tipo"];
$tipo2  = $_POST["tipo2"];
$ordem  = $_POST["ordem"];
$filtro = $_POST["filtro"];

if ($inicio != "" && $fim != "")
{
	if($filtro == "data_baixa"){
		$b_data = "AND ((t.data_baixa>='$inicio' AND t.data_baixa<='$fim') OR (t.data_baixa_manual>='$inicio' AND t.data_baixa_manual<='$fim'))";
	}
	elseif($filtro == "data_venc"){
		$b_data = "AND t.data_venc BETWEEN '{$inicio}' AND '{$fim}'";
	}
	elseif($filtro == "data_emisao"){
		$b_data = "AND t.data_emisao BETWEEN '{$inicio}' AND '{$fim}'";
	}
	else {
		$b_data = "";
	}
}
else {
	$b_data = "";
}
if($tipo == "rec"){
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND (status='05' OR status='06' OR status='07' OR status='08' OR status='15' or( status='96' and compl_status is null))";
}
else if($tipo == "baixa"){
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND t.devolucao IS NOT NULL AND (t.status='09' OR t.status='10' OR(t.status='15'  AND compl_status ='15'))";
}elseif($tipo =="baixa_protesto"){

   $b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND t.devolucao IS NOT NULL AND (t.status='96'  AND compl_status ='15')";

}
else if($tipo=="pend"){
	$b_tipo="AND (t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL)";
}
else if($tipo=="venc"){
	$b_tipo="AND t.data_venc < CURDATE() AND t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL";
}
else if($tipo=="rejeitados"){
	$b_tipo="AND t.status='03' AND t.cancelamento IS NULL";
}
else if($tipo=="nosso_n"){
	$nosso_n = $_POST['nosso_n'];
	$b_tipo="AND t.nossonumero LIKE '%".$nosso_n."%'";
}
else if($tipo=="n_doc"){
	$n_titulo = $_POST['n_titulo'];
	$b_tipo="AND t.documento LIKE '%".$n_titulo."%'";
}
else if($tipo=="cancel"){
	$b_tipo="AND t.cancelamento IS NOT NULL";
}
else if($tipo=="lista_previa"){
    $b_tipo = "AND cad_completo <> 'S' AND status <> '03' AND status <> '99' AND cancelamento IS NULL ";
}
else {
	$b_tipo = "AND cad_completo = 'S' ";
}
if(empty($_POST['tipo2'])){
	$b_tipo2 = "";
}
else{
	$tipo2	= $_POST['tipo2'];
	switch($tipo2){
		case "desc":
			if($tipo == "rec"){
				$b_tipo2 = "AND t.desconto IS NOT NULL AND so_desconto='S'";
			}
			else if($tipo == "todos"){
				$b_tipo2 = "AND so_desconto='S'";
			}
			else{
				$b_tipo2 = "AND so_desconto='S'";
			}
		break;
		case "cobranca_simples":
			if($tipo == "rec"){
				$b_tipo2 = "AND so_desconto='N'";
			}else if($tipo == "todos"){
				$b_tipo2 = "AND so_desconto='N'";
			}else{
				$b_tipo2 = "AND so_desconto='N'";
			}
		break;
	}
}
if(!empty($sacado)){
	$b_sacado = "AND t.sacado='$sacado'";
}
else{
	$b_sacado="";
}
$query = "select t.titulo,t.status, t.documento, t.devolucao, t.nossonumero, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, t.valor, t.modelo, t.sequencia, ";
$query .= "DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') as data_baixa, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito, ";
$query .= "t.valor_baixa, t.sacado, s.nome ";
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.cliente='$cliente' and data_emisao is not null $b_sacado $b_data $b_tipo $b_tipo2 order by $ordem;";



if (false)//($_SERVER['REMOTE_ADDR'] == '186.244.33.114')
die($query);

$sql = mysql_query($query)or die (mysql_error());

if(mysql_num_rows($sql)>0){
	$pdf->SetWidths(array(62,20,33,17,17,17,17,32,30,30));
	$pdf->SetAligns(array('L','C','C','C','C','C','C','R','R','R'));
	$sacado_ant="";
	$titulos = 0;
	while ($linha=mysql_fetch_array($sql)) {
		$sacado=$linha['sacado'];
		$titulos++;
		if($linha['cancelamento']==""){
			$valor_total=$valor_total+$linha['valor'];
		}
		if(!($linha['data_baixa']!=""&&$linha['devolucao']!=""&&($linha['status']=='09'||$linha['status']=='10'))){
			$valor_baixa_total=$valor_baixa_total+$linha['valor_baixa'];
		}
		if($linha['data_credito']==""){
			$data_credito = $linha['data_baixa'];
		}
		else {
			$data_credito = $linha['data_credito'];
		}
		if($linha['status']==9 || $linha['status']==10){
			$pagamento = "";
			$cartorio = $linha['data_baixa'];
		}
		else{
			$pagamento = $linha['data_baixa'];
			$cartorio = "";
		}
		$pdf->SetFont('Arial','',8);
		if($agencia=='4030'){
			$pdf->Row(array($linha['nome'],$linha['documento'],$linha['nossonumero'],$linha['data_emisao'],$linha['data_venc'],$pagamento,$data_credito,strtolower(getStatus($linha['status'],$linha['nossonumero'])),Reais($linha['valor']),Reais($linha['valor_baixa'])));
		}
		else{
			$pdf->Row(array($linha['nome'],$linha['documento']."/".$linha['sequencia'],$linha['nossonumero'],$linha['data_emisao'],$linha['data_venc'],$pagamento,$data_credito,strtolower(getStatus($linha['status'],$linha['nossonumero'])),Reais($linha['valor']),Reais($linha['valor_baixa'])));
		}
	}
	$pdf->Ln(2);
	$pdf->SetFont('DejaVu','B',8);
	$pdf->SetFillColor(226,226,226);
	$pdf->Cell(62,6,'Totais =>',1,0,'L',true);
	$pdf->Cell(20,6,$titulos.' títulos',1,0,'R',true);
	$pdf->Cell(133,6,'',1,0,'R',true);
	$pdf->Cell(30,6,Reais($valor_total),1,0,'R',true);
	$pdf->Cell(30,6,Reais($valor_baixa_total),1,1,'R',true);
}
else{
	$pdf->Cell(275,15,'Nenhum lançamento',1,1,'C');
}

$pdf->Output();
?>

