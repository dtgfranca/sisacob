<?php

include_once('class/tcpdf/tcpdf.php');
include_once("class/PHPJasperXML.inc.php");
include_once ('setting.php');


session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}
$agencia = $_SESSION['userAgencia'];

mysql_connect($server,$user,$pass) or die(mysql_error());
mysql_select_db($db) or die(mysql_error());	

if($agencia == '4117'){
	$xml = simplexml_load_file("r_sintetico_g.jrxml"); //informe onde está seu arquivo jrxml
}
else{
	$xml = simplexml_load_file("r_sintetico.jrxml"); //informe onde está seu arquivo jrxml
}


$PHPJasperXML = new PHPJasperXML();

$PHPJasperXML->debugsql=false;

//recebendo os parâmetros
$data = date('d/m/Y');
$hora = date('H:i:s');
$cliente = $_SESSION['userNome'];
$conta = $_SESSION['userConta'];
$codcliente = $_SESSION['userCliente'];
$logo = '../../img/logo/logo_'.$agencia.'.jpg';
$sacado = $_POST['cliente'];
$data = date('d/m/Y');
$carteira = "CARTEIRA - DE ".$data;
$dData = date('Y-m-d');
$sDados = "SELECT tit.titulo, tit.agencia, tit.cliente, tit.sacado, tit.documento, tit.sequencia, tit.nossonumero, ";
$sDados .= "tit.data_emisao, tit.data_venc, DATEDIFF(tit.data_venc,DATE_FORMAT(NOW(),'%Y-%m-%d')) AS vencido, ";
$sDados .= "tit.valor, tit.data_baixa, tit.data_credito, tit.valor_baixa, ";
$sDados .= "tit.data_baixa_manual, tit.cancelamento, tit.criacao, tit.desconto, ";
$sDados .= "tit.devolucao, tit.so_desconto, tit.status, cli.razao, cli.conta, tit.registro ";
$sDados .= "FROM titulos AS tit ";
$sDados .= "LEFT JOIN clientes AS cli ON tit.cliente=cli.cliente ";
$sDados .= "WHERE data_baixa IS NULL AND cancelamento IS NULL and cad_completo='S' AND tit.status != '03' ";
$sDados .= "AND registro IS NOT NULL and tit.cliente='".$codcliente."'";
$qDados = mysql_query($sDados) or die(mysql_error());

$simples          = 0;
$valor_simples    = 0;
$descontada       = 0;
$valor_descontada = 0;
$cobranca         = 0;
$valor_cobranca   = 0;
$registro         = 0;
$valor_registro   = 0;
$liquidados       = 0;
$valor_liquidados = 0;
$cartorio         = 0;
$valor_cartorio   = 0;
$baixados         = 0;
$valor_baixados   = 0;
$vencidos         = 0;
$valor_vencidos   = 0;
$avencer          = 0;
$valor_avencer    = 0;

while($aDados = mysql_fetch_array($qDados)){
	if($aDados['so_desconto']=="S" && $aDados['data_baixa'] == ""){
		$descontada++;		
		$valor_descontada += $aDados['valor'];
	}
	else if($aDados['so_desconto']=="N" && $aDados['data_baixa'] == ""){
		$simples++;
		$valor_simples += $aDados['valor'];
	}
	if($aDados['registro']!=""){
		$registro++;
		$valor_registro += $aDados['valor'];
	}
	if($aDados['data_baixa'] != "" && $aDados['status']!=9 && $aDados['devolucao'] == ""){
		$liquidados++;
		$valor_liquidados += $aDados['valor'];
	}		
	if($aDados['data_baixa'] != "" && $aDados['status']==9){
		$cartorio++;
		$valor_cartorio += $aDados['valor'];
	}
	if($aDados['devolucao'] != "" && $aDados['status']!=9){
		$baixados++;
		$valor_baixados += $aDados['valor_baixa'];
	}		
	if($aDados['vencido'] < 0 && $aDados['data_baixa'] == ""){
		$vencidos++;
		$valor_vencidos += $aDados['valor'];
	}		
	if($aDados['vencido'] >= 0 && $aDados['data_baixa'] == ""){
		$avencer++;
		$valor_avencer += $aDados['valor'];
	}
}
$cobranca = $descontada + $simples;
$valor_cobranca = $valor_simples + $valor_descontada;



$PHPJasperXML->arrayParameter=array("cCarteira"=>$carteira, "nAgencia"=>$agencia, "cCliente"=>$cliente, "nCliente"=>$codcliente, "nConta"=>$conta, "cLogo"=>$logo, "dDataSis"=>$data, "dHoraSis"=>$hora, "dData"=>$dData, "nCob"=>$cobranca, "tCob"=>$valor_cobranca, "nCobS"=>$simples, "tCobS"=>$valor_simples, "nCobD"=>$descontada, "tCobD"=>$valor_descontada, "nEnt"=>$registro, "tEnt"=>$valor_registro, "nLiq"=>$liquidados, "tLiq"=>$valor_liquidados, "nLiqC"=>$cartorio, "tLiqC"=>$valor_cartorio, "nBai"=>$baixados, "tBai"=>$valor_baixados, "nVenc"=>$vencidos, "tVenc"=>$valor_vencidos, "nAVen"=>$avencer, "tAVen"=>$valor_avencer); //passa o parâmetro cadastrado no iReport

$PHPJasperXML->xml_dismantle($xml);

$PHPJasperXML->connect($server,$user,$pass,$db);

$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);

$PHPJasperXML->outpage("I");

?> 