<?php
error_reporting(0);

ini_set("max_execution_time", 3600);
include '../../config.php';
include '../limpa.php';
include_once 'setting.php';
$cooperativas = parse_ini_file('../../cooperativas.ini');
session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}
mysql_query("SET NAMES UTF8") or die(mysql_error());
$_GET = sanitize($_POST);
$arquivo = "r_simples.jrxml";
$report_unit = "/reports/$reportspath/";
$nome = explode(".", $arquivo);
$report_name = $nome[0];

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];
$nomeCliente = strip_tags($_SESSION['userNome']);
$tipo = $_POST['tipo'];

$filtro = $_POST['filtro'];
$logo = function () {

	$query = "SELECT razao FROM agencias WHERE agencia ={$_SESSION['userAgencia']}";
	$cmd = mysql_query($query);
	$array = mysql_fetch_assoc($cmd);
	return str_replace("SICOOB", "", $array['razao']);
};

// Remove o HTML da cor da fonte
#$razao = str_replace('<b><font color="#f00">', '', $razao);
#$razao = str_replace('</font></b>', '', $razao);

if ($_POST['inicio'] != '' && $_POST['fim'] != '') {
	if ($filtro == "data_emisao") {
		$value_periodo = "EMISSÃO DE " . $_POST['inicio'] . " A " . $_POST['fim'];
	} else if ($filtro == "data_venc") {
		$value_periodo = "VENCIMENTO DE " . $_POST['inicio'] . " A " . $_POST['fim'];
	} else if ($filtro == "data_baixa") {
		$value_periodo = "BAIXA DE " . $_POST['inicio'] . " A " . $_POST['fim'];
	}
} else {
	$value_periodo = '';
}

if ($tipo == "todos") {
	$value_tipo = "RELATÓRIO SIMPLES";
} else if ($tipo == "rec") {
	$value_tipo = "RECEBIDOS";
} else if ($tipo == "baixa") {
	$value_tipo = "BAIXADOS";
} else if ($tipo == "pend") {
	$value_tipo = "EM ABERTO";
} else if ($tipo == "venc") {
	$value_tipo = "VENCIDOS";
} else if ($tipo == "cancel") {
	$value_tipo = "CANCELADOS";
} else if ($tipo == "lista_previa") {
	$value_tipo = "LISTAGEM PRÉVIA";
} else if ($tipo == "rejeitados") {
	$value_tipo = "REJEITADOS";
} else if ($tipo == "nosso_n") {
	$nosso_n = $_POST['nosso_n'];
	$value_tipo = "NOSSO NUMERO " . $nosso_n;
	$value_periodo = '';
} else if ($tipo == "n_doc") {
	$nosso_n = $_POST['n_titulo'];
	$value_tipo = "Documento " . $nosso_n;
	$value_periodo = '';
}




if (empty($_POST['inicio']) || empty($_POST['fim'])) {
	$b_data = "";
} else {
	$inicio = explode('/', $_POST['inicio']);
	$inicio = $inicio[2] . '-' . $inicio[1] . '-' . $inicio[0];
	$fim = explode('/', $_POST['fim']);
	$fim = $fim[2] . '-' . $fim[1] . '-' . $fim[0];
}

if (!empty($_POST['cliente'])) {$sacado = $_POST['cliente'];}
$tipo = $_POST["tipo"];
$tipo2 = $_POST["tipo2"];
$ordem = $_POST["ordem"];
$filtro = $_POST["filtro"];

if ($inicio != "" && $fim != "") {
	if ($filtro == "data_baixa") {
		$b_data = "AND ((t.data_baixa>='$inicio' AND t.data_baixa<='$fim') OR (t.data_baixa_manual>='$inicio' AND t.data_baixa_manual<='$fim'))";
	} elseif ($filtro == "data_venc") {
		$b_data = "AND t.data_venc BETWEEN '{$inicio}' AND '{$fim}'";
	} elseif ($filtro == "data_emisao") {
		$b_data = "AND t.data_emisao BETWEEN '{$inicio}' AND '{$fim}'";
	} else {
		$b_data = "";
	}
} else {
	$b_data = "";
}
if ($tipo == "rec") {
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND (status='05' OR status='06' OR status='07' OR status='08' OR status='15')";
} else if ($tipo == "baixa") {
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND t.devolucao IS NOT NULL AND (t.status='09' OR t.status='10')";
} else if ($tipo == "pend") {
	$b_tipo = "AND (t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL)";
} else if ($tipo == "venc") {
	$b_tipo = "AND t.data_venc < CURDATE() AND t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL";
} else if ($tipo == "rejeitados") {
	$b_tipo = "AND t.status='03' AND t.cancelamento IS NULL";
} else if ($tipo == "nosso_n") {
	$nosso_n = $_POST['nosso_n'];
	$b_tipo = "AND t.nossonumero LIKE '%" . $nosso_n . "%'";
} else if ($tipo == "n_doc") {
	$n_titulo = $_POST['n_titulo'];
	$b_tipo = "AND t.documento LIKE '%" . $n_titulo . "%'";
} else if ($tipo == "cancel") {
	$b_tipo = "AND t.cancelamento IS NOT NULL";
} else if ($tipo == "lista_previa") {
	$b_tipo = "AND cad_completo <> 'S' AND status <> '03' AND status <> '99' AND cancelamento IS NULL ";
} else {
	$b_tipo = "";
	#$b_tipo = "AND cad_completo = 'S' ";
}
if (empty($_POST['tipo2'])) {
	$b_tipo2 = "";
} else {
	$tipo2 = $_POST['tipo2'];
	switch ($tipo2) {
	case "desc":
		if ($tipo == "rec") {
			$b_tipo2 = "AND t.desconto IS NOT NULL AND so_desconto='S'";
		} else if ($tipo == "todos") {
			$b_tipo2 = "";
		} else {
			$b_tipo2 = "AND so_desconto='S'";
		}
		break;
	case "cobranca_simples":
		if ($tipo == "rec") {
			$b_tipo2 = "AND so_desconto='N'";
		} else if ($tipo == "todos") {
			$b_tipo2 = "AND so_desconto='N'";
		} else {
			$b_tipo2 = "AND so_desconto='N'";
		}
		break;
	}
}
if (!empty($sacado)) {
	$b_sacado = "AND t.sacado='$sacado'";
} else {
	$b_sacado = "";
}
if(in_array($_SESSION['userAgencia'],$cooperativas)){



	$query = "select t.titulo,getStatus(t.status) as status , t.documento, t.devolucao, t.nossonumero, coalesce(DATE_FORMAT(t.data_emisao, '%d/%c/%Y'),'-') as data_emisao, ";
$query .= "coalesce(DATE_FORMAT(t.data_venc, '%d/%c/%Y'),'-') as data_venc, if(t.valor is null ,0.00,t.valor)as valor, t.modelo, t.sequencia, coalesce(DATE_FORMAT(t.data_credito, '%d/%c/%Y'),'-') as data_credito , ";
$query .= "IF(DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') is null, '-',DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y')) as data_baixa, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito, ";
$query .= " if(t.valor_baixa is null,0.00,t.valor_baixa) as valor_baixa, t.sacado, coalesce(s.nome,'-') as nome ";
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.cliente='$cliente'  $b_sacado $b_data $b_tipo  order by $ordem ;";
die($query);

}else{

	$query = "select t.titulo,getStatus(t.status) as status , t.documento, t.devolucao, t.nossonumero, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, if(t.valor is null ,0.00,t.valor)as valor, t.modelo, t.sequencia, ";
$query .= "IF(DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') is null, '-',DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y')) as data_baixa, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito, ";
$query .= " if(t.valor_baixa is null,0.00,t.valor_baixa) as valor_baixa, t.sacado, s.nome ";
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.cliente='$cliente'  $b_sacado $b_data $b_tipo $b_tipo2 order by $ordem ;";


}





$arrayParameter = array("nAgencia" => $agencia, "cCliente" => $nomeCliente, "cLogo" => $logo(), "cTitulo" => $value_tipo, "cPeriodo" => $value_periodo, "nConta" => $conta, "cQuery" => $query); //passa o parâmetro cadastrado no iReport

function montaParametro(array $parametos) {
	$url = "&";
	foreach ($parametos as $key => $value) {
		$url .= $key . "=" . rawurlencode($value) . "&";
	}
	return substr($url, 0, strlen($url) - 1);
}

$url = $jasper_url . "?_flowId=viewReportFlow&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=" . rawurlencode($report_unit) . "&reportUnit=" . rawurlencode($report_unit . $report_name) . montaParametro($arrayParameter) . "&decorate=no&j_username=$jasper_username&j_password=$jasper_password&theme=theme_embbed";

header("Location: $url");

?>
