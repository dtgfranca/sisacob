<?php

include_once('class/tcpdf/tcpdf.php');
include_once("class/PHPJasperXML.inc.php");
include_once ('setting.php');

session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}
$agencia = $_SESSION['userAgencia'];

if($agencia == '4117'){
	$xml = simplexml_load_file("r_tarifa_g.jrxml"); //informe onde está seu arquivo jrxml
}
else{
	$xml = simplexml_load_file("r_tarifa.jrxml"); //informe onde está seu arquivo jrxml
}

$PHPJasperXML = new PHPJasperXML();

$PHPJasperXML->debugsql=false;

//recebendo os parâmetros
session_start();
$data = date('d/m/Y');
$hora = date('H:i:s');
$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userNome'];
$conta = $_SESSION['userConta'];
$codcliente = $_SESSION['userCliente'];
$logo = '../../img/logo/logo_'.$agencia.'.jpg';
$dInicio = $_POST['inicio'];
$dFim = $_POST['fim'];
if(!empty($_POST['inicio']) && !empty($_POST['fim'])){
	$inicio = explode('/',$dInicio);
	$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
	$fim = explode('/',$dFim);
	$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];
	$filtro = "AND tar.datatarifa>='$inicio' AND tar.datatarifa<='$fim'";
}
else {
	$filtro = "";
}



$PHPJasperXML->arrayParameter=array("nAgencia"=>$agencia, "cCliente"=>$cliente, "nCliente"=>$codcliente, "nConta"=>$conta, "cLogo"=>$logo, "dDataSis"=>$data, "dHoraSis"=>$hora, "dInicio"=>$dInicio, "dFim"=>$dFim, "filtro"=>$filtro); //passa o parâmetro cadastrado no iReport

$PHPJasperXML->xml_dismantle($xml);

$PHPJasperXML->connect($server,$user,$pass,$db);

$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);

$PHPJasperXML->outpage("I");

?> 