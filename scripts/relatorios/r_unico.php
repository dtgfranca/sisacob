	<?php

ini_set("max_execution_time", 3600);
include('../../config.php');
require('../tfpdf.php');	
session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}

function Reais($num, $cifrao = true){
	$retorno = "";
	
	if ($cifrao)
	$retorno .= "R$ ";
	
	$retorno .= number_format($num, 2, ",", ".");
	return $retorno;
}
function getStatus($status){	
	if ($status == '02' || $status == '12' || $status == '13' || $status == '14' || $status == '16' || $status == '19' || $status == '21' || $status == '22' || $status == '25' || $status == '28' || $status == '31' || $status == '32' || $status == '33' || $status == '34' || $status == '35' || $status == '36' || $status == '37' || $status == '38' || $status == '39' || $status == '44' || $status == '46' || $status == '76' || $status == '96' ){
		$status = '11';
	} 
	if ($status == '20'){
		$status = '06';
	}	
	if ($status == '98'){
		$status = '23';
	}	
	$desc['01']='ANDAMENTO';
	$desc['02']='REGISTRO DE TITULO';                           
	$desc['03']='COMANDO RECUSADO';                             
	$desc['05']='LIQUIDADO SEM REGISTRO';                       
	$desc['06']='LIQUIDACAO NORMAL';                            
	$desc['07']='LIQUIDADO POR CONTA';                          
	$desc['08']='LIQUIDADO POR SALDO';                          
	$desc['09']='BAIXA DO TITULO';                              
	$desc['10']='BAIXA SOLICITADA';                             
	$desc['11']='TITULO EM SER';                                
	$desc['12']='ABATIMENTO CONCEDIDO';                         
	$desc['13']='ABATIMENTO CANCELADO';                         
	$desc['14']='ALTERACAO VENCIMENTO';                         
	$desc['15']='LIQUIDADO EM CARTORIO';                        
	$desc['16']='ALTERACAO JUROS DE MORA';                      
	$desc['19']='INSTRUCAO PROTESTO';                          
	$desc['20']='DEBITO EM CONTA';                              
	$desc['21']='ALTERACAO DO NOME DO SACADO';                  
	$desc['22']='ALTERACAO ENDERECO';                           
	$desc['23']='ENCAMINHAMENTO A CARTORIO';                    
	$desc['24']='SUSTACAO PROTESTO';                           
	$desc['25']='DISPENSA JUROS DE MORA';                       
	$desc['28']='MANUTENCAO DE TITULO VENCIDO';                 
	$desc['31']='CONCESSAO DESCONTO';                           
	$desc['32']='CANCELAMENTO CONCESSAO DESCONTO';              
	$desc['33']='RETIFICAR DESCONTO';                           
	$desc['34']='ALTERAR DATA PARA DESCONTO';                   
	$desc['35']='COBRAR MULTA';                                 
	$desc['36']='DISPENSAR MULTA';                              
	$desc['37']='DISPENSAR INDEXADOR';                          
	$desc['38']='DISPENSAR PRAZO LIMITE PARA RECEBIMENTO';      
	$desc['39']='ALTERAR PRAZO LIMITE PARA RECEBIMENTO';        
	$desc['44']='TITULO PAGO COM CHEQUE DEVOLVIDO';             
	$desc['46']='TITULO PAGO COM CHEQUE AGUARDANDO COMPENSACAO';
	$desc['76']='ALTERCAO DE TIPO DE COBRANCA';                 
	$desc['96']='DEPESAS DE PROTESTO';                          
	$desc['97']='DESPESA SUSTACAO PROTESTO';                    
	$desc['98']='DEBITO DE CUSTAS ANTECIPADAS';     
	$desc['88']='RECUSADO P/ DESCONTO';     
	$desc['99']='CANCELADO';    	
	if (array_key_exists($status, $desc)){
		return $desc[$status];
	}
	else{
		return "DESCONHECIDO";
	}
}
	
class PDF_MC_Table extends tFPDF{
	function Header(){	
		$agencia = $_SESSION['userAgencia'];			
			
		if($agencia == '4117'){
			$tam = '42,15';
		}
		else{
			$tam ='42,15';
		}
			
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',16);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,$tam); // logo do relatório
		$this->Cell(61);
		$this->Cell(153,10,'RELATÓRIO DE BOLETOS',0,0,'C'); // título do relatório
		$this->Cell(61,10,'',0,1);		
		$this->Cell(61);
		$this->SetFont('DejaVu','B',13);
		$this->Cell(153,10,'Liquidados Indevidamente',0,0,'C');
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(61,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(214);			
		$this->Cell(61,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(61);
		$this->SetTextColor(0,127,0);			
		$this->SetFont('DejaVu','B',10);
		$this->Cell(153,10,'',0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(61,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);			
		$this->Cell(30,5,'Agência: '.$agencia,0,1);	
		$this->Ln(2);
		$this->SetFont('DejaVu','B',8);
		$this->SetFillColor(226,226,226);
		$this->Cell(75,6,'Nome',1,0,'C',true);
		$this->Cell(20,6,'Documento',1,0,'C',true);
		$this->Cell(33,6,'Nosso Número',1,0,'C',true);
		$this->Cell(21,6,'Emissão',1,0,'C',true);
		$this->Cell(21,6,'Venc.',1,0,'C',true);
		$this->Cell(21,6,'Dt.Baixa',1,0,'C',true);
		$this->Cell(21,6,'Dt.Desc',1,0,'C',true);
		$this->Cell(30,6,'Valor',1,0,'C',true);
		$this->Cell(33,6,'Status',1,1,'C',true);
	}
	var $widths;
	var $aligns;

	function SetWidths($w){
	//Set the array of column widths
		$this->widths=$w;
	}
	function SetAligns($a){
		//Set the array of column alignments
		$this->aligns=$a;
	}
	function Row($data){
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function CheckPageBreak($h){
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
	function NbLines($w,$txt){
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb){
			$c=$s[$i];
			if($c=="\n"){
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax){
				if($sep==-1){
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}

$pdf = new PDF_MC_Table();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4');

$agencia = $_SESSION['userAgencia'];

$query = "SELECT t.titulo, t.status, t.documento, t.devolucao, t.nossonumero, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, t.valor, t.sequencia,";
$query .= "DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') as data_baixa, ";
$query .= "DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito, DATE_FORMAT(t.desconto, '%d/%c/%Y') as data_desc, t.sacado, s.nome ";
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.agencia='$agencia' and data_baixa is null and data_baixa_manual is null and desconto is null and status='06' ";
$query .= "order by criacao";

$sql = mysql_query($query)or die (mysql_error());
	
if(mysql_num_rows($sql)>0){
	$pdf->SetWidths(array(75,20,33,21,21,21,21,30,33));
	$pdf->SetAligns(array('L','C','C','C','C','C','C','R','C'));
	$sacado_ant="";
	$titulos = 0;
	while ($linha=mysql_fetch_array($sql)) {		
		$pdf->SetFont('Arial','',8);
		$pdf->Row(array($linha['nome'],$linha['documento']."/".$linha['sequencia'],$linha['nossonumero'],$linha['data_emisao'],$linha['data_venc'],$linha['data_baixa'],$linha['data_desc'],Reais($linha['valor']),strtolower(getStatus($linha['status']))));
	}	
}
else{
	$pdf->Cell(275,15,'Nenhum lançamento',1,1,'C');
}

$pdf->Output();
?>