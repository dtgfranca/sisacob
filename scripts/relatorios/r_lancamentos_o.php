<?php

ini_set("max_execution_time", 3600);
include('../../config.php');
include('../limpa.php');
require('../tfpdf.php');	
session_start();
mysql_query("SET NAMES UTF8") or die(mysql_error());
$_GET = sanitize($_POST);

function formatCEP($string){
	$output = preg_replace("[' '-./ t]", '', $string);
	$size = (strlen($output));
	
	if ($size != 8) return false;
	$mask = '##.###-###'; 
	$index = -1;
	
	for ($i=0; $i < strlen($mask); $i++):
		if ($mask[$i]=='#') $mask[$i] = $output[++$index];
	endfor;
	return $mask;
}

function Reais($num, $cifrao = true){
	$retorno = "";
	
	if ($cifrao)	
	$retorno .= number_format($num, 2, ",", ".");
	return $retorno;
}
	
class PDF extends tFPDF{
	function Header(){	
		$agencia = $_SESSION['userAgencia'];
		$conta = $_SESSION['userConta'];			
		$cliente = $_SESSION['userCliente'];
		$nome = explode('<',$_SESSION['userNome']);
		$nome = $nome[0];
		
		$qAg = mysql_query("SELECT razao FROM agencias WHERE agencia='$agencia'") or die(mysql_error());
		$aAg = mysql_fetch_array($qAg);
		
		$qCli = mysql_query("SELECT cgc, inscr, endereco, bairro, cep, cidade, estado FROM clientes WHERE cliente='$cliente'") or die(mysql_error());
		$aCli = mysql_fetch_array($qCli);
			
		if($agencia == '4117'){
			$tam = '42,15';
		}
		else{
			$tam ='42,15';
		}
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',16);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,$tam); // logo do relatório
		$this->Cell(42);
		$this->Cell(106,10,'RELATÓRIO DE LANÇAMENTOS',0,0,'C'); // título do relatório
		$this->Cell(42,10,'',0,1);		
		$this->Cell(42);
		$this->SetFont('DejaVu','B',13);
		$this->Cell(106,10,'01 - COBRANÇAS SIMPLES',0,0,'C');
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(42,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(148);			
		$this->Cell(42,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(42);
		$this->SetTextColor(0,127,0);			
		$this->SetFont('DejaVu','B',10);
		$this->Cell(106,10,"CARTEIRA - DE ".$_POST['inicio']." A ".$_POST['fim'],0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(42,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);		
		$this->Cell(190,5,'Agência: '.$agencia.' - '.$aAg[0],0,1);
		$this->Cell(140,5,'Cliente: '.$cliente.' - '.$nome,0,0);					
		$this->Cell(50,5,'CFP/CNPJ: '.$aCli['cgc'],0,1);			
		$this->Cell(90,5,'Endereço: '.$aCli['endereco'],0,0);			
		$this->Cell(50,5,'Bairro: '.$aCli['bairro'],0,0);				
		$this->Cell(50,5,'CEP: '.formatCEP($aCli['cep']),0,1);		
		$this->Cell(90,5,'Cidade: '.$aCli['cidade'],0,0);			
		$this->Cell(50,5,'Estado: '.$aCli['estado'],0,0);				
		$this->Cell(50,5,'Inscrição: '.$aCli['inscr'],0,1);
		$this->Ln(2);
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
$pdf->AddFont('DejaVu','I','DejaVuSansCondensed-Oblique.ttf',true);
$pdf->AddFont('DejaVu','BI','DejaVuSansCondensed-BoldOblique.ttf',true);

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

$inicio = explode('/',$_POST['inicio']);
$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
$fim = explode('/',$_POST['fim']);
$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];

$nTotalValorEntradas = 0;
$nTotalJurosEntradas = 0;
$nTotalMultaEntradas = 0;
$nTotalDescnEntradas = 0;
$nTotalTitulEntradas = 0;

$nTotalValorLiquidac = 0;
$nTotalJurosLiquidac = 0;
$nTotalMultaLiquidac = 0;
$nTotalDescnLiquidac = 0;
$nTotalTitulLiquidac = 0;
$nTotalBaixaLiquidac = 0;

$nTotalValorBaixa = 0;
$nTotalJurosBaixa = 0;
$nTotalMultaBaixa = 0;
$nTotalDescnBaixa = 0;
$nTotalTitulBaixa = 0;

$sDados = "SELECT COALESCE(seunumero, '-') AS seunumero, nossonumero, DATE_FORMAT(data_emisao, '%d/%m/%Y') AS data_emissao, ";
$sDados .= "DATE_FORMAT(data_venc, '%d/%m/%Y') AS data_venc, DATE_FORMAT(data_baixa, '%d/%m/%Y') AS data_baixa, valor, ";
$sDados .= "multa, juros, descontos, documento ";
$sDados .= "FROM titulos ";
$sDados .= "WHERE agencia = '".$agencia."' AND cliente='".$cliente."' AND (data_emisao>='".$inicio."' AND data_emisao<='".$fim."') ";
$sDados .= "AND LENGTH(nossonumero)=17 AND status != 99 ";		
$sDados .= "ORDER BY data_emisao, data_venc, data_baixa";
$qDados = mysql_query($sDados) or die(mysql_error());

$sAnt = "SELECT SUM(valor) AS soma, COUNT(*) AS cont FROM titulos WHERE cliente=".$cliente." AND (data_baixa IS NULL OR data_baixa>='".$inicio."') AND cancelamento IS NULL AND cad_completo = 'S' AND status != '3' AND registro IS NOT NULL AND data_emisao<'".$inicio."'";
$qAnt = mysql_query($sAnt) or die(mysql_error());
$nAnteriorSaldo = mysql_result($qAnt, 0, "soma");
$nAnteriorCont = mysql_result($qAnt, 0, "cont");

$sDados1 = "SELECT COALESCE(seunumero, '-') AS seunumero, nossonumero, DATE_FORMAT(data_emisao, '%d/%m/%Y') AS data_emissao, ";
$sDados1 .= "DATE_FORMAT(data_venc, '%d/%m/%Y') AS data_venc, DATE_FORMAT(data_baixa, '%d/%m/%Y') AS data_baixa, ";
$sDados1 .= "DATEDIFF(data_baixa, data_venc) - 1 AS atraso, valor, valor_baixa, multa, juros, descontos, documento ";
$sDados1 .= "FROM titulos ";
$sDados1 .= "WHERE agencia='".$agencia."' AND cliente='".$cliente."' AND (data_baixa>='".$inicio."' AND data_baixa<='".$fim."') AND devolucao IS NULL ";
$sDados1 .= "ORDER BY data_emisao, data_venc, data_baixa";
$qDados1 = mysql_query($sDados1) or die(mysql_error());

$sDados2 = "SELECT COALESCE(seunumero, '-') AS seunumero, nossonumero, DATE_FORMAT(data_emisao, '%d/%m/%Y') AS data_emissao, ";
$sDados2 .= "DATE_FORMAT(data_venc, '%d/%m/%Y') AS data_venc, DATE_FORMAT(data_baixa, '%d/%m/%Y') AS data_baixa, ";
$sDados2 .= "DATEDIFF(data_baixa, data_venc) - 1 AS atraso, valor, valor_baixa, multa, juros, descontos, documento ";
$sDados2 .= "FROM titulos ";
$sDados2 .= "WHERE agencia='".$agencia."' AND cliente='".$cliente."' AND (data_baixa>='".$inicio."' AND data_baixa<='".$fim."') AND devolucao IS NOT NULL ";
$sDados2 .= "ORDER BY data_emisao, data_venc, data_baixa";
$qDados2 = mysql_query($sDados2) or die(mysql_error());

$pdf->SetFont('DejaVu','B',9);
$pdf->SetFillColor(226,226,226);
$pdf->Cell(190,6,'ENTRADAS',1,1,'C',true);
$pdf->SetFont('DejaVu','',8);
$pdf->Cell(27,6,'Seu Número',1,0,'C',true);
$pdf->Cell(41,6,'Nosso Número',1,0,'C',true);
$pdf->Cell(20,6,'Emissão',1,0,'C',true);
$pdf->Cell(20,6,'Vencimento',1,0,'C',true);
$pdf->Cell(28,6,'Valor Nominal',1,0,'C',true);
$pdf->Cell(18,6,'Multa',1,0,'C',true);
$pdf->Cell(18,6,'Juros',1,0,'C',true);
$pdf->Cell(18,6,'Descontos',1,1,'C',true);

if(mysql_num_rows($qDados) > 0){
	while($aDados = mysql_fetch_assoc($qDados)){
		$juros = 0; $multa = 0;
		$nTotalValorEntradas += $aDados['valor'];
		$nTotalMultaEntradas += $multa;
		$nTotalDescnEntradas += $aDados['descontos'];
		$nTotalJurosEntradas += $juros;
		$nTotalTitulEntradas++;		
		
		$pdf->SetFont('DejaVu','',8);
		$pdf->Cell(27,6,$aDados['seunumero'],1,0,'C');
		$pdf->Cell(41,6,$aDados['nossonumero'],1,0,'C');
		$pdf->Cell(20,6,$aDados['data_emissao'],1,0,'C');
		$pdf->Cell(20,6,$aDados['data_venc'],1,0,'C');
		$pdf->Cell(28,6,Reais($aDados['valor']),1,0,'R');
		$pdf->Cell(18,6,Reais($multa),1,0,'R');
		$pdf->Cell(18,6,Reais($juros),1,0,'R');
		$pdf->Cell(18,6,Reais($aDados['descontos']),1,1,'R');
	}
}		
else{
	$pdf->Cell(190,15,'Nenhum lançamento',1,1,'C');
}
$pdf->Ln(2);
$pdf->Cell(68,6,'Total Histórico: 001 - ENTRADA NORMAL',1,0,'L',true);
$pdf->Cell(40,6,'Títulos: '.$nTotalTitulEntradas,1,0,'L',true);
$pdf->Cell(28,6,Reais($nTotalValorEntradas),1,0,'R',true);
$pdf->Cell(18,6,Reais($nTotalMultaEntradas),1,0,'R',true);
$pdf->Cell(18,6,Reais($nTotalJurosEntradas),1,0,'R',true);
$pdf->Cell(18,6,Reais($nTotalDescnEntradas),1,1,'R',true);
$pdf->Ln(5);
$pdf->SetFont('DejaVu','B',9);
$pdf->Cell(190,6,'LIQUIDAÇÕES',1,1,'C',true);
$pdf->SetFont('DejaVu','',8);
$pdf->Cell(25,6,'Seu Número',1,0,'C',true);
$pdf->Cell(32,6,'Nosso Número',1,0,'C',true);
$pdf->Cell(16,6,'Emissão',1,0,'C',true);
$pdf->Cell(16,6,'Vencimento',1,0,'C',true);
$pdf->Cell(16,6,'Pagamento',1,0,'C',true);
$pdf->Cell(21,6,'Valor Nominal',1,0,'C',true);
$pdf->Cell(14,6,'Multa',1,0,'C',true);
$pdf->Cell(14,6,'Juros',1,0,'C',true);
$pdf->Cell(14,6,'Desc.',1,0,'C',true);
$pdf->Cell(22,6,'Valor Final',1,1,'C',true);

if(mysql_num_rows($qDados1) > 0){
	while($aDados1 = mysql_fetch_assoc($qDados1)){
		$juros = 0; $multa = 0;
		if($aDados1['valor'] != $aDados1['valor_baixa']){
			if($aDados1['atraso'] > 0){
				$multa = $aDados1['valor'] * ($aDados1['multa'] / 100);
				$juros = $aDados1['valor'] * ($aDados1['juros'] / 100) / 30 * $aDados1['atraso'];
				if($juros+$multa+$aDados1['valor'] != $aDados1['valor_baixa']) {
					//$juros = 'aqui';
					$juros = $aDados1['valor_baixa']-$aDados1['valor']-$multa;
					/*$diferenca = $juros+$multa+$aDados1['valor'] - $aDados1['valor_baixa'];
					if($multa >= $diferenca) {
						$multa = $multa-$diferenca;
					}
					else {
						$diferenca = $diferenca-$multa;
						$multa = 0;
						$juros = $juros-$diferenca;
					}*/
				}			
			}
		}
		$nTotalValorLiquidac += $aDados1['valor'];
		$nTotalMultaLiquidac += $multa;
		$nTotalDescnLiquidac += $aDados1['descontos'];
		$nTotalJurosLiquidac += $juros;
		$nTotalBaixaLiquidac += $aDados1['valor_baixa'];
		$nTotalTitulLiquidac++;	
		
		$pdf->SetFont('DejaVu','',8);
		$pdf->Cell(25,6,$aDados1['seunumero'],1,0,'C');
		$pdf->Cell(32,6,$aDados1['nossonumero'],1,0,'C');
		$pdf->Cell(16,6,$aDados1['data_emissao'],1,0,'C');
		$pdf->Cell(16,6,$aDados1['data_venc'],1,0,'C');
		$pdf->Cell(16,6,$aDados1['data_baixa'],1,0,'C');
		$pdf->Cell(21,6,Reais($aDados1['valor']),1,0,'R');
		$pdf->Cell(14,6,Reais($multa),1,0,'R');
		$pdf->Cell(14,6,$juros,1,0,'R');
		//$pdf->Cell(14,6,Reais($juros),1,0,'R');
		$pdf->Cell(14,6,Reais($aDados1['descontos']),1,0,'R');
		$pdf->Cell(22,6,Reais($aDados1['valor_baixa']),1,1,'R');
	}
}		
else{
	$pdf->Cell(190,15,'Nenhum lançamento',1,1,'C');
}
$pdf->Ln(2);
$pdf->Cell(57,6,'Total Histórico: 006 - LIQUIDAÇÕES',1,0,'L',true);
$pdf->Cell(16,6,'Títulos: '.$nTotalTitulLiquidac,1,0,'L',true);
$pdf->Cell(32,6,'',1,0,'R',true);
$pdf->Cell(21,6,Reais($nTotalValorLiquidac),1,0,'R',true);
$pdf->Cell(14,6,Reais($nTotalMultaLiquidac),1,0,'R',true);
$pdf->Cell(14,6,Reais($nTotalJurosLiquidac),1,0,'R',true);
$pdf->Cell(14,6,Reais($nTotalDescnLiquidac),1,0,'R',true);
$pdf->Cell(22,6,Reais($nTotalBaixaLiquidac),1,1,'R',true);
$pdf->Ln(5);
$pdf->SetFont('DejaVu','B',9);
$pdf->Cell(190,6,'BAIXAS',1,1,'C',true);
$pdf->SetFont('DejaVu','',8);
$pdf->Cell(25,6,'Seu Número',1,0,'C',true);
$pdf->Cell(32,6,'Nosso Número',1,0,'C',true);
$pdf->Cell(16,6,'Emissão',1,0,'C',true);
$pdf->Cell(16,6,'Vencimento',1,0,'C',true);
$pdf->Cell(16,6,'Pagamento',1,0,'C',true);
$pdf->Cell(21,6,'Valor Nominal',1,0,'C',true);
$pdf->Cell(14,6,'Multa',1,0,'C',true);
$pdf->Cell(14,6,'Juros',1,0,'C',true);
$pdf->Cell(14,6,'Desc.',1,0,'C',true);
$pdf->Cell(22,6,'Valor Final',1,1,'C',true);
if(mysql_num_rows($qDados2) > 0){
	while($aDados2 = mysql_fetch_assoc($qDados2)){
		$juros = 0; $multa = 0;
		if ($aDados2['valor'] != $aDados1['valor_baixa']){
			if ($aDados2['atraso'] > 0){
				$juros = $aDados2['valor'] * ($aDados2['juros'] / 100) / 30 * $aDados2['atraso'];
				$multa = $aDados2['valor'] * ($aDados2['multa'] / 100);					
				if ($juros+$multa+$aDados2['valor'] > $aDados2['valor_baixa']) {
					$diferenca = $juros+$multa+$aDados2['valor'] - $aDados2['valor_baixa'];
					if ($multa >= $diferenca) {
						$multa = $multa-$diferenca;
					}
					else {
						$diferenca = $diferenca-$multa;
						$multa = 0;
						$juros = $juros-$diferenca;
					}
				}					
			}
		}
		
		$nTotalValorBaixa += $aDados2['valor'];
		$nTotalMultaBaixa += $multa;
		$nTotalDescnBaixa += $aDados2['descontos'];
		$nTotalJurosBaixa += $juros;
		$nTotalTitulBaixa++;
			
		$pdf->SetFont('DejaVu','',8);
		$pdf->Cell(25,6,$aDados2['seunumero'],1,0,'C');
		$pdf->Cell(32,6,$aDados2['nossonumero'],1,0,'C');
		$pdf->Cell(16,6,$aDados2['data_emissao'],1,0,'C');
		$pdf->Cell(16,6,$aDados2['data_venc'],1,0,'C');
		$pdf->Cell(16,6,$aDados2['data_baixa'],1,0,'C');
		$pdf->Cell(21,6,Reais($aDados2['valor']),1,0,'R');
		$pdf->Cell(14,6,Reais($multa),1,0,'R');
		$pdf->Cell(14,6,Reais($juros),1,0,'R');
		$pdf->Cell(14,6,Reais($aDados2['descontos']),1,0,'R');
		$pdf->Cell(22,6,Reais($aDados2['valor_baixa']),1,1,'R');
	}
}		
else{
	$pdf->Cell(190,15,'Nenhum lançamento',1,1,'C');
}
$pdf->Ln(2);
$pdf->Cell(57,6,'Total Histórico: 006 - BAIXAS',1,0,'L',true);
$pdf->Cell(16,6,'Títulos: '.$nTotalTitulBaixa,1,0,'L',true);
$pdf->Cell(32,6,'',1,0,'L',true);
$pdf->Cell(21,6,Reais($nTotalValorBaixa),1,0,'R',true);
$pdf->Cell(14,6,Reais($nTotalMultaBaixa),1,0,'R',true);
$pdf->Cell(14,6,Reais($nTotalJurosBaixa),1,0,'R',true);
$pdf->Cell(14,6,Reais($nTotalDescnBaixa),1,0,'R',true);
$pdf->Cell(22,6,Reais($nTotalValorBaixa + $nTotalMultaBaixa + $nTotalJurosBaixa - $nTotalDescnBaixa),1,1,'R',true);
$pdf->Ln(7);
$pdf->SetFont('DejaVu','B',9);
$pdf->Cell(44);
$pdf->Cell(102,6,'RESUMO DA CARTEIRA DE '.$_POST['inicio'].' A '.$_POST['fim'].'',1,1,'C',true);
$pdf->SetFont('DejaVu','',9);
$pdf->Cell(44);
$pdf->Cell(34,6,'Saldo',1,0,'C',true);
$pdf->Cell(34,6,'Títulos',1,0,'C',true);
$pdf->Cell(34,6,'Valor',1,1,'C',true);
$pdf->Cell(44);
$pdf->Cell(34,6,'Anterior',1,0,'C');
$pdf->Cell(34,6,$nAnteriorCont,1,0,'C');
$pdf->Cell(34,6,Reais($nAnteriorSaldo),1,1,'C');
$pdf->Cell(44);
$pdf->Cell(34,6,'Entradas',1,0,'C');
$pdf->Cell(34,6,$nTotalTitulEntradas,1,0,'C');
$pdf->Cell(34,6,Reais($nTotalValorEntradas),1,1,'C');
$pdf->Cell(44);
$pdf->Cell(34,6,'Liquidações',1,0,'C');
$pdf->Cell(34,6,$nTotalTitulLiquidac,1,0,'C');
$pdf->Cell(34,6,Reais($nTotalValorLiquidac),1,1,'C');
$pdf->Cell(44);
$pdf->Cell(34,6,'Baixas',1,0,'C');
$pdf->Cell(34,6,$nTotalTitulBaixa,1,0,'C');
$pdf->Cell(34,6,Reais($nTotalValorBaixa),1,1,'C');
$pdf->Cell(44);
$pdf->Cell(34,6,'Atual',1,0,'C');
$pdf->Cell(34,6,$nAnteriorCont+$nTotalTitulEntradas-$nTotalTitulLiquidac-$nTotalTitulBaixa,1,0,'C');
$pdf->Cell(34,6,Reais($nAnteriorSaldo+$nTotalValorEntradas-$nTotalValorLiquidac-$nTotalValorBaixa),1,1,'C');

$pdf->Output();
?>