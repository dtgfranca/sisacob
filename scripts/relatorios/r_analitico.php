<?php

ini_set("max_execution_time", 3600);
include('../../config.php');
include('../limpa.php');
require('../tfpdf.php');
$arquivo = parse_ini_file('../../cooperativas.ini');	
session_start();
mysql_query("SET NAMES UTF8") or die(mysql_error());
$_GET = sanitize($_POST);
	if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}
class PDF extends tFPDF{
	function Header(){	
		$agencia = $_SESSION['userAgencia'];
		$conta = $_SESSION['userConta'];	
		$razao = $_SESSION['userNome'];	
		$carteira = "CARTEIRA - DE ".date('d/m/Y');
			
		if($agencia == '4117'){
			$tam = '42,15';
		}
		else{
			$tam ='42,15';
		}
			
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
		$this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$this->SetFont('DejaVu','B',16);
		$this->SetTextColor(0,127,0);
		$this->Image('../../img/logo/logo_'.$agencia.'.jpg',9,10,$tam); // logo do relatório
		$this->Cell(42);
		$this->Cell(105,10,'RELATÓRIO ANALÍTICO',0,0,'C'); // título do relatório
		$this->Cell(42,10,'',0,1);		
		$this->Cell(42);
		$this->SetFont('DejaVu','B',13);
		$this->Cell(105,10,'DE POSIÇÃO DAS CARTEIRAS',0,0,'C');
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(40,5,'Data: '.date('d/m/Y'),0,1,'R');
		$this->Cell(147);			
		$this->Cell(40,5,'Hora: '.date('H:i:s'),0,1,'R');
		$this->Cell(42);
		$this->SetTextColor(0,127,0);			
		$this->SetFont('DejaVu','B',10);
		$this->Cell(105,10,$carteira,0,0,'C'); // subtitulo do relatório
		$this->SetTextColor(0,0,0);
		$this->SetFont('DejaVu','',9);			
		$this->Cell(40,5,'Página '.$this->PageNo(),0,1,'R');
		$this->Ln(5);
		$this->SetFont('DejaVu','',10);
		$this->Cell(187,5,'Cliente: '.$razao,0,1);			
		$this->Cell(30,5,'Agência: '.$agencia,0,0);			
		$this->Cell(157,5,'Conta: '.$conta,0,1);	
		$this->Ln(2);
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);
$pdf->AddFont('DejaVu','I','DejaVuSansCondensed-Oblique.ttf',true);
$pdf->AddFont('DejaVu','BI','DejaVuSansCondensed-BoldOblique.ttf',true);


$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

$sSac = "SELECT nome, sacado FROM sacados WHERE cliente='$cliente' AND (grupo IS NULL OR grupo <> '99999')";
$qSac = mysql_query($sSac) or die(mysql_error());

$liquidados_geral       = 0;
$valor_liquidados_geral = 0;
$cartorio_geral         = 0;
$valor_cartorio_geral   = 0;
$baixados_geral         = 0;
$valor_baixados_geral   = 0;
$cobranca_geral         = 0;
$valor_cobranca_geral   = 0;

while($aSac = mysql_fetch_array($qSac)){	
	$sDados = "SELECT tit.titulo, tit.agencia, tit.cliente, tit.sacado, tit.documento, tit.sequencia, tit.nossonumero, ";
	$sDados .= "tit.data_emisao, tit.data_venc, DATEDIFF(tit.data_venc,DATE_FORMAT(NOW(),'%Y-%m-%d')) AS vencido, ";
	$sDados .= "tit.valor, tit.data_baixa, tit.data_credito, tit.valor_baixa, tit.data_baixa_manual, tit.cancelamento, ";
	$sDados .= "tit.criacao, tit.desconto, tit.devolucao, tit.so_desconto, tit.registro, tit.status ";
	$sDados .= "FROM titulos AS tit ";
	$sDados .= "WHERE data_baixa IS NULL AND registro IS NOT NULL AND cancelamento IS NULL AND tit.cad_completo='S' ";
	$sDados .= "AND tit.status != '03' AND tit.sacado='".$aSac['sacado']."'";
	$qDados = mysql_query($sDados) or die(mysql_error());

	$simples          = 0;
	$valor_simples    = 0;
	$descontada       = 0;
	$valor_descontada = 0;
	$cobranca         = 0;
	$valor_cobranca   = 0;
	$registro         = 0;
	$valor_registro   = 0;
	$liquidados       = 0;
	$valor_liquidados = 0;
	$cartorio         = 0;
	$valor_cartorio   = 0;
	$baixados         = 0;
	$valor_baixados   = 0;
	$vencidos         = 0;
	$valor_vencidos   = 0;
	$avencer          = 0;
	$valor_avencer    = 0;
	$nome_sacado      = "";
	
	while($aDados = mysql_fetch_array($qDados)){
		if($aDados['so_desconto']=="S" && $aDados['data_baixa'] == ""){
			$descontada++;		
			$valor_descontada += $aDados['valor'];
		}
		else if($aDados['so_desconto']=="N" && $aDados['data_baixa'] == ""){
			$simples++;
			$valor_simples += $aDados['valor'];
		}
		if($aDados['registro']!=""){
			$registro++;
			$valor_registro += $aDados['valor'];
		}
		if($aDados['data_baixa'] != "" && $aDados['status']!=9 && $aDados['devolucao'] == ""){
			$liquidados++;
			$valor_liquidados += $aDados['valor'];
		}		
		if($aDados['data_baixa'] != "" && $aDados['status']==9){
			$cartorio++;
			$valor_cartorio += $aDados['valor'];
		}
		if($aDados['devolucao'] != "" && $aDados['status']!=9){
			$baixados++;
			$valor_baixados += $aDados['valor_baixa'];
		}		
		if($aDados['vencido'] < 0 && $aDados['data_baixa'] == ""){
			$vencidos++;
			$valor_vencidos += $aDados['valor'];
		}		
		if($aDados['vencido'] >= 0 && $aDados['data_baixa'] == ""){
			$avencer++;
			$valor_avencer += $aDados['valor'];
		}		
	}
	$cobranca = $descontada + $simples;
	$valor_cobranca = $valor_simples + $valor_descontada;
	
	if ($simples != 0 || $descontada != 0 || $cobranca != 0 || $registro != 0 || $liquidados != 0 || $cartorio != 0 || $baixados != 0 || $vencidos != 0 || $avencer != 0) {
		$pdf->SetFont('DejaVu','B',9);
		$pdf->SetFillColor(226,226,226);
		$pdf->Cell(187,6,'Sacado: '.$aSac['nome'],1,1,'L',true);
		$pdf->SetFont('DejaVu','',9);
		$pdf->Cell(94,6,'Descrição',1,0,'L',true);
		$pdf->Cell(37,6,'Número de Títulos',1,0,'L',true);
		$pdf->Cell(56,6,'Valor Total',1,1,'L',true);		
		$pdf->Cell(94,6,'Total de Títulos em Cobrança',1,0,'L');
		$pdf->Cell(37,6,$cobranca,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_cobranca,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos em Cobrança Simples',1,0,'L');
		$pdf->Cell(37,6,$simples,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_simples,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos em Cobrança Descontada',1,0,'L');
		$pdf->Cell(37,6,$descontada,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_descontada,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Entradas Confirmadas',1,0,'L');
		$pdf->Cell(37,6,$registro,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_registro,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos Liquidados',1,0,'L');
		$pdf->Cell(37,6,$liquidados,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_liquidados,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos Liquidados em Cartório',1,0,'L');
		$pdf->Cell(37,6,$cartorio,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_cartorio,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos Baixados',1,0,'L');
		$pdf->Cell(37,6,$baixados,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_baixados,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos Vencidos',1,0,'L');
		$pdf->Cell(37,6,$vencidos,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_vencidos,2,",","."),1,1,'R');
		$pdf->Cell(94,6,'Total de Títulos a Vencer',1,0,'L');
		$pdf->Cell(37,6,$avencer,1,0,'R');
		$pdf->Cell(56,6,number_format($valor_avencer,2,",","."),1,1,'R');
		$pdf->Ln(5);		
	}
	$liquidados_geral += $liquidados;
	$valor_liquidados_geral += $valor_liquidados;
	$cartorio_geral += $cartorio;
	$valor_cartorio_geral += $valor_cartorio;
	$baixados_geral += $baixados;
	$valor_baixados_geral += $valor_baixados;
	$cobranca_geral += $cobranca;
	$valor_cobranca_geral += $valor_cobranca;
}
$pdf->SetFont('DejaVu','B',9);
$pdf->SetFillColor(226,226,226);
$pdf->Cell(187,6,'Total Geral',1,1,'C',true);
$pdf->SetFont('DejaVu','',9);
$pdf->Cell(94,6,'Total de Títulos em Cobrança',1,0,'L');
$pdf->Cell(37,6,$cobranca_geral,1,0,'R');
$pdf->Cell(56,6,number_format($valor_cobranca_geral,2,",","."),1,1,'R');
$pdf->Cell(94,6,'Total de Títulos Liquidados',1,0,'L');
$pdf->Cell(37,6,$liquidados_geral,1,0,'R');
$pdf->Cell(56,6,number_format($valor_liquidados_geral,2,",","."),1,1,'R');
$pdf->Cell(94,6,'Total de Títulos Liquidados em Cartório',1,0,'L');
$pdf->Cell(37,6,$cartorio_geral,1,0,'R');
$pdf->Cell(56,6,number_format($valor_cartorio_geral,2,",","."),1,1,'R');
$pdf->Cell(94,6,'Total de Títulos Baixados',1,0,'L');
$pdf->Cell(37,6,$baixados_geral,1,0,'R');
$pdf->Cell(56,6,number_format($valor_baixados_geral,2,",","."),1,1,'R');

$pdf->Output();
?>