<?php
error_reporting(0);
ini_set("max_execution_time", 3600);
include '../../config.php';
include '../limpa.php';
$cooperativas = parse_ini_file('../../cooperativas.ini');
require 'setting.php';
session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location:http://sisacob.com.br/index.php');
}

$_GET = sanitize($_POST);
$arquivo = "r_agrupado.xml";

$report_unit = "/reports/$reportspath/";
$nome = explode(".", $arquivo);
$report_name = $nome[0];

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];
$razao = $_SESSION['userNome'];
$nomeCliente = strip_tags($_SESSION['userNome']);
$tipo = $_POST['tipo'];
$filtro = $_POST['filtro'];
$logo = function () {

	$query = "SELECT razao FROM agencias WHERE agencia ={$_SESSION['userAgencia']}";
	$cmd = mysql_query($query);
	$array = mysql_fetch_assoc($cmd);
	return str_replace("SICOOB", "", $array['razao']);
};

if ($filtro == "data_emisao") {
	$value_periodo = "EMISSÃO DE " . $_POST['inicio'] . " A " . $_POST['fim'];
} else if ($filtro == "data_venc") {
	$value_periodo = "VENCIMENTO DE " . $_POST['inicio'] . " A " . $_POST['fim'];
} else if ($filtro == "data_baixa") {
	$value_periodo = "BAIXA DE " . $_POST['inicio'] . " A " . $_POST['fim'];
}

if ($tipo == "todos") {
	$value_tipo = "RELATÓRIO SIMPLES";
} else if ($tipo == "rec") {
	$value_tipo = "RECEBIDOS";
} else if ($tipo == "baixa") {
	$value_tipo = "BAIXADOS";
} else if ($tipo == "pend") {
	$value_tipo = "EM ABERTO";
} else if ($tipo == "venc") {
	$value_tipo = "VENCIDOS";
} else if ($tipo == "cancel") {
	$value_tipo = "CANCELADOS";
} else if ($tipo == "lista_previa") {
	$value_tipo = "LISTAGEM PRÉVIA";
} else if ($tipo == "rejeitados") {
	$value_tipo = "REJEITADOS";
} else if ($tipo == "nosso_n") {
	$nosso_n = $_POST['nosso_n'];
	$value_tipo = "NOSSO NUMERO " . $nosso_n;
	$value_periodo = '';
} else if ($tipo == "n_doc") {
	$nosso_n = $_POST['n_titulo'];
	$value_tipo = "Documento " . $nosso_n;
	$value_periodo = '';
}

$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

if (empty($_POST['inicio']) || empty($_POST['fim'])) {
	$b_data = "";
} else {
	$inicio = explode('/', $_POST['inicio']);
	$inicio = $inicio[2] . '-' . $inicio[1] . '-' . $inicio[0];
	$fim = explode('/', $_POST['fim']);
	$fim = $fim[2] . '-' . $fim[1] . '-' . $fim[0];
}

if (!empty($_POST['cliente'])) {$sacado = $_POST['cliente'];}
$tipo = $_POST["tipo"];
$tipo2 = $_POST["tipo2"];
$ordem = $_POST["ordem"];
$filtro = $_POST["filtro"];

if ($filtro == "data_baixa") {
	$b_data = "AND ((t.data_baixa>='$inicio' AND t.data_baixa<='$fim') OR (t.data_baixa_manual>='$inicio' AND t.data_baixa_manual<='$fim'))";
	$b_data2 = " and ((data_baixa>='$inicio' and data_baixa<='$fim') or (data_baixa_manual>='$inicio' and data_baixa_manual<='$fim')) ";
} else {
	$b_data = "";
	$b_data2 = "";
}
if ($tipo == "rec") {
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND (status='05' OR status='06' OR status='07' OR status='08' OR status='15')";
} else if ($tipo == "baixa") {
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND t.devolucao IS NOT NULL AND (t.status='09' OR t.status='10')";
} else if ($tipo == "pend") {
	$b_tipo = "AND (t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL)";
} else if ($tipo == "venc") {
	$b_tipo = "AND t.data_venc < CURDATE() AND t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL";
} else if ($tipo == "rejeitados") {
	$b_tipo = "AND t.status='03' AND t.cancelamento IS NULL";
} else if ($tipo == "nosso_n") {
	$nosso_n = $_POST['nosso_n'];
	$b_tipo = "AND t.nossonumero LIKE '%" . $nosso_n . "%'";
} else if ($tipo == "n_doc") {
	$n_titulo = $_POST['n_titulo'];
	$b_tipo = "AND t.documento LIKE '%" . $n_titulo . "%'";
} else if ($tipo == "cancel") {
	$b_tipo = "AND t.cancelamento IS NOT NULL";
} else if ($tipo == "lista_previa") {
	$b_tipo = "AND cad_completo = 'N' AND status <> '99' ";
} else {
	$b_tipo = "AND cad_completo = 'S' ";
}
if (empty($_POST['tipo2'])) {
	$b_tipo2 = "";
} else {
	$tipo2 = $_POST['tipo2'];
	switch ($tipo2) {
	case "desc":
		if ($tipo == "rec") {
			$b_tipo2 = "AND t.desconto IS NOT NULL AND so_desconto='S'";
		} else if ($tipo == "todos") {
			$b_tipo2 = "";
		} else {
			$b_tipo2 = "AND so_desconto='S'";
		}
		break;
	case "cobranca_simples":
		if ($tipo == "rec") {
			$b_tipo2 = "AND so_desconto='N'";
		} else if ($tipo == "todos") {
			$b_tipo2 = "AND so_desconto='N'";
		} else {
			$b_tipo2 = "AND so_desconto='N'";
		}
		break;
	}
}
if (!empty($sacado)) {
	$b_sacado = "AND t.sacado='$sacado'";
} else {
	$b_sacado = "";
}

/*$query = "select t.titulo,t.status, t.documento, t.devolucao, t.nossonumero, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, COALESCE(t.valor,0) AS valor, t.modelo, t.sequencia, ";
$query .= "DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') as data_baixa, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito,  ";
$query .= " COALESCE(t.valor_baixa,0) AS valor_baixa, t.sacado, s.nome,";
$query .= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and (data_baixa is null and cancelamento is null and  data_baixa_manual is null and desconto is null)) AS abertos,";
$query.= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and data_baixa is not null) as baixados ,";
$query.= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and data_baixa_manual is not null) as manual , ";
$query .="(select count(titulo) from titulos where cliente='$cliente' and sacado='$sacado' $b_data2 and cancelamento is not null) as cancelados  ";
$query.
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.cliente='$cliente'  $b_sacado $b_data $b_tipo $b_tipo2 order by  s.nome, $ordem, t.nossonumero";*/

if(in_array($_SESSION['userAgencia'],$cooperativas)){

	$query = "select t.titulo,getStatus(t.status) as status, t.documento, t.devolucao, t.nossonumero, coalesce(DATE_FORMAT(t.data_emisao, '%d/%c/%Y'),'-') as data_emisao, ";
$query .= "coalesce(DATE_FORMAT(t.data_venc, '%d/%c/%Y'),'-') as data_venc, t.valor, t.modelo, t.sequencia, coalesce(DATE_FORMAT(t.data_credito, '%d/%c/%Y'),'-') as data_credito, ";
$query .= "DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') as data_baixa, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito,  ";
$query .= "t.valor_baixa, t.sacado, coalesce(s.nome,'-') as nome , ";
$query .= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and (data_baixa is null and cancelamento is null and  data_baixa_manual is null and desconto is null)) AS abertos,";
$query.= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and data_baixa is not null) as baixados ,";
$query.= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and data_baixa_manual is not null) as manual , ";
$query .="(select count(titulo) from titulos where cliente='$cliente' and sacado='$sacado' $b_data2 and cancelamento is not null) as cancelados  ";
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.cliente='$cliente'    $b_data  $b_tipo2 order by  s.nome, $ordem, t.nossonumero";

}else{

    $query = "select t.titulo,getStatus(t.status) as status, t.documento, t.devolucao, t.nossonumero, DATE_FORMAT(t.data_emisao, '%d/%c/%Y') as data_emisao, ";
$query .= "DATE_FORMAT(t.data_venc, '%d/%c/%Y') as data_venc, t.valor, t.modelo, t.sequencia, coalesce(DATE_FORMAT(t.data_credito, '%d/%c/%Y'),'-') as data_credito,";
$query .= "DATE_FORMAT(coalesce(t.data_baixa,t.data_baixa_manual), '%d/%c/%Y') as data_baixa, ";
$query .= "DATE_FORMAT(t.cancelamento, '%d/%c/%Y') as cancelamento, DATE_FORMAT(t.data_credito, '%d/%c/%Y') as data_credito,  ";
$query .= "t.valor_baixa, t.sacado, s.nome , ";
$query .= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and (data_baixa is null and cancelamento is null and  data_baixa_manual is null and desconto is null)) AS abertos,";
$query.= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and data_baixa is not null) as baixados ,";
$query.= "(select count(titulo) from titulos where cliente='$cliente' and sacado=t.sacado $b_data2 and data_baixa_manual is not null) as manual , ";
$query .="(select count(titulo) from titulos where cliente='$cliente' and sacado='$sacado' $b_data2 and cancelamento is not null) as cancelados  ";
$query .= "from titulos AS t left join sacados AS s on t.sacado=s.sacado ";
$query .= "where t.cliente='$cliente'   $b_sacado $b_data $b_tipo $b_tipo2 order by  s.nome, $ordem, t.nossonumero";

}


$arrayParameter = array("nAgencia" => $agencia, "cCliente" => $nomeCliente, "cLogo" => $logo(), "cTitulo" => $value_tipo, "cPeriodo" => $value_periodo, "nConta" => $conta, "cQuery" => $query, "abertos" => $abertos, "baixados" => $baixados, "manual" => $manuais, "cancelados" => $cancelados); //passa o

function montaParametro(array $parametos) {
	$url = "&";
	foreach ($parametos as $key => $value) {
		$url .= $key . "=" . rawurlencode($value) . "&";
	}
	return substr($url, 0, strlen($url) - 1);
}

$url = $jasper_url . "?_flowId=viewReportFlow&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=" . rawurlencode($report_unit) . "&reportUnit=" . rawurlencode($report_unit . $report_name) . montaParametro($arrayParameter) . "&decorate=no&j_username=$jasper_username&j_password=$jasper_password&theme=theme_embbed";

header("Location: $url");

?>
