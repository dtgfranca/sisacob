<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SisaCob - Importação de Sacados</title>
	<link href="../css/reset.css" rel="stylesheet" type="text/css" />
    <link href="../css/estilo.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id='section' class="duplo" style='margin: 10 auto 0 auto'>
    	<h2>Importação de Sacados</h2>
        <div class='corpo'>
        	<fieldset>
            	<table>
<?php
	error_reporting(0);
   // VERIFICA CPF
   function validaCPF($cpf) {
      $soma = 0;
      
      if (strlen($cpf) <> 11)
         return false;
      
      // Verifica 1º digito      
      for ($i = 0; $i < 9; $i++) {         
         $soma += (($i+1) * $cpf[$i]);
      }

      $d1 = ($soma % 11);
      
      if ($d1 == 10) {
         $d1 = 0;
      }
      
      $soma = 0;
      
      // Verifica 2º digito
      for ($i = 9, $j = 0; $i > 0; $i--, $j++) {
         $soma += ($i * $cpf[$j]);
      }
      
      $d2 = ($soma % 11);
      
      if ($d2 == 10) {
         $d2 = 0;
      }      
      
      if ($d1 == $cpf[9] && $d2 == $cpf[10]) {
         return true;
      }
      else {
         return false;
      }
   }
   
   
   
   
   
   // VERFICA CNPJ
   function validaCNPJ($cnpj) {
   
      if (strlen($cnpj) <> 14)
         return false; 

      $soma = 0;
      
      $soma += ($cnpj[0] * 5);
      $soma += ($cnpj[1] * 4);
      $soma += ($cnpj[2] * 3);
      $soma += ($cnpj[3] * 2);
      $soma += ($cnpj[4] * 9); 
      $soma += ($cnpj[5] * 8);
      $soma += ($cnpj[6] * 7);
      $soma += ($cnpj[7] * 6);
      $soma += ($cnpj[8] * 5);
      $soma += ($cnpj[9] * 4);
      $soma += ($cnpj[10] * 3);
      $soma += ($cnpj[11] * 2); 

      $d1 = $soma % 11; 
      $d1 = $d1 < 2 ? 0 : 11 - $d1; 

      $soma = 0;
      $soma += ($cnpj[0] * 6); 
      $soma += ($cnpj[1] * 5);
      $soma += ($cnpj[2] * 4);
      $soma += ($cnpj[3] * 3);
      $soma += ($cnpj[4] * 2);
      $soma += ($cnpj[5] * 9);
      $soma += ($cnpj[6] * 8);
      $soma += ($cnpj[7] * 7);
      $soma += ($cnpj[8] * 6);
      $soma += ($cnpj[9] * 5);
      $soma += ($cnpj[10] * 4);
      $soma += ($cnpj[11] * 3);
      $soma += ($cnpj[12] * 2); 
      
      
      $d2 = $soma % 11; 
      $d2 = $d2 < 2 ? 0 : 11 - $d2; 
      
      if ($cnpj[12] == $d1 && $cnpj[13] == $d2) {
         return true;
      }
      else {
         return false;
      }
   } 

require('../config.php');
	session_start();
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];	
	function zero($casas,$zero){
	   while (strlen($casas)<$zero) {
		   $casas = "0".$casas;
	   }
	   return $casas;
	   }

$totalsacado=0;
	//$fd = fopen ("public_html/wwwsisacob/arquivo_temp/".$file_name, "r");
	$file_name = $_GET['file_name'];
	$fd = fopen ("http://www.sisacob.com.br/arquivo_temp/$file_name", "r");
	//$fd = fopen ("../../arquivo_temp/$file_name", "r");
	while (!feof($fd)){
			$linha = fgets($fd, 4096);
			$campo = explode(";", $linha);

			$cpfcnpj=trim($campo[2]);
			if(strlen($cpfcnpj)==14){
				$valida=validaCNPJ($cpfcnpj);
				$cpfcnpjformatado="";
				$cpfcnpjformatado=substr($cpfcnpj, 0, 2);
				$cpfcnpjformatado=$cpfcnpjformatado.".".substr($cpfcnpj, 2, 3);
				$cpfcnpjformatado=$cpfcnpjformatado.".".substr($cpfcnpj, 5, 3);
				$cpfcnpjformatado=$cpfcnpjformatado."/".substr($cpfcnpj, 8, 4);
				$cpfcnpjformatado=$cpfcnpjformatado."-".substr($cpfcnpj, 12, 2);
			}
			elseif(strlen($cpfcnpj)==11){
				$valida=validaCPF($cpfcnpj);
				$cpfcnpjformatado="";
				$cpfcnpjformatado=substr($cpfcnpj, 0, 3);
				$cpfcnpjformatado=$cpfcnpjformatado.".".substr($cpfcnpj, 3, 3);
				$cpfcnpjformatado=$cpfcnpjformatado.".".substr($cpfcnpj, 6, 3);
				$cpfcnpjformatado=$cpfcnpjformatado."-".substr($cpfcnpj, 9, 2);
			}
			
			$sql = mysql_query("select cpf from sacados where cpf='$cpfcnpjformatado' and agencia='$agencia' and conta='$conta'")or die ("Não foi possível acessar no banco de dados");
			$sacados=mysql_fetch_array($sql);	
			$cpf_sacado=$sacados['cpf'];

			if($valida){
				if(empty($cpf_sacado)){
					if(!empty($campo[2]) and !empty($campo[0])){
						$sql = mysql_query("insert into sacados (agencia, conta, cliente, nome, identidade, cpf, endereco, bairro, cep, cidade, uf, telef_pess, telef_com, celular, fax, email)values('$agencia', '$conta', '$cliente', '".trim($campo[0])."', '".trim($campo[1])."', '$cpfcnpjformatado', '".trim($campo[3])."', '".trim($campo[4])."', '".trim($campo[5])."', '".trim($campo[6])."', '".trim($campo[7])."', '".trim($campo[8])."', '".trim($campo[9])."', '".trim($campo[10])."', '".trim($campo[11])."', '".trim($campo[12])."')")or die ("O(a) sacado(a) ".$campo[0]." não foi importado<br>");
					$totalsacado=$totalsacado+1;
					}
				}
				else{
					echo '
					<tr>
                    	<td style="border-bottom: 1px solid #ccc; color: #f00">'.trim($campo[0]).' - CPF ou CNPJ já cadastrado</td>
                    </tr>
					';
				}
			}
			else{
				echo '
					<tr>
                    	<td style="border-bottom: 1px solid #ccc; color: #f00">'.trim($campo[0]).' - CPF ou CNPJ inválido</td>
                    </tr>
				';
			}
	}
	fclose ($fd);
	
		include("configFTP.php");
		
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
		ftp_delete($conn_id, $caminhoTemp.$file_name)

?>



<? 	
	echo '
					<tr>
                    	<td style="border-bottom: 1px solid #ccc"><b>'.$totalsacado.' sacados importados</b></td>
                    </tr>
	';
?>
				</table>
            </fieldset>
       	</div>
    </div>
</body>
</html>