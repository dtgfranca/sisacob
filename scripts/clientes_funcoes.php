<?php
	include('../config.php');
	include('limpa.php');
	include('auditoria.php');
	session_start();

	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	$_GET = sanitize($_GET);
	$funcao = $_GET['funcao'];
	if(empty($_SESSION['userAgencia'])){
		header('location: login.php?res=1');
	}
	mysql_query("SET NAMES UTF8") or die(mysql_error());


	//CADASTRO DE CLIENTES
	if($funcao == 'cad_cliente'){

		$nome           = $_GET["nome"];
		$id             = $_GET["identidade"];
		$cpfcnpj        = $_GET["cpfcnpj"];
		$endereco       = $_GET["endereco"];
		$bairro         = $_GET["bairro"];
		$cidade         = $_GET["cidade"];
		$uf             = $_GET["uf"];
		$cep            = $_GET["cep"];
		$tel_pessoal    = $_GET["tel_pessoal"];
		$tel_comercial  = $_GET["tel_comercial"];
		$celular        = $_GET["celular"];
		$fax            = $_GET["fax"];
		$email          = $_GET["email"];
		$grupo          = $_GET["grupo"];
		$grupo = (empty($grupo) || $grupo == '0') ? 'NULL' : $grupo = "'".$grupo."'";

		$query = mysql_query("SELECT cpf FROM sacados WHERE cpf = '$cpfcnpj' AND cliente = '$cliente'");

		if (mysql_num_rows($query) > 0)
			echo 'Já existe um cliente cadastrado com este CPF/CNPJ.';

		else
		{
			$inserir  = "INSERT INTO sacados (agencia, conta, cliente, nome, identidade, cpf, endereco, bairro, cidade, uf, cep, telef_pess, telef_com, celular, fax, email, gsc_id) ";
			$inserir .= "VALUES ('$agencia', '$conta', '$cliente', UPPER('$nome'), UPPER('$id'),'$cpfcnpj', UPPER('$endereco'), ";
			$inserir .= "UPPER('$bairro'), UPPER('$cidade'), UPPER('$uf'), '$cep', '$tel_pessoal', '$tel_comercial', '$celular', '$fax', '$email', $grupo)";

			//die($inserir);
        
			$sql = mysql_query($inserir);
			if($sql) {
				echo 'ok';
			}
			else {
				echo 'Erro!<br>';
				mysql_error();
			}
		}
	}

	// BUSCA CLIENTES

	else if($funcao == 'busca'){
		echo '
			<table>
            	<tr class="cinza" onmouseover="over()">
                	<td class="destaque borda">NOME</td>
                    <td width="25%" class="destaque borda">CPF/CNPJ</td>
                    <td width="10%" class="destaque borda">	AÇÕES</td>
                </tr>
		';

		$filtro = $_GET['filtro'];
		$grupo = $_GET['grupo'];

		if(empty($filtro)){
			$busca = '';
		}
		else {
			$busca=" AND (nome LIKE '%$filtro%' OR cpf LIKE '%$filtro%')";
		}

		if(empty($grupo) || $grupo == '0'){
			$grupo = '';
		}
		else {
			$grupo= "AND gsc_id = '".$grupo."'";
		}

        $sql = "SELECT nome, cpf, sacado FROM sacados WHERE agencia='$agencia' $busca $grupo AND cliente = '$cliente' AND (grupo <> '99999' or grupo is null) ORDER BY UPPER(nome)";
		$query = mysql_query ($sql) or die(mysql_error());
    	if(mysql_num_rows($query)>0){
			while($linha = mysql_fetch_assoc($query)){
				echo '
					<tr>
						<td class="borda">'.$linha['nome'].'</td>
						<td class="borda">'.$linha['cpf'].'</td>
						<td class="borda">
							<a href="javascript:excluir('.$linha['sacado'].');" title="Excluir"><img src="img/cancelar.png" /></a>
							&nbsp;&nbsp;
							<a href="javascript:navega(\'cliente_alterar.php?sacado='.$linha['sacado'].'\');" title="Alterar"><img src="img/editar.png" /></a>
						</td>
					</tr>
				';
			}
		}
		echo '
		    </table>
		';
	}

	//ALTERA CLIENTE

	else if($funcao == 'alt_cliente'){
		$sacado         = $_GET["sacado"];
		$nome           = $_GET["nome"];
		$id             = $_GET["identidade"];
		$cpfcnpj        = $_GET["cpfcnpj"];
		$endereco       = $_GET["endereco"];
		$bairro         = $_GET["bairro"];
		$cidade         = $_GET["cidade"];
		$uf             = $_GET["uf"];
		$cep            = $_GET["cep"];
		$tel_pessoal    = $_GET["tel_pessoal"];
		$tel_comercial  = $_GET["tel_comercial"];
		$celular        = $_GET["celular"];
		$fax            = $_GET["fax"];
		$email          = $_GET["email"];
		$grupo          = $_GET["grupo"];
		$grupo = (empty($grupo) || $grupo == '0') ? 'NULL' : $grupo = "'".$grupo."'";

		$alterar = "UPDATE sacados SET nome=UPPER('$nome'), identidade=UPPER('$id'), cpf='$cpfcnpj', endereco=UPPER('$endereco'), bairro=UPPER('$bairro'), cidade=UPPER('$cidade'), ";
		$alterar .= "uf=UPPER('$uf'), cep='$cep', telef_pess='$tel_pessoal', telef_com='$tel_comercial', celular='$celular', fax='$fax', email='$email', gsc_id = $grupo ";
		$alterar .= "WHERE sacado='$sacado'";
		auditoria($_SESSION['userLogin'],$agencia, $conta, $_SERVER['REMOTE_ADDR'], "cadastrar cliente", mysql_real_escape_string($alterar));
		$sql = mysql_query($alterar);
		if($sql) {
			echo 'ok';
		}
		else {
			echo 'Erro!<br>';
			mysql_error();
		}
	}

	// EXCLUIR CLIENTE

	else if ($funcao=='excluir') {
		$sacado = $_GET['sacado'];
		$excluir = "DELETE FROM sacados WHERE sacado='$sacado'";
		$sql = mysql_query($excluir);
		if($sql) {
			echo 'ok';
		}
		else {
			echo 'Erro!<br>';
			mysql_error();
		}
	}
?>
