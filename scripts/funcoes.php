<?php
	include('../config.php');
	include('limpa.php');
	session_start();
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	$_GET = sanitize($_GET);
	$funcao = $_GET['funcao'];
	mysql_query("SET NAMES UTF8") or die(mysql_error());
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}


	// PESQUISA TITULOS
	$avencer=date("Y-m-d",mktime (0, 0, 0, date("m"), date("d")+15, date("Y")));
	if($funcao == 'pes_prev_per'){ // PREVISAO POR PERÍODO
		if(!empty($_GET['inicio'])&&!empty($_GET['fim'])){
			$inicio = $_GET['inicio'];
			$fim = $_GET['fim'];
			$inicio = explode('/',$inicio);
			$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
			$fim = explode('/',$fim);
			$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];
			$busca = "AND data_venc>='$inicio' AND data_venc<='$fim'";
		}
		else{
			$busca = "";
		}

		$qPrev = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t
			INNER JOIN sacados AS S ON(s.sacado=t.sacado )
			WHERE t.cliente='$cliente'
			AND data_venc>=CURDATE()
			AND data_venc<='$avencer'
			AND data_baixa_manual IS NULL
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND t.cad_completo = 'S'
			$busca
			ORDER BY t.data_venc DESC";
		$sPrev = mysql_query($qPrev) or die(mysql_error());
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda centro" width="25%">Vencimento</td>
                    <td class="destaque borda centro">Sacado</td>
                    <td class="destaque borda centro" width="25%">Valor</td>
                </tr>
		';
		if(mysql_num_rows($sPrev) > 0){
			while($aPrev = mysql_fetch_array($sPrev)){
				echo '
					<tr>
						<td class="borda centro">'.$aPrev['data_vencimento'].'</td>
						<td class="borda centro">'.$aPrev['nome'].'</td>
						<td class="borda centro">R$ '.number_format($aPrev['valor'],2,',','.').'</td>
					</tr>
				';
			}
		}
		else{
			echo '
					<tr>
						<td class="borda centro" colspan="3">Nenhum resultado encontrado</td>
					</tr>
				';
		}
		echo '</table>';
	}

	else if($funcao == 'pes_prev_val'){ // PREVISAO POR VALOR
		if(!empty($_GET['inicio'])&&!empty($_GET['fim'])){
			$inicio = $_GET['inicio'];
			$fim = $_GET['fim'];
			$inicio = str_replace(".","",$inicio);
			$inicio = str_replace(",",".",$inicio);
			$fim = str_replace(".","",$fim);
			$fim = str_replace(",",".",$fim);
			$busca = "AND t.valor >= $inicio AND t.valor <= $fim";
		}
		else{
			$busca = "";
		}

		$qPrev = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t
			INNER JOIN sacados as s ON(s.sacado=t.sacado)
			WHERE t.cliente='$cliente'
			AND data_venc>=CURDATE()
			AND data_venc<='$avencer'
			AND data_baixa_manual IS NULL
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND t.cad_completo = 'S'
			$busca
			ORDER BY t.data_venc DESC";
		$sPrev = mysql_query($qPrev) or die(mysql_error());
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda centro" width="25%">Vencimento</td>
                    <td class="destaque borda centro">Sacado</td>
                    <td class="destaque borda centro" width="25%">Valor</td>
                </tr>
		';
		if(mysql_num_rows($sPrev) > 0){
			while($aPrev = mysql_fetch_array($sPrev)){
				echo '
					<tr>
						<td class="borda centro">'.$aPrev['data_vencimento'].'</td>
						<td class="borda centro">'.$aPrev['nome'].'</td>
						<td class="borda centro">R$ '.number_format($aPrev['valor'],2,',','.').'</td>
					</tr>
				';
			}
		}
		else{
			echo '
					<tr>
						<td class="borda centro" colspan="3">Nenhum resultado encontrado</td>
					</tr>
				';
		}
		echo '</table>';
	}

	else if($funcao == 'pes_prev_nome'){ // PREVISAO POR NOME
		$nome = $_GET['nome'];

		$qPrev = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t
			INNER JOIN sacados AS s on(s.sacado=t.sacado )
			WHERE t.cliente='$cliente'
			AND data_venc>=CURDATE()
			AND data_venc<='$avencer'
			AND data_baixa_manual IS NULL
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND t.cad_completo = 'S'
			AND UPPER(s.nome) LIKE UPPER('%$nome%')
			ORDER BY t.data_venc DESC";
		$sPrev = mysql_query($qPrev) or die(mysql_error());
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda centro" width="25%">Vencimento</td>
                    <td class="destaque borda centro">Sacado</td>
                    <td class="destaque borda centro" width="25%">Valor</td>
                </tr>
		';
		if(mysql_num_rows($sPrev) > 0){
			while($aPrev = mysql_fetch_array($sPrev)){
				echo '
					<tr>
						<td class="borda centro">'.$aPrev['data_vencimento'].'</td>
						<td class="borda centro">'.$aPrev['nome'].'</td>
						<td class="borda centro">R$ '.number_format($aPrev['valor'],2,',','.').'</td>
					</tr>
				';
			}
		}
		else{
			echo '
					<tr>
						<td class="borda centro" colspan="3">Nenhum resultado encontrado</td>
					</tr>
				';
		}
		echo '</table>';
	}

	else if($funcao == 'pes_atr_per'){ // ATRASO POR PERÍODO
		if(!empty($_GET['inicio'])&&!empty($_GET['fim'])){
			$inicio = $_GET['inicio'];
			$fim = $_GET['fim'];
			$inicio = explode('/',$inicio);
			$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
			$fim = explode('/',$fim);
			$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];
			$busca = "AND data_venc>='$inicio' AND data_venc<='$fim'";
		}
		else{
			$busca = "";
		}

		$qAtr = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t
			INNER JOIN sacados AS s on(s.sacado=t.sacado )
			WHERE t.cliente='$cliente'
			AND data_venc<CURDATE()
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND status != '03'
			AND data_baixa_manual IS NULL
			AND t.cad_completo = 'S'
			$busca
			ORDER BY t.data_venc";
		$sAtr = mysql_query($qAtr) or die(mysql_error());
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda centro" width="25%">Vencimento</td>
                    <td class="destaque borda centro">Sacado</td>
                    <td class="destaque borda centro" width="25%">Valor</td>
                </tr>
		';
		if(mysql_num_rows($sAtr) > 0){
			while($aAtr = mysql_fetch_array($sAtr)){
				echo '
					<tr>
						<td class="borda centro">'.$aAtr['data_vencimento'].'</td>
						<td class="borda centro">'.$aAtr['nome'].'</td>
						<td class="borda centro">R$ '.number_format($aAtr['valor'],2,',','.').'</td>
					</tr>
				';
			}
		}
		else{
			echo '
					<tr>
						<td class="borda centro" colspan="3">Nenhum resultado encontrado</td>
					</tr>
				';
		}
		echo '</table>';
	}

	else if($funcao == 'pes_atr_val'){ // ATRASO POR VALOR
		if(!empty($_GET['inicio'])&&!empty($_GET['fim'])){
			$inicio = $_GET['inicio'];
			$fim = $_GET['fim'];
			$inicio = str_replace(".","",$inicio);
			$inicio = str_replace(",",".",$inicio);
			$fim = str_replace(".","",$fim);
			$fim = str_replace(",",".",$fim);
			$busca = "AND t.valor >= $inicio AND t.valor <= $fim";
		}
		else{
			$busca = "";
		}

		$qAtr = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t
			INNER JOIN sacados AS s on(s.sacado=t.sacado )
			WHERE t.cliente='$cliente'
			AND data_venc<CURDATE()
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND status != '03'
			AND data_baixa_manual IS NULL
			AND t.cad_completo = 'S'
			$busca
			ORDER BY t.data_venc";
		$sAtr = mysql_query($qAtr) or die(mysql_error());
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda centro" width="25%">Vencimento</td>
                    <td class="destaque borda centro">Sacado</td>
                    <td class="destaque borda centro" width="25%">Valor</td>
                </tr>
		';
		if(mysql_num_rows($sAtr) > 0){
			while($aAtr = mysql_fetch_array($sAtr)){
				echo '
					<tr>
						<td class="borda centro">'.$aAtr['data_vencimento'].'</td>
						<td class="borda centro">'.$aAtr['nome'].'</td>
						<td class="borda centro">R$ '.number_format($aAtr['valor'],2,',','.').'</td>
					</tr>
				';
			}
		}
		else{
			echo '
					<tr>
						<td class="borda centro" colspan="3">Nenhum resultado encontrado</td>
					</tr>
				';
		}
		echo '</table>';
	}

	else if($funcao == 'pes_atr_nome'){ // ATRASO POR NOME
		$nome = $_GET['nome'];

		$qAtr = "SELECT DATE_FORMAT(t.data_venc, '%d/%c/%Y') AS data_vencimento, s.nome, t.valor
			FROM titulos AS t
			INNER JOIN sacados AS s on(s.sacado=t.sacado )
			WHERE t.cliente='$cliente'
			AND data_venc<CURDATE()
			AND data_baixa IS NULL
			AND cancelamento IS NULL
			AND status != '03'
			AND data_baixa_manual IS NULL
			AND t.cad_completo = 'S'
			AND UPPER(s.nome) LIKE UPPER('%$nome%')
			ORDER BY t.data_venc";
		$sAtr = mysql_query($qAtr) or die(mysql_error());
		echo '
			<table>
            	<tr class="cinza">
                	<td class="destaque borda centro" width="25%">Vencimento</td>
                    <td class="destaque borda centro">Sacado</td>
                    <td class="destaque borda centro" width="25%">Valor</td>
                </tr>
		';
		if(mysql_num_rows($sAtr) > 0){
			while($aAtr = mysql_fetch_array($sAtr)){
				echo '
					<tr>
						<td class="borda centro">'.$aAtr['data_vencimento'].'</td>
						<td class="borda centro">'.$aAtr['nome'].'</td>
						<td class="borda centro">R$ '.number_format($aAtr['valor'],2,',','.').'</td>
					</tr>
				';
			}
		}
		else{
			echo '
					<tr>
						<td class="borda centro" colspan="3">Nenhum resultado encontrado</td>
					</tr>
				';
		}
		echo '</table>';
	}
?>
