<?php
	include('../config.php');
	include('limpa.php');
	include("auditoria.php");
	session_start();
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	$_GET = sanitize($_GET);
	if(empty($_SESSION['userAgencia'])){
		header('location: login.php?res=1');
	}
	$funcao = $_GET['funcao'];


	// ALTERAR INSTRUÇÃO
	if($funcao == 'inst'){
		$instrucao = $_GET['instruc'];
		$instrucao = str_replace("ç","C",$instrucao);
		$instrucao = str_replace("ã","A",$instrucao);
		$instrucao = str_replace("õ","O",$instrucao);
		$instrucao = str_replace("â","A",$instrucao);
		$instrucao = str_replace("ê","E",$instrucao);
		$instrucao = str_replace("ô","O",$instrucao);
		$instrucao = str_replace("à","A",$instrucao);
		$instrucao = str_replace("á","A",$instrucao);
		$instrucao = str_replace("é","E",$instrucao);
		$instrucao = str_replace("í","I",$instrucao);
		$instrucao = str_replace("ó","O",$instrucao);
		$instrucao = str_replace("ú","U",$instrucao);

		$qInst = "UPDATE clientes SET instrucao=UPPER('$instrucao') WHERE cliente='$cliente'";
		$sInst = mysql_query($qInst);

		if($sInst){
			echo 'ok';
		}
		else{
			echo mysql_error();
		}
	}


	// PARAMETRIZAÇÃO
	if($funcao == 'para'){
		$boleto = $_GET['boleto'];
		$nome = $_GET['nome'];
		$modal = $_GET['modal'];

		if (empty($nome))
		{
			$nome = '';
		}
		else
		{
			$nome = ', nome_fantasia=UPPER("'.$nome.'")';
		}

		if (empty($modal))
		{
			$modal = ', modal_padrao = NULL';
		}
		else
		{
			$modal = ', modal_padrao = "'.$modal.'"';
		}

		$nome = str_replace("ç","Ç",$nome);
		$nome = str_replace("ã","Ã",$nome);
		$nome = str_replace("õ","Õ",$nome);
		$nome = str_replace("â","Â",$nome);
		$nome = str_replace("ê","Ê",$nome);
		$nome = str_replace("ô","Ô",$nome);
		$nome = str_replace("à","À",$nome);
		$nome = str_replace("á","Á",$nome);
		$nome = str_replace("é","É",$nome);
		$nome = str_replace("í","Í",$nome);
		$nome = str_replace("ó","Ó",$nome);
		$nome = str_replace("ú","Ú",$nome);

		$qPara = "UPDATE clientes SET boleto_padrao='$boleto' $nome $modal WHERE cliente='$cliente'";
		$sPara = mysql_query($qPara);

		if($sPara){
			echo 'ok';
		}
		else{
			echo mysql_error();
		}
	}


	// ALTERAR INSTRUÇÃO 1ª VIA
	if($funcao == 'inst1'){
		$instrucao = $_GET['instruc'];
		$instrucao = str_replace("ç","C",$instrucao);
		$instrucao = str_replace("ã","A",$instrucao);
		$instrucao = str_replace("õ","O",$instrucao);
		$instrucao = str_replace("â","A",$instrucao);
		$instrucao = str_replace("ê","E",$instrucao);
		$instrucao = str_replace("ô","O",$instrucao);
		$instrucao = str_replace("à","A",$instrucao);
		$instrucao = str_replace("á","A",$instrucao);
		$instrucao = str_replace("é","E",$instrucao);
		$instrucao = str_replace("í","I",$instrucao);
		$instrucao = str_replace("ó","O",$instrucao);
		$instrucao = str_replace("ú","U",$instrucao);
		$instrucao = preg_replace("/(\\r)?\\n/i", " ", $instrucao);

		$qInst1 = "UPDATE clientes SET instrucao_1via=UPPER('$instrucao') WHERE cliente='$cliente'";
		$sInst1 = mysql_query($qInst1) or die (mysql_error());

		if($sInst1){
			echo 'ok';
		}
		else{
			echo mysql_error();
		}
	}
// 
	

	// ALTERAR SENHA

	else if($funcao == "alt_senha"){
		$atual = $_GET['at'];
		$nova = $_GET['n'];

		$qConf = "SELECT cliente FROM clientes WHERE cliente='$cliente' AND senha='$atual'";
		$sConf = mysql_query($qConf) or die(mysql_error());
		$nConf = mysql_num_rows($sConf);
		if($nConf == 1){
			$qAt = "UPDATE clientes SET senha='$nova' WHERE cliente='$cliente'";
			auditoria($_SESSION['userLogin'],$agencia, $conta, $_SERVER['REMOTE_ADDR'], "ALTERAR SENHA", mysql_real_escape_string($qAt));
			$sAt = mysql_query($qAt);
			if($sAt){
				echo 'ok';
			}
			else {
				mysql_error();
			}
		}
		else {
			echo 'Senha inválida';
		}

	}

?>
