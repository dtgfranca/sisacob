﻿<?php

// Prepara conexão com banco
require_once("../config.php");

// Evita timeout e mostra erros
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
ini_set('max_execution_time', 0);

// Formata página para código e instancia variáveis
echo "<pre>";
$total = 0;
$sacados = array();

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

// Seleciona os sacados com repetições
$sql = "SELECT GROUP_CONCAT(sacado) AS sacados, COUNT(*) AS qtde, cpf, nome, cliente, agencia 
		FROM sacados WHERE grupo IS NULL 
		GROUP BY cpf, nome, cliente, agencia 
		HAVING COUNT(*) > 1 
		ORDER BY COUNT(*) DESC LIMIT 1";

// Roda a seleção		
$query = mysql_query($sql) or die(mysql_error());

if (mysql_num_rows($query) == 0)
echo "Nenhum registro duplicado (nome, CNPJ, cliente e agência iguais).";

else
{
	while ($row = mysql_fetch_assoc($query))
	{
		$time2 = microtime();
		$time2 = explode(' ', $time2);
		$time2 = $time2[1] + $time2[0];
		$start2 = $time2;

		echo "{$row['qtde']} sacados encontrados com o nome ".trim($row['nome']).", CPF/CNPJ {$row['cpf']}, cliente {$row['cliente']} e agencia {$row['agencia']}.";
	
		// Lista os valores repetidos e organiza-os do menor para o maior
		$repetidos = explode(",", $row['sacados']);
		sort($repetidos);
		$total += count($repetidos);
		
		echo "<br>Duplicações: ".implode(", ", $repetidos)."<br>";
		
		// Pega o menor valor e remove-o da listagem
		$original = $repetidos[count($repetidos) - 1];
		unset($repetidos[count($repetidos) - 1]);
		
		// Agrupa o restante das repetições
		$sacados = array_merge($sacados, $repetidos);
		
		// Executa a atualização dos registros
		echo "Atualizando chaves estrangeiras para {$original}, nas tabelas cad_instrucoes e titulos.<br>";
		echo "UPDATE cad_instrucoes SET sacado = '".$original."' WHERE sacado IN (".implode(",", $repetidos).")<br>";
		echo "UPDATE titulos SET sacado = '".$original."' WHERE sacado IN (".implode(",", $repetidos).")<br>";
		
		echo "Removendo sacados duplicados.<br>";
		echo "DELETE FROM sacados WHERE sacado IN (".implode(",", $repetidos).")<br>";
		
		$time2 = microtime();
		$time2 = explode(' ', $time2);
		$time2 = $time2[1] + $time2[0];
		$finish2 = $time2;
		$total_time2 = round(($finish2 - $start2), 4);

		echo "Finalizado.<br>Tempo de execução: ".number_format($total_time2, 4, ",", ".")."s<br><br>";
	}
}

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);

// Finaliza a execução e mostra o resultado
echo "<br>|-----------------------------------------------------------------------|<br>";
echo "Total de registros removidos: {$total}<br>Tempo de execução total: ".number_format($total_time, 4, ",", ".")."s</pre>";

?>