<?php

// Instancia as variáveis //
$message = "";
$result = "";
$account = "";

// Verifica se o botão "Entrar" foi pressionado //
if (isset($_POST['submitted']) && $_POST['submitted'] == "yes")
{
	// Atualiza variável //
	$account = $_POST['account'];
	
	// Adiciona hífen se não houver //
	if (!strstr($account, "-"))
		$account = substr($_POST['account'], 0, strlen($_POST['account']) - 1)."-".substr($_POST['account'], strlen($_POST['account']) - 1, 1);
	
	// Adiciona zeros à esquerda //
	$account = str_pad($account, 10, '0', STR_PAD_LEFT);
	
	// Conexão ao banco de dados //
	mysql_connect("localhost", "root", "Skillnet69547") or die("Não foi possível se conectar ao servidor!");
	mysql_select_db("skill_sisacob") or die("Não foi possível selecionar a fonte dos dados!");
	
	// Consulta de usuário e senha //
	$query = mysql_query("SELECT agencia, usuario, conta, senha, razao FROM clientes WHERE (agencia = 4212 OR agencia =4117) AND usuario = '".$_POST['user']."' AND conta = '".$account."' AND senha = '".mysql_real_escape_string($_POST['password'])."' LIMIT 1");
	
	// Não há cliente com estes dados //
	if (mysql_num_rows($query) != 1)
	$message = "LOGIN INVÁLIDO!";
	
	else
	{
		$razao = mysql_result($query, 0, "razao");
		
		// Consulta na tabela de limites //
		$query = mysql_query("SELECT * FROM limites WHERE (agencia = 4212 OR agencia = 4117) AND conta = '".$account."' LIMIT 1");
		$result = mysql_fetch_assoc($query);
		
		// Usuário existe, mas não constam dados na tabela //
		if (mysql_num_rows($query) != 1)
			$message = "NENHUM LIMITE ENCONTRADO!";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consulta de Limite Disponível</title>

<style type="text/css">
body
{
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 13px;
	text-transform: uppercase;
}

input[type=text], input[type=password]
{
	width: 110px;	
	font-size: 13px;	
}

input[type=button]
{
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 13px;
	background: #fff;
	height: 30px;
	width: 100px;
	color: #695E4A;
	font-weight: 600;
}

.uppercase
{
	text-transform: uppercase;
}

.green
{
	text-align: center;
	color: #006600;
}

.red
{
	text-align: center;
	color: red;
}
</style>
<script type="text/javascript">
function contains(char)
{
	if (document.getElementById('account').value.indexOf(char) != -1)
		return true;

	else
		return false;
}

function validateNumber(event)
{
	// Verifica evento //
	var key = window.event ? event.keyCode : event.which;
	//alert(key);

	// Caracteres não permitidos //
	if ((key >= 65 && key <= 90) || key >= 146 || (key >= 106 && key <= 110 && key != 109)) 
		return false;
		
	// Permite o caractere //
	else
	{
		// Adiciona hífen a número da conta //
		//if (document.getElementById('account').value.length == 6)
			//document.getElementById('account').value += '-';

		return true;		
	}
}

function validateForm()
{
	// Variável de backup de segurança //
	abort = false;

	// Valida a conta //
	if (document.getElementById('account').value.length <= 2)
	{
		alert('Preencha corretamente o campo "Conta"!');
		abort = true;
		return false;
	}
	
	// Valida o dígito //
	/*else if (document.getElementById('account').value.indexOf("-") == -1)
	{
		alert('Preencha corretamente o campo dígito da conta!');
		abort = true;
		return false;
	}*/
	
	// Valida caracteres especiais //
	else if (contains('~') || contains('[') || contains(']') || contains(',') || contains('.') || contains(';') || contains(':') || contains('/') || contains('\\') || contains('^') || contains('´') || contains('`'))
	{
		alert('Caracteres não permitidos no campo "Conta"!');
		abort = true;
		return false;
	}

	// Valida o usuário //
	else if (document.getElementById('user').value.length <= 2)
	{
		alert('Preencha corretamente o campo "Usuário"!');
		abort = true;
		return false;
	}
	
	// Valida a senha //
	else if (document.getElementById('password').value.length <= 2)
	{
		alert('Preencha corretamente o campo "Senha"!');
		abort = true;
		return false;
	}
	
	// Tudo validado, prossiga...  //
	else
	{
		// Verifica variável de backup  //
		if (abort == false)
		{
			// Configura o valor de controle de formulário enviado  //
			document.getElementById('hidden').value = "yes";
			
			// Envia o formulário  //
			document.getElementById('myform').submit();
			
			return true;
		}
	}
}
</script>
</head>

<body onload="document.getElementById('account').focus()">
<!-- Formulário -->
<form id="myform" action="" method="post" onkeypress="if (event.keyCode == 13) validateForm()">
<input id="hidden" type="hidden" name="submitted" value="no" />

<!-- Tabela exterior -->
<table border="0" align="center" style="width: 100%; height: 100%"><tr><td>
<table align="center" style="width: 624px; height: 264px; border: #006600 thin solid; color: #666; background-color: #fafafa">
<tr id="verifique"><td style="color: #695E4A; padding: 7px">VERIFIQUE AQUI SEU LIMITE PARA DESCONTO DE CHEQUES E NOTAS PROMISSÓRIAS E AINDA FAÇA SUA CUSTÓDIA.</td></tr><tr><td>

<!-- Tabela interior  -->
<table id="table" align="center" style="width: 250px">
	<tr><td>&nbsp;</td></tr>
    
    <!-- Se houver erro ou não houver dado, mostra formulário -->
	<?php if ($result == "" || $message != "") { ?>
	<tr><td colspan="2" class="red"><?= $message ?></td></tr>
	<tr>
    	<td colspan="2" class="green">Insira os dados abaixo:</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    	<td>Conta:</td><td><input id="account" type="text" name="account" placeholder="EX.: 123456-7" onkeydown="return validateNumber(event)" maxlength="10"/></td>
    </tr>
    <tr>
        <td>Usuário:</td><td><input id="user" type="text" name="user" class="uppercase" placeholder="Ex.: JOSE" maxlength="20"/></td>
    </tr>
    <tr>   
        <td>Senha:</td><td><input id="password" type="password" name="password" placeholder="****"  maxlength="20"/></td>        
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td colspan="2"><input type="button" name="logar" value="ENTRAR" onclick="validateForm()"/></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
	
	<?php } else { ?>
    <!-- Aumenta tamanho da tabela -->	
    <script type="text/javascript">document.getElementById('table').style.width = "500px"; document.getElementById('verifique').style.display='none'</script>
    
    <!-- Nenhum errro encontrado, mostrar resultado -->	
	<tr>
    	<td colspan="2" class="green">Informações referentes à conta <strong><?= $account ?></strong>:</td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">   
        <td align="right" width="30%"><strong>Nome:&nbsp;&nbsp;</strong></td><td align="left"><?= $razao ?></td>        
    </tr>
<tr><td>&nbsp;</td></tr>
    <tr align="center">   
        <td align="right"><strong>Vencimento:&nbsp;&nbsp;</strong></td><td align="left"><?= date("d/m/Y", strtotime($result["vencimento"])) ?></td>        
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td align="right"><strong>Limite:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($result["limite"], 2, ",", ".") ?></td>
    </tr>
    <tr align="center">
        <td align="right"><strong>Utilizado:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format($result["utilizado"], 2, ",", ".") ?></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr align="center">
        <td align="right"><strong>Disponível:&nbsp;&nbsp;</strong></td><td align="left">R$ <?= number_format(($result["limite"] - $result["utilizado"]), 2, ",", ".") ?></td>
    </tr>
	<tr><td>&nbsp;</td></tr>
    <tr align="center">
    	<td colspan="2"><input type="button" name="voltar" value="VOLTAR" onclick="document.location.href='http://www.credigerais.com.br'"/></td>
    </tr>
	
    <!-- Encerra emissão do resultado  -->
	<?php } ?>

<!-- Fecha tabela interior -->
</table></td></tr><tr><td style="color: #695E4A; padding: 7px; text-align: center; font-weight: bold">Para mais informações, ligue: (38) 3821-2989.</td></tr><tr><td>&nbsp;</td></tr></table>

<!-- Fecha tabela exterior -->
</td></tr></table>

<!-- Fecha formulário e página -->
</form></body></html>