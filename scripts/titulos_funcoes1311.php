<?php

include '../config.php';
include 'limpa.php';
session_start();
$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];
$master = $_SESSION['userMaster'];

$_GET = sanitize($_GET);
$funcao = $_GET['funcao'];

if (empty($_SESSION['userAgencia'])) {
	header('location: login.php?res=1');
}
mysql_query("SET NAMES UTF8") or die(mysql_error());

function toDecimal($numero) {
	if ($numero == "") {
		return "0.00";
	}

	return str_replace(",", ".", str_replace(".", "", $numero));
}

// VERIFICAR HORA

if ($funcao == "verificarHora") {
	$sql = mysql_query("SELECT TIME_FORMAT(NOW(), '%H:%i') AS hora_atual,
							TIME_FORMAT(hora_bloqueio, '%H:%i') AS hora_bloqueio
							FROM agencias
							WHERE agencia=$agencia ") or die(mysql_error());
	if (mysql_num_rows($sql) > 0) {
		$tempo = mysql_fetch_assoc($sql);
		if ($tempo['hora_bloqueio'] == "") {
			echo "ok";

		} else {

			$hora_bloqueio = strtotime($tempo['hora_bloqueio']);
			$hora_atual = strtotime($tempo['hora_atual']);
			if ($hora_bloqueio <= $hora_atual) {
				echo 1;
			} else {
				echo "ok";
			}
		}
	} else {
		echo "Consulta não efetuada corretamente!";
	}
}

// VERIFICAR DOCUMENTO

else if ($funcao == "verificarDocumento") {
	$sql = mysql_query("SELECT documento FROM titulos WHERE agencia=$agencia AND cliente=" . $_GET['cliente'] . " AND documento='" . $_GET['documento'] . "'") or die(mysql_error());
	if (mysql_num_rows($sql) > 0) {
		echo 1;
	} else {
		echo "ok";
	}
}

// CADASTRAR TITULO

else if ($funcao == "cad_titulo") {
	$documento = $_GET['documento'];
	$qtd_boletos = $_GET['qtd_boletos'];
	$vencimento = $_GET['venc'];
	$intervalo = $_GET['intervalo'];
	$valor_total = $_GET['valor'];
	if (empty($_GET['desconto']) || $_GET['desconto'] == '') {
		$desconto = '0,00';
	} else {
		$desconto = $_GET['desconto'];
	}

	$valor_total = str_replace('.', '', $valor_total);
	$valor_total = str_replace(',', '.', $valor_total);

	for ($i = 1; $i <= $qtd_boletos; $i++) {
		$valor = $valor_total / $qtd_boletos;
		$valor = number_format($valor, 2, ',', '.');

		echo '
				<table width="575">
					<tr>
						<td width="45"><span style="font-weight: bold">Fatura:</span></td>
						<td width="75">' . $documento;
		if ($qtd_boletos > 1) {
			echo '/' . $i;
		}
		echo '
						</td>
						<td width="40"><span style="font-weight: bold">Valor:</span></td>
						<td width="125">R$ ' . $valor . '</td>
						<td width="65"><span style="font-weight: bold">Desconto:</span></td>
						<td width="80">R$ ' . $desconto . '</td>
						<td width="75"><span style="font-weight: bold">Vencimento:</span></td>
						<td width="80">';
		if (empty($intervalo2)) {
			$intervalo2 = $intervalo;
			$vencimento = $vencimento;
		} else {
			list($dia, $mes, $ano) = explode('/', $vencimento);
			$vencimento = date("d/m/Y", mktime(0, 0, 0, date($mes), date($dia) + $intervalo2, date($ano)));
		}
		echo
		$vencimento . '</td>
					</tr>
				</table>
			';
	}
}

// CONFIRMA CADASTRAR

else if ($funcao == "reg_titulo") {
	$cliente2 = $_GET['cliente'];
	$sacador = $_GET['sacador'];
	$modelo_boleto = $_GET['modelo_boleto'];
	$modalidade_boleto = $_GET['modalidade_boleto'];
	$documento = $_GET['documento'];
	$qtd_boletos = $_GET['qtd_boletos'];
	$data_emissao = $_GET['data_emissao'];
	$vencimento = $_GET['venc'];
	$intervalo = $_GET['intervalo'];
	$protesto = $_GET['protesto'] == '' ? 0 : $_GET['protesto'];
	$dias_protesto = $_GET['dias_protesto'];
	$valor_total = toDecimal($_GET['valor']);
	$multa = "0.00";
	$desconto = toDecimal($_GET['desconto']);
	$juros = "0.00";
	$data_desconto = "0000-00-00";
	$instrucoes = '';

	if (!isset($_SESSION['userAgencia'])) {
		$agencia = $_GET['agencia'];
	}

	if (!isset($_SESSION['userCliente'])) {
		$cliente = $_GET['usuario'];
	}

	if ($_GET['multa'] != '') {
		$multa = toDecimal($_GET['multa']);
		$multa = str_replace('%', '', $multa);
	}
	if ($_GET['juros'] != '') {
		$juros = toDecimal($_GET['juros']);
		$juros = str_replace('%', '', $juros);
	}
	if (!empty($_GET['data_desconto']) || $_GET['data_desconto'] != '' || $_GET['data_desconto'] != NULL) {
		$data_desconto = $_GET['data_desconto'];
		$data_desconto = explode('/', $data_desconto);
		$data_desconto = $data_desconto[2] . '-' . $data_desconto[1] . '-' . $data_desconto[0];
	}

	if (isset($_GET['instrucoes'])) {
		$instrucoes = $_GET['instrucoes'];
	}

	$documento = str_pad($documento, 8, '0', STR_PAD_LEFT);

	$doc = "SELECT documento FROM titulos WHERE cliente='$cliente' AND documento='$documento'";
	$query_doc = mysql_query($doc) or die(mysql_error());
	if (mysql_num_rows($query_doc) > 0 && $agencia != 4097) {
		echo "boleto";
	} else {
		$sql = "SELECT nossonumero FROM clientes WHERE cliente='$cliente'";
		$query = mysql_query($sql) or die(mysql_error());
		$linha = mysql_fetch_assoc($query);
		$nossonumero_base = $linha['nossonumero'];

		$data_emissao = explode('/', $data_emissao);
		$data_emissao = $data_emissao[2] . '-' . $data_emissao[1] . '-' . $data_emissao[0];
		$vencimento = explode('/', $vencimento);
		$vencimento = $vencimento[2] . '-' . $vencimento[1] . '-' . $vencimento[0];

		$a = NULL;

		for ($i = 1; $i <= $qtd_boletos; $i++) {
			$valor = $valor_total / $qtd_boletos;
			$valor = number_format($valor, 2, ',', '.');
			$valor = str_replace('.', '', $valor);
			$valor = str_replace(',', '.', $valor);
			if (empty($intervalo2)) {
				$intervalo2 = $intervalo;
				$vencimento = $vencimento;
			} else {
				list($ano, $mes, $dia) = explode('-', $vencimento);
				$vencimento = date("Y-m-d", mktime(0, 0, 0, date($mes), date($dia) + $intervalo2, date($ano)));
			}
			if ($data_desconto == NULL || $data_desconto == '') {
				$data_desconto = '0000-00-00';
			} else {
				if (empty($intervalo3)) {
					$intervalo3 = $intervalo;
					$data_desconto = $data_desconto;
				} else {
					list($ano2, $mes2, $dia2) = explode('-', $data_desconto);
					$data_desconto = date("Y-m-d", mktime(0, 0, 0, date($mes2), date($dia2) + $intervalo3, date($ano2)));
				}
			}
			$nossonumero_base = $nossonumero_base + 1;
			$nossonumero_base = str_pad($nossonumero_base, 6, "0", STR_PAD_LEFT);
			$cliente = str_pad($cliente, 4, "0", STR_PAD_LEFT);
			$nossonumero = $cliente . $nossonumero_base;

			$cad_completo = $modalidade_boleto == 'R' ? 'S' : 'N';

			if ($agencia == '4097' && $modalidade_boleto == 'R') {
				$atualizar = "UPDATE clientes SET nosso_numero_bco = (nosso_numero_bco + 1) WHERE cliente = '$cliente'";
				$atualiza = mysql_query($atualizar) or die(mysql_error());

				$query_nn = mysql_query("SELECT nosso_numero_bco FROM clientes WHERE cliente = '$cliente'");
				$nossonumero = str_pad(mysql_result($query_nn, 0, 'nosso_numero_bco'), 10, '0', STR_PAD_LEFT);
			}

			$inserir = "INSERT INTO titulos (agencia,cliente,sacado,documento,sequencia,nossonumero,";
			$inserir .= "data_emisao,data_venc,valor,descontos,data_limite_desconto,multa,juros,protesto,dias_protesto,";
			$inserir .= "modelo,sacador,so_desconto,status,criacao,cad_completo,intervalo,instrucao) ";
			$inserir .= "VALUES ('$agencia','$cliente','$cliente2',UPPER('$documento'),'$i','$nossonumero',";
			$inserir .= "'$data_emissao','$vencimento',$valor,'$desconto','" . $data_desconto . "','$multa','$juros','$protesto',";
			$inserir .= "'$dias_protesto','$modelo_boleto','$sacador','$modalidade_boleto','01',NOW(),'" . $cad_completo . "',$intervalo,'$instrucoes')";
			$insere = mysql_query($inserir) or die(mysql_error());
			$cmdSql = mysql_query("SELECT MAX(titulo) as titulo FROM titulos ");
			$dados = mysql_fetch_assoc($cmdSql);
			$insert_id = $dados['titulo'];
			#$insert_id = mysql_insert_id();

			// Insere títulos gerados na listagem, caso seja geração por grupo
			if (isset($_GET['blg_id'])) {
				mysql_query("INSERT INTO bl_titulos (blg_id, tit_id) VALUES ('" . $_GET['blg_id'] . "', '" . $insert_id . "')");
			}

			$atualizar = "UPDATE clientes SET nossonumero='$nossonumero_base' WHERE cliente='$cliente'";
			$atualiza = mysql_query($atualizar) or die(mysql_error());

			if ($insere && $atualiza) {
				$a[] = array("id" => $insert_id, "resultado" => "ok");
			} else {
				$a = "er";
			}
		}

		if (is_array($a)) {
			echo json_encode($a);
		} else {
			echo $a;
		}

	}
}

// ALTERAR TITULO

else if ($funcao == "alt_titulo") {
	$documento = $_GET['documento'];
	$seq = $_GET['sequencia'];
	$vencimento = $_GET['venc'];
	$intervalo = $_GET['intervalo'];
	$valor = $_GET['valor'];
	$desconto = $_GET['desconto'];

	//$valor = str_replace('.','',$valor);
	//$valor = str_replace(',','.',$valor);

	echo '
			<table width="575px">
				<tr>
					<td width="60"><span style="font-weight: bold">Fatura:</span></td>
					<td width="80">' . $documento . '/' . $seq . '</td>
					<td width="50"><span style="font-weight: bold">Valor:</span></td>
					<td width="95">R$ ' . $valor . '</td>
					<td width="75"><span style="font-weight: bold">Desconto:</span></td>
					<td width="50">' . $desconto . '</td>
					<td width="95"><span style="font-weight: bold">Vencimento:</span></td>
					<td width="80">' . $vencimento . '</td>
				</tr>
			</table>
		';
}

// CONFIRMA ALTERAR

else if ($funcao == "alterar") {
	$titulo = $_GET['titulo'];
	$cliente2 = $_GET['cliente'];
	$sacador = $_GET['sacador'];
	$modelo_boleto = $_GET['modelo_boleto'];
	$modalidade_boleto = $_GET['modalidade_boleto'];
	$intervalo = $_GET['intervalo'];
	$valor = $_GET['valor'];
	$vencimento = $_GET['venc'];
	$multa = $_GET['multa'];
	$desconto = $_GET['desconto'];
	$data_desconto = $_GET['data_desconto'] == '' || $_GET['data_desconto'] == 'NULL' || $_GET['data_desconto'] == '(NULL)' ? '00/00/0000' : $_GET['data_desconto'];
	$protesto = $_GET['protesto'];
	$juros = $_GET['juros'] == '' || $_GET['juros'] == NULL ? '0,00' : $_GET['juros'];
	$instrucoes = '';

	if (isset($_GET['instrucoes'])) {
		$instrucoes = $_GET['instrucoes'];
	}

	$valor = str_replace('.', '', $valor);
	$valor = str_replace(',', '.', $valor);
	$multa = str_replace('%', '', $multa);
	$desconto = str_replace('.', '', $desconto);
	$desconto = str_replace(',', '.', $desconto);

	$multa = str_replace('.', '', $multa);
	$multa = str_replace(',', '.', $multa);

	$juros = str_replace('.', '', $juros);
	$juros = str_replace(',', '.', $juros);

	$vencimento = explode('/', $vencimento);
	$vencimento = $vencimento[2] . '-' . $vencimento[1] . '-' . $vencimento[0];

	$intervalo = empty($intervalo) ? 'NULL' : $intervalo;
	if ($data_desconto != '(NULL)') {
		$data_desconto = explode('/', $data_desconto);
		$data_desconto = $data_desconto[2] . '-' . $data_desconto[1] . '-' . $data_desconto[0];
	}

	if (empty($intervalo2)) {
		$intervalo2 = $intervalo;
		$vencimento = $vencimento;
	} else {
		list($ano, $mes, $dia) = explode('-', $vencimento);
		$vencimento = date("Y-m-d", mktime(0, 0, 0, date($mes), date($dia) + $intervalo2, date($ano)));
	}
	if ($data_desconto == '(NULL)') {
		$data_desconto = '0000-00-00';
	} else {
		if (empty($intervalo3)) {
			$intervalo3 = $intervalo;
			$data_desconto = $data_desconto;
		} else {
			list($ano2, $mes2, $dia2) = explode('-', $data_desconto);
			$data_desconto = date("Y-m-d", mktime(0, 0, 0, date($mes2), date($dia2) + $intervalo3, date($ano2)));
		}
	}

	$alterar = "UPDATE titulos SET sacado='$cliente2', data_venc='$vencimento', descontos='$desconto',
					data_limite_desconto='$data_desconto', valor='$valor', multa='$multa', modelo='$modelo_boleto',
					sacador='$sacador', so_desconto='$modalidade_boleto', intervalo= $intervalo,
					instrucao='$instrucoes', protesto='$protesto', juros ='$juros'
					WHERE titulo='$titulo'";

	$altera = mysql_query($alterar) or die(mysql_error());

	if ($altera) {
		$a = 'ok';
	} else {
		$a = "er";
	}

	echo $a;
}
// BUSCA TITULOS

else if ($funcao == 'busca') {
	$busca = $_GET['busca'];
	$filtro = $_GET['filtro'];
	$grupo = $_GET['grupo'];

	echo '<table>
            	<tr class="cinza">
                	<td width="3%" class="destaque borda centro"><a href="#" title="Selecionar todos"><input type="checkbox" id="chk_boxes"/></a></td>
                    <td width="4%" class="destaque borda centro">ID</td>
                    <td width="16%" class="destaque borda centro">CLIENTE</td>
                    <td width="12%" class="destaque borda centro">CPF/CNPJ</td>
                    <td width="7%" class="destaque borda centro">DOCUMENTO</td>
                    <td width="7%" class="destaque borda centro">NOSSO NUMERO</td>
                    <td width="6%" class="destaque borda centro">EMISSÃO</td>
                    <td width="6%" class="destaque borda centro">VENCIMENTO</td>
                    <td width="7%" class="destaque borda centro">VALOR</td>
                    <td width="4%" class="destaque borda centro">DESC.</td>
                    <td width="11%" class="destaque borda centro">AÇÕES</td>
                    <td width="18%" class="destaque borda centro">INSTRUÇÕES</td>
                </tr>';
	if (empty($busca)) {
		$crit = '';
	} else {
		$crit = " AND (scd.nome LIKE '%$busca%' OR scd.cpf LIKE '%$busca%')";
	}
	if ($filtro == 'nenhum') {
		$filt = "AND cancelamento IS NULL ";
	} else if ($filtro == 'n_confirmados') {
		$filt = "AND cad_completo <> 'S' AND status <> '03' AND cancelamento IS NULL ";
	} else if ($filtro == 'aberto') {
		$filt = "AND data_baixa IS NULL AND valor_baixa IS NULL AND data_baixa_manual IS NULL AND cancelamento IS NULL ";
	} else if ($filtro == 'liquidados') {
		$filt = "AND data_baixa IS NOT NULL AND valor_baixa IS NOT NULL AND data_baixa_manual IS NULL AND cancelamento IS NULL ";
	} else if ($filtro == 'baixados') {
		$filt = "AND data_baixa_manual IS NOT NULL AND cancelamento IS NULL ";
	} else if ($filtro == 'cancelados') {
		$filt = "AND cancelamento IS NOT NULL ";
	} else if ($filtro == 'grupo') {
		$filt = "AND scd.gsc_id = '$grupo' ";
	} else if ($filtro == 'exclusivo_desconto'){
		$filt = ' AND tit.so_desconto="N"  AND cad_completo <> "S" AND status <> "03" AND cancelamento IS NULL ';
	}

	$cont = 0;
	$sBusca = "SELECT scd.nome, scd.cpf, scd.gsc_id, tit.documento, tit.sequencia, tit.nossonumero, tit.agencia, tit.cad_completo, tit.titulo, ";
	$sBusca .= "DATE_FORMAT(tit.data_emisao, '%d/%m/%Y') AS data_emisao, DATE_FORMAT(tit.data_venc, '%d/%m/%Y') AS data_venc, ";
	$sBusca .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor, tit.criacao, tit.so_desconto, ";
	$sBusca .= "tit.devolucao, tit.data_baixa, tit.registro, tit.status, tit.sacado, tit.modelo ";
	$sBusca .= "FROM titulos AS tit ";
	$sBusca .= "INNER JOIN sacados AS scd ON scd.sacado=tit.sacado ";
	$sBusca .= "WHERE tit.cliente = '" . $cliente . "' " . $crit . " ";
	$sBusca .= "AND (scd.grupo <> '99999' OR scd.grupo IS NULL) ";
	$sBusca .= $filt;
	$sBusca .= "ORDER BY tit.criacao DESC, scd.nome, tit.documento, tit.sequencia ASC ";
	if(isset($_GET['limite'])){
		$sBusca .=' limit '.$_GET['limite'];
	}


	$query = mysql_query($sBusca) or die(mysql_error());
	while ($result = mysql_fetch_assoc($query)) {
		if ($result["cad_completo"] != "S") {
			$check = '<input type="checkbox" class="chk_boxes1" name="box[]" value="' . $result['titulo'] . '">';
		} else {
			$check = '';
		}
		$venc = explode('/', $result['data_venc']);
		$venc = $venc[2] . '-' . $venc[1] . '-' . $venc[0];
		if ($venc > date('Y-m-d')) {
			$venc = 'S';
		} else {
			$venc = 'N';
		}
		echo '
						<tr>
							<td class="borda centro">' . $check . '</td>
							<td class="borda centro">' . $result['titulo'] . '</td>
							<td class="borda">&nbsp;&nbsp;<span style="font-size:10px;">' . $result['nome'] . '</span></td>
							<td class="borda centro">' . $result['cpf'] . '</td>
							<td class="borda centro">' . $result['documento'] . "/" . $result['sequencia'] . '</td>
							<td class="borda centro">' . $result['nossonumero'] . '</td>
							<td class="borda centro">' . $result['data_emisao'] . '</td>
							<td class="borda centro">' . $result['data_venc'] . '</td>
							<td class="borda" style="text-align: right">R$ ' . $result['valor'] . '&nbsp;&nbsp;</td>
							<td class="borda centro">' . $result['so_desconto'] . '</td>
							<td class="borda centro">';
		if ($result["cad_completo"] != "S" && $result["status"] != "03") {
			echo '
									<a href="javascript:excluir(' . $result['titulo'] . ');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;
									<a href="javascript:navega(\'titulo_alterar.php?titulo=' . $result['titulo'] . '\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma(' . $result['titulo'] . ');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<select name="instrucoes" disabled="disabled">
										<option value="0">Selecione a Instrução</option>
									</select>
								</td>';
		} else if ($result["cad_completo"] != "S" && $result["status"] == "03" && $result["so_desconto"] == "S") {
			echo '
									<a href="javascript:navega(\'titulo_alterar.php?titulo=' . $result['titulo'] . '\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma(' . $result['titulo'] . ');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<b><font color="#F00">Título Devolvido</font></b>
								</td>';
		} else if ($result["cad_completo"] != "S" && $result["status"] == "03" && $result["so_desconto"] == "N") {
			echo '
									<a href="javascript:excluir(' . $result['titulo'] . ');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;
									<a href="javascript:navega(\'titulo_alterar.php?titulo=' . $result['titulo'] . '\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma(' . $result['titulo'] . ');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<b><font color="#F00">Título Devolvido</font></b>
								</td>';
		} else if ($result["cad_completo"] == "S" && $result["devolucao"] == NULL && $result["data_baixa"] == NULL && $result["registro"] != NULL && $result['so_desconto'] == 'N') {
			$titulo = $result['titulo'];
			$seq = $result['sequencia'];
			$doc = $result['documento'];
			$sac = $result['sacado'];
			$modelo = $result['modelo'];
			$link = '../boletophp/';
			//define o arquivo de acordo com o modelo
			if ($modelo == '1') {
// boleto normal
				$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
				$qt = mysql_num_rows($qQtd);
				$base = 'onclick="boleto(' . $titulo . ',' . $seq . ',' . $qt . ')"';
			} else if ($modelo == '2') {
// boleto 3 vias
				$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
				$qt = mysql_num_rows($qQtd);
				$base = 'onclick="boleto3vias(' . $titulo . ',' . $seq . ',' . $qt . ',' . $cliente . ')"';
			} else if ($modelo == '3') {
//carne
				$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
				$qt = mysql_num_rows($qQtd);
				$base = 'onclick="boletocarne(' . $titulo . ',' . $seq . ',' . $qt . ',' . $sac . ')"';
			}
			if ($result["status"] != '23') {
				echo '

										<img src="img/ok.png" />
										&nbsp;
										<img src="img/boleto.png" title="Imprimir Boleto" style="cursor: pointer;" ' . $base . ' />


									</td>

									<td class="borda centro">
										<select name="instrucoes" id="' . $result['titulo'] . '" onchange="javascript: instrucao(' . $result['titulo'] . ');">
											<option value="0">Selecione a Instrução</option>
											<option value="06">Alteração de Vencimento</option>
											<option value="10">Cancel.Sustação Instr. Pr</option>
											<option value="04">Concessão de Abatimento</option>
											<option value="31">Concessão de Desconto</option>
											<option value="35">Multa</option>
											<option value="09">Protestar</option>
											<option value="02">Solicitação de Baixa</option>
											<option value="05">Cancelamento Abatimento</option>
											<option value="32">Cancelamento Desconto</option>
										</select>
									</td>';
			}
			//Para credinova, quando status=23 - Protesto, não mostrar botao imprimir - 25/02/2015
			else {
				echo '
										<img src="img/ok.png" />
										&nbsp;


									</td>

									<td class="borda centro">
										<select name="instrucoes" id="' . $result['titulo'] . '" onchange="javascript: instrucao(' . $result['titulo'] . ');">
											<option value="0">Selecione a Instrução</option>
											<option value="06">Alteração de Vencimento</option>
											<option value="10">Cancel.Sustação Instr. Pr</option>
											<option value="04">Concessão de Abatimento</option>
											<option value="31">Concessão de Desconto</option>
											<option value="35">Multa</option>
											<option value="09">Protestar</option>
											<option value="02">Solicitação de Baixa</option>
											<option value="05">Cancelamento Abatimento</option>
											<option value="32">Cancelamento Desconto</option>
										</select>
									</td>';

			}
			//<option value="12">Alteração Endereço(Cliente)</option>;
		} else if ($result["cad_completo"] == "S" && $result["devolucao"] == NULL && $result["data_baixa"] == NULL && $result["registro"] != NULL && $result['so_desconto'] == 'S') {
			$titulo = $result['titulo'];
			$seq = $result['sequencia'];
			$doc = $result['documento'];
			$sac = $result['sacado'];
			$modelo = $result['modelo'];
			$link = '../boletophp/';
			//define o arquivo de acordo com o modelo
			if ($modelo == '1') {
// boleto normal
				$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
				$qt = mysql_num_rows($qQtd);
				$base = 'onclick="boleto(' . $titulo . ',' . $seq . ',' . $qt . ')"';
			} else if ($modelo == '2') {
// boleto 3 vias
				$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
				$qt = mysql_num_rows($qQtd);
				$base = 'onclick="boleto3vias(' . $titulo . ',' . $seq . ',' . $qt . ',' . $cliente . ')"';
			} else if ($modelo == '3') {
//carne
				if ($seq == 1) {
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boletocarnetudo(' . $titulo . ',' . $seq . ',' . $qt . ',' . $sac . ',' . $doc . ')"';
				} else {
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boletocarne(' . $titulo . ',' . $seq . ',' . $qt . ',' . $sac . ')"';
				}
			}
			if ($result["status"] != '23') {
				echo '
											<img src="img/ok.png" />
											&nbsp;
											<img src="img/boleto.png" title="Imprimir Boleto" style="cursor: pointer;" ' . $base . ' />
										</td>
										<td class="borda centro">
										';
			} else {
				echo '
											<img src="img/ok.png" />
											&nbsp;

										</td>
										<td class="borda centro">
										';
			}

			if ($master == true) {
				echo '
										<select name="instrucoes" id="' . $result['titulo'] . '" onchange="javascript: instrucao(' . $result['titulo'] . ');">
											<option value="0">Selecione a Instrução</option>
											<option value="06">Alteração de Vencimento</option>
											<option value="10">Cancel.Sustação Instr. Pr</option>
											<option value="04">Concessão de Abatimento</option>
											<option value="31">Concessão de Desconto</option>
											<option value="35">Multa</option>
											<option value="09">Protestar</option>
											<option value="02">Solicitação de Baixa</option>
											<option value="05">Cancelamento Abatimento</option>
											<option value="32">Cancelamento Desconto</option>
										</select>
										';
			} else {
				echo '
										<select name="instrucoes" disabled="disabled">
											<option value="0">Selecione a Instrução</option>
										</select>
										';
			}
			echo '</td>';
		} else {
			echo '
									<img src="img/ok.png" />
								</td>
								<td class="borda centro">
									<select name="instrucoes" disabled="disabled">
										<option value="0">Selecione a Instrução</option>
									</select>
								</td>';
		}
		echo '
						</tr>
						';
	}
	echo '
            </table>
		';
}

// BAIXA DE TÍTULO

else if ($funcao == 'excluir') {
	$titulo = $_GET['titulo'];
	$excluir = "UPDATE titulos SET cancelamento=now(), status='99' WHERE titulo='$titulo'";
	$sql = mysql_query($excluir);
	if ($sql) {
		echo 'ok';
	} else {
		echo 'Erro!<br>';
		mysql_error();
	}
}

// CONFIRMAÇÃO DE TITULO

else if ($funcao == 'confirma') {
	$titulo = $_GET['titulo'];
	$confirmar = "UPDATE titulos SET cad_completo='S', status = '01',dataHoraConfirmacao=NOW() WHERE titulo='$titulo'";
	$sql = mysql_query($confirmar);
	if ($sql) {
		echo 'ok';
	} else {
		echo 'Erro!<br>';
		mysql_error();
	}
}

// EXCLUIR VÁRIOS TITULOS

else if ($funcao == 'gexcluir') {
	$ids = $_GET['id'];
	$id = explode(',', $ids);
	$num = count($id) - 1;

	$boletos = '<table border="0">
              <tr class="cinza">
                <td class="borda destaque centro">Nº DOCUMENTO</td>
                <td class="borda destaque centro">NOSSO NÚMERO</td>
                <td class="borda destaque centro">EMISSÃO</td>
                <td class="borda destaque centro">VENCIMENTO</td>
                <td class="borda destaque centro">MULTA</td>
                <td class="borda destaque centro">JUROS</td>
                <td class="borda destaque centro">DESCONTO</td>
                <td class="borda destaque centro">VALOR</td>
              </tr>';

	for ($i = 0; $i < $num; $i++) {
		$titulo = $id[$i];

		$cConsulta = "SELECT documento, sequencia, nossonumero, DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, ";
		$cConsulta .= "DATE_FORMAT(data_venc,'%d/%m/%Y') AS data_venc, ";
		$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor_f, valor, ";
		$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(descontos, 2), '.', '|'), ',', '.'), '|', ',')AS descontos , data_limite_desconto, ";
		$cConsulta .= "REPLACE(multa,'.',',') AS multa, REPLACE(juros,'.',',') as juros ";
		$cConsulta .= "FROM titulos ";
		$cConsulta .= "WHERE titulo='" . $titulo . "' ";
		$cConsulta .= "AND cancelamento is null ";
		$cConsulta .= "LIMIT 1";
		$sql = mysql_query($cConsulta) or die(mysql_error());
		$bolSel = mysql_fetch_assoc($sql);

		$boletos .= '<tr>';
		$boletos .= '<td class="borda centro">' . $bolSel['documento'] . '/' . $bolSel['sequencia'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['nossonumero'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['data_emisao'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['data_venc'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['multa'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['juros'] . '</td>';
		$boletos .= '<td class="borda centro">R$ ' . $bolSel['descontos'] . '</td>';
		$boletos .= '<td class="borda centro">R$ ' . number_format($bolSel['valor'], 2, ",", ".") . '</td>';
		$boletos .= '</tr>';
	}
	$boletos .= '</table>';
	echo $boletos;
}
//marcara para desoncto

else if ($funcao == 'gdescontar') {
	$ids = $_GET['id'];
	$id = explode(',', $ids);
	$num = count($id) - 1;

	$boletos = '<table border="0">
              <tr class="cinza">
                <td class="borda destaque centro">Nº DOCUMENTO</td>
                <td class="borda destaque centro">NOSSO NÚMERO</td>
                <td class="borda destaque centro">EMISSÃO</td>
                <td class="borda destaque centro">VENCIMENTO</td>
                <td class="borda destaque centro">MULTA</td>
                <td class="borda destaque centro">JUROS</td>
                <td class="borda destaque centro">DESCONTO</td>
                <td class="borda destaque centro">VALOR</td>
              </tr>';

	for ($i = 0; $i < $num; $i++) {
		$titulo = $id[$i];

		$cConsulta = "SELECT documento, sequencia, nossonumero, DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, ";
		$cConsulta .= "DATE_FORMAT(data_venc,'%d/%m/%Y') AS data_venc, ";
		$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor_f, valor, ";
		$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(descontos, 2), '.', '|'), ',', '.'), '|', ',')AS descontos , data_limite_desconto, ";
		$cConsulta .= "REPLACE(multa,'.',',') AS multa, REPLACE(juros,'.',',') as juros ";
		$cConsulta .= "FROM titulos ";
		$cConsulta .= "WHERE titulo='" . $titulo . "' ";
		$cConsulta .= "AND cancelamento is null ";
		$cConsulta .= "LIMIT 1";
		$sql = mysql_query($cConsulta) or die(mysql_error());
		$bolSel = mysql_fetch_assoc($sql);

		$boletos .= '<tr>';
		$boletos .= '<td class="borda centro">' . $bolSel['documento'] . '/' . $bolSel['sequencia'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['nossonumero'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['data_emisao'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['data_venc'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['multa'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['juros'] . '</td>';
		$boletos .= '<td class="borda centro">R$ ' . $bolSel['descontos'] . '</td>';
		$boletos .= '<td class="borda centro">R$ ' . number_format($bolSel['valor'], 2, ",", ".") . '</td>';
		$boletos .= '</tr>';
	}
	$boletos .= '</table>';
	echo $boletos;
} else if ($funcao == 'cgdescontar') {
	$ids = $_GET['id'];
	$id = explode(',', $ids);
	$num = count($id) - 1;

	for ($i = 0; $i < $num; $i++) {
		$titulo = $id[$i];

		$confirma = "UPDATE titulos SET so_desconto='S' WHERE titulo='$titulo'";
		$sql = mysql_query($confirma);
	}
	if ($sql) {
		echo 'ok';
	} else {
		echo 'Erro!<br>';
		mysql_error();
	}
}

// EXCLUIR VÁRIOS TITULOS

else if ($funcao == 'cgexcluir') {
	$ids = $_GET['id'];
	$id = explode(',', $ids);
	$num = count($id) - 1;

	for ($i = 0; $i < $num; $i++) {
		$titulo = $id[$i];

		$excluir = "UPDATE titulos SET cancelamento=now(), status='99' WHERE titulo='$titulo'";
		$sql = mysql_query($excluir);
	}
	if ($sql) {
		echo 'ok';
	} else {
		echo 'Erro!<br>';
		mysql_error();
	}
}

// CONFIRMAR VÁRIOS TITULOS

else if ($funcao == 'gconfirma') {
	$ids = $_GET['id'];
	$id = explode(',', $ids);
	$num = count($id) - 1;

	$boletos = '<table border="0">
              <tr class="cinza">
                <td class="borda destaque centro">Nº DOCUMENTO</td>
                <td class="borda destaque centro">NOSSO NÚMERO</td>
                <td class="borda destaque centro">EMISSÃO</td>
                <td class="borda destaque centro">VENCIMENTO</td>
                <td class="borda destaque centro">MULTA</td>
                <td class="borda destaque centro">JUROS</td>
                <td class="borda destaque centro">DESCONTO</td>
                <td class="borda destaque centro">VALOR</td>
              </tr>';

	for ($i = 0; $i < $num; $i++) {
		$titulo = $id[$i];

		$cConsulta = "SELECT documento, sequencia, nossonumero, DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, ";
		$cConsulta .= "DATE_FORMAT(data_venc,'%d/%m/%Y') AS data_venc, ";
		$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor_f, valor, ";
		$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(descontos, 2), '.', '|'), ',', '.'), '|', ',')AS descontos , data_limite_desconto, ";
		$cConsulta .= "REPLACE(multa,'.',',') AS multa, REPLACE(juros,'.',',') as juros ";
		$cConsulta .= "FROM titulos ";
		$cConsulta .= "WHERE titulo='" . $titulo . "' ";
		$cConsulta .= "AND cancelamento is null ";
		$cConsulta .= "LIMIT 1";
		$sql = mysql_query($cConsulta) or die(mysql_error());
		$bolSel = mysql_fetch_assoc($sql);

		$boletos .= '<tr>';
		$boletos .= '<td class="borda centro">' . $bolSel['documento'] . '/' . $bolSel['sequencia'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['nossonumero'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['data_emisao'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['data_venc'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['multa'] . '</td>';
		$boletos .= '<td class="borda centro">' . $bolSel['juros'] . '</td>';
		$boletos .= '<td class="borda centro">R$ ' . $bolSel['descontos'] . '</td>';
		$boletos .= '<td class="borda centro">R$ ' . number_format($bolSel['valor'], 2, ",", ".") . '</td>';
		$boletos .= '</tr>';
	}
	$boletos .= '</table>';
	echo $boletos;
}

// CONFIRMAR VÁRIOS TITULOS

else if ($funcao == 'cgconfirma') {
	$ids = $_GET['id'];
	$id = explode(',', $ids);
	$num = count($id) - 1;

	for ($i = 0; $i < $num; $i++) {
		$titulo = $id[$i];

		$confirma = "UPDATE titulos SET cad_completo='S', status = '01' WHERE titulo='$titulo'";
		$sql = mysql_query($confirma);
	}
	if ($sql) {
		echo 'ok';
	} else {
		echo 'Erro!<br>';
		mysql_error();
	}
}

// I N S T R U Ç Õ E S //

// ALTERAÇÃO DE VENCIMENTO

else if ($funcao == 'altvenc') {
	$titulo = $_GET['id'];
	$data = "SELECT data_venc FROM titulos WHERE titulo='$titulo'";
	$sql = mysql_query($data);
	if ($sql) {
		$ret = mysql_fetch_assoc($sql);
		$dat = explode('-', $ret['data_venc']);
		$dat = $dat[2] . '/' . $dat[1] . '/' . $dat[0];

		echo '
			<p><b>Confirmar alteração de vencimento:</b></p>
			<br>
			<form id="instruc" name="instruc" method="post" action="">
				<table>
					<tr>
						<td><b>Data de vencimento atual:</b></td>
						<td>' . $dat . '</td>
					</tr>
					<tr>
						<td><b>Nova data de vencimento:</b></td>
						<td><input type="text" name="venc" id="venc" size="10" maxlength="10" onkeypress="formataCampo(this, \'00/00/0000\', event)" /></td>
					</tr>
				</table>
			</form>
			';
	} else {
		echo 'Erro!<br>';
		mysql_error();
	}
} else if ($funcao == 'altervenc') {
	$titulo = $_GET['id'];
	$acao = $_GET['acao'];
	$venc = $_GET['data'];
	$data = explode('/', $venc);
	$ano = date('Y');
	$mes = date('m');
	$dia = date('d');
	$venc = $data[2] . '-' . $data[1] . '-' . $data[0];
	if ($data[0] > 31) {
		echo "Data de vencimento inválida";
	} else if ($data[1] > 12) {
		echo "Data de vencimento inválida";
	}
	if ($data[2] < $ano) {
		echo "Data de vencimento inválida";
	} else if ($data[2] > $ano) {
		$sacado = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
		$s_query = mysql_query($sacado) or die(mysql_error());
		$sac = mysql_fetch_assoc($s_query);
		$sacado = $sac['sacado'];
		$verifica_inst = mysql_query("SELECT * FROM cad_instrucoes WHERE conta LIKE '%$conta' AND comando='$acao' AND data_instr=curdate() AND boleto='$titulo'");
		if (mysql_num_rows($verifica_inst) == 0) {
			$inserir = "INSERT INTO cad_instrucoes (agencia, cliente, conta, comando, data_instr, boleto, nova_data, sacado, data_hora_instr)";
			$inserir .= "VALUES ('$agencia', '$cliente', '$conta', '$acao', curdate(),'$titulo', '$venc','$sacado',now())";
			if ($agencia == "" || $cliente == "" || $conta == "" || $acao == "" || $titulo == "" || $venc == "") {
				echo "erro1";
			} else {
				$sql_data = mysql_query($inserir);
				if ($sql_data) {
					echo "ok";
				} else {
					#die($inserir);
					echo "erro2";
				}
			}
		} else {
			echo "erro3";
		}
	} else {
		if ($data[1] < $mes) {
			echo "Data de vencimento inválida";
		} else if ($data[1] > $mes) {
			$sacado = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
			$s_query = mysql_query($sacado) or die(mysql_error());
			$sac = mysql_fetch_assoc($s_query);
			$sacado = $sac['sacado'];
			$verifica_inst = mysql_query("SELECT * FROM cad_instrucoes WHERE conta='$conta' AND comando='$acao' AND data_instr=curdate() AND boleto='$titulo'");
			if (mysql_num_rows($verifica_inst) == 0) {
				$inserir = "INSERT INTO cad_instrucoes (agencia, cliente, conta, comando, data_instr, boleto, nova_data, sacado, data_hora_instr)";
				$inserir .= "VALUES ('$agencia', '$cliente', '$conta', '$acao', curdate(),'$titulo', '$venc','$sacado',now())";
				if ($agencia == "" || $cliente == "" || $conta == "" || $acao == "" || $titulo == "" || $venc == "") {
					echo "erro1";
				} else {
					$sql_data = mysql_query($inserir);
					if ($sql_data) {
						echo "ok";
					} else {
						echo "erro2";
					}
				}
			} else {
				echo "erro3";
			}
		} else {
			if ($data[0] < $dia) {
				echo "Data de vencimento inválida";
			} else {
				$sacado = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
				$s_query = mysql_query($sacado) or die(mysql_error());
				$sac = mysql_fetch_assoc($s_query);
				$sacado = $sac['sacado'];
				$verifica_inst = mysql_query("SELECT * FROM cad_instrucoes WHERE conta LIKE '%$conta' AND comando='$acao' AND data_instr=curdate() AND boleto='$titulo'");
				if (mysql_num_rows($verifica_inst) == 0) {
					$inserir = "INSERT INTO cad_instrucoes (agencia, cliente, conta, comando, data_instr, boleto, nova_data, sacado, data_hora_instr)";
					$inserir .= "VALUES ('$agencia', '$cliente', '$conta', '$acao', curdate(),'$titulo', '$venc','$sacado',now())";
					if ($agencia == "" || $cliente == "" || $conta == "" || $acao == "" || $titulo == "" || $venc == "") {
						echo "erro1";
					} else {
						$sql_data = mysql_query($inserir);
						if ($sql_data) {
							echo "ok";
						} else {
							echo "erro2";
						}
					}
				} else {
					echo "erro3";
				}
			}
		}
	}
}

// ALTERAÇÃO DE COMANDOS

else if ($funcao == 'alter') {
	$titulo = $_GET['id'];
	$acao = $_GET['acao'];

	$verifica_inst = mysql_query("SELECT * FROM cad_instrucoes WHERE conta LIKE '%$conta' and comando='$acao' and data_instr=curdate() and boleto='$titulo'");
	if (mysql_num_rows($verifica_inst) == 0) {
		$sacado = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
		$s_query = mysql_query($sacado) or die(mysql_error());
		$sac = mysql_fetch_assoc($s_query);
		$sacado = $sac['sacado'];
		$inserir = "Insert into cad_instrucoes (agencia, cliente, conta, comando, data_instr, boleto, sacado, data_hora_instr)";
		$inserir .= "values ('$agencia', '$cliente', '$conta', '$acao', curdate(),'$titulo', '$sacado', now())";
		if ($agencia == "" || $cliente == "" || $conta == "" || $acao == "" || $titulo == "") {
			echo "erro1";
		} else {
			$sql_comando = mysql_query($inserir);
			if ($sql_comando) {
				echo "ok";
			} else {
				echo "erro2";
			}
		}
	} else {
		echo "erro3";
	}
}

//ALTERAÇÃO DE VALOR

else if ($funcao == 'altervalor') {
	$titulo = $_GET['id'];
	$acao = $_GET['acao'];
	$valor = $_GET['valor'];
	$valor = str_replace(".", "", $valor);
	$valor = str_replace(",", ".", $valor);
	$verifica_inst = mysql_query("SELECT * FROM cad_instrucoes WHERE conta='$conta' and comando='$acao' and data_instr=curdate() and boleto='$titulo'");
	if (mysql_num_rows($verifica_inst) == 0) {
		$sacado = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
		$s_query = mysql_query($sacado) or die(mysql_error());
		$sac = mysql_fetch_assoc($s_query);
		$sacado = $sac['sacado'];
		$inserir = "Insert into cad_instrucoes (agencia, cliente, conta, comando, data_instr, boleto, novo_valor, sacado, data_hora_instr)";
		$inserir .= "values ('$agencia', '$cliente', '$conta', '$acao', curdate(),'$titulo', '$valor', '$sacado', now())";
		if ($agencia == "" || $cliente == "" || $conta == "" || $acao == "" || $titulo == "" || $valor == "") {
			echo "erro1";
		} else {
			$sql_valor = mysql_query($inserir);
			if ($sql_valor) {
				echo "ok";
			} else {
				echo "erro2";
			}
		}
	} else {
		echo "erro3";
	}
}

// ALTERAÇÃO DE INFORMAÇÃO

else if ($funcao == 'altinfo') {
	$titulo = $_GET['id'];
	$cli = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
	$scli = mysql_query($cli) or die(mysql_error());
	$cliente = mysql_fetch_assoc($scli);
	$end = "SELECT endereco, bairro, cidade, uf, cep FROM sacados where sacado='$cliente[0]'";
	$send = mysql_query($end) or die(mysql_error());
	$endereco = mysql_fetch_assoc($send);

	echo '
		<form id="alt_end" name="alt_end" method="post" action="">
  			<table>
  				<tr>
					<td><label for="endereco">End.:</label></td>
					<td colspan="3"><input type="text" class="largo" name="endereco" id="endereco" value="' . $endereco['endereco'] . '" /></td>
				</tr>
				<tr>
					<td><label for="bairro">Bairro:</label></td>
					<td><input type="text" name="bairro" id="bairro" size="18" maxlength="20" value="' . $endereco['bairro'] . '" /></td>
					<td><label for="cep">CEP:</label></td>
					<td><input type="text" name="cep" id="cep" maxlength="9" size="10" value="' . $endereco['cep'] . '" /></td>
				</tr>
				<tr>
					<td><label for="cidade">Cidade:</label></td>
					<td><input type="text" name="cidade" id="cidade" size="18" maxlength="20" value="' . $endereco['cidade'] . '" /></td>
					<td><label for="uf">UF:</label></td>
					<td><input type="text" name="uf" id="uf" size="2" maxlength="2" value="' . $endereco['uf'] . '" /></td>
    			</tr>
  			</table>
		</form>
		';
} else if ($funcao == 'alterinfo') {
	$titulo = $_GET['id'];
	$acao = $_GET['acao'];
	$endereco = $_GET['end'];
	$bairro = $_GET['bairro'];
	$cidade = $_GET['cidade'];
	$uf = $_GET['uf'];
	$cep = $_GET['cep'];
	$endereco = $endereco . ', ' . $bairro;

	$verifica_inst = mysql_query("SELECT * FROM cad_instrucoes WHERE conta='$conta' and comando='$acao' and data_instr=curdate() and boleto='$titulo'");
	if (mysql_num_rows($verifica_inst) == 0) {
		$sacado = "SELECT sacado FROM titulos WHERE titulo='$titulo'";
		$s_query = mysql_query($sacado) or die(mysql_error());
		$sac = mysql_fetch_assoc($s_query);
		$sacado = $sac['sacado'];
		$inserir = "Insert into cad_instrucoes (agencia, cliente, conta, comando, data_instr, boleto, sac_endereco, sac_cep, sac_cidade, sac_uf, sacado, data_hora_instr)";
		$inserir .= "values ('$agencia', '$cliente', '$conta', '$acao', curdate(),'$titulo', '$endereco', '$cep', '$cidade', '$uf', '$sacado', now())";
		if ($agencia == "" || $cliente == "" || $conta == "" || $acao == "" || $titulo == "" || $endereco == "" || $cep == "" || $cidade == "" || $uf == "") {
			echo "erro1";
		} else {
			$sql_valor = mysql_query($inserir);
			if ($sql_valor) {
				echo "ok";
			} else {
				echo "erro2";
			}
		}
	} else {
		echo "erro3";
	}
}

// CARREGAR TITULOS

else if ($funcao == "carrega") {
	$n = $_GET['i'];
	$n = $n * 25;
	$i = 0 + $n;
	$f = 25 + $n;
	$filt = "AND cancelamento IS NULL ";
	$crit = '';
	if( isset($_GET['busca']) && isset($_GET['filtro']) && isset($_GET['grupo']) ){
		$busca = $_GET['busca'];
		$filtro = $_GET['filtro'];
		$grupo = $_GET['grupo'];
		if (empty($busca)) {
			$crit = '';
		} else {
			$crit = " AND (scd.nome LIKE '%$busca%' OR scd.cpf LIKE '%$busca%')";
		}
		
		if ($filtro == 'n_confirmados') {
			$filt = "AND cad_completo <> 'S' AND status <> '03' AND cancelamento IS NULL ";
		} else if ($filtro == 'aberto') {
			$filt = "AND data_baixa IS NULL AND valor_baixa IS NULL AND data_baixa_manual IS NULL AND cancelamento IS NULL ";
		} else if ($filtro == 'liquidados') {
			$filt = "AND data_baixa IS NOT NULL AND valor_baixa IS NOT NULL AND data_baixa_manual IS NULL AND cancelamento IS NULL ";
		} else if ($filtro == 'baixados') {
			$filt = "AND data_baixa_manual IS NOT NULL AND cancelamento IS NULL ";
		} else if ($filtro == 'cancelados') {
			$filt = "AND cancelamento IS NOT NULL ";
		} else if ($filtro == 'grupo') {
			$filt = "AND scd.gsc_id = '$grupo' ";
		} else if ($filtro == 'exclusivo_desconto'){
			$filt = ' AND tit.so_desconto="N"  AND cad_completo <> "S" AND status <> "03" AND cancelamento IS NULL ';
		}
	}

	mysql_query("SET NAMES UTF8") or die(mysql_error());
	$cont = 0;
	$busca = "SELECT scd.nome, scd.cpf, tit.documento, tit.sequencia, tit.nossonumero, tit.agencia, tit.cad_completo, tit.titulo, ";
	$busca .= "DATE_FORMAT(tit.data_emisao, '%d/%m/%Y') AS data_emisao, DATE_FORMAT(tit.data_venc, '%d/%m/%Y') AS data_venc, ";
	$busca .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor, tit.criacao, tit.so_desconto, ";
	$busca .= "tit.devolucao, tit.data_baixa, tit.registro, tit.status, tit.sacado, tit.modelo ";
	$busca .= "FROM titulos AS tit ";
	$busca .= "INNER JOIN sacados AS scd ON scd.sacado=tit.sacado ";
	$busca .= "WHERE tit.cliente = '" . $cliente . "' " . $crit . " ";
	$busca .= "AND (scd.grupo <> '99999' OR scd.grupo IS NULL) ";
	$busca .= $filt;
	$busca .= "ORDER BY tit.criacao DESC, scd.nome, tit.documento, tit.sequencia ASC LIMIT $i,$f";
	$query = mysql_query($busca) or die(mysql_error());
	$cont = mysql_num_rows($query);
	if ($cont < 1) {
		echo "";
	} else {
		while ($result = mysql_fetch_assoc($query)) {
			if ($result["cad_completo"] != "S") {
				$check = '<input type="checkbox" class="chk_boxes1" name="box[]" value="' . $result['titulo'] . '">';
			} else {
				$check = '';
			}
			echo '
						<tr>
							<td class="borda centro">' . $check . '</td>
							<td class="borda centro">' . $result['titulo'] . '</td>
							<td class="borda">&nbsp;&nbsp;<span style="font-size:10px;">' . $result['nome'] . '</span></td>
							<td class="borda centro">' . $result['cpf'] . '</td>
							<td class="borda centro">' . $result['documento'] . "/" . $result['sequencia'] . '</td>
							<td class="borda centro">' . $result['nossonumero'] . '</td>
							<td class="borda centro">' . $result['data_emisao'] . '</td>
							<td class="borda centro">' . $result['data_venc'] . '</td>
							<td class="borda" style="text-align: right">R$ ' . $result['valor'] . '&nbsp;&nbsp;</td>
							<td class="borda centro">' . $result['so_desconto'] . '</td>
							<td class="borda centro">';
			if ($result["cad_completo"] != "S" && $result["status"] != "03") {
				echo '
									<a href="javascript:excluir(' . $result['titulo'] . ');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;
									<a href="javascript:navega(\'titulo_alterar.php?titulo=' . $result['titulo'] . '\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma(' . $result['titulo'] . ');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<select name="instrucoes" disabled="disabled">
										<option value="0">Selecione a Instrução</option>
									</select>
								</td>';
			} else if ($result["cad_completo"] != "S" && $result["status"] == "03" && $result["so_desconto"] == "S") {
				echo '
									<a href="javascript:navega(\'titulo_alterar.php?titulo=' . $result['titulo'] . '\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma(' . $result['titulo'] . ');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<b><font color="#F00">Título Devolvido</font></b>
								</td>';
			} else if ($result["cad_completo"] != "S" && $result["status"] == "03" && $result["so_desconto"] == "N") {
				echo '
									<a href="javascript:excluir(' . $result['titulo'] . ');" title="Excluir"><img src="img/cancelar.png" /></a>
									&nbsp;
									<a href="javascript:navega(\'titulo_alterar.php?titulo=' . $result['titulo'] . '\');" title="Alterar"><img src="img/editar.png" /></a>
									&nbsp;
									<a href="javascript:confirma(' . $result['titulo'] . ');" title="Confirmar"><img src="img/confirmar.png" /></a>
								</td>
								<td class="borda centro">
									<b><font color="#F00">Título Devolvido</font></b>
								</td>';
			} else if ($result["cad_completo"] == "S" && $result["devolucao"] == NULL && $result["data_baixa"] == NULL && $result["registro"] != NULL && $result['so_desconto'] == 'N') {
				$titulo = $result['titulo'];
				$seq = $result['sequencia'];
				$doc = $result['documento'];
				$sac = $result['sacado'];
				$modelo = $result['modelo'];
				$link = '../boletophp/';
				//define o arquivo de acordo com o modelo
				if ($modelo == '1') {
// boleto normal
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boleto(' . $titulo . ',' . $seq . ',' . $qt . ')"';
				} else if ($modelo == '2') {
// boleto 3 vias
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boleto3vias(' . $titulo . ',' . $seq . ',' . $qt . ',' . $cliente . ')"';
				} else if ($modelo == '3') {
//carne
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boletocarne(' . $titulo . ',' . $seq . ',' . $qt . ',' . $sac . ')"';
				}
				if ($result["status"] != '23') {
					echo '
									<img src="img/ok.png" />
									&nbsp;
									<img src="img/boleto.png" title="Imprimir Boleto" style="cursor: pointer;" ' . $base . ' />
								    </td>
								    <td class="borda centro">
									<select name="instrucoes" id="' . $result['titulo'] . '" onchange="javascript: instrucao(' . $result['titulo'] . ');">
										<option value="0">Selecione a Instrução</option>
										<option value="06">Alteração de Vencimento</option>
										<option value="10">Cancel.Sustação Instr. Pr</option>
										<option value="04">Concessão de Abatimento</option>
										<option value="31">Concessão de Desconto</option>
										<option value="35">Multa</option>
										<option value="09">Protestar</option>
										<option value="02">Solicitação de Baixa</option>
										<option value="05">Cancelamento Abatimento</option>
										<option value="32">Cancelamento Desconto</option>
									</select>
								</td>';
				} else {
					echo '
									<img src="img/ok.png" />
									&nbsp;

								    </td>
								    <td class="borda centro">
									<select name="instrucoes" id="' . $result['titulo'] . '" onchange="javascript: instrucao(' . $result['titulo'] . ');">
										<option value="0">Selecione a Instrução</option>
										<option value="06">Alteração de Vencimento</option>
										<option value="10">Cancel.Sustação Instr. Pr</option>
										<option value="04">Concessão de Abatimento</option>
										<option value="31">Concessão de Desconto</option>
										<option value="35">Multa</option>
										<option value="09">Protestar</option>
										<option value="02">Solicitação de Baixa</option>
										<option value="05">Cancelamento Abatimento</option>
										<option value="32">Cancelamento Desconto</option>
									</select>
								    </td>';
				}
				//<option value="12">Alteração Endereço(Cliente)</option>;
			} else if ($result["cad_completo"] == "S" && $result["devolucao"] == NULL && $result["data_baixa"] == NULL && $result["registro"] != NULL && $result['so_desconto'] == 'S') {
				$titulo = $result['titulo'];
				$seq = $result['sequencia'];
				$doc = $result['documento'];
				$sac = $result['sacado'];
				$modelo = $result['modelo'];
				$link = '../boletophp/';
				//define o arquivo de acordo com o modelo
				if ($modelo == '1') {
// boleto normal
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boleto(' . $titulo . ',' . $seq . ',' . $qt . ')"';
				} else if ($modelo == '2') {
// boleto 3 vias
					$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
					$qt = mysql_num_rows($qQtd);
					$base = 'onclick="boleto3vias(' . $titulo . ',' . $seq . ',' . $qt . ',' . $cliente . ')"';
				} else if ($modelo == '3') {
//carne
					if ($seq == 1) {
						$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
						$qt = mysql_num_rows($qQtd);
						$base = 'onclick="boletocarnetudo(' . $titulo . ',' . $seq . ',' . $qt . ',' . $sac . ',' . $doc . ')"';
					} else {
						$qQtd = mysql_query("SELECT titulo FROM titulos WHERE documento='" . mysql_real_escape_string($doc) . "' AND sacado='$sac'") or die(mysql_error());
						$qt = mysql_num_rows($qQtd);
						$base = 'onclick="boletocarne(' . $titulo . ',' . $seq . ',' . $qt . ',' . $sac . ')"';
					}
				}
				if ($result["status"] != '23') {
					echo '
											<img src="img/ok.png" />
											&nbsp;
											<img src="img/boleto.png" title="Imprimir Boleto" style="cursor: pointer;" ' . $base . ' />
										</td>
										<td class="borda centro">
										';
				} else {
					echo '
											<img src="img/ok.png" />
											&nbsp;

										</td>
										<td class="borda centro">
										';
				}
				if ($master == true) {
					echo '
										<select name="instrucoes" id="' . $result['titulo'] . '" onchange="javascript: instrucao(' . $result['titulo'] . ');">
											<option value="0">Selecione a Instrução</option>
											<option value="06">Alteração de Vencimento</option>
											<option value="10">Cancel.Sustação Instr. Pr</option>
											<option value="04">Concessão de Abatimento</option>
											<option value="31">Concessão de Desconto</option>
											<option value="35">Multa</option>
											<option value="09">Protestar</option>
											<option value="02">Solicitação de Baixa</option>
											<option value="05">Cancelamento Abatimento</option>
											<option value="32">Cancelamento Desconto</option>
										</select>
										';
				} else {
					echo '
										<select name="instrucoes" disabled="disabled">
											<option value="0">Selecione a Instrução</option>
										</select>
										';
				}
				echo '</td>';
			} else {
				echo '
									<img src="img/ok.png" />
								</td>
								<td class="borda centro">
									<select name="instrucoes" disabled="disabled">
										<option value="0">Selecione a Instrução</option>
									</select>
								</td>';
			}
			echo '
						</tr>
						';
		}
	}
}
?>
