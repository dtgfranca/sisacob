<?php
	include('config.php');
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login.php?res=1');
	}
	$agencia = $_SESSION['userAgencia'];
	$cliente = $_SESSION['userCliente'];
	$conta = $_SESSION['userConta'];
	
	$qPara = "SELECT boleto_padrao, nome_fantasia FROM clientes WHERE cliente='$cliente'";
	$sPara = mysql_query($qPara) or die(mysql_error());
	$aPara = mysql_fetch_array($sPara);
?>
<script type="text/javascript">
	function altera(){
		var boleto = document.getElementById('boleto').value;		
		//var nome = document.getElementById('nome').value;
		$.ajax({
			type: "GET",
			url: "scripts/utilit_funcoes.php",
			data: "funcao=para&boleto="+boleto,//+"&nome="+nome,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Parametrização alterada com sucesso!");
					navega("principal.php");
				}
				else{
					alerta(retorno);
				}		
			}
		});
	}
</script>
<div id="section" class="quarto">
	<div class="titulo">
        <h2>PARAMETRIZAÇÃO</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
      	<fieldset>
           	<table>
            	<tr class="cinza">
                	<td class="destaque borda centro">Modelo do Boleto</td>
                </tr>
                <tr>
                	<td class="borda centro">
                    	<select name="boleto" id="boleto">
                        <?php						
						if($aPara['boleto_padrao'] == 1){
							echo "
							<option value='1' selected>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3'>Carnê</option>0
							";
						}
						else if($aPara['boleto_padrao']==2){
							echo "
							<option value='1'>2 Vias</option>
							<option value='2' selected>3 Vias</option>
							<option value='3'>Carnê</option>
							";
						}
						else if($aPara['boleto_padrao']==3){
							echo "
							<option value='1'>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3' selected>Carnê</option>
							";
						}
						else {
							echo "
							<option value='1' selected>2 Vias</option>
							<option value='2'>3 Vias</option>
							<option value='3'>Carnê</option>0
							";
						}
						?>
                        </select>
                    </td>
                </tr>
                <!--<tr class="cinza">
                	<td class="destaque borda centro">Nome Fantasia</td>
                </tr>
                <tr>
                	<td class="borda centro"><input name="nome" id="nome" type="text" class="largo" maxlength="30" value="<?php echo $aPara['nome_fantasia']; ?>" /></td>
                </tr>-->
            </table>
		</fieldset>
  		<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="altera()" />
        <br class="clear" />
    </div>
</div>