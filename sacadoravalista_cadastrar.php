<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="meio">
<script type="text/javascript">
	$(document).ready( function() {		
		$("#novo").click(function(){
			$("#cadastra_sacador")[0].reset();
			$("#cadastra_sacador input").prop('disabled', false);
			$("#cadastra_sacador #novo").attr('class','oculto');
		});
		$("#cadastra_sacador input[name=selec]").change(function(){
			var value = $(this).val();
			if(value == 'vcpf'){
				$("#cadastra_sacador #cnpj").attr('style','display:none');
				$("#cadastra_sacador #cpf").removeAttr('style');
			}
			else if(value == 'vcnpj'){
				$("#cadastra_sacador #cpf").attr('style','display:none');
				$("#cadastra_sacador #cnpj").removeAttr('style');
			}
		});
	});
	function cad_sacador(){
		var nome = document.getElementById('nome').value;
		var cpf = document.getElementById('cpf').value;
		var cnpj = document.getElementById('cnpj').value;
		var tnome = nome.length;
		if(cnpj == ''){
			var tcpf = $("#cadastra_sacador #cpf").val().length;
			var cpfcnpj = cpf;
		}
		else if(cpf == ''){
			var tcnpj = $("#cadastra_sacador #cnpj").val().length;
			var cpfcnpj = cnpj;
		}
		
		if(tnome < 10){
			alerta('Campo nome é obrigatório');
		}
		else if(tcpf == 14 || tcnpj == 18) {		
			$.ajax({
				type: "GET",
				url: "scripts/sacador_funcoes.php",
				data: "funcao=cad_sacador&nome="+nome+"&cpfcnpj="+cpfcnpj,
				success: function(retorno){
					if(retorno == "ok"){
						alerta("Cadastro realizado com sucesso!");
						$("#cadastra_sacador input").prop('disabled', true);
						$("#cadastra_sacador #novo").attr('class','btn botao margins dir');
						$("#cadastra_sacador #novo").prop('disabled', false);
					}
					else{
						alerta(retorno);
					}		
				}
			});
		}
		else{
			alerta('CPF/CNPJ inválido');
		}
	};
</script>
	<div class="titulo">
        <h2>Sacador/Avalista</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="cadastra_sacador" name="cadastra_sacador" method="post" action="">
  		<fieldset>
    	  <legend>Cadastrar Sacador/Avalista</legend>
          <table>
    	    <tr>
    	      	<td>
                	<label for="nome">Nome:&nbsp;&nbsp;</label><input name="nome" type="text" id="nome" size="45" maxlength="40" />&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;<label id="ncpf">
    	          	<input type="radio" name="selec" value="vcpf" id="selec_0" />
                      CPF&nbsp;&nbsp;</label>
                    <label id="ncnpj">
                      <input type="radio" name="selec" value="vcnpj" id="selec_1" />
                    CNPJ&nbsp;&nbsp;</label>
                    <input name="cpf" style="display:none" type="text" id="cpf" size="12" maxlength="14" onkeypress="formataCampo(this, '000.000.000-00', event); return SomenteNumero(event)" />
                    <input name="cnpj" style="display:none" type="text" id="cnpj" size="16" maxlength="18" onkeypress="formataCampo(this, '00.000.000/0000-00', event); return SomenteNumero(event)" />
                </td>
  	      </tr>
  	    </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:cad_sacador()">Cadastrar</a>
  		<!--<input class="btn botao margins dir" type="button" name="cadastrar" id="cadastrar" value="Cadastrar" onclick="cad_sacador()" />-->
        <input class="btn botao margins dir" type="reset" name="limpar" id="limpar" value="Limpar" /> 
        <input class="oculto" type="button" name="novo" id="novo" value="Novo" onclick="" />
        <br class="clear" />
	  </form>
    </div>
</div>