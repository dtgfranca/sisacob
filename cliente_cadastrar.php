<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready( function() {
		$("#cadastra_cliente #cep").attr('onkeypress', "formataCampo(this, '00000-0', event); return SomenteNumero(event)");
		$("#novo").click(function(){
			$("#cadastra_cliente")[0].reset();
			$("#cadastra_cliente input").prop('disabled', false);
			$("#cadastra_cliente #novo").attr('class','oculto');
		});
		$('#cadastra_cliente #cpfcnpj').change(function(){
			if($("#cadastra_cliente #cpfcnpj").val() == 'cpf'){
				$("#cadastra_cliente #cnpj").attr('style','display:none');
				$("#cadastra_cliente #cpf").removeAttr('style');
				$("#cadastra_cliente #cpf").attr('onkeypress', "formataCampo(this, '000.000.000-00', event); return SomenteNumero(event)");
			}
			else if($("#cadastra_cliente #cpfcnpj").val() == 'cnpj'){
				$("#cadastra_cliente #cpf").attr('style','display:none');
				$("#cadastra_cliente #cnpj").removeAttr('style');
				$("#cadastra_cliente #cnpj").attr('onkeypress', "formataCampo(this, '00.000.000/0000-00', event); return SomenteNumero(event)");
			}
			else {
				$("#cadastra_cliente #cnpj").attr('style','display:none');
				$("#cadastra_cliente #cpf").attr('style','display:none');
			}
		});
		$("#cadastra_cliente #uf").change(function(){
			if($("#cadastra_cliente #uf").val()=='SP'){
				$("#cadastra_cliente #fax").removeAttr('maxlength');
				$("#cadastra_cliente #fax").attr('maxlength','14');
				$("#cadastra_cliente #fax").removeAttr('onkeypress');
				$("#cadastra_cliente #fax").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_pessoal").removeAttr('maxlength');
				$("#cadastra_cliente #tel_pessoal").attr('maxlength','14');
				$("#cadastra_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #celular").removeAttr('maxlength');
				$("#cadastra_cliente #celular").attr('maxlength','14');
				$("#cadastra_cliente #celular").removeAttr('onkeypress');
				$("#cadastra_cliente #celular").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_comercial").removeAttr('maxlength');
				$("#cadastra_cliente #tel_comercial").attr('maxlength','14');
				$("#cadastra_cliente #tel_comercial").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
			}
			else {
				$("#cadastra_cliente #fax").removeAttr('maxlength');
				$("#cadastra_cliente #fax").attr('maxlength','13');
				$("#cadastra_cliente #fax").removeAttr('onkeypress');
				$("#cadastra_cliente #fax").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_pessoal").removeAttr('maxlength');
				$("#cadastra_cliente #tel_pessoal").attr('maxlength','13');
				$("#cadastra_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #celular").removeAttr('maxlength');
				$("#cadastra_cliente #celular").attr('maxlength','13');
				$("#cadastra_cliente #celular").removeAttr('onkeypress');
				$("#cadastra_cliente #celular").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#cadastra_cliente #tel_comercial").removeAttr('maxlength');
				$("#cadastra_cliente #tel_comercial").attr('maxlength','13');
				$("#cadastra_cliente #tel_comercial").removeAttr('onkeypress');
				$("#cadastra_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
			}
		});
	});
	function Caracteres(e){
		var tecla=(window.event)?event.keyCode:e.which;   
		if((tecla>=48 && tecla<=57)||(tecla>=65 && tecla<=90)||(tecla>=97 && tecla<=122)) return true;
		else{
			if (tecla==32 || tecla==8 || tecla==0) return true;
		else  return false;
		}
	}
	function CPF(cpf){
		 cpf = cpf.replace(/[^\d]+/g,'');
		if(cpf == '') return false;
	 
		// Elimina CPFs invalidos conhecidos
		if (cpf.length != 11 || 
			cpf == "00000000000" || 
			cpf == "11111111111" || 
			cpf == "22222222222" || 
			cpf == "33333333333" || 
			cpf == "44444444444" || 
			cpf == "55555555555" || 
			cpf == "66666666666" || 
			cpf == "77777777777" || 
			cpf == "88888888888" || 
			cpf == "99999999999")
			return false;
		 
		// Valida 1o digito
		add = 0;
		for (i=0; i < 9; i ++)
			add += parseInt(cpf.charAt(i)) * (10 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(9)))
			return false;
		 
		// Valida 2o digito
		add = 0;
		for (i = 0; i < 10; i ++)
			add += parseInt(cpf.charAt(i)) * (11 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(10)))
			return false;
			 
		return true;
	}
	function CNPJ(cnpj){
		cnpj = cnpj.replace(/[^\d]+/g,'');
 
		if(cnpj == '') return false;
		 
		if (cnpj.length < 14)
			return false;
	 
		// Elimina CNPJs invalidos conhecidos
		if (cnpj == "00000000000000" || 
			cnpj == "11111111111111" || 
			cnpj == "22222222222222" || 
			cnpj == "33333333333333" || 
			cnpj == "44444444444444" || 
			cnpj == "55555555555555" || 
			cnpj == "66666666666666" || 
			cnpj == "77777777777777" || 
			cnpj == "88888888888888" || 
			cnpj == "99999999999999")
			return false;
			 
		// Valida DVs
		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0,tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
		  soma += numeros.charAt(tamanho - i) * pos--;
		  if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;
			 
		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
		  soma += numeros.charAt(tamanho - i) * pos--;
		  if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			  return false;
			   
		return true;
		
	}
	function cad_cliente(){
		var nome = document.getElementById('nome').value;
		var identidade = document.getElementById('identidade').value;
		var endereco = document.getElementById('endereco').value;
		var bairro = document.getElementById('bairro').value;
		var cidade = document.getElementById('cidade').value;
		var uf = document.getElementById('uf').value;
		var cep = document.getElementById('cep').value;
		var tel_pessoal = document.getElementById('tel_pessoal').value;
		var celular = document.getElementById('celular').value;
		var tel_comercial = document.getElementById('tel_comercial').value;
		var fax = document.getElementById('fax').value;
		var email = document.getElementById('email').value;
		var cpf = document.getElementById('cpf').value;
		var cnpj = document.getElementById('cnpj').value;
		var tnome = nome.length;
		var tid = identidade.length;
		var tend = endereco.length;
		var tbai = bairro.length;
		var tcid = cidade.length;
		var tcep = cep.length;
		var tcpf = cpf.length;
		var tcnpj = cnpj.length;
		var tipo = $("#cadastra_cliente #cpfcnpj").val();	
		var conf = '';
		if(tipo == '0'){
			alerta('Selecione CPF ou CNPJ');
		}
		else if(tipo == 'cpf'){
			var cpfcnpj = cpf;
			if(CPF(cpf) == false){
				alerta('CPF inválido');
			}
			else {
				var conf = 'ok';
			}
		}
		else if(tipo == 'cnpj'){
			var cpfcnpj = cnpj;
			if(CNPJ(cnpj) == false){
				alerta('CNPJ inválido');
			}
			else {
				var conf = 'ok';
			}
		}
		 if(tnome < 10){
			alerta('Campo nome é obrigatório');
		}
		else if(tend < 1){
			alerta('Campo endereço é obrigatório');
		}
		else if(tbai < 1){
			alerta('Campo bairro é obrigatório');
		}
		else if(tcid < 1){
			alerta('Campo cidade é obrigatório');
		}
		else if(uf == '0'){
			alerta('Selecione a UF');
		}
		else if(tcep < 1){
			alerta('Campo CEP é obrigatório');
		}
		else if(tcep != 9){
			alerta('CEP inválido! Insira no formato 99999-999.');
		}
		else if(conf == 'ok') {		
			$.ajax({
				type: "GET",
				url: "scripts/clientes_funcoes.php",
				data: "funcao=cad_cliente&nome="+nome+"&identidade="+identidade+"&cpfcnpj="+cpfcnpj+"&endereco="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&email="+email+"&tel_pessoal="+tel_pessoal+"&fax="+fax+"&tel_comercial="+tel_comercial+"&celular="+celular+"&cep="+cep,
				success: function(retorno){
					if(retorno == "ok"){
						alerta("Cadastro realizado com sucesso!");
						$("#cadastra_cliente input").prop('disabled', true);
						$("#cadastra_cliente #novo").attr('class','btn botao margins dir');
						$("#cadastra_cliente #novo").prop('disabled', false);
					}
					else{
						alerta(retorno);
					}		
				}
			});
		}
		else {
			alerta('CPF/CNPJ inválido.');
		}
	}
</script>
	<div class="titulo">
        <h2>Clientes</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="cadastra_cliente" name="cadastra_cliente" method="post" action="">
  		<fieldset>
    	  <legend>Cadastrar  Clientes</legend>
          <table>
          	<tr>
            	<td>
                	<label for="nome">Nome:&nbsp;&nbsp;</label><input name="nome" type="text" id="nome" size="50" maxlength="40" onkeypress='return Caracteres(event)' />&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<label id="ncpf">
    	          	</label>
                  <label for="cpfcnpj"></label>
                    <select name="cpfcnpj" id="cpfcnpj">
                      <option value="0">Selecione</option>
                      <option value="cpf">CPF</option>
                      <option value="cnpj">CNPJ</option>
                    </select>
                  <input style="display:none" name="cpf" type="text" id="cpf" size="12" maxlength="14" onkeypress="formataCampo(this, '000.000.000-00', event); return SomenteNumero(event)" />
                    <input style="display:none" name="cnpj" type="text" id="cnpj" size="16" maxlength="18" onkeypress="formataCampo(this, '00.000.000/0000-00', event); return SomenteNumero(event)" />
                </td>
            </tr>
          </table>
    	  <table>
    	    <tr>
    	      <td width="34%"><label for="identidade">RG:&nbsp;&nbsp;</label>    	        <input name="identidade" type="text" id="identidade" size="14" maxlength="13" /></td>
    	      <td colspan="2"><label for="endereco">Ender.:&nbsp;&nbsp;</label>    	        <input name="endereco" type="text" id="endereco" size="50" maxlength="40" onkeypress='return Caracteres(event)' /></td>
   	        </tr>
    	    <tr>
    	      <td><label for="bairro">Bairro:&nbsp;&nbsp;</label>    	        <input name="bairro" type="text" id="bairro" size="18" maxlength="20" onkeypress='return Caracteres(event)'/></td>
    	      <td width="32%"><label for="cidade">Cidade:&nbsp;&nbsp;</label>    	        <input name="cidade" type="text" id="cidade" size="18" maxlength="20" onkeypress='return Caracteres(event)'/></td>
    	      <td><label for="uf">UF:&nbsp;&nbsp;</label>
              	<select name="uf" id="uf">
                	<option value="0" selected="selected">--</option>
	         	  	<option value="AC">AC</option>
	         	  	<option value="AL">AL</option>
	         	  	<option value="AP">AP</option>
	         	  	<option value="AM">AM</option>
	         	  	<option value="BA">BA</option>
	         	  	<option value="CE">CE</option>
	         	  	<option value="DF">DF</option>
	         	  	<option value="ES">ES</option>
	         	  	<option value="GO">GO</option>
	         	  	<option value="MA">MA</option>
	         	  	<option value="MT">MT</option>
	         	  	<option value="MS">MS</option>
	         	  	<option value="MG">MG</option>
	         	  	<option value="PA">PA</option>
	         	  	<option value="PB">PB</option>
	         	  	<option value="PE">PE</option>
	         	  	<option value="PI">PI</option>
	         	  	<option value="PR">PR</option>
	         	  	<option value="RJ">RJ</option>
	         	  	<option value="RN">RN</option>
	         	  	<option value="RO">RO</option>
	         	  	<option value="RR">RR</option>
	         	  	<option value="RS">RS</option>
	         	  	<option value="SC">SC</option>
	         	  	<option value="SE">SE</option>
	         	  	<option value="SP">SP</option>
	         	  	<option value="TO">TO</option>
   	  	      	</select>              </td>
   	        </tr>
          <tr>
    	      <td><label for="cep">CEP:&nbsp;&nbsp;</label>    	        <input name="cep" type="text" id="cep" maxlength="9" size="10" onkeypress="formataCampo(this, '00000-000', event);  return SomenteNumero(event)" /></td>
    	      <td><label for="email">E-mail:&nbsp;&nbsp;</label>    	        <input type="text" name="email" id="email" size="15" maxlength="50" /></td>
    	      <td><label for="fax">Fax:&nbsp;&nbsp;</label>    	        <input name="fax" type="text" id="fax" size="12" maxlength="13" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /></td>
   	        </tr>
    	   <tr>
    	      <td><label for="tel_pessoal">Tel. pessoal:&nbsp;&nbsp;</label>    	        <input name="tel_pessoal" type="text" id="tel_pessoal" maxlength="13" size="12" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /></td>
    	      <td><label for="celular">Celular:</label>    	        <input name="celular" type="text" id="celular" maxlength="13" size="12" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /> </td>
    	      <td><label for="tel_comercial">Tel. Comercial:</label>    	        <input name="tel_comercial" type="text" id="tel_comercial" size="12" maxlength="13" onkeypress="formataCampo(this, '(00)0000-0000', event);  return SomenteNumero(event)" /></td>
   	        </tr>
  	    </table>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:cad_cliente()">Cadastrar</a>
  		<!--<input class="btn botao margins dir" type="button" name="cadastrar" id="cadastrar" value="Cadastrar" onclick="cad_cliente()" />-->
        <input class="btn botao margins dir" type="reset" name="limpar" id="limpar" value="Limpar" /> 
        <input class="oculto" type="reset" name="novo" id="novo" value="Novo" />
        <br class="clear" />
	  </form>
    </div>
</div>