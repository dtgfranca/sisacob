<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
	//Limpando o erro de fontes do relatório
	if (file_exists('scripts/font/unifont/dejavusans-bold.mtx.php')) {
		unlink('scripts/font/unifont/dejavusans-bold.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusans-bold.cw.dat')) {
		unlink('scripts/font/unifont/dejavusans-bold.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat');	
	}
?>
<script type="text/javascript">
	/*$(function() {
		$("#inicio").datepicker({
			numberOfMonths: 2,
			onClose: function(selectedDate) {
				$("#fim").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#fim").datepicker({
			numberOfMonths: 2,
			onClose: function(selectedDate) {
				$("#inicio").datepicker("option", "maxDate", selectedDate);
			}
		});
	});*/
	$(document).ready(function(){
		//$("#inicio").mask("99/99/9999",{placeholder:" "});
		//$("#fim").mask("99/99/9999",{placeholder:" "});
		$("#relatorio").validate({
			rules:{
				inicio:{
					required: true, minlength: 10
				},
				fim:{
					required: true, minlength: 10
				},
			},
			messages:{
				inicio:{
					required: "<br>Digite a data inicial", 
					minlength: "<br>Data inválida",
				},
				fim:{
					required: "<br>Digite a data final", 
					minlength: "<br>Data inválida",
				}
			}
		});
	});
</script>
<div id="section" class="quarto">
	<div class="titulo">
        <h2>RELATÓRIOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    	<form id="relatorio" name="relatorio" method="post" action="scripts/relatorios/r_lancamentos.php" target="_blank">
            <fieldset>
                <legend>LANÇAMENTOS</legend>
                Escolha o período:
                <br /><br />
                <table>
                    <tr>
                      <td width="10%"><label for="inicio">De:</label></td>
                        <td width="40%"><input name="inicio" type="text" id="inicio" size="7" maxlength="10" onkeypress="formataCampo(this, '00/00/0000', event); return SomenteNumero(this)" /></td>
                        <td width="10%"><label for="fim">Até:</label></td>
                      <td width="40%"><input name="fim" type="text" id="fim" size="7" maxlength="10" onkeypress="formataCampo(this, '00/00/0000', event); return SomenteNumero(this)" /></td>
                    </tr>
                </table>
            </fieldset>
            <input class="btn botao margins dir" type="submit" name="gerar" id="gerar" value="Gerar Relatório" />
            <br class="clear" />
        </form>
    </div>
</div>