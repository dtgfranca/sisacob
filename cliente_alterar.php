﻿<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div id="section" class="mid">
<script type="text/javascript">
	$(document).ready( function() {		
		$("#cpfcnpj").keyup(function(){
			var tam = $(this).val().length+1;
			if(tam >= 15){
				$("#cpfcnpj").removeAttr('onkeypress');
				$("#cpfcnpj").attr('onkeypress','formataCampo(this, "00.000.000/0000-00", event); return SomenteNumero(event)');
			}
			else {
				$("#cpfcnpj").removeAttr('onkeypress');
				$("#cpfcnpj").attr('onkeypress','formataCampo(this, "000.000.000-00", event); return SomenteNumero(event)');
			}
		});
		$("#altera_cliente #uf").keyup(function(){
			if($("#altera_cliente #uf").val()=='SP'){
				$("#altera_cliente #fax").removeAttr('maxlength');
				$("#altera_cliente #fax").attr('maxlength','14');
				$("#altera_cliente #fax").removeAttr('onkeypress');
				$("#altera_cliente #fax").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_pessoal").removeAttr('maxlength');
				$("#altera_cliente #tel_pessoal").attr('maxlength','14');
				$("#altera_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#altera_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #celular").removeAttr('maxlength');
				$("#altera_cliente #celular").attr('maxlength','14');
				$("#altera_cliente #celular").removeAttr('onkeypress');
				$("#altera_cliente #celular").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_comercial").removeAttr('maxlength');
				$("#altera_cliente #tel_comercial").attr('maxlength','14');
				$("#altera_cliente #tel_comercial").removeAttr('onkeypress');
				$("#altera_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)00000-0000", event);  return SomenteNumero(event)');
			}
			else {
				$("#altera_cliente #fax").removeAttr('maxlength');
				$("#altera_cliente #fax").attr('maxlength','13');
				$("#altera_cliente #fax").removeAttr('onkeypress');
				$("#altera_cliente #fax").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_pessoal").removeAttr('maxlength');
				$("#altera_cliente #tel_pessoal").attr('maxlength','13');
				$("#altera_cliente #tel_pessoal").removeAttr('onkeypress');
				$("#altera_cliente #tel_pessoal").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #celular").removeAttr('maxlength');
				$("#altera_cliente #celular").attr('maxlength','13');
				$("#altera_cliente #celular").removeAttr('onkeypress');
				$("#altera_cliente #celular").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
				$("#altera_cliente #tel_comercial").removeAttr('maxlength');
				$("#altera_cliente #tel_comercial").attr('maxlength','13');
				$("#altera_cliente #tel_comercial").removeAttr('onkeypress');
				$("#altera_cliente #tel_comercial").attr('onkeypress','formataCampo(this, "(00)0000-0000", event);  return SomenteNumero(event)');
			}
		});
	});
	function Caracteres(e){
		var tecla=(window.event)?event.keyCode:e.which;   
		if((tecla>=97 && tecla<=122)) return true;
		else{
			if (tecla==32 || tecla==8 || tecla==0) return true;
		else  return false;
		}
	}
	function CPF(cpf){
		 cpf = cpf.replace(/[^\d]+/g,'');
		if(cpf == '') return false;
	 
		// Elimina CPFs invalidos conhecidos
		if (cpf.length != 11 || 
			cpf == "00000000000" || 
			cpf == "11111111111" || 
			cpf == "22222222222" || 
			cpf == "33333333333" || 
			cpf == "44444444444" || 
			cpf == "55555555555" || 
			cpf == "66666666666" || 
			cpf == "77777777777" || 
			cpf == "88888888888" || 
			cpf == "99999999999")
			return false;
		 
		// Valida 1o digito
		add = 0;
		for (i=0; i < 9; i ++)
			add += parseInt(cpf.charAt(i)) * (10 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(9)))
			return false;
		 
		// Valida 2o digito
		add = 0;
		for (i = 0; i < 10; i ++)
			add += parseInt(cpf.charAt(i)) * (11 - i);
		rev = 11 - (add % 11);
		if (rev == 10 || rev == 11)
			rev = 0;
		if (rev != parseInt(cpf.charAt(10)))
			return false;
			 
		return true;
	}
	function CNPJ(cnpj){
		cnpj = cnpj.replace(/[^\d]+/g,'');
 
		if(cnpj == '') return false;
		 
		if (cnpj.length != 14)
			return false;
	 
		// Elimina CNPJs invalidos conhecidos
		if (cnpj == "00000000000000" || 
			cnpj == "11111111111111" || 
			cnpj == "22222222222222" || 
			cnpj == "33333333333333" || 
			cnpj == "44444444444444" || 
			cnpj == "55555555555555" || 
			cnpj == "66666666666666" || 
			cnpj == "77777777777777" || 
			cnpj == "88888888888888" || 
			cnpj == "99999999999999")
			return false;
			 
		// Valida DVs
		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0,tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
		  soma += numeros.charAt(tamanho - i) * pos--;
		  if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;
			 
		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
		  soma += numeros.charAt(tamanho - i) * pos--;
		  if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			  return false;
			   
		return true;
		
	}
function alt_cliente(){
	var sacado = document.getElementById("sacado").value;
	var nome = document.getElementById("nome").value;
	var identidade = document.getElementById("identidade").value;
	var cpfcnpj = document.getElementById("cpfcnpj").value;
	var endereco = document.getElementById("endereco").value;
	var bairro = document.getElementById("bairro").value;
	var cidade = document.getElementById("cidade").value;
	var uf = document.getElementById("uf").value;
	var cep = document.getElementById("cep").value;
	var tel_pessoal = document.getElementById("tel_pessoal").value;
	var celular = document.getElementById("celular").value;
	var tel_comercial = document.getElementById("tel_comercial").value;
	var fax = document.getElementById("fax").value;
	var email = document.getElementById("email").value;
	var tcpf = $("#altera_cliente #cpfcnpj").val().length;
	var tnome = nome.length;
	var tend = endereco.length;
	var tbai = bairro.length;
	var tcid = cidade.length;
	var tuf = uf.length;
	var tcep = cep.length;
	var conf = '';
	
	if(tcpf == 14){
		if(CPF(cpfcnpj) == false){
			alerta('CPF inválido');
		}
		else {
			var conf = 'ok';
		}
	}
	else if(tcpf == 18){
		if(CNPJ(cpfcnpj) == false){
			alerta('CNPJ inválido');
		}
		else {
			var conf = 'ok';
		}
	}
	
	if(tnome < 10){
		alerta('Campo nome é obrigatório (pelo menos 10 caracters)');
	}
	else if(tend < 2){
		alerta('Campo endereço é obrigatório');
	}
	else if(tbai < 2){
		alerta('Campo bairro é obrigatório');
	}
	else if(tcid < 2){
		alerta('Campo cidade é obrigatório');
	}
	else if(tuf < 2){
		alerta('Campo UF é obrigatório');
	}
	else if(tcep < 9){
		alerta('Campo CEP é obrigatório');
	}
	else if(conf == 'ok'){
		$.ajax({
				type: "GET",
				url: "scripts/clientes_funcoes.php",
			data: "funcao=alt_cliente&sacado="+sacado+"&nome="+nome+"&identidade="+identidade+"&cpfcnpj="+cpfcnpj+"&endereco="+endereco+"&bairro="+bairro+"&cidade="+cidade+"&uf="+uf+"&email="+email+"&tel_pessoal="+tel_pessoal+"&fax="+fax+"&tel_comercial="+tel_comercial+"&celular="+celular+"&cep="+cep,
			success: function(retorno){
				if(retorno == "ok"){
					alerta("Alteração realizada com sucesso!");
					$("#altera_cliente a#alterar").attr('class','btn bdesab margins dir');
					$("#altera_cliente a#alterar").attr('href','#');
				}
				else{
					alerta(retorno);
				}		
			}
		});
	}
	else{
		alerta('CPF/CNPJ inválido');
	}
};
</script>
<?php 
	include('config.php');
   	$sacado = $_GET['sacado'];
	$sql = "SELECT nome, identidade, cpf, endereco, bairro, cidade, uf, cep, telef_pess, telef_com, celular, fax, email FROM sacados WHERE sacado='$sacado'";
	$query = mysql_query($sql) or die(mysql_error());
	$linha = mysql_fetch_array($query);
	if($linha['uf'] == 'SP'){
		$param = 'maxlength="13" onkeypress="formataCampo(this, \'(00)00000-0000\', event);  return SomenteNumero(event)"';
	}
	else{
		$param = 'maxlength="13" onkeypress="formataCampo(this, \'(00)0000-0000\', event);  return SomenteNumero(event)"';
	}
?>
	<div class="titulo">
        <h2>Clientes</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
		<form id="altera_cliente" name="altera_cliente" method="post">
			<fieldset>
				<legend>Alterar  Clientes</legend>
				<table>
					<tr>
						<td width="12%"><label for="nome">Nome:</label></td>
						<td colspan="3"><input type="hidden" id="sacado" value="<?php echo $sacado; ?>" /><input name="nome" type="text" class="largo" id="nome" maxlength="40" value="<?php echo $linha['nome']; ?>"  onkeypress='return Caracteres(event)'/></td>
					  	<td><label for="cpfcnpj">CPF/CNPJ:</label></td>
				  		<td width="20%"><input name="cpfcnpj" type="text" id="cpfcnpj" size="16" maxlength="18" onkeypress="formataCampo(this, '000.000.000-00', event); return SomenteNumero(event)" value="<?php echo $linha['cpf']; ?>" /></td>
				  	</tr>
					<tr>
						<td><label for="identidade">RG:</label></td>
						<td><input name="identidade" type="text" id="identidade" size="14" maxlength="13" value="<?php echo $linha['identidade']; ?>" /></td>
						<td><label for="endereco">Ender.:</label></td>
						<td colspan="3"><input name="endereco" type="text" class="largo" id="endereco" maxlength="100" value="<?php echo $linha['endereco']; ?>"/></td>
					</tr>
					<tr>
						<td><label for="bairro">Bairro:</label></td>
						<td><input name="bairro" type="text" id="bairro" size="18" maxlength="20" value="<?php echo $linha['bairro']; ?>" onkeypress='return Caracteres(event)' /></td>
						<td><label for="cidade">Cidade:</label></td>
						<td><input name="cidade" type="text" id="cidade" size="18" maxlength="20" value="<?php echo $linha['cidade']; ?>" onkeypress='return Caracteres(event)' /></td>
						<td><label for="uf">UF:</label></td>
						<td><input type="text" name="uf" id="uf" maxlength="2" size="2" value="<?php echo $linha['uf']; ?>" /></td>
					</tr>
					<tr>
						<td><label for="cep">CEP:</label></td>
						<td><input name="cep" type="text" id="cep" maxlength="9" size="10" onkeypress="formataCampo(this, '00000-000', event);  return SomenteNumero(event)" value="<?php echo $linha['cep']; ?>" /></td>
						<td><label for="email">E-mail:</label></td>
						<td><input type="text" name="email" id="email" size="15" maxlength="30" value="<?php echo $linha['email']; ?>" /></td>
						<td><label for="fax">Fax:</label></td>
						<td><input name="fax" type="text" id="fax" size="12" <?php echo $param; ?> value="<?php echo $linha['fax']; ?>" /></td>
					</tr>
					<tr>
						<td><label for="tel_pessoal">Tel. pessoal:</label></td>
						<td><input name="tel_pessoal" type="text" id="tel_pessoal" size="12" <?php echo $param; ?> value="<?php echo $linha['telef_pess']; ?>" /></td>
						<td><label for="celular">Celular:</label></td>
						<td><input name="celular" type="text" id="celular" size="12" <?php echo $param; ?> value="<?php echo $linha['celular']; ?>" /></td>
						<td><label for="tel_comercial">Tel. Comercial:</label></td>
						<td><input name="tel_comercial" type="text" id="tel_comercial" size="12" <?php echo $param; ?> value="<?php echo $linha['telef_com']; ?>" /></td>
					</tr>
				</table>
			</fieldset>
            <a id="alterar" class="btn botao margins dir" href="javascript:alt_cliente()">Alterar</a>
			<!--<input class="btn botao margins dir" type="button" name="alterar" id="alterar" value="Alterar" onclick="alt_cliente()" />-->
			<a class="btn botao margins dir" href="javascript:navega('cliente_buscar.php')">Voltar</a>
        	<br class="clear" />
		</form>
	</div>
</div>