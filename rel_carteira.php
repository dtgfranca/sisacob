<?php
	session_start();
	if(empty($_SESSION['userAgencia'])){
		header('location: login_mini.php');
	}
	//Limpando o erro de fontes do relatório
	if (file_exists('scripts/font/unifont/dejavusans-bold.mtx.php')) {
		unlink('scripts/font/unifont/dejavusans-bold.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusans-bold.cw.dat')) {
		unlink('scripts/font/unifont/dejavusans-bold.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-boldoblique.cw.dat');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw127.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.mtx.php');
	}
	if (file_exists('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat')) {
		unlink('scripts/font/unifont/dejavusanscondensed-oblique.cw.dat');	
	}
?>
<script type="text/javascript">
	function r_analitico(){	
		document.forms["relatorio"].action="scripts/relatorios/r_analitico.php";
		document.forms["relatorio"].submit();
	}
	function r_sintetico(){		
		document.forms["relatorio"].action="scripts/relatorios/r_sintetico.php";
		document.forms["relatorio"].submit();
	}
</script>
<div id="section" class="small">
	<div class="titulo">
        <h2>RELATÓRIOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
    	<form id="relatorio" name="relatorio" method="post" action="" target="_blank">
            <fieldset>
                <legend>Carteira</legend>
                <a class="btn botao margins dir" style="margin-right: 0; margin-left: 0" id="analitico" href="javascript:r_analitico()">Gerar Relatório Analítico</a>
                <br />
                <a class="btn botao margins dir" style="margin-right: 0; margin-left: 0" id="sintetico" href="javascript:r_sintetico()">Gerar Relatório Sintético</a>
            </fieldset>
        </form>
    </div>
</div>