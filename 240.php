<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SisaCob</title>
	<link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
    <link href="js/upload/uploadify.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/jquery-ui-1.10.1.custom.css">
    <script language="javascript" src="js/validacao.js" type="text/javascript"></script>
    <script language="javascript" src="js/Mascaras.js" type="text/javascript"></script>
    <script language="javascript" src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script language="javascript" src="js/ui/jquery.ui.core.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.widget.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.mouse.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.draggable.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.position.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.button.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.dialog.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.tooltip.js" type="text/javascript"></script>    
    <script language="javascript" src="js/ui/jquery.ui.datepicker.js" type="text/javascript"></script>   
    <script language="javascript" src="js/jquery.validate.js" type="text/javascript"></script>
    <script language="javascript" src="js/jquery.maskedinput.js" type="text/javascript"></script>
    <script language="javascript" src="js/script.js" type="text/javascript"></script>
    <script>
	$(function() {
		$(document).tooltip();
	});
	</script>
    <!--<script language="javascript" src="js/pers.js" type="text/javascript"></script>-->
</head>

<body>
    <div id="wrap">
        <div id="header">
            <div class="quarto esq altura">
                <div class="interno">
                    <b>Usuário: </b><br />
                    <b>Último Acesso: </b><br />
                    <b>IP da Última Conexão: </b>
                </div>
            </div>
            <div class="min esq altura">
                <img class="top" src="img/proposta.png" style="cursor: pointer" onClick="window.open('http://www.skillnet.com.br')" />
            </div>
            <div class="duplo esq altura">
                <img class="top" src="img/logo/logo_41.jpg" width="130" style="position:relative; left:15px;" />
            </div>
            <div class="quarto esq altura">
                <div class="interno tdir">
                    v2.0<br>
                    <span style="cursor: pointer" onClick="window.location='scripts/logout.php'"><b>Encerrar (x)</b></span>
                </div>
            </div>
        </div>
        
<!-- MENU -->
	
    <div id="nav">
    	<ul class="menu">
        	<li><a href="javascript:navega('principal.php');">A - Principal</a></li>
            <li><a href="javascript:navega('cliente_buscar.php');">B -  Clientes</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega('cliente_buscar.php');">1 - Busca</a></li>
                    <li><a href="javascript:navega('cliente_cadastrar.php');">2 - Cadastro</a></li>
               	</ul>
           	</li>
            <li><a href="javascript:navega('titulo_buscar.php');">C - Títulos</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega('titulo_buscar.php');">1 - Busca</a></li>
                    <li><a href="javascript:navega('titulo_cadastrar.php');">2 - Cadastro</a></li>
               	</ul>
           	</li>
            <li><a href="javascript:navega('rel_historico.php');">D - Relatórios</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega('rel_historico.php');">1 - Histórico Títulos</a></li>
                    <li><a href="javascript:navega('rel_carteira.php');">2 - Carteira</a></li>
                    <li><a href="javascript:navega('rel_instrucoes.php');">3 - Instruções</a></li>
                    <li><a href="javascript:navega('rel_tarifas.php');">4 - Tarifas</a></li>
                    <li><a href="javascript:navega('rel_lancamentos.php');">5 - Lançamentos</a></li>
               	</ul>
           	</li>
            <li><a href="javascript:navega('sacadoravalista_buscar.php');">E - Sacador/Avalista</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega('sacadoravalista_buscar.php');">1 - Busca</a></li>
                    <li><a href="javascript:navega('sacadoravalista_cadastrar.php');">2 - Cadastro</a></li>
               	</ul>
           	</li>
            <li><a href="javascript:navega('cadastrar_usuario.php');">F - Utilitários</a>
            	<ul class="sub-menu">
                	<li><a href="javascript:navega('ut_senha.php');">1 - Alterar Senha</a></li>
                    <!--<li><a href="javascript:navega('cnab/inicio_240.php');">2 - Integração CNAB 240</a></li>-->
                    <li><a href="javascript:navega('cnab/inicio.php');">2 - Integração CNAB 400</a></li>
                    <li><a href="javascript:navega('ut_instrucoes.php');">3 - Instruções</a></li>
                    <li><a href="javascript:navega('ut_parametriza.php');">4 - Parametrização Boletos</a></li>
                    <!--<li><a href="javascript:navega('ut_importar_n.php');">5 - Importar Sacados</a></li>-->
                    <li><a href="javascript:navega('ut_instrucoes1via.php');">5 - Obs. 1ª via</a></li>
               	</ul>
           	</li>
        </ul>
    </div>

<div id="section" class="qua">
	<div class="titulo">
        <h2>INTEGRAÇÃO CNAB 240</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
		<form action="cnab/actions/importacao_240.php" target="_blank" method="POST" name="form_importacao" enctype="multipart/form-data" >
        	<fieldset>
                <legend>IMPORTAÇÃO CNAB 240</legend>
                <label for="remessa"><b>Selecione o arquivo de remessa:</b></label>
                <br />
                <br />
                <p>
                	<input id="remessa" name="remessa" type="file" size="42"class="submit_botao" />
                    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                </p>
            	<input type="submit" name="submit" value="Processar arquivo" class="botao margins dir" />
        	</fieldset>
       	</form>
        <!--<form action="cnab/actions/exportacao.php" target="_blank" method="POST" name="form_exportacao" enctype="multipart/form-data" >
        	<fieldset>
              <legend>EXPORTAÇÃO CNAB 400</legend>
                <table>
                	<tr>
                   	  	<td width="10%"><label for="inicio">De:</label></td>
                        <td width="40%"><input name="inicio" type="text" id="inicio" size="7" maxlength="10" value="<?php echo date("d/m/Y", strtotime("-5 days")); ?>" /></td>
                        <td width="10%"><label for="fim">Até:</label></td>
                        <td width="40%"><input name="fim" type="text" id="fim" size="7" maxlength="10" value="<?php echo date("d/m/Y"); ?>" /></td>
                    </tr>
                </table>
            	<input type="submit" name="submit" value="Processar arquivo" class="botao margins dir" />
        	</fieldset>   
       	</form> -->
	</div>
</div>

</div>
</body>
</html>