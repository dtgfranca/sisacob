<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="X-UA-Compatible" content="IE=9"> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SisaCob</title>
	<link href="css/reset.css" rel="stylesheet" type="text/css">
    <link href="css/estilo.css" rel="stylesheet" type="text/css">
    <link href="js/upload/uploadify.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui-1.10.1.custom.css">
    <script language="javascript" src="js/validacao.js" type="text/javascript"></script>
    <script language="javascript" src="js/Mascaras.js" type="text/javascript"></script>
    <script language="javascript" src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script language="javascript" src="js/ui/jquery.ui.core.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.widget.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.mouse.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.draggable.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.position.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.button.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.dialog.js" type="text/javascript"></script>
	<script language="javascript" src="js/ui/jquery.ui.tooltip.js" type="text/javascript"></script>    
    <script language="javascript" src="js/jquery.validate.js" type="text/javascript"></script>
    <script language="javascript" src="js/script.js" type="text/javascript"></script>
    <script>
	$(function() {
		$(document).tooltip();
	});
	</script>
	<script type="text/javascript">
	var url = new String(window.location.href);
	var res = url.split("res=");
	if(res[1]==0){
		window.onload = function(){
			alerta('Dados de login inválidos!');
            $("#senha").focus();
		}
	}
	else if(res[1]==1){
		window.onload = function(){
			alerta('Sessão expirada. Faça login novamente.');
		}
	}
	else if(res[1]==2){
		window.onload = function(){
			alerta('Você não tem permissão para acessar o sistema.');
		}
	}
</script>
<!--[if lt IE 10]>
    <script type="text/javascript" src="js/IE9.js"></script>
    <link href="css/ie9.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/html5.js"></script>
<![endif]-->

</head>

<body>

<!-- CABEÇALHO -->
<script type="text/javascript">
	$(document).ready(function(){
    	$('#main').ajusta();
        $('body').fadeIn('fast');
        	});
</script>
    
	<div id="header">
    	<div class="quarto esq altura">
        </div>
        <div class="min esq altura">
        </div>
        <div class="duplo esq altura">
        	<img class="top" src="img/proposta.png" style="cursor: pointer" onclick="window.open('http://www.skillnet.com.br')">
        </div>
        <div class="quarto esq altura">
          	</div>
        </div>    	      
    
    <!-- FIM DO CABEÇALHO -->
<div id="message">
        <div id="message_content">
           Erro desconhecido.<br>
            <input type="button" value="OK" class="botao mbotao" onclick="$('#message').fadeOut('fast');">
        </div>
    </div>

	<div id="main" style="display: table-cell; vertical-align: middle; width: 1360px; height: 459px;">  
    
    	<div id="section" class="small">
            <h2>Manutenção</h2>
            	<div class="corpo" style="padding:10px;font-size:20px">
<p>Sistema indisponível temporariamente!        </p>      		
			<p></p></div>          		
		</div>
	</div>    
<script language="JavaScript" src="js/pers.js" type="text/javascript"></script>

</body></html>