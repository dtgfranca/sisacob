<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include 'config.php';
session_start();
if (empty($_SESSION['userAgencia'])) {
	header('location: login.php?res=1');
}
$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userCliente'];
$conta = $_SESSION['userConta'];

$data_base = date("d/m/Y");
$titulo = $_GET['titulo'];
$boletos = "";
//recupera informa��es do t�tulo com base no codigo no codigo informado
$cConsulta = "SELECT titulo, agencia, cliente, sacado, documento, sequencia, nossonumero, seunumero,dias_protesto,  ";
$cConsulta .= "DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, DATE_FORMAT(data_venc,'%d/%m/%Y') as data_venc, ";
$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') as valor, descontos, data_limite_desconto, ";
$cConsulta .= "REPLACE(multa,'.',',') as multa, REPLACE(juros,'.',',') as juros, protesto, dias_protesto, ";
$cConsulta .= "data_baixa, valor_baixa, codigo_barra, instrucao, data_baixa_manual, cancelamento, ";
$cConsulta .= "criacao, desconto, devolucao, cdd_ndoc, modelo, sacador, registro, so_desconto, intervalo ";
$cConsulta .= "FROM titulos ";
$cConsulta .= "WHERE titulo=" . $titulo;
$cConsulta .= " LIMIT 1";

$selSql = mysql_query($cConsulta);
$aSel = mysql_fetch_assoc($selSql);

//pesquisa todos os titulos ligados ao codigo informado anteriormente
$cConsulta = "SELECT titulo, agencia, cliente, sacado, documento, sequencia, nossonumero, seunumero, ";
$cConsulta .= "DATE_FORMAT(data_emisao,'%d/%m/%Y') AS data_emisao, DATE_FORMAT(data_venc,'%d/%m/%Y') AS data_venc, ";
$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(valor, 2), '.', '|'), ',', '.'), '|', ',') AS valor_f, valor, ";
$cConsulta .= "REPLACE( REPLACE( REPLACE( FORMAT(descontos, 2), '.', '|'), ',', '.'), '|', ',')AS descontos , data_limite_desconto, ";
$cConsulta .= "REPLACE(multa,'.',',') AS multa, REPLACE(juros,'.',',') as juros, protesto, dias_protesto, ";
$cConsulta .= "data_baixa, valor_baixa, codigo_barra, instrucao, data_baixa_manual, cancelamento, ";
$cConsulta .= "criacao, desconto, devolucao, cdd_ndoc, modelo, sacador, registro, so_desconto, intervalo ";
$cConsulta .= "FROM titulos ";
$cConsulta .= "WHERE documento='" . $aSel['documento'] . "' ";
$cConsulta .= "AND sequencia='" . $aSel['sequencia'] . "' ";
$cConsulta .= "AND agencia='" . $aSel['agencia'] . "' ";
$cConsulta .= "AND cliente='" . $aSel['cliente'] . "' ";
$cConsulta .= "AND sacado='" . $aSel['sacado'] . "' ";
$cConsulta .= "AND cancelamento is null ";
$cConsulta .= "ORDER BY sequencia ";
$cConsulta .= "LIMIT 1";

$sql = mysql_query($cConsulta);

$sVal = "SELECT SUM(valor) as valor FROM titulos WHERE documento='" . $aSel['documento'] . "' AND agencia='" . $aSel['agencia'] . "' AND cliente='" . $aSel['cliente'] . "' AND cancelamento is null";
$qVal = mysql_query($sVal) or die(mysql_error());
$aVal = mysql_fetch_assoc($qVal);

$i = 0;
$valor_total_fatura = $aVal['valor'];
$valor_fatura = 0;
$vencimento1 = "";
$sequencia = "";
$documento = "";

while ($bolSel = mysql_fetch_assoc($sql)) {
	if ($vencimento1 == "") {
		$vencimento1 = $bolSel['data_venc'];
	}

	$valor_fatura = $bolSel['valor'];
	$documento = $bolSel['documento'];

	$boletos .= '<tr>';
	$boletos .= '<td class="borda centro">' . $bolSel['documento'] . '/' . $bolSel['sequencia'] . '</td>';
	$boletos .= '<td class="borda centro">' . $bolSel['nossonumero'] . '</td>';
	$boletos .= '<td class="borda centro">' . $bolSel['data_emisao'] . '</td>';
	$boletos .= '<td class="borda centro" id="data">' . $bolSel['data_venc'] . '</td>';
	$boletos .= '<td class="borda centro" id="multa">' . $bolSel['multa'] . '</td>';
	$boletos .= '<td class="borda centro">' . $bolSel['juros'] . '</td>';
	$boletos .= '<td class="borda centro" id="desconto">R$ ' . $bolSel['descontos'] . '</td>';
	$boletos .= '<td class="borda centro" id="valor_boleto">R$ ' . number_format($bolSel['valor'], 2, ",", ".") . '</td>';
	$boletos .= '</tr>';
	$i++;

	$sequencia .= "&sequencia[" . $bolSel['sequencia'] . "]=" . $bolSel['titulo'];
}

$cConsulta = "SELECT valor FROM titulos WHERE documento = '" . $documento . "' AND cliente = '" . $cliente . "' AND agencia = '" . $agencia . "' ";
$sql = mysql_query($cConsulta);

$data_base = date("d/m/Y", mktime(0, 0, 0, date("m"), date("d"), date("y")));
?>
<div id="section">
<script type="text/javascript">
	$(document).ready(function () {
		$('#valor').blur(function(){
			var valor = document.getElementById('valor').value;
			var boletos = document.getElementById('qtd_boletos').value;
			valor = valor.replace(',','.');
			var parcelas = valor/boletos;
			$('#fatura').attr('value',parcelas);
		});
		if($('#modalidade_boleto').is(':checked')){
			$('#protesto').attr('value','5');
				$('#protesto').attr('disabled','disabled');
				$('#dias_protesto_1').attr('checked','checked');
				$('#dias_protesto_0').attr('disabled','disabled');
				$('#dias_protesto_1').attr('disabled','disabled');
				$('#juros').attr('value','10,00');
				$('#juros').attr('disabled','disabled');
		}
		$('#modalidade_boleto').change(function(){
			if($(this).is(':checked')){
				$('#protesto').attr('value','5');
				$('#protesto').attr('disabled','disabled');
				$('#dias_protesto_1').attr('checked','checked');
				$('#dias_protesto_0').attr('disabled','disabled');
				$('#dias_protesto_1').attr('disabled','disabled');
				$('#juros').attr('value','10,00');
				$('#juros').attr('disabled','disabled');
			}
			else{
				$('#protesto').removeAttr('disabled');
				$('#protesto').removeAttr('readonly');
				$('#dias_protesto_0').removeAttr('disabled');
				$('#dias_protesto_1').removeAttr('disabled');
				$('#juros').removeAttr('value');
				$('#juros').removeAttr('disabled');
				$('#juros').removeAttr('readonly');
			}
		});
		$('#qtd_boletos').blur(function(){
			if($('#qtd_boletos').val() == "1"){
				$('#intervalo').attr('value','1');
				$('#intervalo').attr('disabled','disabled');
			}
			else {
				$('#intervalo').removeAttr('value');
				$('#intervalo').removeAttr('disabled');
			}
		});
		$('#desconto').blur(function(){
			if($('#desconto').val() < "0,01"){
				$('#data_desconto').attr('value','NULL');
				$('#data_desconto').attr('disabled','disabled');
				$('#data_desconto').removeAttr('onblur');
			}
			else {
				$('#data_desconto').removeAttr('value');
				$('#data_desconto').removeAttr('disabled');
				$('#data_desconto').attr('onblur','verificaDesc(this.value)');
			}
		});
		$('#limpar').click(function(){
			$('#protesto').removeAttr('value');
			$('#protesto').removeAttr('disabled');
			$('#dias_protesto_0').removeAttr('disabled');
			$('#dias_protesto_1').removeAttr('disabled');
			$('#juros').removeAttr('value');
			$('#juros').removeAttr('disabled');
		});
	});
	$(document).ready( function() {
		$("#altera_titulo").validate({
			rules:{
				documento:{
					required: true, minlength: 2
				},
				qtd_boletos:{
					required: true, minlength: 1
				},
				venc:{
					required: true, minlength: 10
				},
				intervalo:{
					required: true, minlength: 1
				},
				protesto:{
					required: true, minlength: 1
				},
				valor:{
					required: true, minlength: 4
				},
				multa:{
					required: true, minlength: 4
				},
				desconto:{
					required: true, minlength: 4
				},
				juros:{
					required: true, minlength: 4
				},
			},
			messages:{
				documento:{
					required: "Digite o documento",
					minlength: "Mínimo 2 caracteres."
				},
				qtd_boletos:{
					required: "Digite a quantidade de boletos",
					minlength: "Mínimo 1 caractere."
				},
				venc:{
					required: "Digite a data de vencimento inicial",
					minlength: "Mínimo 10 caracteres."
				},
				intervalo:{
					required: "Digite o intervalo",
					minlength: "Mínimo 1 caractere."
				},
				protesto:{
					required: "Digite os dias para protesto",
					minlength: "Mínimo 1 caractere."
				},
				valor:{
					required: "Digite o valor total das faturas",
					minlength: "Mínimo 4 caracteres."
				},
				multa:{
					required: "Digite o valor da multa(%)",
					minlength: "Mínimo 4 caracteres."
				},
				desconto:{
					required: "Digite o valor do desconto",
					minlength: "Mínimo 4 caracteres."
				},
				juros:{
					required: "Digite o valor dos juros(%)",
					minlength: "Mínimo 4 caracteres."
				}
			}
		});
	});
	function number_format (number, decimals, dec_point, thousands_sep) {
	  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	  var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
		  var k = Math.pow(10, prec);
		  return '' + Math.round(n * k) / k;
		};
	  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
	  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	  if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	  }
	  if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	  }
	  return s.join(dec);
	}
	function atualizavalor(valor){
		var totalinicial = <?php echo $valor_total_fatura;?>;
		var faturainicial = <?php echo $valor_fatura;?>;
		var totalfinal = 0;
		var faturafinal = document.getElementById('fatura').value;
		faturafinal = faturafinal.replace('.','');
		faturafinal = faturafinal.replace(',','.');
		totalfinal = totalinicial-(faturainicial-faturafinal);
		totalfinal = totalfinal.toFixed(2);
		totalfinal = number_format(totalfinal,2,',','.');
		faturafinal = number_format(faturafinal,2,',','.');

		$('#valor').attr('value',totalfinal);
	}
	function prepara_alterar(){
		if (document.altera_titulo.cliente.value=="0"){
			alerta("Favor selecionar o cliente.");
			return false;
		}
		else if (document.altera_titulo.sacador.value=="0"){
			alerta("Favor selecionar o sacador/avalista.");
			return false;
		}
		else if (document.altera_titulo.qtd_boletos.value<1){
			alerta("A quantidade de boletos não pode ser menor que 1.");
			return false;
		}
		else if (document.altera_titulo.fatura.value<1){
			alerta("O valor do boleto não pode ser menor que 1.");
			return false;
		}
		else {
			if(document.altera_titulo.modalidade_boleto.checked){
				$.ajax({
					type: 'GET',
					url: "scripts/titulos_funcoes.php",
					data: "funcao=verificarHora&agencia=<?php echo $agencia;?>",
					success: function(retorno){
						if(retorno == "1"){
							alerta("Horário para cadastro de títulos para desconto encerrado.");
							return false;
						}
						else if(retorno == "ok"){ // SÓ DESCONTO - HORARIO OK
							alt_titulo();
						}
					}
				});
			}
			else {
				alt_titulo();
			}
		}
	}
	function alt_titulo(){
		var documento = document.getElementById('documento').value;
		var seq = document.getElementById('sequencia').value;
		var vencimento = document.getElementById('venc').value;
		var intervalo = document.getElementById('intervalo').value;
		var valor = document.getElementById('fatura').value;
		var desconto = document.getElementById('desconto').value;
		var dias_protesto= $('[name=dias_protesto]:checked').val() ;

		$.ajax({
			type: "GET",
			url: "scripts/titulos_funcoes.php",
			data: "funcao=alt_titulo&documento="+documento+"&sequencia="+seq+"&venc="+vencimento+"&intervalo="+intervalo+"&valor="+valor+"&desconto="+desconto+"&dias_protesto="+dias_protesto,
			success: function(retorno){
				$('#dialog').html('<div id="dialog-confirm" title="Alterar título?" style="display:none">'+retorno+'</div>');
				// cria janela de confirmação
				$( "#dialog-confirm" ).dialog({
					resizable: false,
					height:200,
					width: 585,
					modal: true,
					buttons: {
						"Alterar": function() {
							alterar();
							$(this).dialog("close");
						},
						"Editar": function() {
							$(this).dialog("close");
						},
						"Cancelar": function() {
							$(this).dialog("close");
							navega('principal.php');
						}
					}
				});
			}
		});
	}
	function alterar(){
		var titulo = document.getElementById('titulo').value;
		var cliente = document.getElementById('cliente').value;
		var sacador = document.getElementById('sacador').value;
		var modelo_boleto = document.getElementById('modelo_boleto').value;
		var vencimento = document.getElementById('venc').value;
		var intervalo = document.getElementById('intervalo').value;
		var valor = document.getElementById('fatura').value;
		var multa = document.getElementById('multa').value;
		var desconto = document.getElementById('desconto').value;
		var protesto = document.getElementById('protesto').value;
		var juros = document.getElementById('juros').value;
		var dias_protesto= $('[name=dias_protesto]:checked').val() ;

		if (desconto < '0,01'){
			var data_desconto = '(NULL)';
		}
		else {
			var data_desconto = document.getElementById('data_desconto').value;
		}
		var modalidade_boleto = document.getElementById('modalidade_boleto');
		if (modalidade_boleto.checked){
			var modalidade_boleto = document.getElementById('modalidade_boleto').value;
		}
		else {
			var modalidade_boleto = "N";
		}

		$.ajax({
			type: "GET",
			url: "scripts/titulos_funcoes.php",
			data: "funcao=alterar&titulo="+titulo+"&cliente="+cliente+"&sacador="+sacador+"&modelo_boleto="+modelo_boleto+"&modalidade_boleto="+modalidade_boleto+"&venc="+vencimento+"&intervalo="+intervalo+"&valor="+valor+"&multa="+multa+"&desconto="+desconto+"&data_desconto="+data_desconto+"&protesto="+protesto+"&juros="+juros+"&dias_protesto="+dias_protesto,
			success: function(retorno){
				if(retorno == 'ok'){
					alerta('Fatura alterada com sucesso!');
					$("#altera_titulo input").prop('disabled', true);
					$("#altera_titulo select").prop('disabled', true);
					$('#data').html(vencimento);
					$('#multa').html(multa);
					$('#desconto').html(desconto);
					$('#valor_boleto').html('R$ '+valor);
				}
				else if(retorno == 'erro'){
					alerta('Erro ao alterar.');
				}
				else{
					alerta(retorno);
				}
			}
		});
	}
</script>

<div id="dialog"></div>
	<div class="titulo">
        <h2>TÍTULOS</h2>
       	<a href="javascript:navega('principal.php');" class="sair"></a>
  	</div>
    <br class="clear" />
    <div class="corpo">
	  <form id="altera_titulo" name="altera_titulo" method="post" action="">
      <?php
$sql2 = mysql_query("SELECT inf_sacador FROM clientes WHERE agencia='$agencia' AND conta='$conta'") or die("N�o foi poss�vel realizar a consulta ao banco de dados");
$linha = mysql_fetch_assoc($sql2);
$ob_sacador = $linha["inf_sacador"];
?>
    <input name="titulo" id="titulo" type="hidden" value="<?php echo $titulo;?>" />
  		<fieldset>
    	  <legend>Alterar  TÍTULOS</legend>
          <table>
          	<tr>
          	  <td>Cliente:</td>
          	  <td colspan="4">
              	<select disabled name="cliente" id="cliente" class="largo">
          	    	<option value="0">Selecione um sacado</option>
          	    	<?php
$sql2 = mysql_query("select sacado,nome,cpf from sacados where agencia='$agencia' and cliente = '$cliente' and (grupo <> '99999' or grupo is null) AND scd_desativado = '0' order by upper(nome);") or die(mysql_error());

if (mysql_num_rows($sql2) > 0) {
	while ($linha = mysql_fetch_assoc($sql2)) {
		if ($aSel['sacado'] == $linha["sacado"]) {
			echo "<option value='" . $linha["sacado"] . "' selected >" . $linha["nome"] . " - " . $linha["cpf"] . "</option>";
		} else {
			echo "<option value='" . $linha["sacado"] . "'>" . $linha["nome"] . " - " . $linha["cpf"] . "</option>";
		}
	}
}
?>
       	    	</select>
          	</td>
            <td colspan="2">Sacador/Avalista:</td>
            <td colspan="3"><select  name="sacador" id="sacador" class="largo">
              <option value="0">Selecione...</option>
              <?php
$sql3 = mysql_query("select * from sacados where agencia='$agencia' AND cliente='$cliente' and grupo='99999' and scd_desativado = '0' order by upper(nome);") or die(mysql_error());
if (mysql_num_rows($sql3) > 0) {
	while ($linha = mysql_fetch_assoc($sql3)) {
		if ($aSel['sacador'] == $linha["sacado"]) {
			echo "<option value='" . $linha["sacado"] . "' selected >" . $linha["nome"] . "</option>";
		} else {
			echo "<option value='" . $linha["sacado"] . "'>" . $linha["nome"] . "</option>";
		}
	}
}
?>
            </select></td>
           	</tr>
          	<tr>
          	  <td width="11%">Modelo Boleto:</td>
          	  <td width="10%">
              	<select name="modelo_boleto" id="modelo_boleto" class="largo">
              <?php
if ($aSel['modelo'] == 1) {
	echo "
					<option value='1' selected>2 Vias</option>
					
					";
} else if ($aSel['modelo'] == 2) {
	echo "
					<option value='1' selected>2 Vias</option>
					
					";
} else if ($aSel['modelo'] == 3) {
	echo "
					<option value='1' selected >2 Vias</option>

					";
}
?>
       	      </select></td>
          	  <td width="1%">&nbsp;</td>
          	  <td width="12%">Modal. Boleto:</td>
          	  <td width="13%"><input type="checkbox" <?php echo ($aSel['so_desconto'] == "S" ? "checked" : "");?> name="modalidade_boleto" id="modalidade_boleto" value="S" />
       	      Exclusivo Desconto</td>
          	  <td width="3%">&nbsp;</td>
          	  <td width="11%">Documento:</td>
          	  <td width="12%"><input name="documento" type="text" disabled="disabled" id="documento" value="<?php echo $aSel['documento'];?>" size="6" maxlength="8" /> / <input type="hidden" id="sequencia" name="sequencia" value="<?php echo $aSel['sequencia'];?>" /> <?php echo $aSel['sequencia'];?></td>
          	  <td width="9%">Qtd. Boletos:</td>
          	  <td width="18%"><input name="qtd_boletos" type="text" disabled="disabled" id="qtd_boletos" value="<?php echo $i;?>" size="3" maxlength="2" /></td>
       	    </tr>
          	<tr>
          	  <td>Data Emissão:</td>
          	  <td><input name="data_emissao" type="text" id="data_emissao" size="7" maxlength="10" class="largo" value="<?php echo $aSel['data_emisao'];?>" readonly /></td>
          	  <td>&nbsp;</td>
          	  <td>Vencimento:</td>
          	  <td><input name="venc" type="text" id="venc" size="6" maxlength="10" class="largo" onkeypress="formataCampo(this, '00/00/0000', event)" onblur="verificaVenc(this.value)" value="<?php echo $vencimento1;?>" /></td>
          	  <td>&nbsp;</td>
          	  <td>Intervalo:</td>
          	  <td><input name="intervalo" type="text" id="intervalo" value="<?php echo $aSel['intervalo'];?>" size="5" maxlength="5" /></td>
          	  <td>Dias protestos:</td>
          	  <td>
              <?php
        $dias_corridos='';
        $dias_uteis='';

if ( trim($aSel['dias_protesto']) == "corrido" || trim($aSel['dias_protesto']) == "corridos") {
	
	$dias_corridos = "checked";
} else {
	
	$dias_uteis = "checked";
}
?>
              <input name="protesto" type="text" id="protesto" style="width:20px" value="<?php echo $aSel['protesto'];?>" size="1" maxlength="2" />
                <label>
                  <input type="radio" name="dias_protesto" value="corridos" <?php echo $dias_corridos;?> id="dias_protesto_0" />
                  Corridos</label>
                <label>
                  <input name="dias_protesto" type="radio" id="dias_protesto_1" value="uteis" <?php echo $dias_uteis;?>  />
              Úteis</label></td>
       	    </tr>
          	<tr>
          	  <td>Valor Total<span style="font-size:10px;">(R$)</span>:</td>
          	  <td><input name="valor" type="text" class="largo" id="valor" value="<?php echo number_format($valor_total_fatura, 2, ",", ".");?>" size="6" maxlength="25" readonly /></td>
          	  <td>&nbsp;</td>
          	  <td>Valor Faturas<span style="font-size:10px;">(R$)</span>:</td>
          	  <td><input name="fatura" type="text" class="largo" id="fatura" onkeydown="FormataValor(this,event,17,2);" value="<?php echo number_format($valor_fatura, 2, ",", ".");?>" size="6" maxlength="25" onblur="atualizavalor(this.value)" /></td>
          	  <td>&nbsp;</td>
          	  <td>Multa<span style="font-size:10px;">(%)</span>:</td>
          	  <td><input name="multa" type="text" id="multa" onkeydown="FormataValor(this,event,5,2);" value="<?php echo $aSel['multa'];?>" size="5" maxlength="5" /></td>
          	  <td><label for="juros">Juros Mora Mensal<span style="font-size:10px;">(%)</span>:</label></td>
          	  <td><input name="juros" type="text" id="juros" onkeydown="FormataValor(this,event,5,2);" value="<?php echo $aSel['juros'];?>" size="3" maxlength="5" /></td>
       	    </tr>
          	<tr>
          	  <td>Valor Desconto<span style="font-size:10px;">(R$)</span>:</td>
          	  <td><input name="desconto" type="text" class="largo" id="desconto" value="<?php echo number_format($aSel["descontos"], 2, ",", ".");?>" size="10" maxlength="10" /></td>
          	  <td>&nbsp;</td>
          	  <td>Data Lim. Desconto:</td>
              <?php
if ($aSel["data_limite_desconto"] == NULL || $aSel["data_limite_desconto"] == '0000-00-00') {
	$data_desconto = '';
} else {
	$data_desconto = date('d/m/Y', strtotime($aSel["data_limite_desconto"]));
}
?>
          	  <td><input name="data_desconto" type="text" class="largo" id="data_desconto" onblur="verificaDesc(this.value)" onkeypress="formataCampo(this, '00/00/0000', event)" value="<?php echo $data_desconto;?>" size="10" maxlength="10" /></td>
          	  <td>&nbsp;</td>
          	  <td>Instruções:</td>
          	  <td colspan="3"><label for="textarea"></label>
       	      <textarea name="textarea" id="textarea" rows="2" class="largo" style="resize:none;"><?php echo $aSel['instrucao'];?></textarea></td>
       	    </tr>
          </table>
          <fieldset>
          	<legend>BOLETO</legend>
            <table border="0">
              <tr class="cinza">
                <td class="borda destaque centro">Nº DOCUMENTO</td>
                <td class="borda destaque centro">NOSSO NÚMERO</td>
                <td class="borda destaque centro">EMISSÃO</td>
                <td class="borda destaque centro">VENCIMENTO</td>
                <td class="borda destaque centro">MULTA</td>
                <td class="borda destaque centro">JUROS</td>
                <td class="borda destaque centro">DESCONTO</td>
                <td class="borda destaque centro">VALOR</td>
              </tr>
              <tr>
              	<?php echo $boletos;?>
              </tr>
            </table>
          </fieldset>
  		</fieldset>
        <a class="btn botao margins dir" href="javascript:prepara_alterar()">Alterar</a>
		<a class="btn botao margins dir" href="javascript:navega('titulo_buscar.php')">Voltar</a>
        <br class="clear" />
	  </form>
    </div>
</div>
