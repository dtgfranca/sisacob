<?php

include_once('class/tcpdf/tcpdf.php');
include_once("class/PHPJasperXML.inc.php");
include_once ('setting2.php');

mysql_connect($server,$user,$pass) or die(mysql_error());
mysql_select_db($db) or die(mysql_error());	

$xml = simplexml_load_file("rel.jrxml"); //informe onde está seu arquivo jrxml

$PHPJasperXML = new PHPJasperXML();

$PHPJasperXML->debugsql=false;

//recebendo os parâmetros
session_start();
$data = date('d/m/Y');
$hora = date('H:i:s');
$agencia = $_SESSION['userAgencia'];
$cliente = $_SESSION['userNome'];
$conta = $_SESSION['userConta'];
$codcliente = $_SESSION['userCliente'];
$logo = '../../img/logo/logo_'.$agencia.'.jpg';
$dData = date('d/m/Y');
$tipo = "todos";//$_POST['tipo'];
$filtro = $_POST['filtro'];
if($tipo=="todos"){ 
	$value_tipo = "RELATÓRIO SIMPLES";
}
if($tipo=="rec"){ 
	$value_tipo = "RECEBIDOS";
}
if($tipo=="baixa"){ 
   	$value_tipo = "BAIXADOS";
}
if($tipo=="pend"){ 
   	$value_tipo = "EM ABERTO";
}
if($tipo=="venc"){
   	$value_tipo = "VENCIDOS";
}
if($tipo=="cancel"){ 
   	$value_tipo = "CANCELADOS";
}
if($tipo=="lista_previa"){ 
	$value_tipo = "LISTAGEM PRÉVIA";
}
if($tipo=="rejeitados"){ 
	$value_tipo = "REJEITADOS";
}
if($tipo=="nosso_n"){ 
	$nosso_n = $_POST['nosso_n'];
	$value_tipo = "NOSSO NUMERO ".$nosso_n;
}		
if($filtro=="data_emisao"){
	$value_periodo = "EMISSÃO DE ".$_POST['inicio']." A ".$_POST['fim'];
}
if($filtro=="data_venc"){
	$value_periodo = "VENCIMENTO DE ".$_POST['inicio']." A ".$_POST['fim'];
}
if($filtro=="data_baixa"){
	$value_periodo = "BAIXA DE ".$_POST['inicio']." A ".$_POST['fim'];
}
$inicio = explode('/',$_POST['inicio']);
$inicio = $inicio[2].'-'.$inicio[1].'-'.$inicio[0];
$fim = explode('/',$_POST['fim']);
$fim = $fim[2].'-'.$fim[1].'-'.$fim[0];

if(!empty($_POST['cliente'])){$sacado = $_POST['cliente'];}
$tipo   = $_POST["tipo"];
$tipo2  = $_POST["tipo2"];
$ordem  = $_POST["ordem"];
$filtro = $_POST["filtro"];

if($filtro == "data_baixa"){
	$b_data = "AND ((t.data_baixa>=$inicio AND t.data_baixa<=$fim) OR (t.data_baixa_manual>=$inicio AND t.data_baixa_manual<=$fim))";
}
else{
	$b_data = "AND t.$filtro>='$inicio' AND t.$filtro<='$fim'";
}
if($tipo == "rec"){
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND (status='05' OR status='06' OR status='07' OR status='08' OR status='15')";
}
else if($tipo == "baixa"){
	$b_tipo = "AND (t.data_baixa IS NOT NULL OR t.data_baixa_manual IS NOT NULL) AND t.devolucao IS NOT NULL AND (t.status='09' OR t.status='10')";
}
else if($tipo=="pend"){
	$b_tipo="AND (t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL)";
}
else if($tipo=="venc"){
	$b_tipo="AND t.data_venc < CURDATE() AND t.data_baixa IS NULL AND t.data_baixa_manual IS NULL AND t.cancelamento IS NULL";
}
else if($tipo=="rejeitados"){
	$b_tipo="AND t.status='03' AND t.cancelamento IS NULL";
}
else if($tipo=="nosso_n"){
	$nosso_n = $_POST['nosso_n'];
	$b_tipo="AND t.nossonumero LIKE '%".$nosso_n."%'";
}
else if($tipo=="cancel"){
	$b_tipo="AND t.cancelamento IS NOT NULL";
}
else if($tipo=="lista_previa"){
    $b_tipo = "AND cad_completo = 'N' AND status <> '99' ";
}
else {
	$b_tipo = "AND cad_completo = 'S' ";	
}
if(!empty($_POST['tipo2'])){
	$n_titulo = $_POST['n_titulo'];
	$b_tipo.= "AND t.documento LIKE '%".$n_titulo."%'";
}
if(empty($_POST['tipo2'])){
	$b_tipo2 = "";
}
else{
	$tipo2	= $_POST['tipo2'];
	switch($tipo2){
		case "desc":
			if($tipo == "rec"){
				$b_tipo2 = "AND t.desconto IS NOT NULL AND so_desconto='S'";
			}
			else if($tipo == "todos"){
				$b_tipo2 = "AND so_desconto='S'";
			}
			else{
				$b_tipo2 = "AND so_desconto='S'";
			}
		break;
		case "cobranca_simples":
			if($tipo == "rec"){
				$b_tipo2 = "AND so_desconto='N'";
			}else if($tipo == "todos"){
				$b_tipo2 = "AND so_desconto='N'";
			}else{
				$b_tipo2 = "AND so_desconto='N'";
			}
		break;
	}
}
if(!empty($sacado)){
	$b_sacado = "AND t.sacado='$sacado'";
}
else{ 
	$b_sacado="";
}


$PHPJasperXML->arrayParameter=array("cTitulo"=>$value_tipo, "nAgencia"=>$agencia, "cCliente"=>$cliente, "nCliente"=>$codcliente, "nConta"=>$conta, "cLogo"=>$logo, "dDataSis"=>$data, "dHoraSis"=>$hora, "dData"=>$dData, "nCob"=>$cobranca, "tCob"=>$valor_cobranca, "nCobS"=>$simples, "tCobS"=>$valor_simples, "nCobD"=>$descontada, "tCobD"=>$valor_descontada, "nEnt"=>$registro, "tEnt"=>$valor_registro, "nLiq"=>$liquidados, "tLiq"=>$valor_liquidados, "nLiqC"=>$cartorio, "tLiqC"=>$valor_cartorio, "nBai"=>$baixados, "tBai"=>$valor_baixados, "nVenc"=>$vencidos, "tVenc"=>$valor_vencidos, "nAVen"=>$avencer, "tAVen"=>$valor_avencer); //passa o parâmetro cadastrado no iReport

$PHPJasperXML->xml_dismantle($xml);

$PHPJasperXML->connect($server,$user,$pass,$db);

$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);

$PHPJasperXML->outpage("I");

?> 