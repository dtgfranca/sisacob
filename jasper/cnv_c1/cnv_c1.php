<?php

include_once('class/tcpdf/tcpdf.php');
include_once("class/PHPJasperXML.inc.php");
include_once ('setting.php');

//include('../../config.php');

$xml = simplexml_load_file("cnv_c1.jrxml"); //informe onde está seu arquivo jrxml

$PHPJasperXML = new PHPJasperXML();

$PHPJasperXML->debugsql=false;

//recebendo os parâmetros
//$cooperativa=htmlspecialchars(utf8_encode($_POST["cCooperativa"]),ENT_QUOTES);
//$titulo=htmlspecialchars(utf8_encode($_POST["cTitulo"]),ENT_QUOTES);
$cooperativa=$_POST["cCooperativa"];
$titulo=$_POST["cTitulo"];
$logo=$_POST["cLogo"];
$data=date('d/m/Y');
$hora=date('H:i:s');

$PHPJasperXML->arrayParameter=array("cCooperativa"=>$cooperativa, "cTitulo"=>$titulo, "cLogo"=>$logo, "dDataSis"=>$data, "dHoraSis"=>$hora); //passa o parâmetro cadastrado no iReport

$PHPJasperXML->xml_dismantle($xml);

$PHPJasperXML->connect($server,$user,$pass,$db);

$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);

$PHPJasperXML->outpage("I");

?> 